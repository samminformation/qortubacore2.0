﻿using qortuba.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.VModel
{
    public class customTimetable
    {
        public TTConfigerDur tTConfigerDur { get; set; }
        public TTDur tTDur { get; set; }

        public TTColor tTColor { get; set; }

        public TTRowsshape tTRowsshape { get; set; }

        public TTTeacharCustom tTTeacharCustom { get; set; }
    }
}