﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.VModel
{
    public class TimeTableV
    {
        public int id { get; set; }
        public Nullable<int> sch_id { get; set; }
        public string cr_date { get; set; }
        public string cr_id { get; set; }
        public string up_date { get; set; }
        public Nullable<int> up_id { get; set; }
        public Nullable<int> day_num { get; set; }
        public Nullable<int> per_num { get; set; }
        public Nullable<int> caseA { get; set; }
        public Nullable<int> viewM { get; set; }
    }
}