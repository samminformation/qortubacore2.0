﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using qortuba.Models;

namespace qortuba.VModel
{
    public class SchoolsRows
    {
        public IEnumerable<TSchools> Schools { get; set; }
        public IEnumerable<TCLassRow> cLassRows { get; set; }
    }
}