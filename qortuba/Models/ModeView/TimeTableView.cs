﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qortuba.Models.ModeView
{
    public class TimeTableView
    {
        public TimeTable timeTable { get; set; }
        public supervision superVision { get; set; }
        public day_s day_S { get; set; }
        public IEnumerable<periods> periods { get; set; }
        public IEnumerable<TCLassRow> tCLassRows { get; set; }
        public IEnumerable<TSubject> tSubject { get; set; }
        public IEnumerable<TImployees> tImployees { get; set; }
        public List<SelectListItem> vMethods { get; set; }
        public IEnumerable<TeachReport> teachReports { get; set; }
    }
}
