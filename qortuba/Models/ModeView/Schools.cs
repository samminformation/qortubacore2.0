﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qortuba.Models.ModeView
{
    public class Schools
    {

        public TSchools tSchools { get; set; }
        public IEnumerable<TCLassRow> tCLassRow { get; set; }
        public IEnumerable<DesForTime> desForTime { get; set; }
        public IEnumerable<DesForTime> desTimeTable { get; set; }
        public IEnumerable<TSubject> Tsubjects { get; set; }
        public IEnumerable<ReducedTypes> reducedTypes { get; set; }
        public IEnumerable<TimeTable> ttimetable { get; set; }
        public IEnumerable<DelegateShow> dele { get; set; }
    }
}
