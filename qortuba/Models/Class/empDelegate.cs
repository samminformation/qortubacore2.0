﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class empDelegate
    {
        public string empId { get; set; }
        public string schollID  { get; set; }
        public string specId { get; set; }

        public string empName { get; set; }
        public string schoolName { get; set; }
        public string specName { get; set; }

        public string CirId { get; set; }
    }
}