﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class TeachReport
    {
        public string id { get; set; }
        public string t_id { get; set; }
        public int day_id { get; set; }
        public string dayname { get; set; }
        public string period_id { get; set; }
        public string class_id { get; set; }
        public string Subject { get; set; }
        public int daynum { get; set; }

        public int pernum { get; set; }

    }
}