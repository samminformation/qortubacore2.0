﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class DelegationDecision
    {
        public String DelID { get; set; }
        public String TID { get; set; }
        public String Sname { get; set; }
        public String Tsub { get; set; }
        public String TSubId { get; set; }

        public String SCircle { get; set; }
        public String SchId { get; set; }
        public String dayname1 { get; set; }
        public String dateHajry { get; set; }
        public String TeachName { get; set; }
        public String ManagName { get; set; }

        public String other { get; set; }

        public String DelType { get; set; }
        public String DelDays { get; set; }
        public String DelClass { get; set; }


    }
}