﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class GReduce
    {
        public string id { get; set; }

        public string RSub { get; set; }
        public string NameLevel { get; set; }
        public string NameRow { get; set; }

        public string SchoolsType { get; set; }
        public string Class_No { get; set; }

        public string PlaneType { get; set; }

    }
}