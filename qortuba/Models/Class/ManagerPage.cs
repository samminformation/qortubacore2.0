﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class ManagerPage
    {
        public string id { get; set; }
        public string FullName { get; set; }
        public int SchoolId { get; set; }
        public string School { get; set; }
        public string Circle { get; set; }

    }
}