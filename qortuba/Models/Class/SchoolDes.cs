﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class SchoolDes
    {
        public string schID { get; set; }
        public string schName { get; set; }
        public string subID { get; set; }
        public string subName { get; set; }
        public string dess { get; set; }
        public string req { get; set; }

        public string dessDate { get; set; }
    }
}