﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class DesForTime
    {
        public string empId { get; set; }
        public string Teacher { get; set; }
        public string suID { get; set; }
        public string subj { get; set; }
        public string stat { get; set; }
        public string Reduce { get; set; }
        public string def { get; set; }
        public string addclass { get; set; }
        public string addSub { get; set; }

        public string Estat { get; set; }
    }
}