﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class DelegateShow
    {
        public string id { get; set; }
        public string FSchoolId { get; set; }
        public string FSchoolName { get; set; }
        public string FEdCircle { get; set; }
        public string EmpId { get; set; }
        public string EmpName { get; set; }
        public string SubCode { get; set; }
        public string SubName { get; set; }
        public string TSchoolId { get; set; }
        public string TSchoolName { get; set; }
        public string TEdCircle { get; set; }
        public string DelegationDate { get; set; }
        public string DelState { get; set; }
        public string DayName { get; set; }
        public string ManagerName { get; set; }
        public string FirstDeleDate { get; set; }
        public string AcceptedDelDate { get; set; }
        public string ReqDelDate { get; set; }
        public Nullable<int> StateAfter { get; set; }

        public string EndDel { get; set; }
        public string goodOrNot { get; set; }
        public string ManagerTo { get; set; }

        public string EndDay { get; set; }
        public string EndMonth { get; set; }
        public string EndYear { get; set; }

        public string Deldate { get; set; }
        public string hajry { get; set; }
        public string Melad { get; set; }

        public string other { get; set; }

        public string resid { get; set; }

        public string DelType { get; set; }

        public string DelTypeID { get; set; }
        public string DelDays { get; set; }
        public string DelClass { get; set; }

        public string ManName { get; set; }
        public string ManCode { get; set; }
        public string ManSchid { get; set; }

        public string AssesName { get; set; }
        public string AssesCode { get; set; }
        public string AssesSchid { get; set; }


    }
}