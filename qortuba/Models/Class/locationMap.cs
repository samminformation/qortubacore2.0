﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class locationMap
    {
        public int SchID  { get; set; }

        public string Schname { get; set; }

        public string lat { get; set; }

        public string lag { get; set; }

        public double latD { get; set; }

        public double lagD { get; set; }

        public double dis { get; set; }


        public string levelNo { get; set; }

        public int levcode { get; set; }

    }
}