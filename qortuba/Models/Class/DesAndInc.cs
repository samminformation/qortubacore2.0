﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace qortuba.Models
{
    public class DesAndInc
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public string SchoolName { get; set; }      
        public string Req { get; set; }
        public string Exs { get; set; }
        public string Dess { get; set; }
        public string Inc { get; set; }

        public string IncTeach { get; set; }

    }
}