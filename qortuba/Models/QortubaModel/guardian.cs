//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace qortuba.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class guardian
    {
        public int id { get; set; }
        public Nullable<int> NidGuardian { get; set; }
        public string Gname { get; set; }
        public Nullable<int> NidStudient { get; set; }
        public string mob { get; set; }
        public string Gaddress { get; set; }
    }
}
