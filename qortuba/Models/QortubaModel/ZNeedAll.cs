//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace qortuba.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ZNeedAll
    {
        public int Id { get; set; }         
        public short SID { get; set; }
        public short STID { get; set; }
        public short SUBID { get; set; }
        public short NeedYear { get; set; }
        public string NeedYearName { get; set; }
        public string State { get; set; }
        public string Subject { get; set; }
        public string Stage { get; set; }
        public Nullable<short> NEEDWez { get; set; }
        public Nullable<short> NEEDNow { get; set; }
        public Nullable<short> TO { get; set; }
        public Nullable<short> NewFormal { get; set; }
        public Nullable<short> NewContr { get; set; }
        public Nullable<short> TotalTo { get; set; }
        public Nullable<short> From { get; set; }
        public Nullable<short> LossFinishQar { get; set; }
        public Nullable<short> LossFinishQarNo { get; set; }
        public Nullable<short> TotalFrom { get; set; }
        public Nullable<short> Result { get; set; }
        public Nullable<short> ResultNow { get; set; }
    }
}
