//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace qortuba.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class NeedOptions
    {
        [Key]
        public byte LevId { get; set; }
        public short SubIDOpt { get; set; }
        public short Plan { get; set; }
        public Nullable<byte> NegOne { get; set; }
        public Nullable<byte> NegOne0 { get; set; }
        public Nullable<byte> NegMore { get; set; }
        public Nullable<byte> PosOne { get; set; }
        public Nullable<byte> PosMore { get; set; }
        public Nullable<byte> Teach { get; set; }
    }
}
