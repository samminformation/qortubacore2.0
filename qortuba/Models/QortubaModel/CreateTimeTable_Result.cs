//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace qortuba.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class CreateTimeTable_Result
    {  [Key]
        public Nullable<int> id { get; set; }
        public Nullable<long> RW { get; set; }
      
        public Nullable<int> ROWSS { get; set; }
        public Nullable<int> SUBCODE { get; set; }
        public string SUB { get; set; }
        public string CLASS { get; set; }
        public string TEACHAR { get; set; }
        public Nullable<int> empId { get; set; }
    }
}
