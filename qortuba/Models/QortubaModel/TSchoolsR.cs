//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace qortuba.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class TSchoolsR
    {
        [Key]
        [Display(Name = "����� �������")]
        public bool TSchools_SchoolID { get; set; }
        [Display(Name = "�������")]
        public bool TSchools_School { get; set; }
        [Display(Name = "������ ������")]
        public bool TSchools_OldCenter { get; set; }
        [Display(Name = "����� ������")]
        public bool TSchools_OldName { get; set; }
        [Display(Name = "�������")]
        public bool TL_NameLevel { get; set; }
        [Display(Name = "���� �������")]
        public bool fin_neme { get; set; }
        [Display(Name = "������")]
        public bool TSchools_FAx { get; set; }
        [Display(Name = "������")]
        public bool tc_Center1 { get; set; }
        [Display(Name = "������")]
        public bool com_neme { get; set; }
        [Display(Name = "��������")]
        public bool ar_name6 { get; set; }
        [Display(Name = "������")]
        public bool Bak_neme { get; set; }
        [Display(Name = "�������")]
        public bool TSchools_City { get; set; }
        [Display(Name = "������ ����������")]
        public bool EMail { get; set; }
        [Display(Name = "�����")]
        public bool st_SchoolsType { get; set; }
        [Display(Name = "�����")]
        public bool cre_neme { get; set; }
        [Display(Name = "������")]
        public bool TSchools_street { get; set; }
        [Display(Name = "��� �������")]
        public bool tu_Tuition1 { get; set; }
        [Display(Name = "������")]
        public bool bild_Bulding { get; set; }
        [Display(Name = "����")]
        public bool TSchools_Liver { get; set; }
        [Display(Name = "���� ������")]
        public bool out1_neme { get; set; }
        [Display(Name = "���� ������")]
        public bool Cad_CadE_door { get; set; }
        [Display(Name = "����� �� �������")]
        public bool TSchools_Distance { get; set; }
        [Display(Name = "��� �������")]
        public bool TSchools_FDate { get; set; }
        [Display(Name = "��� �������")]
        public bool sty_SchTypeName { get; set; }
        [Display(Name = "����� �����")]
        public bool TSchools_dismiss { get; set; }
        [Display(Name = "�����")]
        public bool desr_neme { get; set; }
        [Display(Name = "������")]
        public bool kind_neme { get; set; }
        [Display(Name = "������")]
        public bool TSchools_Phone { get; set; }
        [Display(Name = "���� ��������")]
        public bool TSchools_NearerSH1 { get; set; }
        [Display(Name = "����� ��������")]
        public bool TSchools_Space1 { get; set; }
        [Display(Name = "������ ��������")]
        public bool v1_Varway1 { get; set; }
        [Display(Name = "���� ������")]
        public bool TSchools_NearerSH2 { get; set; }
        [Display(Name = "����� ������")]
        public bool TSchools_Space2 { get; set; }
        [Display(Name = "������ ������")]
        public bool v2_Varway1 { get; set; }
        [Display(Name = "���� �����")]
        public bool TSchools_NearerSH3 { get; set; }
        [Display(Name = "����� �����")]
        public bool TSchools_Space3 { get; set; }
        [Display(Name = "������ �����")]
        public bool v3_Varway1 { get; set; }
        [Display(Name = "����� �������")]
        public bool TSchools_Rooms { get; set; }
        [Display(Name = "����")]
        public bool TSchools_TsrPys { get; set; }
        [Display(Name = "����")]
        public bool TSchools_Tsertailor { get; set; }
        [Display(Name = "�����")]
        public bool TSchools_Tester { get; set; }
        [Display(Name = "�������")]
        public bool TSchools_TsrEn { get; set; }
        [Display(Name = "�����")]
        public bool TSchools_office { get; set; }
        [Display(Name = "������")]
        public bool TSchools_Periority { get; set; }
        [Display(Name = "����� ����")]
        public bool TSchools_Strip { get; set; }
        [Display(Name = "������ ����")]
        public bool TSchools_StripPeriority { get; set; }
        [Display(Name = "����� �������")]
        public bool Rooms { get; set; }
        [Display(Name = "������� ���������")]
        public bool circleEd { get; set; }
        public int reportID { get; set; }
        public string Rname { get; set; }
        public string viewM { get; set; }
        public virtual ICollection<ReportD>  ReportD { get; set; }
    }
}
