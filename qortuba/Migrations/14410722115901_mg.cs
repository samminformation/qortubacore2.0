﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace qortuba.Migrations
{
    public partial class mg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "acceptstates",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    acceptState = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_acceptstates", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "adela",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    adFile = table.Column<string>(nullable: true),
                    adName = table.Column<string>(nullable: true),
                    adServ = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_adela", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Classes",
                columns: table => new
                {
                    ClassId = table.Column<double>(nullable: false),
                    Aall = table.Column<double>(nullable: true),
                    Actall = table.Column<double>(nullable: true),
                    Ain = table.Column<double>(nullable: true),
                    AllEdu = table.Column<bool>(nullable: true),
                    Aout = table.Column<double>(nullable: true),
                    Classname = table.Column<string>(nullable: true),
                    Hall = table.Column<double>(nullable: true),
                    Hin = table.Column<double>(nullable: true),
                    Hout = table.Column<double>(nullable: true),
                    InAll = table.Column<double>(nullable: true),
                    Outall = table.Column<double>(nullable: true),
                    ShowClass = table.Column<bool>(nullable: true),
                    SubI = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Classes", x => x.ClassId);
                });

            migrationBuilder.CreateTable(
                name: "CommonSch",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CalcNeed = table.Column<bool>(nullable: true),
                    SchCom = table.Column<string>(nullable: true),
                    SchIDCommon = table.Column<short>(nullable: false),
                    SchLevelCom = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommonSch", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Custom_Reduce",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    S_Level = table.Column<int>(nullable: true),
                    S_Row = table.Column<int>(nullable: true),
                    SchID = table.Column<int>(nullable: false),
                    Sub_id = table.Column<int>(nullable: true),
                    W_Class = table.Column<int>(nullable: true),
                    planeType = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Custom_Reduce", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "daysName",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dayname = table.Column<string>(nullable: true),
                    daynameENG = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_daysName", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Delegation",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AcceptedDelDate = table.Column<DateTime>(nullable: true),
                    DayName = table.Column<string>(nullable: true),
                    DelClass = table.Column<int>(nullable: true),
                    DelDays = table.Column<int>(nullable: true),
                    DelState = table.Column<int>(nullable: true),
                    DelType = table.Column<int>(nullable: true),
                    DelegationDate = table.Column<DateTime>(nullable: true),
                    EmpId = table.Column<int>(nullable: true),
                    EmpName = table.Column<string>(nullable: true),
                    EndDay = table.Column<int>(nullable: true),
                    EndDel = table.Column<DateTime>(nullable: true),
                    EndMonth = table.Column<int>(nullable: true),
                    EndYear = table.Column<int>(nullable: true),
                    FEdCircle = table.Column<string>(nullable: true),
                    FSchoolId = table.Column<int>(nullable: true),
                    FSchoolName = table.Column<string>(nullable: true),
                    FirstDeleDate = table.Column<DateTime>(nullable: true),
                    ManagerName = table.Column<string>(nullable: true),
                    ManagerTo = table.Column<string>(nullable: true),
                    ReqDelDate = table.Column<DateTime>(nullable: true),
                    StateAfter = table.Column<int>(nullable: true),
                    SubCode = table.Column<int>(nullable: true),
                    SubName = table.Column<string>(nullable: true),
                    TEdCircle = table.Column<string>(nullable: true),
                    TSchoolId = table.Column<int>(nullable: true),
                    TSchoolName = table.Column<string>(nullable: true),
                    goodOrNot = table.Column<int>(nullable: true),
                    other = table.Column<string>(nullable: true),
                    resid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Delegation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "DelegationType",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DelegationType1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DelegationType", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Departs",
                columns: table => new
                {
                    Departid = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DepartName = table.Column<string>(nullable: true),
                    ManagId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departs", x => x.Departid);
                });

            migrationBuilder.CreateTable(
                name: "ExportDelegation",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DeleId = table.Column<int>(nullable: true),
                    EmpId = table.Column<int>(nullable: true),
                    EmpName = table.Column<string>(nullable: true),
                    ExportDate = table.Column<DateTime>(nullable: true),
                    ExportDelegationDate = table.Column<DateTime>(nullable: true),
                    FEdCircle = table.Column<string>(nullable: true),
                    FSchoolId = table.Column<int>(nullable: true),
                    FSchoolName = table.Column<string>(nullable: true),
                    SubCode = table.Column<int>(nullable: true),
                    TEdCircle = table.Column<string>(nullable: true),
                    TSchoolId = table.Column<int>(nullable: true),
                    TSchoolName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExportDelegation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "FormsTypes",
                columns: table => new
                {
                    FormCode = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmpStatus = table.Column<string>(nullable: true),
                    ErrNote = table.Column<string>(nullable: true),
                    FormName = table.Column<string>(nullable: true),
                    FormNum = table.Column<byte>(nullable: true),
                    FormType = table.Column<byte>(nullable: true),
                    IAmAMove = table.Column<bool>(nullable: true),
                    NewEmpStatus = table.Column<string>(nullable: true),
                    NewSubId = table.Column<byte>(nullable: true),
                    NewWid = table.Column<byte>(nullable: true),
                    QueryMe = table.Column<bool>(nullable: true),
                    ReportName = table.Column<string>(nullable: true),
                    ViewMe = table.Column<bool>(nullable: true),
                    Wid = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormsTypes", x => x.FormCode);
                });

            migrationBuilder.CreateTable(
                name: "GroupN",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GroupN1 = table.Column<string>(nullable: true),
                    SuperGN = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupN", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "guardian",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Gaddress = table.Column<string>(nullable: true),
                    Gname = table.Column<string>(nullable: true),
                    NidGuardian = table.Column<int>(nullable: true),
                    NidStudient = table.Column<int>(nullable: true),
                    mob = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_guardian", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "HayNames",
                columns: table => new
                {
                    HayCode = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HayName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HayNames", x => x.HayCode);
                });

            migrationBuilder.CreateTable(
                name: "ImportDelegation",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DeleId = table.Column<int>(nullable: true),
                    EmpId = table.Column<int>(nullable: true),
                    EmpName = table.Column<string>(nullable: true),
                    FEdCircle = table.Column<string>(nullable: true),
                    FSchoolId = table.Column<int>(nullable: true),
                    FSchoolName = table.Column<string>(nullable: true),
                    ImportDate = table.Column<DateTime>(nullable: true),
                    ImportDelegationDate = table.Column<DateTime>(nullable: true),
                    SubCode = table.Column<int>(nullable: true),
                    TEdCircle = table.Column<string>(nullable: true),
                    TSchoolId = table.Column<int>(nullable: true),
                    TSchoolName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImportDelegation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "jobsMember",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    jobName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_jobsMember", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "locationnss",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dd = table.Column<double>(nullable: true),
                    latlng = table.Column<string>(nullable: true),
                    lk = table.Column<string>(nullable: true),
                    loc = table.Column<string>(nullable: true),
                    schid = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_locationnss", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Mboard",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    empCode = table.Column<int>(nullable: true),
                    empName = table.Column<string>(nullable: true),
                    job = table.Column<string>(nullable: true),
                    sid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mboard", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "NeedCondition",
                columns: table => new
                {
                    ConId = table.Column<string>(nullable: false),
                    Minus = table.Column<string>(nullable: true),
                    Plus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NeedCondition", x => x.ConId);
                });

            migrationBuilder.CreateTable(
                name: "NeedCondition1",
                columns: table => new
                {
                    ConId = table.Column<string>(nullable: false),
                    Minus = table.Column<string>(nullable: true),
                    Plus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NeedCondition1", x => x.ConId);
                });

            migrationBuilder.CreateTable(
                name: "NeedOptions",
                columns: table => new
                {
                    LevId = table.Column<byte>(nullable: false),
                    NegMore = table.Column<byte>(nullable: true),
                    NegOne = table.Column<byte>(nullable: true),
                    NegOne0 = table.Column<byte>(nullable: true),
                    Plan = table.Column<short>(nullable: false),
                    PosMore = table.Column<byte>(nullable: true),
                    PosOne = table.Column<byte>(nullable: true),
                    SubIDOpt = table.Column<short>(nullable: false),
                    Teach = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NeedOptions", x => x.LevId);
                });

            migrationBuilder.CreateTable(
                name: "OutMove",
                columns: table => new
                {
                    Identity = table.Column<string>(nullable: false),
                    ModalR = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NeedYear = table.Column<short>(nullable: false),
                    QrarNo = table.Column<string>(nullable: true),
                    Qraradate = table.Column<DateTime>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    ReqNo = table.Column<short>(nullable: true),
                    SYmbol = table.Column<byte>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    TarfNo = table.Column<string>(nullable: true),
                    Tarfdate = table.Column<DateTime>(nullable: true),
                    ToSect = table.Column<short>(nullable: true),
                    ToSub = table.Column<short>(nullable: true),
                    Toclass = table.Column<short>(nullable: true),
                    d = table.Column<byte>(nullable: true),
                    movenotes = table.Column<string>(nullable: true),
                    sortNo = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutMove", x => x.Identity);
                });

            migrationBuilder.CreateTable(
                name: "PlanNeed",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CLAS1 = table.Column<float>(nullable: true),
                    CLAS2 = table.Column<float>(nullable: true),
                    CLAS3 = table.Column<float>(nullable: true),
                    CLAS4 = table.Column<float>(nullable: true),
                    CLAS5 = table.Column<float>(nullable: true),
                    CLAS6 = table.Column<float>(nullable: true),
                    CLAS7 = table.Column<float>(nullable: true),
                    CLAS8 = table.Column<float>(nullable: true),
                    CLAS9 = table.Column<float>(nullable: true),
                    LevelNeed = table.Column<byte>(nullable: false),
                    SubNeed = table.Column<short>(nullable: false),
                    SubjName = table.Column<string>(nullable: true),
                    TypeNeed = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanNeed", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PSubClas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Classes = table.Column<byte>(nullable: false),
                    LevelNo = table.Column<byte>(nullable: false),
                    SubID = table.Column<short>(nullable: false),
                    WID = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PSubClas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PWorkClas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Classes = table.Column<byte>(nullable: false),
                    LevelNo = table.Column<byte>(nullable: false),
                    PeriorSchool = table.Column<bool>(nullable: true),
                    SchGroup = table.Column<int>(nullable: false),
                    WID = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PWorkClas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QType",
                columns: table => new
                {
                    QTID = table.Column<byte>(nullable: false),
                    QType1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QType", x => x.QTID);
                });

            migrationBuilder.CreateTable(
                name: "QYtBEMARK",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NUM = table.Column<byte>(nullable: false),
                    Qtypmark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QYtBEMARK", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "recordCat",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    role_id = table.Column<int>(nullable: true),
                    wid = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_recordCat", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ReducedTypes",
                columns: table => new
                {
                    ReduedCode = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Classes = table.Column<byte>(nullable: false),
                    ReducedName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReducedTypes", x => x.ReduedCode);
                });

            migrationBuilder.CreateTable(
                name: "ReportCat",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    cat_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportCat", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "reson",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    chos = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_reson", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "restriction",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dayid = table.Column<int>(nullable: true),
                    empid = table.Column<int>(nullable: true),
                    periodid = table.Column<int>(nullable: true),
                    stutas1 = table.Column<int>(nullable: true),
                    tid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_restriction", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ReviewCodes",
                columns: table => new
                {
                    ReviewCode = table.Column<byte>(nullable: false),
                    ReviewType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReviewCodes", x => x.ReviewCode);
                });

            migrationBuilder.CreateTable(
                name: "RowName",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Row_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RowName", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "SchoolDess",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    des = table.Column<int>(nullable: true),
                    dessDate = table.Column<DateTime>(nullable: true),
                    req = table.Column<double>(nullable: true),
                    schID = table.Column<int>(nullable: true),
                    schName = table.Column<string>(nullable: true),
                    stateReq = table.Column<int>(nullable: true),
                    subID = table.Column<int>(nullable: true),
                    subName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolDess", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "SchTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SchType = table.Column<byte>(nullable: false),
                    SchTypeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "students",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Sstate = table.Column<int>(nullable: true),
                    acceptS = table.Column<int>(nullable: true),
                    care = table.Column<string>(nullable: true),
                    dateA = table.Column<DateTime>(nullable: true),
                    familyCert = table.Column<string>(nullable: true),
                    fid = table.Column<string>(nullable: true),
                    fname1 = table.Column<string>(nullable: true),
                    fname2 = table.Column<string>(nullable: true),
                    fname3 = table.Column<string>(nullable: true),
                    fname4 = table.Column<string>(nullable: true),
                    homeCert = table.Column<string>(nullable: true),
                    mob = table.Column<string>(nullable: true),
                    nat = table.Column<int>(nullable: true),
                    notess = table.Column<string>(nullable: true),
                    oldCert = table.Column<string>(nullable: true),
                    oldSchid = table.Column<int>(nullable: true),
                    rowName = table.Column<string>(nullable: true),
                    schid = table.Column<int>(nullable: true),
                    sid = table.Column<string>(nullable: true),
                    sname = table.Column<string>(nullable: true),
                    sname1 = table.Column<string>(nullable: true),
                    sname2 = table.Column<string>(nullable: true),
                    sname3 = table.Column<string>(nullable: true),
                    snname4 = table.Column<string>(nullable: true),
                    srow = table.Column<int>(nullable: true),
                    transfare = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_students", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "studentsR",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Sstate = table.Column<int>(nullable: true),
                    acceptS = table.Column<int>(nullable: true),
                    care = table.Column<string>(nullable: true),
                    dateA = table.Column<DateTime>(nullable: true),
                    dateR = table.Column<DateTime>(nullable: true),
                    familyCert = table.Column<string>(nullable: true),
                    fid = table.Column<string>(nullable: true),
                    fname1 = table.Column<string>(nullable: true),
                    fname2 = table.Column<string>(nullable: true),
                    fname3 = table.Column<string>(nullable: true),
                    fname4 = table.Column<string>(nullable: true),
                    homeCert = table.Column<string>(nullable: true),
                    mob = table.Column<string>(nullable: true),
                    nat = table.Column<int>(nullable: true),
                    notess = table.Column<string>(nullable: true),
                    oldCert = table.Column<string>(nullable: true),
                    oldSchid = table.Column<int>(nullable: true),
                    rowName = table.Column<string>(nullable: true),
                    schid = table.Column<int>(nullable: true),
                    sid = table.Column<string>(nullable: true),
                    sname = table.Column<string>(nullable: true),
                    sname1 = table.Column<string>(nullable: true),
                    sname2 = table.Column<string>(nullable: true),
                    sname3 = table.Column<string>(nullable: true),
                    snname4 = table.Column<string>(nullable: true),
                    srow = table.Column<int>(nullable: true),
                    transfare = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_studentsR", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Studentstate",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    stat = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Studentstate", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "studentsWait",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Sstate = table.Column<int>(nullable: true),
                    acceptS = table.Column<int>(nullable: true),
                    care = table.Column<string>(nullable: true),
                    dateA = table.Column<DateTime>(nullable: true),
                    familyCert = table.Column<string>(nullable: true),
                    fid = table.Column<string>(nullable: true),
                    fname1 = table.Column<string>(nullable: true),
                    fname2 = table.Column<string>(nullable: true),
                    fname3 = table.Column<string>(nullable: true),
                    fname4 = table.Column<string>(nullable: true),
                    homeCert = table.Column<string>(nullable: true),
                    messageWati = table.Column<int>(nullable: true),
                    mob = table.Column<string>(nullable: true),
                    nat = table.Column<int>(nullable: true),
                    notess = table.Column<string>(nullable: true),
                    oldCert = table.Column<string>(nullable: true),
                    oldSchid = table.Column<int>(nullable: true),
                    rowName = table.Column<string>(nullable: true),
                    schid = table.Column<int>(nullable: true),
                    sid = table.Column<string>(nullable: true),
                    sname = table.Column<string>(nullable: true),
                    sname1 = table.Column<string>(nullable: true),
                    sname2 = table.Column<string>(nullable: true),
                    sname3 = table.Column<string>(nullable: true),
                    snname4 = table.Column<string>(nullable: true),
                    srow = table.Column<int>(nullable: true),
                    transfare = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_studentsWait", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "superJob",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    sjob = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_superJob", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "SuperLaw",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<short>(nullable: false),
                    Departid = table.Column<short>(nullable: false),
                    ManagId = table.Column<byte>(nullable: false),
                    NumOfClass = table.Column<short>(nullable: true),
                    NumOfOver = table.Column<short>(nullable: true),
                    NumOfSuperv = table.Column<short>(nullable: true),
                    NumOfSupervNeed = table.Column<short>(nullable: true),
                    NumberOfItems = table.Column<int>(nullable: true),
                    SupervId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuperLaw", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SuperVis",
                columns: table => new
                {
                    SupervId = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Actual = table.Column<int>(nullable: true),
                    ActualLoss = table.Column<int>(nullable: true),
                    ActualNoQarar = table.Column<short>(nullable: true),
                    ActualQarar = table.Column<int>(nullable: true),
                    Need = table.Column<short>(nullable: true),
                    Result = table.Column<short>(nullable: true),
                    SuperGroup = table.Column<int>(nullable: true),
                    SuperGroupS = table.Column<int>(nullable: true),
                    SuperName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuperVis", x => x.SupervId);
                });

            migrationBuilder.CreateTable(
                name: "supervision_tasks",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    task = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_supervision_tasks", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "supervisors",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dayid = table.Column<int>(nullable: true),
                    empid = table.Column<int>(nullable: true),
                    supervisor = table.Column<int>(nullable: true),
                    tid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_supervisors", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "T_area",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IDNU = table.Column<byte>(nullable: false),
                    name6 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_area", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Center",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Center1 = table.Column<string>(nullable: true),
                    Center_Phone = table.Column<string>(nullable: true),
                    Center_liver = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Center", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "T_CenterPa",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommSchoolAdd = table.Column<bool>(nullable: true),
                    CommSchoolDelete = table.Column<bool>(nullable: true),
                    CommSchoolShow = table.Column<bool>(nullable: true),
                    CommSchoolUpdate = table.Column<bool>(nullable: true),
                    DataExport = table.Column<bool>(nullable: true),
                    EdarNeedExecute = table.Column<bool>(nullable: true),
                    EdarNeedShow = table.Column<bool>(nullable: true),
                    ManagData = table.Column<bool>(nullable: true),
                    MirrorDelNeed = table.Column<bool>(nullable: true),
                    MirrorExecute = table.Column<bool>(nullable: true),
                    MirrorSetting = table.Column<bool>(nullable: true),
                    MirrorShow = table.Column<bool>(nullable: true),
                    NeedCalc = table.Column<bool>(nullable: true),
                    NeedShow = table.Column<bool>(nullable: true),
                    NeedSubQuery = table.Column<bool>(nullable: true),
                    PassChange = table.Column<bool>(nullable: true),
                    ReportDataChange = table.Column<bool>(nullable: true),
                    ReportInnCancel = table.Column<bool>(nullable: true),
                    ReportProb = table.Column<bool>(nullable: true),
                    ReportSchoolLog = table.Column<bool>(nullable: true),
                    ReportStat = table.Column<bool>(nullable: true),
                    ReportSymbole = table.Column<bool>(nullable: true),
                    Reporting = table.Column<bool>(nullable: true),
                    SchoolAdd = table.Column<bool>(nullable: true),
                    SchoolDelete = table.Column<bool>(nullable: true),
                    SchoolShow = table.Column<bool>(nullable: true),
                    SchoolUpdate = table.Column<bool>(nullable: true),
                    Settings = table.Column<bool>(nullable: true),
                    SpecialDefine = table.Column<bool>(nullable: true),
                    StripShow = table.Column<bool>(nullable: true),
                    SuperAct = table.Column<bool>(nullable: true),
                    SuperClassAct = table.Column<bool>(nullable: true),
                    SuperData = table.Column<bool>(nullable: true),
                    SuperExport = table.Column<bool>(nullable: true),
                    SuperLaw = table.Column<bool>(nullable: true),
                    SuperLoss = table.Column<bool>(nullable: true),
                    SuperRep = table.Column<bool>(nullable: true),
                    SuperShow = table.Column<bool>(nullable: true),
                    TeachAdd = table.Column<bool>(nullable: true),
                    TeachDelete = table.Column<bool>(nullable: true),
                    TeachDirect = table.Column<bool>(nullable: true),
                    TeachDirectOut = table.Column<bool>(nullable: true),
                    TeachEkmal = table.Column<bool>(nullable: true),
                    TeachIdUpd = table.Column<bool>(nullable: true),
                    TeachMarkShow = table.Column<bool>(nullable: true),
                    TeachPrint = table.Column<bool>(nullable: true),
                    TeachProbShow = table.Column<bool>(nullable: true),
                    TeachProbUpd = table.Column<bool>(nullable: true),
                    TeachQuery = table.Column<bool>(nullable: true),
                    TeachRecChang = table.Column<bool>(nullable: true),
                    TeachShow = table.Column<bool>(nullable: true),
                    TeachUpdate = table.Column<bool>(nullable: true),
                    TeachorgSpec = table.Column<bool>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    UserPW = table.Column<string>(nullable: true),
                    UserType = table.Column<byte>(nullable: false),
                    UsersAdd = table.Column<bool>(nullable: true),
                    UsersDelete = table.Column<bool>(nullable: true),
                    UsersEdit = table.Column<bool>(nullable: true),
                    UsersShow = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_CenterPa", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_CenterPaTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PermFldName = table.Column<string>(nullable: true),
                    PermType = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_CenterPaTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_ClassType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ct = table.Column<byte>(nullable: false),
                    CtName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_ClassType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_contract",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    cod = table.Column<string>(nullable: true),
                    nup = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_contract", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_country",
                columns: table => new
                {
                    CntID = table.Column<byte>(nullable: false),
                    country = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_country", x => x.CntID);
                });

            migrationBuilder.CreateTable(
                name: "T_Door",
                columns: table => new
                {
                    DoorID = table.Column<byte>(nullable: false),
                    Door = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Door", x => x.DoorID);
                });

            migrationBuilder.CreateTable(
                name: "T_Married",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NU = table.Column<byte>(nullable: false),
                    nem = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Married", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_PlanTemp",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Compl = table.Column<int>(nullable: true),
                    LevelNo = table.Column<byte>(nullable: false),
                    Qur = table.Column<int>(nullable: true),
                    Subject = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_PlanTemp", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_Varway1",
                columns: table => new
                {
                    Varway1ID = table.Column<byte>(nullable: false),
                    Varway1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_Varway1", x => x.Varway1ID);
                });

            migrationBuilder.CreateTable(
                name: "T_yes_no",
                columns: table => new
                {
                    on = table.Column<byte>(nullable: false),
                    neme = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_yes_no", x => x.on);
                });

            migrationBuilder.CreateTable(
                name: "TBuilding",
                columns: table => new
                {
                    BuildingID = table.Column<byte>(nullable: false),
                    Bulding = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TBuilding", x => x.BuildingID);
                });

            migrationBuilder.CreateTable(
                name: "TCadE_door",
                columns: table => new
                {
                    CadE_ID = table.Column<byte>(nullable: false),
                    CadE_door = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TCadE_door", x => x.CadE_ID);
                });

            migrationBuilder.CreateTable(
                name: "TempStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BeforWez = table.Column<string>(nullable: true),
                    CalcForSuper = table.Column<bool>(nullable: true),
                    EmpStatus = table.Column<string>(nullable: true),
                    EndType = table.Column<string>(nullable: true),
                    MinisLoss = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TempStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TempTSubj",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LevN = table.Column<byte>(nullable: false),
                    NameLev = table.Column<string>(nullable: true),
                    NeedCond = table.Column<string>(nullable: true),
                    NeedCondT = table.Column<string>(nullable: true),
                    NeedCondText = table.Column<string>(nullable: true),
                    NeedItem = table.Column<string>(nullable: true),
                    NeedItemName = table.Column<string>(nullable: true),
                    NeedValue = table.Column<short>(nullable: true),
                    SelectNeed = table.Column<bool>(nullable: true),
                    ShowNeed = table.Column<bool>(nullable: true),
                    SubN = table.Column<int>(nullable: false),
                    SubOrder = table.Column<byte>(nullable: true),
                    Subject = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TempTSubj", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TimplAutoUpdate",
                columns: table => new
                {
                    Identity = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Amail = table.Column<string>(nullable: true),
                    AppDateD = table.Column<byte>(nullable: true),
                    AppDateM = table.Column<byte>(nullable: true),
                    AppDatey = table.Column<short>(nullable: true),
                    Appointment = table.Column<string>(nullable: true),
                    BirthDated = table.Column<byte>(nullable: true),
                    BirthDatem = table.Column<byte>(nullable: true),
                    BirthDatey = table.Column<short>(nullable: true),
                    BirthPlace = table.Column<string>(nullable: true),
                    Box = table.Column<string>(nullable: true),
                    Checked = table.Column<byte>(nullable: true),
                    ClassNo = table.Column<byte>(nullable: true),
                    Contract = table.Column<byte>(nullable: true),
                    ContractDated = table.Column<string>(nullable: true),
                    ContractDatem = table.Column<string>(nullable: true),
                    ContractDatey = table.Column<string>(nullable: true),
                    Country = table.Column<byte>(nullable: true),
                    Degree = table.Column<byte>(nullable: true),
                    Educational = table.Column<byte>(nullable: true),
                    EmpStatus = table.Column<string>(nullable: true),
                    FName = table.Column<string>(nullable: true),
                    IDW = table.Column<byte>(nullable: true),
                    Identity2 = table.Column<string>(nullable: true),
                    JobID = table.Column<int>(nullable: true),
                    Level = table.Column<byte>(nullable: true),
                    Level2 = table.Column<byte>(nullable: true),
                    Married = table.Column<byte>(nullable: true),
                    Mopaul = table.Column<string>(nullable: true),
                    NOTBAD = table.Column<string>(nullable: true),
                    NOTBAD1 = table.Column<string>(nullable: true),
                    Naey = table.Column<float>(nullable: true),
                    NatID = table.Column<byte>(nullable: true),
                    OtherClasses = table.Column<byte>(nullable: true),
                    Post = table.Column<string>(nullable: true),
                    PreSchool = table.Column<int>(nullable: true),
                    PreSchoolS = table.Column<int>(nullable: true),
                    Problem = table.Column<bool>(nullable: true),
                    ProblemDesc = table.Column<string>(nullable: true),
                    QDated = table.Column<byte>(nullable: true),
                    QDatem = table.Column<byte>(nullable: true),
                    QDatey = table.Column<short>(nullable: true),
                    QID = table.Column<byte>(nullable: true),
                    QTerm = table.Column<int>(nullable: true),
                    Quien_Weight = table.Column<float>(nullable: true),
                    Quien_mark = table.Column<float>(nullable: true),
                    Quien_markFrom = table.Column<float>(nullable: true),
                    Reduced = table.Column<byte>(nullable: true),
                    Reduction = table.Column<byte>(nullable: true),
                    SchID = table.Column<int>(nullable: true),
                    SchIDPrev = table.Column<int>(nullable: true),
                    Sile_contract = table.Column<string>(nullable: true),
                    SpecialID = table.Column<byte>(nullable: true),
                    SpecialID2 = table.Column<byte>(nullable: true),
                    SpecialIDOrig = table.Column<byte>(nullable: true),
                    SubID = table.Column<short>(nullable: true),
                    SubIDSV = table.Column<byte>(nullable: true),
                    SubIDSpec = table.Column<byte>(nullable: true),
                    SunNo = table.Column<byte>(nullable: true),
                    TStatus = table.Column<string>(nullable: true),
                    Tsymbole = table.Column<string>(nullable: true),
                    University = table.Column<byte>(nullable: true),
                    WID = table.Column<byte>(nullable: true),
                    WIDJ = table.Column<byte>(nullable: true),
                    WID_D = table.Column<byte>(nullable: true),
                    WID_M = table.Column<byte>(nullable: true),
                    WID_Y = table.Column<short>(nullable: true),
                    WorkDated = table.Column<byte>(nullable: true),
                    WorkDatem = table.Column<byte>(nullable: true),
                    WorkDatey = table.Column<short>(nullable: true),
                    Work_Area_M = table.Column<byte>(nullable: true),
                    Work_Area_Y = table.Column<short>(nullable: true),
                    Work_Count_D = table.Column<byte>(nullable: true),
                    Work_Count_M = table.Column<byte>(nullable: true),
                    Work_Count_Y = table.Column<short>(nullable: true),
                    Work_Shl_D = table.Column<byte>(nullable: true),
                    Work_Shl_M = table.Column<byte>(nullable: true),
                    Work_Shl_Y = table.Column<short>(nullable: true),
                    absent = table.Column<short>(nullable: true),
                    absent_W = table.Column<short>(nullable: true),
                    completion = table.Column<string>(nullable: true),
                    deciInvo = table.Column<string>(nullable: true),
                    decision = table.Column<string>(nullable: true),
                    guidance_date_d = table.Column<short>(nullable: true),
                    guidance_date_m = table.Column<short>(nullable: true),
                    guidance_date_y = table.Column<short>(nullable: true),
                    guidance_no = table.Column<string>(nullable: true),
                    holiday_d = table.Column<byte>(nullable: true),
                    holiday_m = table.Column<byte>(nullable: true),
                    holiday_y = table.Column<short>(nullable: true),
                    mark1 = table.Column<float>(nullable: true),
                    mark2 = table.Column<float>(nullable: true),
                    mark3 = table.Column<float>(nullable: true),
                    name1 = table.Column<string>(nullable: true),
                    name2 = table.Column<string>(nullable: true),
                    name3 = table.Column<string>(nullable: true),
                    naql = table.Column<int>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    quien = table.Column<byte>(nullable: true),
                    salary = table.Column<float>(nullable: true),
                    tagdeer = table.Column<byte>(nullable: true),
                    work_Area_D = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimplAutoUpdate", x => x.Identity);
                });

            migrationBuilder.CreateTable(
                name: "TImployeesCompl",
                columns: table => new
                {
                    IdentityComp = table.Column<string>(nullable: false),
                    Bost = table.Column<int>(nullable: true),
                    ClassNoC = table.Column<byte>(nullable: true),
                    SchIDC = table.Column<int>(nullable: false),
                    SubIDC = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TImployeesCompl", x => x.IdentityComp);
                });

            migrationBuilder.CreateTable(
                name: "TimployeesDel",
                columns: table => new
                {
                    Identity = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Amail = table.Column<string>(nullable: true),
                    AppDateD = table.Column<byte>(nullable: true),
                    AppDateM = table.Column<byte>(nullable: true),
                    AppDatey = table.Column<short>(nullable: true),
                    Appointment = table.Column<string>(nullable: true),
                    BirthDated = table.Column<byte>(nullable: true),
                    BirthDatem = table.Column<byte>(nullable: true),
                    BirthDatey = table.Column<short>(nullable: true),
                    BirthPlace = table.Column<string>(nullable: true),
                    Box = table.Column<string>(nullable: true),
                    Checked = table.Column<byte>(nullable: true),
                    ClassNo = table.Column<byte>(nullable: true),
                    Contract = table.Column<byte>(nullable: true),
                    ContractDated = table.Column<string>(nullable: true),
                    ContractDatem = table.Column<string>(nullable: true),
                    ContractDatey = table.Column<string>(nullable: true),
                    Country = table.Column<byte>(nullable: true),
                    Degree = table.Column<byte>(nullable: true),
                    Educational = table.Column<byte>(nullable: true),
                    EmpStatus = table.Column<string>(nullable: true),
                    FName = table.Column<string>(nullable: true),
                    IDW = table.Column<byte>(nullable: true),
                    Identity2 = table.Column<string>(nullable: true),
                    JobID = table.Column<int>(nullable: true),
                    Level = table.Column<byte>(nullable: true),
                    Level2 = table.Column<byte>(nullable: true),
                    Married = table.Column<byte>(nullable: true),
                    Mopaul = table.Column<string>(nullable: true),
                    NOTBAD = table.Column<string>(nullable: true),
                    NOTBAD1 = table.Column<string>(nullable: true),
                    Naey = table.Column<float>(nullable: true),
                    NatID = table.Column<byte>(nullable: true),
                    OtherClasses = table.Column<byte>(nullable: true),
                    Post = table.Column<string>(nullable: true),
                    PreSchool = table.Column<int>(nullable: true),
                    PreSchoolS = table.Column<int>(nullable: true),
                    Problem = table.Column<bool>(nullable: true),
                    ProblemDesc = table.Column<string>(nullable: true),
                    QDated = table.Column<byte>(nullable: true),
                    QDatem = table.Column<byte>(nullable: true),
                    QDatey = table.Column<short>(nullable: true),
                    QID = table.Column<byte>(nullable: true),
                    QTerm = table.Column<int>(nullable: true),
                    Quien_Weight = table.Column<float>(nullable: true),
                    Quien_mark = table.Column<float>(nullable: true),
                    Quien_markFrom = table.Column<float>(nullable: true),
                    Reduced = table.Column<byte>(nullable: true),
                    Reduction = table.Column<byte>(nullable: true),
                    SchID = table.Column<int>(nullable: true),
                    SchIDPrev = table.Column<int>(nullable: true),
                    Sile_contract = table.Column<string>(nullable: true),
                    SpecialID = table.Column<byte>(nullable: true),
                    SpecialID2 = table.Column<byte>(nullable: true),
                    SpecialIDOrig = table.Column<byte>(nullable: true),
                    SubID = table.Column<short>(nullable: true),
                    SubIDSV = table.Column<byte>(nullable: true),
                    SubIDSpec = table.Column<byte>(nullable: true),
                    SunNo = table.Column<byte>(nullable: true),
                    TStatus = table.Column<string>(nullable: true),
                    Tsymbole = table.Column<string>(nullable: true),
                    University = table.Column<byte>(nullable: true),
                    WID = table.Column<byte>(nullable: true),
                    WIDJ = table.Column<byte>(nullable: true),
                    WID_D = table.Column<byte>(nullable: true),
                    WID_M = table.Column<byte>(nullable: true),
                    WID_Y = table.Column<short>(nullable: true),
                    WorkDated = table.Column<byte>(nullable: true),
                    WorkDatem = table.Column<byte>(nullable: true),
                    WorkDatey = table.Column<short>(nullable: true),
                    Work_Area_M = table.Column<byte>(nullable: true),
                    Work_Area_Y = table.Column<short>(nullable: true),
                    Work_Count_D = table.Column<byte>(nullable: true),
                    Work_Count_M = table.Column<byte>(nullable: true),
                    Work_Count_Y = table.Column<short>(nullable: true),
                    Work_Shl_D = table.Column<byte>(nullable: true),
                    Work_Shl_M = table.Column<byte>(nullable: true),
                    Work_Shl_Y = table.Column<short>(nullable: true),
                    absent = table.Column<short>(nullable: true),
                    absent_W = table.Column<short>(nullable: true),
                    completion = table.Column<string>(nullable: true),
                    deciInvo = table.Column<string>(nullable: true),
                    decision = table.Column<string>(nullable: true),
                    guidance_date_d = table.Column<short>(nullable: true),
                    guidance_date_m = table.Column<short>(nullable: true),
                    guidance_date_y = table.Column<short>(nullable: true),
                    guidance_no = table.Column<string>(nullable: true),
                    holiday_d = table.Column<byte>(nullable: true),
                    holiday_m = table.Column<byte>(nullable: true),
                    holiday_y = table.Column<short>(nullable: true),
                    mark1 = table.Column<float>(nullable: true),
                    mark2 = table.Column<float>(nullable: true),
                    mark3 = table.Column<float>(nullable: true),
                    name1 = table.Column<string>(nullable: true),
                    name2 = table.Column<string>(nullable: true),
                    name3 = table.Column<string>(nullable: true),
                    naql = table.Column<int>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    quien = table.Column<byte>(nullable: true),
                    salary = table.Column<float>(nullable: true),
                    tagdeer = table.Column<byte>(nullable: true),
                    work_Area_D = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimployeesDel", x => x.Identity);
                });

            migrationBuilder.CreateTable(
                name: "TImployeesR",
                columns: table => new
                {
                    Identity = table.Column<bool>(nullable: false),
                    Address = table.Column<bool>(nullable: false),
                    Amail = table.Column<bool>(nullable: false),
                    AppDateD = table.Column<bool>(nullable: false),
                    AppDateM = table.Column<bool>(nullable: false),
                    AppDatey = table.Column<bool>(nullable: false),
                    Appointment = table.Column<bool>(nullable: false),
                    BirthDated = table.Column<bool>(nullable: false),
                    BirthDatem = table.Column<bool>(nullable: false),
                    BirthDatey = table.Column<bool>(nullable: false),
                    BirthPlace = table.Column<bool>(nullable: false),
                    Box = table.Column<bool>(nullable: false),
                    Checked = table.Column<bool>(nullable: false),
                    ClassNo = table.Column<bool>(nullable: false),
                    Contract = table.Column<bool>(nullable: false),
                    ContractDated = table.Column<bool>(nullable: false),
                    ContractDatem = table.Column<bool>(nullable: false),
                    ContractDatey = table.Column<bool>(nullable: false),
                    Country = table.Column<bool>(nullable: false),
                    Degree = table.Column<bool>(nullable: false),
                    Educational = table.Column<bool>(nullable: false),
                    FName = table.Column<bool>(nullable: false),
                    IDW = table.Column<bool>(nullable: false),
                    Identity2 = table.Column<bool>(nullable: false),
                    JobID = table.Column<bool>(nullable: false),
                    Level = table.Column<bool>(nullable: false),
                    Level2 = table.Column<bool>(nullable: false),
                    Mopaul = table.Column<bool>(nullable: false),
                    NOTBAD = table.Column<bool>(nullable: false),
                    NOTBAD1 = table.Column<bool>(nullable: false),
                    Naey = table.Column<bool>(nullable: false),
                    OtherClasses = table.Column<bool>(nullable: false),
                    Post = table.Column<bool>(nullable: false),
                    PreSchool = table.Column<bool>(nullable: false),
                    PreSchoolS = table.Column<bool>(nullable: false),
                    Problem = table.Column<bool>(nullable: false),
                    Problem2 = table.Column<bool>(nullable: false),
                    ProblemDesc = table.Column<bool>(nullable: false),
                    QDated = table.Column<bool>(nullable: false),
                    QDatem = table.Column<bool>(nullable: false),
                    QDatey = table.Column<bool>(nullable: false),
                    QID = table.Column<bool>(nullable: false),
                    QTerm = table.Column<bool>(nullable: false),
                    Quien_Weight = table.Column<bool>(nullable: false),
                    Quien_mark = table.Column<bool>(nullable: false),
                    Quien_markFrom = table.Column<bool>(nullable: false),
                    Reduced = table.Column<bool>(nullable: false),
                    Rname = table.Column<string>(nullable: true),
                    SSMA_TimeStamp = table.Column<bool>(nullable: false),
                    Sile_contract = table.Column<bool>(nullable: false),
                    SpecialID2 = table.Column<bool>(nullable: false),
                    SubIDSV = table.Column<bool>(nullable: false),
                    SubIDSpec = table.Column<bool>(nullable: false),
                    SunNo = table.Column<bool>(nullable: false),
                    TStatus = table.Column<bool>(nullable: false),
                    Tsymbole = table.Column<bool>(nullable: false),
                    University = table.Column<bool>(nullable: false),
                    WIDJ = table.Column<bool>(nullable: false),
                    WID_D = table.Column<bool>(nullable: false),
                    WID_M = table.Column<bool>(nullable: false),
                    WID_Y = table.Column<bool>(nullable: false),
                    WorkDated = table.Column<bool>(nullable: false),
                    WorkDatem = table.Column<bool>(nullable: false),
                    WorkDatey = table.Column<bool>(nullable: false),
                    Work_Area_M = table.Column<bool>(nullable: false),
                    Work_Area_Y = table.Column<bool>(nullable: false),
                    Work_Count_D = table.Column<bool>(nullable: false),
                    Work_Count_M = table.Column<bool>(nullable: false),
                    Work_Count_Y = table.Column<bool>(nullable: false),
                    Work_Shl_D = table.Column<bool>(nullable: false),
                    Work_Shl_M = table.Column<bool>(nullable: false),
                    Work_Shl_Y = table.Column<bool>(nullable: false),
                    absent = table.Column<bool>(nullable: false),
                    absent_W = table.Column<bool>(nullable: false),
                    c_Subject = table.Column<bool>(nullable: false),
                    class_num = table.Column<bool>(nullable: false),
                    completion = table.Column<bool>(nullable: false),
                    deciInvo = table.Column<bool>(nullable: false),
                    decision = table.Column<bool>(nullable: false),
                    e_Special = table.Column<bool>(nullable: false),
                    es_EndType = table.Column<bool>(nullable: false),
                    guidance_date_d = table.Column<bool>(nullable: false),
                    guidance_date_m = table.Column<bool>(nullable: false),
                    guidance_date_y = table.Column<bool>(nullable: false),
                    guidance_no = table.Column<bool>(nullable: false),
                    holiday_d = table.Column<bool>(nullable: false),
                    holiday_m = table.Column<bool>(nullable: false),
                    holiday_y = table.Column<bool>(nullable: false),
                    img = table.Column<bool>(nullable: false),
                    mark1 = table.Column<bool>(nullable: false),
                    mark2 = table.Column<bool>(nullable: false),
                    mark3 = table.Column<bool>(nullable: false),
                    mark4 = table.Column<bool>(nullable: false),
                    mr_nem = table.Column<bool>(nullable: false),
                    n_Nationality = table.Column<bool>(nullable: false),
                    name1 = table.Column<bool>(nullable: false),
                    name2 = table.Column<bool>(nullable: false),
                    name3 = table.Column<bool>(nullable: false),
                    naql = table.Column<bool>(nullable: false),
                    password = table.Column<bool>(nullable: false),
                    phone = table.Column<bool>(nullable: false),
                    quien = table.Column<bool>(nullable: false),
                    reportID = table.Column<int>(nullable: false),
                    rt_ReducedName = table.Column<bool>(nullable: false),
                    s_school = table.Column<bool>(nullable: false),
                    salary = table.Column<bool>(nullable: false),
                    so_Special = table.Column<bool>(nullable: false),
                    sp_school = table.Column<bool>(nullable: false),
                    tagdeer = table.Column<bool>(nullable: false),
                    viewM = table.Column<string>(nullable: true),
                    w_Work = table.Column<bool>(nullable: false),
                    work_Area_D = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TImployeesR", x => x.Identity);
                });

            migrationBuilder.CreateTable(
                name: "TImployeesTemp",
                columns: table => new
                {
                    Identity = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Amail = table.Column<string>(nullable: true),
                    AppDateD = table.Column<byte>(nullable: true),
                    AppDateM = table.Column<byte>(nullable: true),
                    AppDatey = table.Column<short>(nullable: true),
                    Appointment = table.Column<string>(nullable: true),
                    BirthDated = table.Column<byte>(nullable: true),
                    BirthDatem = table.Column<byte>(nullable: true),
                    BirthDatey = table.Column<short>(nullable: true),
                    BirthPlace = table.Column<string>(nullable: true),
                    Box = table.Column<string>(nullable: true),
                    Checked = table.Column<byte>(nullable: true),
                    ClassNo = table.Column<byte>(nullable: true),
                    Contract = table.Column<byte>(nullable: true),
                    ContractDated = table.Column<string>(nullable: true),
                    ContractDatem = table.Column<string>(nullable: true),
                    ContractDatey = table.Column<string>(nullable: true),
                    Country = table.Column<byte>(nullable: true),
                    Degree = table.Column<byte>(nullable: true),
                    Educational = table.Column<byte>(nullable: true),
                    EmpRule = table.Column<int>(nullable: true),
                    EmpStatus = table.Column<string>(nullable: true),
                    FName = table.Column<string>(nullable: true),
                    IDW = table.Column<byte>(nullable: true),
                    Identity2 = table.Column<string>(nullable: true),
                    JobID = table.Column<int>(nullable: true),
                    Level = table.Column<byte>(nullable: true),
                    Level2 = table.Column<byte>(nullable: true),
                    Married = table.Column<byte>(nullable: true),
                    Mopaul = table.Column<string>(nullable: true),
                    NOTBAD = table.Column<string>(nullable: true),
                    NOTBAD1 = table.Column<string>(nullable: true),
                    Naey = table.Column<float>(nullable: true),
                    NatID = table.Column<byte>(nullable: true),
                    OtherClasses = table.Column<byte>(nullable: true),
                    Post = table.Column<string>(nullable: true),
                    PreSchool = table.Column<int>(nullable: true),
                    PreSchoolS = table.Column<int>(nullable: true),
                    Problem = table.Column<bool>(nullable: false),
                    Problem2 = table.Column<bool>(nullable: false),
                    ProblemDesc = table.Column<string>(nullable: true),
                    QDated = table.Column<byte>(nullable: true),
                    QDatem = table.Column<byte>(nullable: true),
                    QDatey = table.Column<short>(nullable: true),
                    QID = table.Column<byte>(nullable: true),
                    QTerm = table.Column<int>(nullable: true),
                    Quien_Weight = table.Column<float>(nullable: true),
                    Quien_mark = table.Column<float>(nullable: true),
                    Quien_markFrom = table.Column<float>(nullable: true),
                    Reduced = table.Column<byte>(nullable: true),
                    Reduction = table.Column<byte>(nullable: true),
                    SSMA_TimeStamp = table.Column<byte[]>(nullable: true),
                    SchID = table.Column<int>(nullable: true),
                    SchIDPrev = table.Column<int>(nullable: true),
                    Sile_contract = table.Column<string>(nullable: true),
                    SpecialID = table.Column<byte>(nullable: true),
                    SpecialID2 = table.Column<byte>(nullable: true),
                    SpecialIDOrig = table.Column<byte>(nullable: true),
                    SubID = table.Column<short>(nullable: true),
                    SubIDSV = table.Column<byte>(nullable: true),
                    SubIDSpec = table.Column<byte>(nullable: true),
                    SunNo = table.Column<byte>(nullable: true),
                    TStatus = table.Column<string>(nullable: true),
                    Tsymbole = table.Column<string>(nullable: true),
                    University = table.Column<byte>(nullable: true),
                    WID = table.Column<byte>(nullable: true),
                    WIDJ = table.Column<byte>(nullable: true),
                    WID_D = table.Column<byte>(nullable: true),
                    WID_M = table.Column<byte>(nullable: true),
                    WID_Y = table.Column<short>(nullable: true),
                    WorkDated = table.Column<byte>(nullable: true),
                    WorkDatem = table.Column<byte>(nullable: true),
                    WorkDatey = table.Column<short>(nullable: true),
                    Work_Area_M = table.Column<byte>(nullable: true),
                    Work_Area_Y = table.Column<short>(nullable: true),
                    Work_Count_D = table.Column<byte>(nullable: true),
                    Work_Count_M = table.Column<byte>(nullable: true),
                    Work_Count_Y = table.Column<short>(nullable: true),
                    Work_Shl_D = table.Column<byte>(nullable: true),
                    Work_Shl_M = table.Column<byte>(nullable: true),
                    Work_Shl_Y = table.Column<short>(nullable: true),
                    absent = table.Column<short>(nullable: true),
                    absent_W = table.Column<short>(nullable: true),
                    addClasses = table.Column<int>(nullable: true),
                    addSubClasses = table.Column<int>(nullable: true),
                    class_num = table.Column<int>(nullable: true),
                    completion = table.Column<string>(nullable: true),
                    deciInvo = table.Column<string>(nullable: true),
                    decision = table.Column<string>(nullable: true),
                    guidance_date_d = table.Column<short>(nullable: true),
                    guidance_date_m = table.Column<short>(nullable: true),
                    guidance_date_y = table.Column<short>(nullable: true),
                    guidance_no = table.Column<string>(nullable: true),
                    holiday_d = table.Column<byte>(nullable: true),
                    holiday_m = table.Column<byte>(nullable: true),
                    holiday_y = table.Column<short>(nullable: true),
                    img = table.Column<string>(nullable: true),
                    major = table.Column<int>(nullable: true),
                    mark1 = table.Column<float>(nullable: true),
                    mark2 = table.Column<float>(nullable: true),
                    mark3 = table.Column<float>(nullable: true),
                    mark4 = table.Column<float>(nullable: true),
                    name1 = table.Column<string>(nullable: true),
                    name2 = table.Column<string>(nullable: true),
                    name3 = table.Column<string>(nullable: true),
                    naql = table.Column<int>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    quien = table.Column<byte>(nullable: true),
                    salary = table.Column<float>(nullable: true),
                    tagdeer = table.Column<byte>(nullable: true),
                    work_Area_D = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TImployeesTemp", x => x.Identity);
                });

            migrationBuilder.CreateTable(
                name: "TLevel",
                columns: table => new
                {
                    LevelNo = table.Column<byte>(nullable: false),
                    NameLevel = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TLevel", x => x.LevelNo);
                });

            migrationBuilder.CreateTable(
                name: "TLevel_Row",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LevelNo = table.Column<byte>(nullable: false),
                    NameRow = table.Column<string>(nullable: true),
                    Rows = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TLevel_Row", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TLevel_Row_Code",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NameRow = table.Column<string>(nullable: true),
                    NewCode = table.Column<byte>(nullable: true),
                    Rows = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TLevel_Row_Code", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TLevel_Subject",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Class_No = table.Column<float>(nullable: true),
                    LevelNo = table.Column<byte>(nullable: true),
                    PlanType = table.Column<short>(nullable: true),
                    Rows = table.Column<byte>(nullable: true),
                    Subject = table.Column<byte>(nullable: true),
                    class_on1 = table.Column<float>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TLevel_Subject", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TNationality",
                columns: table => new
                {
                    NatID = table.Column<byte>(nullable: false),
                    Nationality = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TNationality", x => x.NatID);
                });

            migrationBuilder.CreateTable(
                name: "TQualified",
                columns: table => new
                {
                    QID = table.Column<byte>(nullable: false),
                    Qualified = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TQualified", x => x.QID);
                });

            migrationBuilder.CreateTable(
                name: "TransTypes",
                columns: table => new
                {
                    TransType = table.Column<byte>(nullable: false),
                    TrnasName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransTypes", x => x.TransType);
                });

            migrationBuilder.CreateTable(
                name: "TSchoolsR",
                columns: table => new
                {
                    TSchools_SchoolID = table.Column<bool>(nullable: false),
                    Bak_neme = table.Column<bool>(nullable: false),
                    Cad_CadE_door = table.Column<bool>(nullable: false),
                    EMail = table.Column<bool>(nullable: false),
                    Rname = table.Column<string>(nullable: true),
                    Rooms = table.Column<bool>(nullable: false),
                    TL_NameLevel = table.Column<bool>(nullable: false),
                    TSchools_City = table.Column<bool>(nullable: false),
                    TSchools_Distance = table.Column<bool>(nullable: false),
                    TSchools_FAx = table.Column<bool>(nullable: false),
                    TSchools_FDate = table.Column<bool>(nullable: false),
                    TSchools_Liver = table.Column<bool>(nullable: false),
                    TSchools_NearerSH1 = table.Column<bool>(nullable: false),
                    TSchools_NearerSH2 = table.Column<bool>(nullable: false),
                    TSchools_NearerSH3 = table.Column<bool>(nullable: false),
                    TSchools_OldCenter = table.Column<bool>(nullable: false),
                    TSchools_OldName = table.Column<bool>(nullable: false),
                    TSchools_Periority = table.Column<bool>(nullable: false),
                    TSchools_Phone = table.Column<bool>(nullable: false),
                    TSchools_Rooms = table.Column<bool>(nullable: false),
                    TSchools_School = table.Column<bool>(nullable: false),
                    TSchools_Space1 = table.Column<bool>(nullable: false),
                    TSchools_Space2 = table.Column<bool>(nullable: false),
                    TSchools_Space3 = table.Column<bool>(nullable: false),
                    TSchools_Strip = table.Column<bool>(nullable: false),
                    TSchools_StripPeriority = table.Column<bool>(nullable: false),
                    TSchools_Tester = table.Column<bool>(nullable: false),
                    TSchools_Tsertailor = table.Column<bool>(nullable: false),
                    TSchools_TsrEn = table.Column<bool>(nullable: false),
                    TSchools_TsrPys = table.Column<bool>(nullable: false),
                    TSchools_dismiss = table.Column<bool>(nullable: false),
                    TSchools_office = table.Column<bool>(nullable: false),
                    TSchools_street = table.Column<bool>(nullable: false),
                    ar_name6 = table.Column<bool>(nullable: false),
                    bild_Bulding = table.Column<bool>(nullable: false),
                    circleEd = table.Column<bool>(nullable: false),
                    com_neme = table.Column<bool>(nullable: false),
                    cre_neme = table.Column<bool>(nullable: false),
                    desr_neme = table.Column<bool>(nullable: false),
                    fin_neme = table.Column<bool>(nullable: false),
                    kind_neme = table.Column<bool>(nullable: false),
                    out1_neme = table.Column<bool>(nullable: false),
                    reportID = table.Column<int>(nullable: false),
                    st_SchoolsType = table.Column<bool>(nullable: false),
                    sty_SchTypeName = table.Column<bool>(nullable: false),
                    tc_Center1 = table.Column<bool>(nullable: false),
                    tu_Tuition1 = table.Column<bool>(nullable: false),
                    v1_Varway1 = table.Column<bool>(nullable: false),
                    v2_Varway1 = table.Column<bool>(nullable: false),
                    v3_Varway1 = table.Column<bool>(nullable: false),
                    viewM = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSchoolsR", x => x.TSchools_SchoolID);
                });

            migrationBuilder.CreateTable(
                name: "TSchoolsType",
                columns: table => new
                {
                    SchoolTyp = table.Column<byte>(nullable: false),
                    BeforWez = table.Column<int>(nullable: true),
                    SchoolsType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSchoolsType", x => x.SchoolTyp);
                });

            migrationBuilder.CreateTable(
                name: "TSector",
                columns: table => new
                {
                    SectID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BeforWez = table.Column<int>(nullable: true),
                    Sector = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSector", x => x.SectID);
                });

            migrationBuilder.CreateTable(
                name: "TSpecial",
                columns: table => new
                {
                    SpeID = table.Column<byte>(nullable: false),
                    BeforeWez = table.Column<int>(nullable: true),
                    PrivateSpec = table.Column<bool>(nullable: true),
                    Special = table.Column<string>(nullable: true),
                    s = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSpecial", x => x.SpeID);
                });

            migrationBuilder.CreateTable(
                name: "TSpecial_LevErr",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LevelNo = table.Column<byte>(nullable: false),
                    SpecialID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSpecial_LevErr", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TSpecial_Sub",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LevelNo = table.Column<byte>(nullable: false),
                    SpecialID = table.Column<short>(nullable: false),
                    SubID = table.Column<short>(nullable: false),
                    SubIdCorrect = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSpecial_Sub", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TSpecial_Sub1",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Count_SpecialID = table.Column<short>(nullable: true),
                    LevelNo = table.Column<byte>(nullable: false),
                    SpecialID = table.Column<short>(nullable: false),
                    SubID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSpecial_Sub1", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TSpecialSub",
                columns: table => new
                {
                    SpeID = table.Column<byte>(nullable: false),
                    Special = table.Column<string>(nullable: true),
                    Sub = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSpecialSub", x => x.SpeID);
                });

            migrationBuilder.CreateTable(
                name: "Tstatus",
                columns: table => new
                {
                    NO = table.Column<byte>(nullable: false),
                    NAME = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tstatus", x => x.NO);
                });

            migrationBuilder.CreateTable(
                name: "TSubject",
                columns: table => new
                {
                    SubID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BeforWez = table.Column<int>(nullable: true),
                    PrivateSub = table.Column<bool>(nullable: true),
                    ShowMee = table.Column<bool>(nullable: true),
                    Subject = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSubject", x => x.SubID);
                });

            migrationBuilder.CreateTable(
                name: "TSubjectSuperv",
                columns: table => new
                {
                    SubIDSuperv = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActualCount = table.Column<int>(nullable: true),
                    ActualLoss = table.Column<int>(nullable: true),
                    ActualNoQarar = table.Column<short>(nullable: true),
                    ActualQarar = table.Column<int>(nullable: true),
                    Order = table.Column<int>(nullable: true),
                    ScEd = table.Column<int>(nullable: true),
                    SubjectSuperV = table.Column<string>(nullable: true),
                    sc = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSubjectSuperv", x => x.SubIDSuperv);
                });

            migrationBuilder.CreateTable(
                name: "TSubTotal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LevelNo = table.Column<byte>(nullable: false),
                    PlanType = table.Column<short>(nullable: false),
                    SubID = table.Column<short>(nullable: false),
                    SubIDTotal = table.Column<int>(nullable: true),
                    SubTotalname = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSubTotal", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TTColor",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Scolor = table.Column<string>(nullable: true),
                    subID = table.Column<int>(nullable: true),
                    tid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TTColor", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TTConfigerDur",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dur = table.Column<int>(nullable: false),
                    stratDurH = table.Column<int>(nullable: false),
                    stratDurM = table.Column<int>(nullable: false),
                    tid = table.Column<int>(nullable: true),
                    vacAfter = table.Column<int>(nullable: true),
                    vacAfter2 = table.Column<int>(nullable: true),
                    vacAfterPray = table.Column<int>(nullable: true),
                    vacDur = table.Column<int>(nullable: true),
                    vacDur2 = table.Column<int>(nullable: true),
                    vacDurPray = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TTConfigerDur", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TTDur",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    endH = table.Column<int>(nullable: true),
                    endM = table.Column<int>(nullable: true),
                    idC = table.Column<int>(nullable: true),
                    periodt = table.Column<string>(nullable: true),
                    startH = table.Column<int>(nullable: true),
                    startM = table.Column<int>(nullable: true),
                    teach = table.Column<bool>(nullable: true),
                    tid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TTDur", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TTRowsshape",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    @class = table.Column<int>(name: "class", nullable: true),
                    rowss = table.Column<int>(nullable: true),
                    shapess = table.Column<int>(nullable: true),
                    shapesss = table.Column<string>(nullable: true),
                    tid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TTRowsshape", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TTTeacharCustom",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ECClass = table.Column<int>(nullable: true),
                    EClass = table.Column<string>(nullable: true),
                    ERow = table.Column<int>(nullable: true),
                    Esubj = table.Column<string>(nullable: true),
                    Esubject = table.Column<int>(nullable: true),
                    empID = table.Column<int>(nullable: true),
                    empName = table.Column<string>(nullable: true),
                    tid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TTTeacharCustom", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TTteachClassess",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    classid = table.Column<int>(nullable: true),
                    clsType = table.Column<int>(nullable: true),
                    clsinweek = table.Column<int>(nullable: true),
                    empid = table.Column<int>(nullable: true),
                    subId = table.Column<int>(nullable: true),
                    tid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TTteachClassess", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Tuition",
                columns: table => new
                {
                    TuitionID = table.Column<byte>(nullable: false),
                    Tuition1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tuition", x => x.TuitionID);
                });

            migrationBuilder.CreateTable(
                name: "TUniversity",
                columns: table => new
                {
                    UnivId = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ARD = table.Column<byte>(nullable: true),
                    University = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TUniversity", x => x.UnivId);
                });

            migrationBuilder.CreateTable(
                name: "TWork",
                columns: table => new
                {
                    WID = table.Column<byte>(nullable: false),
                    BeforWez = table.Column<int>(nullable: true),
                    Ed = table.Column<bool>(nullable: true),
                    Work = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TWork", x => x.WID);
                });

            migrationBuilder.CreateTable(
                name: "TWorkEdar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FieldN = table.Column<string>(nullable: true),
                    Work = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TWorkEdar", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TWorkJob",
                columns: table => new
                {
                    WIDJ = table.Column<byte>(nullable: false),
                    CodeGroup = table.Column<int>(nullable: true),
                    SCode = table.Column<int>(nullable: true),
                    Wez = table.Column<bool>(nullable: true),
                    WorkJ = table.Column<string>(nullable: true),
                    WorkJClass = table.Column<string>(nullable: true),
                    s = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TWorkJob", x => x.WIDJ);
                });

            migrationBuilder.CreateTable(
                name: "viewMethod",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    viewName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_viewMethod", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Years",
                columns: table => new
                {
                    YearCode = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    YearName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Years", x => x.YearCode);
                });

            migrationBuilder.CreateTable(
                name: "ZCurrent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CurrentYear = table.Column<int>(nullable: false),
                    DEnd = table.Column<short>(nullable: false),
                    DStart = table.Column<short>(nullable: false),
                    MEnd = table.Column<short>(nullable: false),
                    MStart = table.Column<short>(nullable: false),
                    YEnd = table.Column<short>(nullable: false),
                    YStart = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZCurrent", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ZIDLosses",
                columns: table => new
                {
                    IdLoss = table.Column<string>(nullable: false),
                    Counted = table.Column<bool>(nullable: true),
                    OutMove = table.Column<bool>(nullable: true),
                    Qarar = table.Column<bool>(nullable: true),
                    SID = table.Column<short>(nullable: true),
                    STID = table.Column<short>(nullable: true),
                    SUBIDLoss = table.Column<short>(nullable: true),
                    WidLoss = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZIDLosses", x => x.IdLoss);
                });

            migrationBuilder.CreateTable(
                name: "ZNeedAll",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    From = table.Column<short>(nullable: true),
                    LossFinishQar = table.Column<short>(nullable: true),
                    LossFinishQarNo = table.Column<short>(nullable: true),
                    NEEDNow = table.Column<short>(nullable: true),
                    NEEDWez = table.Column<short>(nullable: true),
                    NeedYear = table.Column<short>(nullable: false),
                    NeedYearName = table.Column<string>(nullable: true),
                    NewContr = table.Column<short>(nullable: true),
                    NewFormal = table.Column<short>(nullable: true),
                    Result = table.Column<short>(nullable: true),
                    ResultNow = table.Column<short>(nullable: true),
                    SID = table.Column<short>(nullable: false),
                    STID = table.Column<short>(nullable: false),
                    SUBID = table.Column<short>(nullable: false),
                    Stage = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    TO = table.Column<short>(nullable: true),
                    TotalFrom = table.Column<short>(nullable: true),
                    TotalTo = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZNeedAll", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TCLassRow",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CLass = table.Column<byte>(nullable: true),
                    CLassR = table.Column<byte>(nullable: true),
                    LevelNo = table.Column<byte>(nullable: true),
                    Rows = table.Column<byte>(nullable: true),
                    SchoolID = table.Column<int>(nullable: true),
                    TLevel_RowId = table.Column<int>(nullable: true),
                    pupil = table.Column<short>(nullable: true),
                    pupilR = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TCLassRow", x => x.id);
                    table.ForeignKey(
                        name: "FK_TCLassRow_TLevel_Row_TLevel_RowId",
                        column: x => x.TLevel_RowId,
                        principalTable: "TLevel_Row",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReportD",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ReportCatid = table.Column<int>(nullable: true),
                    Rname = table.Column<string>(nullable: true),
                    TImployeesRIdentity = table.Column<bool>(nullable: true),
                    TSchoolsRTSchools_SchoolID = table.Column<int>(nullable: false),
                    TSchoolsRTSchools_SchoolID1 = table.Column<bool>(nullable: true),
                    cat_id = table.Column<int>(nullable: false),
                    sqlstring = table.Column<string>(nullable: true),
                    viewMethod = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportD", x => x.id);
                    table.ForeignKey(
                        name: "FK_ReportD_ReportCat_ReportCatid",
                        column: x => x.ReportCatid,
                        principalTable: "ReportCat",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReportD_TImployeesR_TImployeesRIdentity",
                        column: x => x.TImployeesRIdentity,
                        principalTable: "TImployeesR",
                        principalColumn: "Identity",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReportD_TSchoolsR_TSchoolsRTSchools_SchoolID1",
                        column: x => x.TSchoolsRTSchools_SchoolID1,
                        principalTable: "TSchoolsR",
                        principalColumn: "TSchools_SchoolID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TImployees",
                columns: table => new
                {
                    Identity = table.Column<string>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Amail = table.Column<string>(nullable: true),
                    AppDateD = table.Column<byte>(nullable: true),
                    AppDateM = table.Column<byte>(nullable: true),
                    AppDatey = table.Column<short>(nullable: true),
                    Appointment = table.Column<string>(nullable: true),
                    BirthDated = table.Column<byte>(nullable: true),
                    BirthDatem = table.Column<byte>(nullable: true),
                    BirthDatey = table.Column<short>(nullable: true),
                    BirthPlace = table.Column<string>(nullable: true),
                    Box = table.Column<string>(nullable: true),
                    Checked = table.Column<byte>(nullable: true),
                    ClassNo = table.Column<byte>(nullable: true),
                    Contract = table.Column<byte>(nullable: true),
                    ContractDated = table.Column<string>(nullable: true),
                    ContractDatem = table.Column<string>(nullable: true),
                    ContractDatey = table.Column<string>(nullable: true),
                    Country = table.Column<byte>(nullable: true),
                    Degree = table.Column<byte>(nullable: true),
                    Educational = table.Column<byte>(nullable: true),
                    EmpRule = table.Column<int>(nullable: true),
                    EmpStatus = table.Column<string>(nullable: true),
                    FName = table.Column<string>(nullable: true),
                    IDW = table.Column<byte>(nullable: true),
                    Identity2 = table.Column<string>(nullable: true),
                    JobID = table.Column<int>(nullable: true),
                    Level = table.Column<byte>(nullable: true),
                    Level2 = table.Column<byte>(nullable: true),
                    Married = table.Column<byte>(nullable: true),
                    Mopaul = table.Column<string>(nullable: true),
                    NOTBAD = table.Column<string>(nullable: true),
                    NOTBAD1 = table.Column<string>(nullable: true),
                    Naey = table.Column<float>(nullable: true),
                    NatID = table.Column<byte>(nullable: true),
                    OtherClasses = table.Column<byte>(nullable: true),
                    Post = table.Column<string>(nullable: true),
                    PreSchool = table.Column<int>(nullable: true),
                    PreSchoolS = table.Column<int>(nullable: true),
                    Problem = table.Column<bool>(nullable: true),
                    Problem2 = table.Column<bool>(nullable: true),
                    ProblemDesc = table.Column<string>(nullable: true),
                    QDated = table.Column<byte>(nullable: true),
                    QDatem = table.Column<byte>(nullable: true),
                    QDatey = table.Column<short>(nullable: true),
                    QID = table.Column<byte>(nullable: true),
                    QTerm = table.Column<int>(nullable: true),
                    Quien_Weight = table.Column<float>(nullable: true),
                    Quien_mark = table.Column<float>(nullable: true),
                    Quien_markFrom = table.Column<float>(nullable: true),
                    Reduced = table.Column<int>(nullable: true),
                    ReducedTypesReduedCode = table.Column<int>(nullable: true),
                    Reduction = table.Column<byte>(nullable: true),
                    SSMA_TimeStamp = table.Column<byte[]>(nullable: true),
                    SchID = table.Column<int>(nullable: true),
                    SchIDPrev = table.Column<int>(nullable: true),
                    Sile_contract = table.Column<string>(nullable: true),
                    SpecialID = table.Column<byte>(nullable: true),
                    SpecialID2 = table.Column<byte>(nullable: true),
                    SpecialIDOrig = table.Column<byte>(nullable: true),
                    SubID = table.Column<int>(nullable: true),
                    SubIDSV = table.Column<byte>(nullable: true),
                    SubIDSpec = table.Column<byte>(nullable: true),
                    SunNo = table.Column<byte>(nullable: true),
                    TSpecialSpeID = table.Column<byte>(nullable: true),
                    TStatus = table.Column<string>(nullable: true),
                    Tsymbole = table.Column<string>(nullable: true),
                    University = table.Column<byte>(nullable: true),
                    WID = table.Column<byte>(nullable: true),
                    WIDJ = table.Column<byte>(nullable: true),
                    WID_D = table.Column<byte>(nullable: true),
                    WID_M = table.Column<byte>(nullable: true),
                    WID_Y = table.Column<short>(nullable: true),
                    WorkDated = table.Column<byte>(nullable: true),
                    WorkDatem = table.Column<byte>(nullable: true),
                    WorkDatey = table.Column<short>(nullable: true),
                    Work_Area_M = table.Column<byte>(nullable: true),
                    Work_Area_Y = table.Column<short>(nullable: true),
                    Work_Count_D = table.Column<byte>(nullable: true),
                    Work_Count_M = table.Column<byte>(nullable: true),
                    Work_Count_Y = table.Column<short>(nullable: true),
                    Work_Shl_D = table.Column<byte>(nullable: true),
                    Work_Shl_M = table.Column<byte>(nullable: true),
                    Work_Shl_Y = table.Column<short>(nullable: true),
                    absent = table.Column<short>(nullable: true),
                    absent_W = table.Column<short>(nullable: true),
                    addClasses = table.Column<int>(nullable: true),
                    addSubClasses = table.Column<int>(nullable: true),
                    class_num = table.Column<int>(nullable: true),
                    completion = table.Column<string>(nullable: true),
                    deciInvo = table.Column<string>(nullable: true),
                    decision = table.Column<string>(nullable: true),
                    guidance_date_d = table.Column<short>(nullable: true),
                    guidance_date_m = table.Column<short>(nullable: true),
                    guidance_date_y = table.Column<short>(nullable: true),
                    guidance_no = table.Column<string>(nullable: true),
                    holiday_d = table.Column<byte>(nullable: true),
                    holiday_m = table.Column<byte>(nullable: true),
                    holiday_y = table.Column<short>(nullable: true),
                    img = table.Column<string>(nullable: true),
                    major = table.Column<int>(nullable: true),
                    mark1 = table.Column<float>(nullable: true),
                    mark2 = table.Column<float>(nullable: true),
                    mark3 = table.Column<float>(nullable: true),
                    mark4 = table.Column<float>(nullable: true),
                    name1 = table.Column<string>(nullable: true),
                    name2 = table.Column<string>(nullable: true),
                    name3 = table.Column<string>(nullable: true),
                    naql = table.Column<int>(nullable: true),
                    password = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    quien = table.Column<byte>(nullable: true),
                    salary = table.Column<float>(nullable: true),
                    tagdeer = table.Column<byte>(nullable: true),
                    work_Area_D = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TImployees", x => x.Identity);
                    table.ForeignKey(
                        name: "FK_TImployees_ReducedTypes_ReducedTypesReduedCode",
                        column: x => x.ReducedTypesReduedCode,
                        principalTable: "ReducedTypes",
                        principalColumn: "ReduedCode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TImployees_TSpecial_TSpecialSpeID",
                        column: x => x.TSpecialSpeID,
                        principalTable: "TSpecial",
                        principalColumn: "SpeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TSchools",
                columns: table => new
                {
                    SchoolID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AfterNoon = table.Column<byte>(nullable: true),
                    Auhority = table.Column<byte>(nullable: true),
                    Backswept = table.Column<byte>(nullable: true),
                    BeforWezSchC = table.Column<int>(nullable: true),
                    Build = table.Column<byte>(nullable: true),
                    CadE_door = table.Column<byte>(nullable: true),
                    Center = table.Column<byte>(nullable: true),
                    Checked = table.Column<byte>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Common = table.Column<byte>(nullable: true),
                    DUbdat = table.Column<string>(nullable: true),
                    Distance = table.Column<float>(nullable: true),
                    Door = table.Column<byte>(nullable: true),
                    EMail = table.Column<string>(nullable: true),
                    FAx = table.Column<string>(nullable: true),
                    FDate = table.Column<short>(nullable: true),
                    LevelNo = table.Column<byte>(nullable: true),
                    Liver = table.Column<string>(nullable: true),
                    Mcde = table.Column<byte>(nullable: true),
                    NearerSH1 = table.Column<string>(nullable: true),
                    NearerSH2 = table.Column<string>(nullable: true),
                    NearerSH3 = table.Column<string>(nullable: true),
                    OldCenter = table.Column<string>(nullable: true),
                    OldName = table.Column<string>(nullable: true),
                    OldSchoolId = table.Column<int>(nullable: true),
                    Out = table.Column<byte>(nullable: true),
                    Periority = table.Column<short>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Rooms = table.Column<byte>(nullable: true),
                    SSMA_TimeStamp = table.Column<byte[]>(nullable: true),
                    SchIDCom = table.Column<short>(nullable: true),
                    SchType = table.Column<byte>(nullable: true),
                    School = table.Column<string>(nullable: true),
                    SchoolID2 = table.Column<int>(nullable: true),
                    SchoolRID = table.Column<int>(nullable: true),
                    SectID = table.Column<short>(nullable: true),
                    Site = table.Column<string>(nullable: true),
                    Space1 = table.Column<short>(nullable: true),
                    Space2 = table.Column<short>(nullable: true),
                    Space3 = table.Column<short>(nullable: true),
                    Strip = table.Column<int>(nullable: true),
                    StripPeriority = table.Column<int>(nullable: true),
                    TBuildingBuildingID = table.Column<byte>(nullable: true),
                    TSchoolsTypeSchoolTyp = table.Column<byte>(nullable: true),
                    T_CenterID = table.Column<byte>(nullable: true),
                    T_areaId = table.Column<int>(nullable: true),
                    Tester = table.Column<byte>(nullable: true),
                    Tsertailor = table.Column<byte>(nullable: true),
                    TsrEn = table.Column<byte>(nullable: true),
                    TsrPys = table.Column<byte>(nullable: true),
                    Tuition = table.Column<byte>(nullable: true),
                    Tuition1TuitionID = table.Column<byte>(nullable: true),
                    Varway1 = table.Column<byte>(nullable: true),
                    Varway2 = table.Column<byte>(nullable: true),
                    Varway3 = table.Column<byte>(nullable: true),
                    WezSchC = table.Column<int>(nullable: true),
                    addition = table.Column<byte>(nullable: true),
                    area = table.Column<byte>(nullable: true),
                    circleEd = table.Column<int>(nullable: true),
                    circleEd2 = table.Column<int>(nullable: true),
                    circleEd3 = table.Column<int>(nullable: true),
                    circleEd4 = table.Column<int>(nullable: true),
                    created = table.Column<byte>(nullable: true),
                    desert = table.Column<byte>(nullable: true),
                    dismiss = table.Column<int>(nullable: true),
                    finalize = table.Column<byte>(nullable: true),
                    kind_SCH = table.Column<byte>(nullable: true),
                    location = table.Column<string>(nullable: true),
                    locshot = table.Column<string>(nullable: true),
                    office = table.Column<short>(nullable: true),
                    repetID1 = table.Column<byte>(nullable: true),
                    repetID2 = table.Column<byte>(nullable: true),
                    street = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TSchools", x => x.SchoolID);
                    table.ForeignKey(
                        name: "FK_TSchools_TLevel_LevelNo",
                        column: x => x.LevelNo,
                        principalTable: "TLevel",
                        principalColumn: "LevelNo",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TSchools_TSector_SectID",
                        column: x => x.SectID,
                        principalTable: "TSector",
                        principalColumn: "SectID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TSchools_TBuilding_TBuildingBuildingID",
                        column: x => x.TBuildingBuildingID,
                        principalTable: "TBuilding",
                        principalColumn: "BuildingID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TSchools_TSchoolsType_TSchoolsTypeSchoolTyp",
                        column: x => x.TSchoolsTypeSchoolTyp,
                        principalTable: "TSchoolsType",
                        principalColumn: "SchoolTyp",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TSchools_T_Center_T_CenterID",
                        column: x => x.T_CenterID,
                        principalTable: "T_Center",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TSchools_T_area_T_areaId",
                        column: x => x.T_areaId,
                        principalTable: "T_area",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TSchools_Tuition_Tuition1TuitionID",
                        column: x => x.Tuition1TuitionID,
                        principalTable: "Tuition",
                        principalColumn: "TuitionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClassCompare",
                columns: table => new
                {
                    SCh_Code = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amen = table.Column<byte>(nullable: true),
                    CLAS1 = table.Column<double>(nullable: true),
                    CLAS2 = table.Column<double>(nullable: true),
                    CLAS3 = table.Column<double>(nullable: true),
                    CLAS4 = table.Column<double>(nullable: true),
                    CLAS5 = table.Column<double>(nullable: true),
                    CLAS6 = table.Column<double>(nullable: true),
                    CLAS7 = table.Column<double>(nullable: true),
                    CLAS8 = table.Column<double>(nullable: true),
                    CLAS9 = table.Column<double>(nullable: true),
                    CLass = table.Column<byte>(nullable: true),
                    Hares = table.Column<short>(nullable: true),
                    Kateb = table.Column<short>(nullable: true),
                    Masader = table.Column<byte>(nullable: true),
                    MohadHass = table.Column<byte>(nullable: true),
                    MohaderOloom = table.Column<byte>(nullable: true),
                    Morshed = table.Column<byte>(nullable: true),
                    Mosaed = table.Column<short>(nullable: true),
                    Mosjel = table.Column<short>(nullable: true),
                    Mostakhdem = table.Column<short>(nullable: true),
                    Mouder = table.Column<byte>(nullable: true),
                    NoTeach = table.Column<byte>(nullable: true),
                    Raed = table.Column<short>(nullable: true),
                    St1 = table.Column<double>(nullable: true),
                    St2 = table.Column<double>(nullable: true),
                    St3 = table.Column<double>(nullable: true),
                    St4 = table.Column<double>(nullable: true),
                    St5 = table.Column<double>(nullable: true),
                    St6 = table.Column<double>(nullable: true),
                    St7 = table.Column<double>(nullable: true),
                    St8 = table.Column<double>(nullable: true),
                    St9 = table.Column<double>(nullable: true),
                    Stud = table.Column<short>(nullable: true),
                    TSchoolsSchoolID = table.Column<int>(nullable: true),
                    TeachSp = table.Column<byte>(nullable: true),
                    Teachers = table.Column<byte>(nullable: true),
                    TotalTeach = table.Column<byte>(nullable: true),
                    Wakel = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassCompare", x => x.SCh_Code);
                    table.ForeignKey(
                        name: "FK_ClassCompare_TSchools_TSchoolsSchoolID",
                        column: x => x.TSchoolsSchoolID,
                        principalTable: "TSchools",
                        principalColumn: "SchoolID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TimeTable",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TSchoolsSchoolID = table.Column<int>(nullable: true),
                    caseA = table.Column<int>(nullable: true),
                    cr_date = table.Column<DateTime>(nullable: true),
                    cr_id = table.Column<string>(nullable: true),
                    day_num = table.Column<int>(nullable: true),
                    per_num = table.Column<int>(nullable: true),
                    sch_id = table.Column<int>(nullable: true),
                    up_date = table.Column<DateTime>(nullable: true),
                    up_id = table.Column<int>(nullable: true),
                    viewM = table.Column<int>(nullable: true),
                    viewMethodid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeTable", x => x.id);
                    table.ForeignKey(
                        name: "FK_TimeTable_TSchools_TSchoolsSchoolID",
                        column: x => x.TSchoolsSchoolID,
                        principalTable: "TSchools",
                        principalColumn: "SchoolID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TimeTable_viewMethod_viewMethodid",
                        column: x => x.viewMethodid,
                        principalTable: "viewMethod",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "day_s",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Friday = table.Column<bool>(nullable: false),
                    Monday = table.Column<bool>(nullable: false),
                    Saturday = table.Column<bool>(nullable: false),
                    Sunday = table.Column<bool>(nullable: false),
                    T_id = table.Column<int>(nullable: true),
                    Thursday = table.Column<bool>(nullable: false),
                    TimeTableid = table.Column<int>(nullable: true),
                    Tuesday = table.Column<bool>(nullable: false),
                    Wednesday = table.Column<bool>(nullable: false),
                    day_id = table.Column<int>(nullable: true),
                    day_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_day_s", x => x.id);
                    table.ForeignKey(
                        name: "FK_day_s_TimeTable_TimeTableid",
                        column: x => x.TimeTableid,
                        principalTable: "TimeTable",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "periods",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    T_id = table.Column<int>(nullable: false),
                    TimeTableid = table.Column<int>(nullable: true),
                    Tnum = table.Column<int>(nullable: true),
                    Trow = table.Column<int>(nullable: true),
                    class_id = table.Column<string>(nullable: true),
                    day_id = table.Column<int>(nullable: true),
                    emp_id = table.Column<int>(nullable: true),
                    period_id = table.Column<int>(nullable: true),
                    subject_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_periods", x => x.id);
                    table.ForeignKey(
                        name: "FK_periods_TimeTable_TimeTableid",
                        column: x => x.TimeTableid,
                        principalTable: "TimeTable",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "supervision",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    T_id = table.Column<int>(nullable: false),
                    TimeTableid = table.Column<int>(nullable: true),
                    day_id = table.Column<int>(nullable: true),
                    emp_id = table.Column<int>(nullable: true),
                    supervision_tasksid = table.Column<int>(nullable: true),
                    task_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_supervision", x => x.id);
                    table.ForeignKey(
                        name: "FK_supervision_TimeTable_TimeTableid",
                        column: x => x.TimeTableid,
                        principalTable: "TimeTable",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_supervision_supervision_tasks_supervision_tasksid",
                        column: x => x.supervision_tasksid,
                        principalTable: "supervision_tasks",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ClassCompare_TSchoolsSchoolID",
                table: "ClassCompare",
                column: "TSchoolsSchoolID");

            migrationBuilder.CreateIndex(
                name: "IX_day_s_TimeTableid",
                table: "day_s",
                column: "TimeTableid");

            migrationBuilder.CreateIndex(
                name: "IX_periods_TimeTableid",
                table: "periods",
                column: "TimeTableid");

            migrationBuilder.CreateIndex(
                name: "IX_ReportD_ReportCatid",
                table: "ReportD",
                column: "ReportCatid");

            migrationBuilder.CreateIndex(
                name: "IX_ReportD_TImployeesRIdentity",
                table: "ReportD",
                column: "TImployeesRIdentity");

            migrationBuilder.CreateIndex(
                name: "IX_ReportD_TSchoolsRTSchools_SchoolID1",
                table: "ReportD",
                column: "TSchoolsRTSchools_SchoolID1");

            migrationBuilder.CreateIndex(
                name: "IX_supervision_TimeTableid",
                table: "supervision",
                column: "TimeTableid");

            migrationBuilder.CreateIndex(
                name: "IX_supervision_supervision_tasksid",
                table: "supervision",
                column: "supervision_tasksid");

            migrationBuilder.CreateIndex(
                name: "IX_TCLassRow_TLevel_RowId",
                table: "TCLassRow",
                column: "TLevel_RowId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeTable_TSchoolsSchoolID",
                table: "TimeTable",
                column: "TSchoolsSchoolID");

            migrationBuilder.CreateIndex(
                name: "IX_TimeTable_viewMethodid",
                table: "TimeTable",
                column: "viewMethodid");

            migrationBuilder.CreateIndex(
                name: "IX_TImployees_ReducedTypesReduedCode",
                table: "TImployees",
                column: "ReducedTypesReduedCode");

            migrationBuilder.CreateIndex(
                name: "IX_TImployees_TSpecialSpeID",
                table: "TImployees",
                column: "TSpecialSpeID");

            migrationBuilder.CreateIndex(
                name: "IX_TSchools_LevelNo",
                table: "TSchools",
                column: "LevelNo");

            migrationBuilder.CreateIndex(
                name: "IX_TSchools_SectID",
                table: "TSchools",
                column: "SectID");

            migrationBuilder.CreateIndex(
                name: "IX_TSchools_TBuildingBuildingID",
                table: "TSchools",
                column: "TBuildingBuildingID");

            migrationBuilder.CreateIndex(
                name: "IX_TSchools_TSchoolsTypeSchoolTyp",
                table: "TSchools",
                column: "TSchoolsTypeSchoolTyp");

            migrationBuilder.CreateIndex(
                name: "IX_TSchools_T_CenterID",
                table: "TSchools",
                column: "T_CenterID");

            migrationBuilder.CreateIndex(
                name: "IX_TSchools_T_areaId",
                table: "TSchools",
                column: "T_areaId");

            migrationBuilder.CreateIndex(
                name: "IX_TSchools_Tuition1TuitionID",
                table: "TSchools",
                column: "Tuition1TuitionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "acceptstates");

            migrationBuilder.DropTable(
                name: "adela");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ClassCompare");

            migrationBuilder.DropTable(
                name: "Classes");

            migrationBuilder.DropTable(
                name: "CommonSch");

            migrationBuilder.DropTable(
                name: "Custom_Reduce");

            migrationBuilder.DropTable(
                name: "day_s");

            migrationBuilder.DropTable(
                name: "daysName");

            migrationBuilder.DropTable(
                name: "Delegation");

            migrationBuilder.DropTable(
                name: "DelegationType");

            migrationBuilder.DropTable(
                name: "Departs");

            migrationBuilder.DropTable(
                name: "ExportDelegation");

            migrationBuilder.DropTable(
                name: "FormsTypes");

            migrationBuilder.DropTable(
                name: "GroupN");

            migrationBuilder.DropTable(
                name: "guardian");

            migrationBuilder.DropTable(
                name: "HayNames");

            migrationBuilder.DropTable(
                name: "ImportDelegation");

            migrationBuilder.DropTable(
                name: "jobsMember");

            migrationBuilder.DropTable(
                name: "locationnss");

            migrationBuilder.DropTable(
                name: "Mboard");

            migrationBuilder.DropTable(
                name: "NeedCondition");

            migrationBuilder.DropTable(
                name: "NeedCondition1");

            migrationBuilder.DropTable(
                name: "NeedOptions");

            migrationBuilder.DropTable(
                name: "OutMove");

            migrationBuilder.DropTable(
                name: "periods");

            migrationBuilder.DropTable(
                name: "PlanNeed");

            migrationBuilder.DropTable(
                name: "PSubClas");

            migrationBuilder.DropTable(
                name: "PWorkClas");

            migrationBuilder.DropTable(
                name: "QType");

            migrationBuilder.DropTable(
                name: "QYtBEMARK");

            migrationBuilder.DropTable(
                name: "recordCat");

            migrationBuilder.DropTable(
                name: "ReportD");

            migrationBuilder.DropTable(
                name: "reson");

            migrationBuilder.DropTable(
                name: "restriction");

            migrationBuilder.DropTable(
                name: "ReviewCodes");

            migrationBuilder.DropTable(
                name: "RowName");

            migrationBuilder.DropTable(
                name: "SchoolDess");

            migrationBuilder.DropTable(
                name: "SchTypes");

            migrationBuilder.DropTable(
                name: "students");

            migrationBuilder.DropTable(
                name: "studentsR");

            migrationBuilder.DropTable(
                name: "Studentstate");

            migrationBuilder.DropTable(
                name: "studentsWait");

            migrationBuilder.DropTable(
                name: "superJob");

            migrationBuilder.DropTable(
                name: "SuperLaw");

            migrationBuilder.DropTable(
                name: "SuperVis");

            migrationBuilder.DropTable(
                name: "supervision");

            migrationBuilder.DropTable(
                name: "supervisors");

            migrationBuilder.DropTable(
                name: "T_CenterPa");

            migrationBuilder.DropTable(
                name: "T_CenterPaTypes");

            migrationBuilder.DropTable(
                name: "T_ClassType");

            migrationBuilder.DropTable(
                name: "T_contract");

            migrationBuilder.DropTable(
                name: "T_country");

            migrationBuilder.DropTable(
                name: "T_Door");

            migrationBuilder.DropTable(
                name: "T_Married");

            migrationBuilder.DropTable(
                name: "T_PlanTemp");

            migrationBuilder.DropTable(
                name: "T_Varway1");

            migrationBuilder.DropTable(
                name: "T_yes_no");

            migrationBuilder.DropTable(
                name: "TCadE_door");

            migrationBuilder.DropTable(
                name: "TCLassRow");

            migrationBuilder.DropTable(
                name: "TempStatus");

            migrationBuilder.DropTable(
                name: "TempTSubj");

            migrationBuilder.DropTable(
                name: "TimplAutoUpdate");

            migrationBuilder.DropTable(
                name: "TImployees");

            migrationBuilder.DropTable(
                name: "TImployeesCompl");

            migrationBuilder.DropTable(
                name: "TimployeesDel");

            migrationBuilder.DropTable(
                name: "TImployeesTemp");

            migrationBuilder.DropTable(
                name: "TLevel_Row_Code");

            migrationBuilder.DropTable(
                name: "TLevel_Subject");

            migrationBuilder.DropTable(
                name: "TNationality");

            migrationBuilder.DropTable(
                name: "TQualified");

            migrationBuilder.DropTable(
                name: "TransTypes");

            migrationBuilder.DropTable(
                name: "TSpecial_LevErr");

            migrationBuilder.DropTable(
                name: "TSpecial_Sub");

            migrationBuilder.DropTable(
                name: "TSpecial_Sub1");

            migrationBuilder.DropTable(
                name: "TSpecialSub");

            migrationBuilder.DropTable(
                name: "Tstatus");

            migrationBuilder.DropTable(
                name: "TSubject");

            migrationBuilder.DropTable(
                name: "TSubjectSuperv");

            migrationBuilder.DropTable(
                name: "TSubTotal");

            migrationBuilder.DropTable(
                name: "TTColor");

            migrationBuilder.DropTable(
                name: "TTConfigerDur");

            migrationBuilder.DropTable(
                name: "TTDur");

            migrationBuilder.DropTable(
                name: "TTRowsshape");

            migrationBuilder.DropTable(
                name: "TTTeacharCustom");

            migrationBuilder.DropTable(
                name: "TTteachClassess");

            migrationBuilder.DropTable(
                name: "TUniversity");

            migrationBuilder.DropTable(
                name: "TWork");

            migrationBuilder.DropTable(
                name: "TWorkEdar");

            migrationBuilder.DropTable(
                name: "TWorkJob");

            migrationBuilder.DropTable(
                name: "Years");

            migrationBuilder.DropTable(
                name: "ZCurrent");

            migrationBuilder.DropTable(
                name: "ZIDLosses");

            migrationBuilder.DropTable(
                name: "ZNeedAll");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "ReportCat");

            migrationBuilder.DropTable(
                name: "TImployeesR");

            migrationBuilder.DropTable(
                name: "TSchoolsR");

            migrationBuilder.DropTable(
                name: "TimeTable");

            migrationBuilder.DropTable(
                name: "supervision_tasks");

            migrationBuilder.DropTable(
                name: "TLevel_Row");

            migrationBuilder.DropTable(
                name: "ReducedTypes");

            migrationBuilder.DropTable(
                name: "TSpecial");

            migrationBuilder.DropTable(
                name: "TSchools");

            migrationBuilder.DropTable(
                name: "viewMethod");

            migrationBuilder.DropTable(
                name: "TLevel");

            migrationBuilder.DropTable(
                name: "TSector");

            migrationBuilder.DropTable(
                name: "TBuilding");

            migrationBuilder.DropTable(
                name: "TSchoolsType");

            migrationBuilder.DropTable(
                name: "T_Center");

            migrationBuilder.DropTable(
                name: "T_area");

            migrationBuilder.DropTable(
                name: "Tuition");
        }
    }
}
