﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace qortuba.Migrations
{
    public partial class mg2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "TImployees",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TImployees_ApplicationUserId",
                table: "TImployees",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TImployees_AspNetUsers_ApplicationUserId",
                table: "TImployees",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TImployees_AspNetUsers_ApplicationUserId",
                table: "TImployees");

            migrationBuilder.DropIndex(
                name: "IX_TImployees_ApplicationUserId",
                table: "TImployees");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "TImployees");
        }
    }
}
