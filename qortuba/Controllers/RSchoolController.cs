﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using qortuba.Models;
using System.Data;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using ClosedXML.Excel;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using qortuba.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using qortuba.Models.Class;
using Microsoft.AspNetCore.Hosting;
using Rotativa.AspNetCore;

namespace qortuba.Controllers
{
  //  [Authorize]
    public class RSchoolController : Controller
    {
        // GET: RSchool
        private readonly ApplicationDbContext db;
        MyFunctions fun = new MyFunctions();
        private readonly IHostingEnvironment _HostEnvironment;
        public RSchoolController(ApplicationDbContext Context, IHostingEnvironment HostEnvironment)  
        {
            db = Context;
            _HostEnvironment = HostEnvironment;
        }
        // GET: Remp
        public ActionResult Index()
        {
            var reportD = db.ReportD.Where(t=>t.cat_id == 2 && t.id > 7);
            return View(reportD.ToList());
        }
        public ActionResult AllSchool(int id)
        {
            var rschool = db.TSchoolsR.Where(t => t.reportID == id).FirstOrDefault();
            return View(rschool);
        }

        [HttpPost]
        public ActionResult Create(TSchoolsR tSchoolsR)
        {
            if (ModelState.IsValid)
            {

                string chk = fun.fireDataRow("SELECT count([id])FROM [dbo].[ReportD]")[0].ToString();
                string chki = fun.fireDataRow("SELECT count([TSchools.SchoolID])FROM [dbo].[TSchoolsR]")[0].ToString();
                if (chk != "0")
                {
                    if (chki != "0")
                    {
                        int id = db.ReportD.Max(t => t.id) + 1;
                        tSchoolsR.reportID = id;
                        fun.fireSQL("INSERT INTO [dbo].[ReportD]([id],[Rname],[cat_id],[sqlstring],[viewMethod]) VALUES (" + id + ",N'" + tSchoolsR.Rname + "',2,'',N'" + tSchoolsR.viewM + "' );");
                        db.TSchoolsR.Add(tSchoolsR);
                        db.SaveChanges();
                    }
                    else
                    {
                        int id = db.ReportD.Max(t => t.id) + 1;
                        tSchoolsR.reportID = id;
                        fun.fireSQL("INSERT INTO [dbo].[ReportD]([id],[Rname],[cat_id],[sqlstring],[viewMethod]) VALUES (" + id + ",N'" + tSchoolsR.Rname + "',2,'',N'" + tSchoolsR.viewM + "' );");
                        db.TSchoolsR.Add(tSchoolsR);
                        db.SaveChanges();
                    }
                }
            }
            else
            {

                int id = 1;
                tSchoolsR.reportID = id;
                fun.fireSQL("INSERT INTO [dbo].[ReportD]([id],[Rname],[cat_id],[sqlstring],[viewMethod]) VALUES (" + id + ",N'" + tSchoolsR.Rname + "',2,'',N'" + tSchoolsR.viewM + "' );");
                db.TSchoolsR.Add(tSchoolsR);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Edit(TSchoolsR tSchoolsR)
        {

            if (ModelState.IsValid)
            {
                string upd = getSql(tSchoolsR);

                var result = db.ReportD.SingleOrDefault(b => b.id == tSchoolsR.reportID);
                result.sqlstring = upd;
                result.viewMethod = tSchoolsR.viewM;
                fun.fireSQL("UPDATE ReportD SET Rname = N'" + tSchoolsR.Rname + "' WHERE id = " + tSchoolsR.reportID + ";");
                db.Update(tSchoolsR);
                db.SaveChanges();
                TempData["success"] = "تم الحفظ";
                return RedirectToAction("AllSchool", new { id = tSchoolsR.reportID });
            }
            TempData["error"] = "يوجد مشكلة فى الحفظ";
            return RedirectToAction("AllSchool", new { id = tSchoolsR.reportID });



        }

        private string getSql(TSchoolsR tSchoolsR)
        {

            string sqlstat = "Select ROW_NUMBER() OVER(ORDER BY [SchoolID] ASC) AS م ,";

            foreach (var lp in tSchoolsR.GetType().GetProperties())
            {
                if (lp.Name != "ReportD")
                {
                    try
                    {
                        string vl = lp.GetValue(tSchoolsR, null).ToString();

                        if (vl == "True")
                        {
                            if (lp.Name == "Cad_CadE_door")
                            {
                                string arbicname = lp.GetCustomAttributesData()[0].NamedArguments[0].TypedValue.Value.ToString();
                                sqlstat += "Cad.CadE_door" + " as [" + arbicname + "] ,";
                            }
                            else if (lp.Name == "circleEd")
                            {
                                string arbicname = lp.GetCustomAttributesData()[0].NamedArguments[0].TypedValue.Value.ToString();
                                sqlstat += "CONCAT(TSchools.circleEd,'-',TSchools.circleEd2,'-',TSchools.circleEd3,'-',TSchools.circleEd4)" + " as [" + arbicname + "] ,";

                            }
                            else
                            {
                                string nm = lp.Name.ToString().Replace("_", ".");
                                string arbicname = lp.GetCustomAttributesData()[0].NamedArguments[0].TypedValue.Value.ToString();
                                sqlstat += nm + " as [" + arbicname + "] ,";
                            }

                        }

                    }
                    catch
                    {

                    }
                }

            }
            sqlstat = sqlstat.Remove(sqlstat.Length - 1);
            string state = sqlstat + " FROM TSchools "+
"INNER JOIN T_area ar ON TSchools.area = ar.IDNU " +
"INNER JOIN TLevel Tl ON TSchools.LevelNo = Tl.LevelNo " +
"INNER JOIN TSchoolsType st ON TSchools.SchType = st.SchoolTyp " +
"INNER JOIN Tuition tu ON TSchools.Tuition = tu.TuitionID " +
"INNER JOIN T_Center tc ON TSchools.Center = tc.ID " +
"INNER JOIN T_yes_no fin ON TSchools.finalize = fin.[on] " +
"INNER JOIN T_yes_no com ON TSchools.Common = com.[on] " +
"INNER JOIN T_yes_no Bak ON TSchools.Backswept = Bak.[on] " +
"INNER JOIN T_yes_no cre ON TSchools.created = cre.[on] " +
"INNER JOIN T_yes_no out1 ON TSchools.[Out] = out1.[on] " +
"INNER JOIN TBuilding bild ON TSchools.Build = bild.BuildingID " +
"INNER JOIN TCadE_door Cad ON TSchools.CadE_door = Cad.CadE_ID " +
"INNER JOIN SchTypes sty ON TSchools.AfterNoon = sty.SchType " +
"INNER JOIN T_yes_no desr ON TSchools.desert = desr.[on] " +
"INNER JOIN T_yes_no kind ON TSchools.kind_SCH = kind.[on] " +
"INNER JOIN T_Varway1 v1 ON TSchools.Varway1 = v1.Varway1ID " +
"INNER JOIN T_Varway1 v2 ON TSchools.Varway2 = v2.Varway1ID " +
"INNER JOIN T_Varway1 v3 ON TSchools.Varway3 = v3.Varway1ID " +
"where Center = 14 ";

            return state;
        }

        // All School
        public IActionResult SchoolReport()

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 8 ");
            if (statment != null)
            {
           
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\header School.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("School", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    var pdf = new ViewAsPdf("Emps", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }

        public FileResult SchoolReportX()

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 8 ");
      
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());

            dt.TableName = "جميع المدارس";
            XLWorkbook wb = new XLWorkbook();

            wb.Worksheets.Add(dt);
            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع المدارس.xlsx");

        }

        public ActionResult School()
        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 8 ");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());

            return View(dt);
        }

        //  School by Level
        public ActionResult SchoolLevel()
        {
            ViewBag.LevelNo = new SelectList(db.TLevel, "LevelNo", "NameLevel");
            return View();
        }

  

        public ActionResult SchoolP(int id)
        {
            DataTable dt = fun.fireStoreProcedureWithP("school", id.ToString());

            return View(dt);
        }

        public IActionResult SchoolPReport(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 9");
            if (statment != null)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\headerSL.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);

                string sql1 = statment["sqlstring"].ToString() + " where s.LevelNo = " + id.ToString()+"";
                DataTable dt = fun.fireDataTable(sql1);

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("SchoolP", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    var pdf = new ViewAsPdf("SchoolP", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }

        //excel
        public FileResult SchoolPReportX(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 9 ");

            string sql1 = statment["sqlstring"].ToString() + " and TSchools.LevelNo = " + id.ToString() + "";
            DataTable dt = fun.fireDataTable(sql1);

            dt.TableName = " جميع المدارس حسب المرحلة";
            XLWorkbook wb = new XLWorkbook();

            wb.Worksheets.Add(dt);
            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع المدارس حسب المرحلة.xlsx");

        }
        //  School by Tuition
        public ActionResult SchoolTuition()
        {
            ViewBag.Tuition = new SelectList(db.Tuition, "TuitionID", "Tuition1");
            return View();
        }

        public ActionResult SchoolT(int id)
        {
            DataTable dt = fun.fireStoreProcedureWithP("school", id.ToString());

            return View(dt);
        }

        public IActionResult SchoolTuitionReport(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 10 ");
            if (statment != null)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\headerTut.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);

                string sql1 = statment["sqlstring"].ToString() + " and TSchools.Tuition = " + id.ToString() + "";
                DataTable dt = fun.fireDataTable(sql1);

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("SchoolT", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    var pdf = new ViewAsPdf("SchoolT", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }
        public FileResult SchoolTuitionReportX(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 10 ");

            string sql1 = statment["sqlstring"].ToString() + " and TSchools.Tuition = " + id.ToString() + "";
            DataTable dt = fun.fireDataTable(sql1);

            dt.TableName = " جميع المدارس حسب نوع التعليم";
            XLWorkbook wb = new XLWorkbook();

            wb.Worksheets.Add(dt);
            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع المدارس حسب نوع التعليم.xlsx");

        }



    }
}