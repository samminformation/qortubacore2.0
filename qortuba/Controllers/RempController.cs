﻿using System;
using System.Collections.Generic;
using System.Linq;
using qortuba.Models;
using System.Data;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using qortuba.Data;
using System.IO;
using qortuba.Models.Class;

namespace qortuba.Controllers
{
   // [Authorize]
    public class RempController : Controller
    {
        private readonly ApplicationDbContext db;
        MyFunctions fun = new MyFunctions();
        public RempController(ApplicationDbContext Context) 
        {
            db = Context;
        }
        // GET: Remp
        public ActionResult Index()
        {
            var reportD = db.ReportD.Where(t=>t.cat_id == 1 && t.id <5);
            return View(reportD.ToList());
        }
        public ActionResult AllEmp(int id)
        {
            var remp = db.TImployeesR.Where(t => t.reportID == id).FirstOrDefault();
            return View(remp);
        }
       
        [HttpPost]
        public ActionResult Create(TImployeesR imployeesR)
        {
            if (ModelState.IsValid)
            {
                string chk = fun.fireDataRow("SELECT count([id])FROM [dbo].[ReportD]")[0].ToString();
                string chki = fun.fireDataRow("SELECT count([Identity])FROM [dbo].[TImployeesR]")[0].ToString();
                if (chk != "0")
                {
                    if (chki != "0")
                    {
                        int id = db.ReportD.Max(t => t.id) + 1;
                        imployeesR.reportID = id;
                        fun.fireSQL("INSERT INTO [dbo].[ReportD]([id],[Rname],[cat_id],[sqlstring],[viewMethod]) VALUES (" + id + ",N'" + imployeesR.Rname + "',1,'',N'" + imployeesR.viewM + "' );");
                        db.TImployeesR.Add(imployeesR);
                        db.SaveChanges();
                    }
                    else {
                        int id = db.ReportD.Max(t => t.id) + 1;
                        imployeesR.reportID = id;
                        fun.fireSQL("INSERT INTO [dbo].[ReportD]([id],[Rname],[cat_id],[sqlstring],[viewMethod]) VALUES (" + id + ",N'" + imployeesR.Rname + "',1,'',N'" + imployeesR.viewM + "' );");
                        db.TImployeesR.Add(imployeesR);
                        db.SaveChanges();

                    }
                }
                else {

                    int id =  1;
                    imployeesR.reportID = id;
                    fun.fireSQL("INSERT INTO [dbo].[ReportD]([id],[Rname],[cat_id],[sqlstring],[viewMethod]) VALUES (" + id + ",N'" + imployeesR.Rname + "',1,'',N'" + imployeesR.viewM + "' );");
                    db.TImployeesR.Add(imployeesR);
                    db.SaveChanges();

                }
            }

            return RedirectToAction("Index");
        }
        public ActionResult Create() {
            return View();
        }
        public ActionResult Edit(TImployeesR tImployeesR)
        {

            if (ModelState.IsValid)
            {
                string upd =  getSql(tImployeesR);
                
                var result = db.ReportD.SingleOrDefault(b => b.id == tImployeesR.reportID);
                result.sqlstring = upd;
                result.viewMethod = tImployeesR.viewM;
                fun.fireSQL("UPDATE ReportD SET Rname = N'"+tImployeesR.Rname+"' WHERE id = "+tImployeesR.reportID+";");
                db.Update(tImployeesR);
                db.SaveChanges();
                TempData["success"] = "تم الحفظ";
                return RedirectToAction("AllEmp",new {id = tImployeesR.reportID });
            }
            TempData["error"] = "يوجد مشكلة فى الحفظ";
            return RedirectToAction("AllEmp", new { id = tImployeesR.reportID });



        }

        private string getSql(TImployeesR tImployeesR)
        {
            
            string sqlstat = "Select ROW_NUMBER() OVER(ORDER BY [Identity] ASC) AS م ,";

            foreach (var lp in tImployeesR.GetType().GetProperties())
            {
                if (lp.Name != "ReportD")
                {
                    try
                    {
                        string vl = lp.GetValue(tImployeesR, null).ToString();
    
                       if (vl == "True")
                        {
                            if (lp.Name == "Identity")
                            {
                                string arbicname = lp.GetCustomAttributesData()[0].NamedArguments[0].TypedValue.Value.ToString();
                                sqlstat +="["+ lp.Name + "]"+ " as [" + arbicname + "] ,";
                            }
                           else if (lp.Name == "name1")
                            {
                                string arbicname = lp.GetCustomAttributesData()[0].NamedArguments[0].TypedValue.Value.ToString();
                                sqlstat += "concat(TImployees.name1 ,SPACE(1), TImployees.name2 , SPACE(1) , TImployees.name3 , SPACE(1) , TImployees.FName)" + " as [" + arbicname + "] ,";

                            }
                            else if (lp.Name == "WorkDated")
                            {
                                string arbicname = lp.GetCustomAttributesData()[0].NamedArguments[0].TypedValue.Value.ToString();
                                sqlstat += "concat( TImployees.WorkDatey , SPACE(1),'-' , TImployees.WorkDatem , SPACE(1),'-' ,  TImployees.WorkDated)" + " as [" + arbicname + "] ,";

                            }
                            else if (lp.Name == "BirthDated")
                            {
                                string arbicname = lp.GetCustomAttributesData()[0].NamedArguments[0].TypedValue.Value.ToString();
                                sqlstat += "concat( TImployees.BirthDatey  , SPACE(1),'-' , TImployees.BirthDatem , SPACE(1),'-' ,TImployees.BirthDated )" + " as [" + arbicname + "] ,";

                            }
                            else if (lp.Name == "phone")
                            {
                                string arbicname = lp.GetCustomAttributesData()[0].NamedArguments[0].TypedValue.Value.ToString();
                                sqlstat += "TImployees." + lp.Name  + " as [" + arbicname + "] ,";

                            }
                            else {
                                string nm = lp.Name.ToString().Replace("_", ".");
                            string arbicname = lp.GetCustomAttributesData()[0].NamedArguments[0].TypedValue.Value.ToString();
                            sqlstat +=  nm  + " as [" + arbicname + "] ,";
                            }

                        }

                    }
                    catch {

                    }
                }

            }
            sqlstat = sqlstat.Remove(sqlstat.Length - 1);
            string state = sqlstat + "  FROM TImployees " +
"INNER JOIN TSchools s ON TImployees.SchID = s.SchoolID " +
"INNER JOIN TSchools sp ON TImployees.SchIDPrev = sp.SchoolID " +
"INNER JOIN TSubject c ON TImployees.SubID = c.SubID " +
"INNER JOIN TSpecialSub e ON TImployees.SpecialID = e.SpeID " +
"INNER JOIN TSpecialSub so ON TImployees.SpecialIDOrig = so.SpeID " +
"INNER JOIN TWork w ON TImployees.WID = w.WID " +
"INNER JOIN TempStatus es ON TImployees.EmpStatus = es.EmpStatus " +
"INNER JOIN ReducedTypes rt ON TImployees.Reduced = rt.ReduedCode " +
"INNER JOIN T_Married mr ON TImployees.Married = mr.NU " +
"INNER JOIN TNationality n ON TImployees.NatID = n.NatID where s.Center = 14";

            return state;
        }

        public ActionResult EmpsReport()

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 1 ");
            if (statment != null)
            {
                var header = Path.GetFileName("~/static/header.html");
                var footer = Path.GetFileName("~/static/footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    //var pdf = new ViewAsPdf("Emps", dt)
                    //{

                    //    CustomSwitches = customSwitches,
                  
                    //    PageMargins = new Rotativa.Options.Margins(40, 10, 15, 10),
                    //    PageOrientation = Rotativa.Options.Orientation.Portrait,
                    //    PageSize = Rotativa.Options.Size.A4
                    //};
                    //return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    //var pdf = new ViewAsPdf("Emps", dt)
                    //{

                    //    CustomSwitches = customSwitches,
                      
                    //    PageMargins = new Rotativa.Options.Margins(40, 10, 15, 10),
                    //    PageOrientation = Rotativa.Options.Orientation.Landscape,
                    //    PageSize = Rotativa.Options.Size.A4
                    //};
                    //return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }

        public ActionResult Emps()
        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 1 ");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());
          
            return View(dt);
        }

      

    }
}