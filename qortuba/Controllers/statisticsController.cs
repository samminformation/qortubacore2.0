﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using qortuba.Data;
using System.IO;
using System.Data;
using Rotativa.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace qortuba.Controllers
{
    public class statisticsController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly IHostingEnvironment _HostEnvironment;
        Models.Class.MyFunctions fun = new Models.Class.MyFunctions();
        public statisticsController(ApplicationDbContext Context, IHostingEnvironment HostEnvironment)
        {
            db = Context;
            _HostEnvironment = HostEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }

        /*   بيان العجز والزيادة لجميع المدارس*/

        public ActionResult tDesAndInc(int id)
        {


            return View();
        }

        public IActionResult DesAndIncPDF(int id)

        {
            if (id > 0)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\DesAndInc.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);

                DataTable dt = fun.fireStoreProcedure1("DesAndInc", id.ToString());


                var pdf = new ViewAsPdf("DesInc", dt)
                {

                    CustomSwitches = customSwitches,

                    PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                    PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                    PageSize = Rotativa.AspNetCore.Options.Size.A4
                };
                return pdf;


            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");

        }

        /*   بيان الكوادر البشرية*/

        public ActionResult tHR()
        {
            DataTable dt = fun.fireStoreProcedure("hr");

            return View(dt);
        }

        public IActionResult HRPDF()

        {
           
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\headerHR.html");
                var footer = Path.Combine(webRootPath, "static\\footerHR.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);

                DataTable dt = fun.fireStoreProcedure("hr");


                var pdf = new ViewAsPdf("tHR", dt)
                {

                    CustomSwitches = customSwitches,

                    PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                    PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                    PageSize = Rotativa.AspNetCore.Options.Size.A3

                };
                return pdf;


            }

        /*   بيان العجز والزيادة بالمواد*/

        public ActionResult rSubjects()
        {
            DataTable dt = fun.fireStoreProcedure("runReportAllSubj");
            return View(dt);
        }

        public IActionResult rSubjectsPDF()

        {

            string webRootPath = _HostEnvironment.WebRootPath;
            var header = Path.Combine(webRootPath, "static\\headerallSubject.html");
            var footer = Path.Combine(webRootPath, "static\\footerHR.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);
            DataTable dt = fun.fireStoreProcedure("runReportAllSubj");

            var pdf = new ViewAsPdf("rSubjects", dt)
            {

                CustomSwitches = customSwitches,

                PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageSize = Rotativa.AspNetCore.Options.Size.A3

            };
            return pdf;


        }

        /*   بيان باعداد طلاب مكتب قرطبة*/

        public ActionResult rQortobaStd()
        {
            DataTable dt = fun.fireStoreProcedure("studentStat");
            return View(dt);
        }

        public IActionResult rQortobaStdPDF()

        {

            string webRootPath = _HostEnvironment.WebRootPath;
            var header = Path.Combine(webRootPath, "static\\headerQortobastd.html");
            var footer = Path.Combine(webRootPath, "static\\footerHR.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);
            DataTable dt = fun.fireStoreProcedure("studentStat");

            var pdf = new ViewAsPdf("rQortobaStd", dt)
            {

                CustomSwitches = customSwitches,

                PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageSize = Rotativa.AspNetCore.Options.Size.A4

            };
            return pdf;


        }


        /*   بيان باعداد فصول مكتب قرطبة*/

        public ActionResult rQortobaClasses()
        {
            DataTable dt = fun.fireStoreProcedure("QortobaClasses");
            return View(dt);
        }

        public IActionResult rQortobaClassesPDF()

        {

            string webRootPath = _HostEnvironment.WebRootPath;
            var header = Path.Combine(webRootPath, "static\\headerQortobaClasses.html");
            var footer = Path.Combine(webRootPath, "static\\footerHR.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);
            DataTable dt = fun.fireStoreProcedure("QortobaClasses");

            var pdf = new ViewAsPdf("rQortobaClasses", dt)
            {

                CustomSwitches = customSwitches,

                PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageSize = Rotativa.AspNetCore.Options.Size.A4

            };
            return pdf;


        }

        /*   بيان باعداد المبانى المستأجرة مكتب قرطبة*/

        public ActionResult rQortobaBuildRent()
        {
            DataTable dt = fun.fireStoreProcedure("QortobaBuilsRent");
            return View(dt);
        }

        public IActionResult rQortobaBuildRentPDF()

        {

            string webRootPath = _HostEnvironment.WebRootPath;
            var header = Path.Combine(webRootPath, "static\\headerQortobaBRent.html");
            var footer = Path.Combine(webRootPath, "static\\footerHR.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);
            DataTable dt = fun.fireStoreProcedure("QortobaBuilsRent");

            var pdf = new ViewAsPdf("rQortobaBuildRent", dt)
            {

                CustomSwitches = customSwitches,

                PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageSize = Rotativa.AspNetCore.Options.Size.A4

            };
            return pdf;


        }

        /*   بيان باعداد المبانى الحكومية مكتب قرطبة*/

        public ActionResult rQortobaBuildGov()
        {
            DataTable dt = fun.fireStoreProcedure("QortobaBuilsGov");
            return View(dt);
        }

        public IActionResult rQortobaBuildGovPDF()

        {

            string webRootPath = _HostEnvironment.WebRootPath;
            var header = Path.Combine(webRootPath, "static\\headerQortobaBGov.html");
            var footer = Path.Combine(webRootPath, "static\\footerHR.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);
            DataTable dt = fun.fireStoreProcedure("QortobaBuilsGov");

            var pdf = new ViewAsPdf("rQortobaBuildGov", dt)
            {

                CustomSwitches = customSwitches,

                PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageSize = Rotativa.AspNetCore.Options.Size.A4

            };
            return pdf;


        }

        /*   بيان باعداد مدارس التعليم العام مكتب قرطبة*/

        public ActionResult rQortobaSchoolsG()
        {
            DataTable dt = fun.fireStoreProcedure("QortobaGlobal");
            return View(dt);
        }

        public IActionResult rQortobaSchoolsGPDF()

        {

            string webRootPath = _HostEnvironment.WebRootPath;
            var header = Path.Combine(webRootPath, "static\\headerQortobaSG.html");
            var footer = Path.Combine(webRootPath, "static\\footerHR.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);
            DataTable dt = fun.fireStoreProcedure("QortobaGlobal");

            var pdf = new ViewAsPdf("rQortobaSchoolsG", dt)
            {

                CustomSwitches = customSwitches,

                PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageSize = Rotativa.AspNetCore.Options.Size.A4

            };
            return pdf;


        }

        /*   بيان باعداد مدارس تحفيظ القرآن مكتب قرطبة*/

        public ActionResult rQortobaSchoolsQ()
        {
            DataTable dt = fun.fireStoreProcedure("QortobaQuran");
            return View(dt);
        }

        public IActionResult rQortobaSchoolsQPDF()

        {

            string webRootPath = _HostEnvironment.WebRootPath;
            var header = Path.Combine(webRootPath, "static\\headerQortobaSQ.html");
            var footer = Path.Combine(webRootPath, "static\\footerHR.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);
            DataTable dt = fun.fireStoreProcedure("QortobaQuran");

            var pdf = new ViewAsPdf("rQortobaSchoolsQ", dt)
            {

                CustomSwitches = customSwitches,

                PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageSize = Rotativa.AspNetCore.Options.Size.A4

            };
            return pdf;


        }

        /*   بيان باعداد مدارس التعليم الخاص مكتب قرطبة*/

        public ActionResult rQortobaSchoolsP()
        {
            DataTable dt = fun.fireStoreProcedure("QortobaPrivate");
            return View(dt);
        }

        public IActionResult rQortobaSchoolsPPDF()

        {

            string webRootPath = _HostEnvironment.WebRootPath;
            var header = Path.Combine(webRootPath, "static\\headerQortobaSP.html");
            var footer = Path.Combine(webRootPath, "static\\footerHR.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);
            DataTable dt = fun.fireStoreProcedure("QortobaPrivate");

            var pdf = new ViewAsPdf("rQortobaSchoolsP", dt)
            {

                CustomSwitches = customSwitches,
                PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                PageSize = Rotativa.AspNetCore.Options.Size.A4

            };
            return pdf;


        }
    }
}