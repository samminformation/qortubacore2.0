﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using qortuba.Data;
using qortuba.Models;

namespace qortuba.Controllers
{
    public class AdelaController : Controller
    {
       private readonly ApplicationDbContext db;
        public AdelaController(ApplicationDbContext applicationDbContext)
        {
            db = applicationDbContext;
        }
        private string[] ex = { ".pdf", ".doc", ".docx", ".xls", ".xlsx"};
        // GET: Adela
                public ActionResult Index()
        {
            var ad = db.adela;
            return View(ad);
        }

        // create emp
        public ActionResult Create(int id)
        {
            ViewBag.type = id;
            return View();
        }
        [HttpPost]
        // create emp add
        public ActionResult CreateEmp(adela adela, IFormFile AddFile)
        {
            string addfile1 = "";
            if (ModelState.IsValid)
            {
                if (AddFile != null && AddFile.Length > 0)
                {
                    string extension = Path.GetExtension(AddFile.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = AddFile.Length;
                    adela ad = new adela();
                    ad.adName = adela.adName;
                    ad.adServ = adela.adServ;
                    ad.adFile = extension;
                    db.adela.Add(ad);
                    db.SaveChanges();
                    
                    if (a == true && sz <= 16777216)
                    {
                        if (!Directory.Exists(Path.GetDirectoryName("~/Upload/Emp")))
                        {

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/Emp"));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/Emp/"+ ad.id + extension));
                            addfile1 = adela.id + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                AddFile.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/Emp/"+ ad.id + extension));
                            addfile1 = adela.id + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                AddFile.CopyToAsync(fileSteam);
                            }
                        }
                    }

                }

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.error = "الرجاء مراجعة البيانات";
                return RedirectToAction("Create",new {id = adela.adServ });
            }
     
        }

        // edit emp
        public ActionResult EditEmp(int id)
        {

            var dd = db.adela.Where(p=>p.id == id).FirstOrDefault();

            ViewBag.type = dd.adServ;
            return View(dd);
        }
        [HttpPost]
        // edit emp 
        public ActionResult EditEmp(adela adela, IFormFile AddFile)
        {
            string addfile1 = "";
            if (ModelState.IsValid)
            {
                if (AddFile != null && AddFile.Length > 0)
                {
                    string extension = Path.GetExtension(AddFile.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = AddFile.Length;
                    adela ad = db.adela.Where(p=>p.id == adela.id).FirstOrDefault();
                    string oex = ad.adFile;
                    ad.adName = adela.adName;
                    ad.adServ = adela.adServ;
                    ad.adFile = extension;
                    db.Entry(ad).State = EntityState.Modified;
                    db.SaveChanges();
                    if (a == true && sz <= 16777216)
                    {
                        if (!Directory.Exists(Path.GetFileName("~/Upload/Emp")))
                        {
                            var FullPathd = Path.Combine(Path.GetFileName("~/Upload/Emp/" + adela.id + oex));
                            if (System.IO.File.Exists(FullPathd))
                            {
                                System.IO.File.Delete(FullPathd);
                            }

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/Emp"));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/Emp/" + adela.id + extension));
                            addfile1 = adela.id + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                AddFile.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {
                            var FullPathd = Path.Combine(Path.GetFileName("~/Upload/Emp/" + adela.id + oex));
                            if (System.IO.File.Exists(FullPathd))
                            {
                                System.IO.File.Delete(FullPathd);
                            }

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/Emp/" + adela.id + extension));
                            addfile1 = adela.id + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                AddFile.CopyToAsync(fileSteam);
                            }
                        }
                    }

                }
                else
                {
                    adela ad = db.adela.Where(p => p.id == adela.id).FirstOrDefault();
                    ad.adName = adela.adName;
                    ad.adServ = adela.adServ;
                    db.Entry(ad).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.error = "الرجاء مراجعة البيانات";
                return View();
            }
        }

        // delete emp 
        public ActionResult DeleteEmp(int id)
        {
            if (id > 0)
            {

                var dd = db.adela.Where(p=>p.id == id).FirstOrDefault();
                if (dd != null)
                {
                    var FullPathd = Path.Combine(Path.GetFileName("~/Upload/Emp/" + dd.id + dd.adFile));
                    if (System.IO.File.Exists(FullPathd))
                    {
                        System.IO.File.Delete(FullPathd);
                    }
                    db.adela.Remove(dd);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            }
            return RedirectToAction("Index");
        }


 

    }
}