﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;
using qortuba.Models;
using ClosedXML.Excel;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using qortuba.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using qortuba.Models.Class;

namespace qortuba.Controllers
{

    public class TSchoolsController : Controller
    {

        private readonly ApplicationDbContext db;
      Models.Class.MyFunctions fun = new Models.Class.MyFunctions();
        public TSchoolsController(ApplicationDbContext Context)
        {
            db = Context;
        }

        // GET: TSchools
      //  [Authorize(Roles = "schoolShow")]
        public ActionResult Index()
        {
           

            List<School> list = new List<School>();
            DataTable dtable = fun.fireStoreProcedure("school");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Bulding = sdr["Bulding"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }
            ViewData["asd"] = dtable.Rows[0][3].ToString();
            ViewBag.adela = db.adela.Where(p => p.adServ == 2).ToList();
            return View(list);

        }

        // GET: TSchools/Details/5
     //   [Authorize(Roles = "schoolDetails")]
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                // return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TSchools tSchools = db.TSchools.Find(id);
            if (tSchools == null)
            {
                return NotFound();
            }
            vballModel(tSchools);
            return View(tSchools);


        }

        // GET: TSchools/Create
       // [Authorize(Roles = "schoolInser")]
        public ActionResult Create()
        {
            vball();

            return View();
        }

        // POST: TSchools/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
      //  [Authorize(Roles = "schoolInser")]
        public ActionResult Create(TSchools tSchools)
        {
            //try {

            var chk = db.TSchools.Where(t => t.SchoolID == tSchools.SchoolID).FirstOrDefault();
            if (chk != null)
            {

                ViewBag.error = "هذه المدرسة مسجلة من قبل";
                return View(tSchools);
            }
            if (ModelState.IsValid)
            {
                db.TSchools.Add(tSchools);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            vballModel(tSchools);
            return View(tSchools);


            //}
            //catch (Exception e)
            //{
            //    vballModel(tSchools);
            //    ViewBag.error = e.Message;
            //    return View(tSchools);
            //}
        }

        // GET: TSchools/Edit/5
        //[Authorize(Roles = "schoolUpdate")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                //  return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TSchools tSchools = db.TSchools.Find(id);
            if (tSchools == null)
            {
                return NotFound();
            }
            vballModel(tSchools);
            return View(tSchools);
        }

        // POST: TSchools/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
      //  [Authorize(Roles = "schoolUpdate")]
        public ActionResult Edit(TSchools tSchools)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    vballModel(tSchools);
                    db.Update(tSchools);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                vballModel(tSchools);
                return View(tSchools);


            }
            catch (Exception e)
            {
                vballModel(tSchools);
                ViewBag.error = "يوجد تداخل بين الرقم الوزارى المدخل مع رقم اخر .. تأكد من الرقم الرقم الوزارى";
                return View(tSchools);
            }
        }

        // GET: TSchools/Delete/5
     //   [Authorize(Roles = "schoolDelete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TSchools tSchools = db.TSchools.Find(id);
            if (tSchools == null)
            {
                return NotFound();
            }
            vballModel(tSchools);
            return View(tSchools);
        }

        // POST: TSchools/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
   //     [Authorize(Roles = "schoolDelete")]
        public ActionResult DeleteConfirmed(int id)
        {

            TSchools tSchools = db.TSchools.Find(id);
            db.TSchools.Remove(tSchools);
            db.SaveChanges();
            return RedirectToAction("Index");


        }

        public void vball()
        {
            ViewBag.area = new SelectList(db.T_area, "IDNU", "name6");
            ViewBag.Center = new SelectList(db.T_Center, "ID", "Center1");
            ViewBag.Build = new SelectList(db.TBuilding, "BuildingID", "Bulding");
            ViewBag.LevelNo = new SelectList(db.TLevel, "LevelNo", "NameLevel");
            ViewBag.SchType = new SelectList(db.TSchoolsType, "SchoolTyp", "SchoolsType");
            ViewBag.SectID = new SelectList(db.TSector, "SectID", "Sector");
            ViewBag.Tuition = new SelectList(db.Tuition, "TuitionID", "Tuition1");
            ViewBag.finalize = new SelectList(db.T_yes_no, "on", "neme");
            ViewBag.Common = new SelectList(db.T_yes_no, "on", "neme");
            ViewBag.Backswept = new SelectList(db.T_yes_no, "on", "neme");
            ViewBag.created = new SelectList(db.T_yes_no, "on", "neme");
            ViewBag.Liver = new SelectList(db.HayNames, "HayName", "HayName");
            ViewBag.Out = new SelectList(db.T_yes_no, "on", "neme");
            ViewBag.CadE_door = new SelectList(db.TCadE_door, "CadE_ID", "CadE_door");
            ViewBag.AfterNoon = new SelectList(db.SchTypes, "SchType", "SchTypeName");
            ViewBag.desert = new SelectList(db.T_yes_no, "on", "neme");
            ViewBag.kind_SCH = new SelectList(db.T_yes_no, "on", "neme");
            ViewBag.Varway1 = new SelectList(db.T_Varway1, "Varway1ID", "Varway1");
            ViewBag.Varway2 = new SelectList(db.T_Varway1, "Varway1ID", "Varway1");
            ViewBag.Varway3 = new SelectList(db.T_Varway1, "Varway1ID", "Varway1");
            ViewBag.Rows = new SelectList(db.TLevel_Row, "Rows", "NameRow");

        }
        public void vballModel(TSchools tSchools)
        {

            ViewBag.area = new SelectList(db.T_area, "IDNU", "name6", tSchools.area);
            ViewBag.Center = new SelectList(db.T_Center, "ID", "Center1", tSchools.Center);
            ViewBag.Build = new SelectList(db.TBuilding, "BuildingID", "Bulding", tSchools.Build);
            ViewBag.LevelNo = new SelectList(db.TLevel, "LevelNo", "NameLevel", tSchools.LevelNo);
            ViewBag.SchType = new SelectList(db.TSchoolsType, "SchoolTyp", "SchoolsType", tSchools.SchType);
            ViewBag.SectID = new SelectList(db.TSector, "SectID", "Sector", tSchools.SectID);
            ViewBag.Tuition = new SelectList(db.Tuition, "TuitionID", "Tuition1", tSchools.Tuition);
            ViewBag.finalize = new SelectList(db.T_yes_no, "on", "neme", tSchools.finalize);
            ViewBag.Common = new SelectList(db.T_yes_no, "on", "neme", tSchools.Common);
            ViewBag.Backswept = new SelectList(db.T_yes_no, "on", "neme", tSchools.Backswept);
            ViewBag.created = new SelectList(db.T_yes_no, "on", "neme", tSchools.created);
            ViewBag.Liver = new SelectList(db.HayNames, "HayName", "HayName", tSchools.Liver);
            ViewBag.Out = new SelectList(db.T_yes_no, "on", "neme", tSchools.Out);
            ViewBag.CadE_door = new SelectList(db.TCadE_door, "CadE_ID", "CadE_door", tSchools.CadE_door);
            ViewBag.AfterNoon = new SelectList(db.SchTypes, "SchType", "SchTypeName", tSchools.AfterNoon);
            ViewBag.desert = new SelectList(db.T_yes_no, "on", "neme", tSchools.desert);
            ViewBag.kind_SCH = new SelectList(db.T_yes_no, "on", "neme", tSchools.kind_SCH);
            ViewBag.Varway1 = new SelectList(db.T_Varway1, "Varway1ID", "Varway1", tSchools.Varway1);
            ViewBag.Varway2 = new SelectList(db.T_Varway1, "Varway1ID", "Varway1", tSchools.Varway2);
            ViewBag.Varway3 = new SelectList(db.T_Varway1, "Varway1ID", "Varway1", tSchools.Varway3);
            ViewBag.Rows1 = new SelectList(db.TLevel_Row.Where(t => t.LevelNo == tSchools.LevelNo), "Rows", "NameRow");
            ViewBag.Rows2 = new SelectList(db.TLevel_Row.Where(t => t.LevelNo == tSchools.LevelNo), "Rows", "NameRow");
        }
        [HttpGet]
        public JsonResult ReturnLevel(int id)
        {
            if (id < 5) { var ru = db.TLevel_Row.Where(t => t.LevelNo == id).Select(s => new { Rows = s.Rows, NameRow = s.NameRow }); return Json(ru/*, JsonRequestBehavior.AllowGet*/); }

            if (id == 5) { var ru = db.TLevel_Row.Where(t => t.Rows <= 9).Select(s => new { Rows = s.Rows, NameRow = s.NameRow }); return Json(ru/*, JsonRequestBehavior.AllowGet*/); }
            if (id == 6) { var ru = db.TLevel_Row.Where(t => t.Rows >= 7 && t.Rows <= 20).Select(s => new { Rows = s.Rows, NameRow = s.NameRow }); return Json(ru/*, JsonRequestBehavior.AllowGet*/); }
            if (id == 7) { var ru = db.TLevel_Row.Where(t => t.Rows <= 20).Select(s => new { Rows = s.Rows, NameRow = s.NameRow }); return Json(ru/*, JsonRequestBehavior.AllowGet*/); }
            if (id == 9) { var ru = db.TLevel_Row.Where(t => t.LevelNo == 1).Select(s => new { Rows = s.Rows, NameRow = s.NameRow }); return Json(ru/*, JsonRequestBehavior.AllowGet*/); }
            if (id == 10) { var ru = db.TLevel_Row.Where(t => t.LevelNo == 2).Select(s => new { Rows = s.Rows, NameRow = s.NameRow }); return Json(ru/*, JsonRequestBehavior.AllowGet*/); }
            if (id == 11) { var ru = db.TLevel_Row.Where(t => t.LevelNo == 3).Select(s => new { Rows = s.Rows, NameRow = s.NameRow }); return Json(ru/*, JsonRequestBehavior.AllowGet*/); }



            return Json(null);
        }

        public JsonResult ReturnClass(int id)
        {
            //db.Configuration.ProxyCreationEnabled = false;

            var ru = db.TCLassRow.Where(t => t.SchoolID == id).ToList().OrderBy(p => p.Rows);

            return Json(ru/*, JsonRequestBehavior.AllowGet*/);
        }
        [HttpPost]
        public JsonResult addRow(TCLassRow tCLassRow)
        {
            fun.fireSQL("insert into TCLassRow values (" + tCLassRow.Rows + "," + tCLassRow.SchoolID + "," + tCLassRow.LevelNo + "," + tCLassRow.CLass + "," + tCLassRow.pupil + "," + tCLassRow.CLassR + ",'')");

            return Json(null);
        }
        [HttpPost]
        public JsonResult updateRow(TCLassRow tCLassRow)
        {

            fun.fireSQL("update TCLassRow set CLass = " + tCLassRow.CLass + " , pupil = " + tCLassRow.pupil + " , CLassR = " + tCLassRow.CLassR + " where id = " + tCLassRow.id + "");

            return Json(null);
        }
        [HttpPost]
        public JsonResult deleteRow(int id)
        {
            fun.fireSQL("delete from TCLassRow where id = " + id + "");
            return Json(null);
        }
      //  [Authorize(Roles = "managTWork")]
        public ActionResult IndexMboard()
        {


            List<School> list = new List<School>();
            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }
            return View(list);


        }
        public ActionResult Mboard(int id)
        {
            ViewBag.empName = new SelectList(db.TImployees.Where(p => p.SchID == id).ToList(), "FullName", "FullName");
            ViewBag.job = new SelectList(db.jobsMember, "jobName", "jobName");
            ViewBag.sid = id;
            var mm = db.Mboard.Where(p => p.sid == id).ToList();
            return View(mm);
        }
        public ActionResult CeareMboard(Mboard mbord)
        {
            Mboard mb = new Mboard();
            mb.sid = mbord.sid;
            mb.empName = mbord.empName;
            mb.job = mbord.job;
            db.Mboard.Add(mb);
            db.SaveChanges();

            return RedirectToAction("Mboard", new { id = mbord.sid });
        }
        public JsonResult DeleteMboard(int id)
        {
            var nn = db.Mboard.Where(p => p.id == id).FirstOrDefault();
            db.Mboard.Remove(nn);
            db.SaveChanges();
            return Json(null);
        }

        public ActionResult MboardManager()
        {
            string id1 = "10141";// sc.DecParametar(Request.Cookies["main"]["schid"]);

            int id = Convert.ToInt32(id1);
            ViewBag.empName = new SelectList(db.TImployees.Where(p => p.SchID == id).ToList(), "FullName", "FullName");
            ViewBag.job = new SelectList(db.jobsMember, "jobName", "jobName");
            ViewBag.sid = id;
            var mm = db.Mboard.Where(p => p.sid == id).ToList();
            ViewBag.adela = db.adela.Where(p => p.adServ == 2).ToList();
            return View(mm);
        }
        public ActionResult CeareMboardManager(Mboard mbord)
        {
            Mboard mb = new Mboard();
            mb.sid = mbord.sid;
            mb.empName = mbord.empName;
            mb.job = mbord.job;
            db.Mboard.Add(mb);
            db.SaveChanges();

            return RedirectToAction("MboardManager");
        }
        public ActionResult IndexAcceptance()
        {


            List<School> list = new List<School>();
            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }
            return View(list);


        }
        public ActionResult AcceptanceState(int id)
        {
            List<students> st = new List<students>();
            var dd = (from ts in db.students
                      join sc in db.TSchools on ts.oldSchid equals sc.SchoolID into gj
                      from sc in gj.DefaultIfEmpty()

                      where ts.schid == id
                      orderby (ts.id)
                      select new
                      {
                          id = ts.id,
                          sid = ts.sid,
                          stname = ts.sname1 + " " + ts.sname2 + " " + ts.sname3 + " " + ts.snname4,
                          fid = ts.fid,
                          ftname = ts.fname1 + " " + ts.fname2 + " " + ts.fname3 + " " + ts.fname4,
                          oldsch = sc.School,
                          stuts1 = ts.Sstate == 1 ? "قبول نهائى" : ts.Sstate == 2 ? "يلزم مراجعة اللجنة المركزية " : ts.Sstate == 3 ? "لا يوجد مقعد شاغر" : ts.Sstate == 1002 ? "يوجد ملاحظات على نظامية التسجيل" : " غير محدد",
                          srow = ts.rowName,
                          mob = ts.mob


                      });
            foreach (var lp in dd)
            {
                students sd = new students();
                sd.id = lp.id;
                sd.sname1 = lp.stname;
                sd.fname1 = lp.ftname;
                sd.sid = lp.sid;
                sd.fid = lp.fid;
                sd.sname = lp.oldsch; // old school
                sd.sname2 = lp.stuts1; //status
                sd.rowName = lp.srow;
                sd.mob = lp.mob;
                st.Add(sd);
            }


            var dd1 = (from tc in db.TCLassRow
                       join lv in db.TLevel_Row on tc.Rows equals lv.Rows
                       join sc in db.TSchools on tc.SchoolID equals sc.SchoolID
                       where tc.SchoolID == id
                       orderby (tc.Rows)
                       select new
                       {
                           RW = tc.Rows,
                           Rname = lv.NameRow,
                           emty = tc.CLassR > tc.pupil ? tc.CLassR - tc.pupil : 0,
                           pupil = tc.pupil,
                           Schid = tc.SchoolID,
                           SchName = sc.School

                       });

            List<students> ls = new List<students>();
            foreach (var et in dd1)
            {
                students sty = new students();
                sty.srow = et.RW;
                sty.rowName = et.Rname;
                sty.schid = et.emty;

                ls.Add(sty);
            }

            List<students> lsa = new List<students>();
            foreach (var et in dd1)
            {
                students sty = new students();
                var asw = db.students.Where(p => p.schid == id && p.srow == et.RW && p.Sstate == 1).Count();
                if (asw > 0)
                {
                    sty.schid = asw;
                }
                else
                {
                    sty.schid = 0;
                }
                sty.srow = et.RW;
                sty.rowName = et.Rname;


                lsa.Add(sty);
            }


            List<students> lsr = new List<students>();
            foreach (var et in dd1)
            {
                students sty = new students();
                var asw = db.students.Where(p => p.schid == id && p.srow == et.RW && p.Sstate == 1).Count();
                if (asw > 0)
                {
                    sty.schid = et.pupil - asw;
                }
                else
                {
                    sty.schid = et.pupil;
                }
                sty.srow = et.RW;
                sty.rowName = et.Rname;


                lsr.Add(sty);
            }

            ViewBag.sid = id;
            ViewBag.tarhel = lsr;

            ViewBag.free = ls;
            ViewBag.Accept = lsa;
            return View(st);

        }
        public FileResult AcceptReportX(int id)
        {

            var dd = (from ts in db.students
                      join sc in db.TSchools on ts.oldSchid equals sc.SchoolID into gj
                      from sc in gj.DefaultIfEmpty()

                      where ts.schid == id
                      orderby (ts.id)
                      select new
                      {
                          id = ts.id,
                          sid = ts.sid,
                          stname = ts.sname1 + " " + ts.sname2 + " " + ts.sname3 + " " + ts.snname4,
                          fid = ts.fid,
                          ftname = ts.fname1 + " " + ts.fname2 + " " + ts.fname3 + " " + ts.fname4,
                          oldsch = sc.School,
                          stuts1 = ts.Sstate == 1 ? "قبول نهائى" : ts.Sstate == 2 ? "يلزم مراجعة اللجنة المركزية " : ts.Sstate == 3 ? "لا يوجد مقعد شاغر" : ts.Sstate == 1002 ? "يوجد ملاحظات على نظامية التسجيل" : " غير محدد",
                          srow = ts.rowName,
                          mob = ts.mob


                      });

            DataTable dt = new DataTable();
            dt.Columns.Add("م");
            dt.Columns.Add("كود الطلب");
            dt.Columns.Add("اسم الطالب");
            dt.Columns.Add("سجل الطالب");
            dt.Columns.Add("اسم ول الامر");
            dt.Columns.Add("سجل ولى الامر");
            dt.Columns.Add("المدرسة السابقة");
            dt.Columns.Add("الصف");
            dt.Columns.Add("رقم الموبايل");
            dt.Columns.Add("حالة الطلب");
            int i = 1;
            foreach (var lp in dd)
            {
                DataRow dr = dt.NewRow();
                dr["م"] = i;
                dr["كود الطلب"] = lp.id;
                dr["اسم الطالب"] = lp.stname;
                dr["سجل الطالب"] = lp.ftname;
                dr["اسم ول الامر"] = lp.sid;
                dr["سجل ولى الامر"] = lp.fid;
                dr["المدرسة السابقة"] = lp.oldsch; // old school

                dr["الصف"] = lp.srow;
                dr["رقم الموبايل"] = lp.mob;
                dr["حالة الطلب"] = lp.stuts1; //status
                dt.Rows.Add(dr);
                i++;
            }
            i = 1;

            dt.TableName = "جميع المقبولين";
            XLWorkbook wb = new XLWorkbook();

            wb.Worksheets.Add(dt);
            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع المقبولين.xlsx");
        }

        public FileResult waitReportX(int id)
        {

            var dd = (from ts in db.studentsWait
                      join sc in db.TSchools on ts.oldSchid equals sc.SchoolID into gj
                      from sc in gj.DefaultIfEmpty()

                      where ts.schid == id
                      orderby (ts.id)
                      select new
                      {
                          id = ts.id,
                          sid = ts.sid,
                          stname = ts.sname1 + " " + ts.sname2 + " " + ts.sname3 + " " + ts.snname4,
                          fid = ts.fid,
                          ftname = ts.fname1 + " " + ts.fname2 + " " + ts.fname3 + " " + ts.fname4,
                          oldsch = sc.School,
                          stuts1 = ts.Sstate == 1 ? "قبول نهائى" : ts.Sstate == 2 ? "يلزم مراجعة اللجنة المركزية " : ts.Sstate == 3 ? "لا يوجد مقعد شاغر" : ts.Sstate == 1002 ? "يوجد ملاحظات على نظامية التسجيل" : " غير محدد",
                          srow = ts.rowName,
                          mob = ts.mob


                      });

            DataTable dt = new DataTable();
            dt.Columns.Add("م");
            dt.Columns.Add("كود الطلب");
            dt.Columns.Add("اسم الطالب");
            dt.Columns.Add("سجل الطالب");
            dt.Columns.Add("اسم ول الامر");
            dt.Columns.Add("سجل ولى الامر");
            dt.Columns.Add("المدرسة السابقة");
            dt.Columns.Add("الصف");
            dt.Columns.Add("رقم الموبايل");
            dt.Columns.Add("حالة الطلب");
            int i = 1;
            foreach (var lp in dd)
            {
                DataRow dr = dt.NewRow();
                dr["م"] = i;
                dr["كود الطلب"] = lp.id;
                dr["اسم الطالب"] = lp.stname;
                dr["سجل الطالب"] = lp.ftname;
                dr["اسم ول الامر"] = lp.sid;
                dr["سجل ولى الامر"] = lp.fid;
                dr["المدرسة السابقة"] = lp.oldsch; // old school

                dr["الصف"] = lp.srow;
                dr["رقم الموبايل"] = lp.mob;
                dr["حالة الطلب"] = lp.stuts1; //status
                dt.Rows.Add(dr);
                i++;
            }
            i = 1;

            dt.TableName = "جميع المنتظرين";
            XLWorkbook wb = new XLWorkbook();

            wb.Worksheets.Add(dt);
            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع المنتظرين.xlsx");
        }

        public FileResult RemoveReportX(int id)
        {

            var dd = (from ts in db.studentsR
                      join sc in db.TSchools on ts.oldSchid equals sc.SchoolID into gj
                      from sc in gj.DefaultIfEmpty()

                      where ts.schid == id
                      orderby (ts.id)
                      select new
                      {
                          id = ts.id,
                          sid = ts.sid,
                          stname = ts.sname1 + " " + ts.sname2 + " " + ts.sname3 + " " + ts.snname4,
                          fid = ts.fid,
                          ftname = ts.fname1 + " " + ts.fname2 + " " + ts.fname3 + " " + ts.fname4,
                          oldsch = sc.School,
                          stuts1 = ts.Sstate == 1 ? "قبول نهائى" : ts.Sstate == 2 ? "يلزم مراجعة اللجنة المركزية " : ts.Sstate == 3 ? "لا يوجد مقعد شاغر" : ts.Sstate == 1002 ? "يوجد ملاحظات على نظامية التسجيل" : " غير محدد",
                          srow = ts.rowName,
                          mob = ts.mob,
                          dateA = ts.dateA,
                          dateR = ts.dateR

                      });

            DataTable dt = new DataTable();
            dt.Columns.Add("م");
            dt.Columns.Add("كود الطلب");
            dt.Columns.Add("اسم الطالب");
            dt.Columns.Add("سجل الطالب");
            dt.Columns.Add("اسم ول الامر");
            dt.Columns.Add("سجل ولى الامر");
            dt.Columns.Add("المدرسة السابقة");
            dt.Columns.Add("الصف");
            dt.Columns.Add("رقم الموبايل");
            dt.Columns.Add("حالة الطلب");
            dt.Columns.Add("تاريخ الاضافة");
            dt.Columns.Add("تاريخ الحذف");
            int i = 1;
            foreach (var lp in dd)
            {
                DataRow dr = dt.NewRow();
                dr["م"] = i;
                dr["كود الطلب"] = lp.id;
                dr["اسم الطالب"] = lp.stname;
                dr["سجل الطالب"] = lp.ftname;
                dr["اسم ول الامر"] = lp.sid;
                dr["سجل ولى الامر"] = lp.fid;
                dr["المدرسة السابقة"] = lp.oldsch; // old school

                dr["الصف"] = lp.srow;
                dr["رقم الموبايل"] = lp.mob;
                dr["حالة الطلب"] = lp.stuts1; //status
                dr["تاريخ الاضافة"] = lp.dateA;
                dr["تاريخ الحذف"] = lp.dateR;
                dt.Rows.Add(dr);
                i++;
            }
            i = 1;

            dt.TableName = "جميع الطلاب المرفوضين";
            XLWorkbook wb = new XLWorkbook();

            wb.Worksheets.Add(dt);
            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع الطلاب المرفوضين.xlsx");
        }

        public ActionResult AcceptanceStateManager()
        {
            string id1 = "10141";// scm.DecParametar(Request.Cookies["main"]["schid"]);
            int id = Convert.ToInt32(id1);
            List<students> st = new List<students>();
            var dd = (from ts in db.students
                      join sc in db.TSchools on ts.oldSchid equals sc.SchoolID into gj
                      from sc in gj.DefaultIfEmpty()

                      where ts.schid == id
                      orderby (ts.id)
                      select new
                      {
                          id = ts.id,
                          sid = ts.sid,
                          stname = ts.sname1 + " " + ts.sname2 + " " + ts.sname3 + " " + ts.snname4,
                          fid = ts.fid,
                          ftname = ts.fname1 + " " + ts.fname2 + " " + ts.fname3 + " " + ts.fname4,
                          oldsch = sc.School,
                          stuts1 = ts.Sstate == 1 ? "مقبول" : ts.Sstate == 2 ? "مرفوض" : " غير محدد",
                          srow = ts.rowName,
                          mob = ts.mob


                      });
            foreach (var lp in dd)
            {
                students sd = new students();
                sd.id = lp.id;
                sd.sname1 = lp.stname;
                sd.fname1 = lp.ftname;
                sd.sid = lp.sid;
                sd.fid = lp.fid;
                sd.sname = lp.oldsch; // old school
                sd.sname2 = lp.stuts1; //status
                sd.rowName = lp.srow;
                sd.mob = lp.mob;
                st.Add(sd);
            }

            var dd1 = (from tc in db.TCLassRow
                       join lv in db.TLevel_Row on tc.Rows equals lv.Rows
                       join sc in db.TSchools on tc.SchoolID equals sc.SchoolID
                       where tc.SchoolID == id
                       orderby (tc.Rows)
                       select new
                       {
                           RW = tc.Rows,
                           Rname = lv.NameRow,
                           emty = tc.CLassR > tc.pupil ? tc.CLassR - tc.pupil : 0,
                           pupil = tc.pupil,
                           Schid = tc.SchoolID,
                           SchName = sc.School

                       });

            List<students> ls = new List<students>();
            foreach (var et in dd1)
            {
                students sty = new students();
                sty.srow = et.RW;
                sty.rowName = et.Rname;
                sty.schid = et.emty;

                ls.Add(sty);
            }

            List<students> lsa = new List<students>();
            foreach (var et in dd1)
            {
                students sty = new students();
                var asw = db.students.Where(p => p.schid == id && p.srow == et.RW && p.Sstate == 1).Count();
                if (asw > 0)
                {
                    sty.schid = asw;
                }
                else
                {
                    sty.schid = 0;
                }
                sty.srow = et.RW;
                sty.rowName = et.Rname;


                lsa.Add(sty);
            }


            List<students> lsr = new List<students>();
            foreach (var et in dd1)
            {
                students sty = new students();
                var asw = db.students.Where(p => p.schid == id && p.srow == et.RW && p.Sstate == 1).Count();
                if (asw > 0)
                {
                    sty.schid = et.pupil - asw;
                }
                else
                {
                    sty.schid = et.pupil;
                }
                sty.srow = et.RW;
                sty.rowName = et.Rname;


                lsr.Add(sty);
            }

            ViewBag.sid = id;
            ViewBag.tarhel = lsr;

            ViewBag.free = ls;
            ViewBag.Accept = lsa;

            return View(st);

        }
        // view acceptance
        public ActionResult AcceptanceApprove(int id)
        {
            List<students> st = new List<students>();
            var dd = db.students.Where(p => p.id == id).FirstOrDefault();
            if (dd != null)
            {
                ViewBag.Sstate = new SelectList(db.acceptstates, "id", "acceptState", dd.Sstate);
                ViewBag.oldSchid = new SelectList(db.TSchools, "SchoolID", "School", dd.oldSchid);
                ViewBag.nat = new SelectList(db.TNationality, "NatID", "Nationality", dd.nat);
            }
            else
            {
                ViewBag.Sstate = new SelectList(db.acceptstates, "id", "acceptState");
                ViewBag.oldSchid = new SelectList(db.TSchools, "SchoolID", "School");
                ViewBag.nat = new SelectList(db.TNationality, "NatID", "Nationality");

            }
            var iid = "";// sc.DecParametar(User.Identity.Name);
            if (iid != null)
            {
                var vl = db.TImployees.Where(p => p.Identity == iid).FirstOrDefault();
                var vll = dd.Sstate;
                if (vll > 0)
                {
                    ViewBag.change = 1;
                }
                else
                {
                    ViewBag.change = 2;
                }
                ViewBag.Rolee = vl.EmpRule;
            }
            ViewBag.state = 1;
            return View(dd);

        }


        public ActionResult AcceptanceApproveWait(int id)
        {
            List<students> st = new List<students>();
            var dd = db.studentsWait.Where(p => p.id == id).FirstOrDefault();
            if (dd != null)
            {
                ViewBag.Sstate = new SelectList(db.acceptstates, "id", "acceptState", dd.Sstate);
                ViewBag.oldSchid = new SelectList(db.TSchools, "SchoolID", "School", dd.oldSchid);
                ViewBag.nat = new SelectList(db.TNationality, "NatID", "Nationality", dd.nat);
            }
            else
            {
                ViewBag.Sstate = new SelectList(db.acceptstates, "id", "acceptState");
                ViewBag.oldSchid = new SelectList(db.TSchools, "SchoolID", "School");
                ViewBag.nat = new SelectList(db.TNationality, "NatID", "Nationality");
            }
            var iid = "";// sc.DecParametar(User.Identity.Name);
            if (iid != null)
            {
                var vl = db.TImployees.Where(p => p.Identity == iid).FirstOrDefault();
                var vll = dd.Sstate;
                if (vll > 0)
                {
                    ViewBag.change = 1;
                }
                else
                {
                    ViewBag.change = 2;
                }
                ViewBag.Rolee = vl.EmpRule;
            }
            ViewBag.state = 1;
            return View(dd);

        }

        public ActionResult AcceptanceWaitSend(int id)
        {
            List<students> st = new List<students>();
            var dd = db.studentsWait.Where(p => p.id == id).FirstOrDefault();
            var sc = db.TSchools.Where(p => p.SchoolID == dd.schid).FirstOrDefault();
            string messages = " عزيزي ولي الأمر " +
           " يلزمك مراجعة إدارة المدرسة خلال ٧٢ ساعة مصطحباً الإثبابات اللازمة أو ستفقد حقك في القبول " +
           "  موقع المدرسة   " + sc.location;
            var res = SendSms(dd.mob, messages);

            if (res == true)
            {
                ViewBag.state = 3;
                var da = db.studentsWait.Where(p => p.id == id).FirstOrDefault();
                if (da != null)
                {
                    da.messageWati = 3;
                    db.Update(da);
                    db.SaveChanges();
                }
            }
            ViewBag.Sstate = new SelectList(db.acceptstates, "id", "acceptState", dd.Sstate);
            ViewBag.oldSchid = new SelectList(db.TSchools, "SchoolID", "School", dd.oldSchid);
            ViewBag.nat = new SelectList(db.TNationality, "NatID", "Nationality", dd.nat);

            ViewBag.state = 1;
            return RedirectToAction("AcceptanceApproveWait", "TSchools", new { id = id });

        }

        public ActionResult AcceptanceTrue(int id)
        {
            List<students> st = new List<students>();
            MyFunctions fun = new MyFunctions();
            DataTable dt = fun.fireDataTable("select * from students,studentsWait where students.Sstate = 1 or studentsWait.Sstate = 1 or (students.schid = " + id + " and studentsWait.schid = " + id + ")");
            foreach (DataRow dr in dt.Rows)
            {
                int id1 = Convert.ToInt32(dr["id"].ToString());

                string stnam = dr["sname1"].ToString() + " " + dr["sname2"].ToString() + " " + dr["sname3"].ToString() + " " + dr["snname4"].ToString();
                string ftnam = dr["fname1"].ToString() + " " + dr["fname2"].ToString() + " " + dr["fname3"].ToString() + " " + dr["fname4"].ToString();

                students sd = new students();
                sd.id = id1;
                sd.sname1 = stnam;
                sd.fname1 = ftnam;
                sd.sid = dr["sid"].ToString();
                sd.fid = dr["fid"].ToString();
                //sd.sname = dr["sname"].ToString(); // old school
                //sd.sname2 = dr["id"].ToString(); //status
                sd.rowName = dr["rowName"].ToString();
                sd.mob = dr["mob"].ToString();
                st.Add(sd);
            }

            return View(st);

        }
        public ActionResult AcceptanceTrueM()
        {
            string id5 = "";// sc.DecParametar(Request.Cookies["main"]["schid"]);

            int id = Convert.ToInt32(id5);
            List<students> st = new List<students>();
            MyFunctions fun = new MyFunctions();
            DataTable dt = fun.fireDataTable("select * from students,studentsWait where students.Sstate = 1 or studentsWait.Sstate = 1 or (students.schid = " + id + " and studentsWait.schid = " + id + ")");
            foreach (DataRow dr in dt.Rows)
            {
                int id1 = Convert.ToInt32(dr["id"].ToString());

                string stnam = dr["sname1"].ToString() + " " + dr["sname2"].ToString() + " " + dr["sname3"].ToString() + " " + dr["snname4"].ToString();
                string ftnam = dr["fname1"].ToString() + " " + dr["fname2"].ToString() + " " + dr["fname3"].ToString() + " " + dr["fname4"].ToString();

                students sd = new students();
                sd.id = id1;
                sd.sname1 = stnam;
                sd.fname1 = ftnam;
                sd.sid = dr["sid"].ToString();
                sd.fid = dr["fid"].ToString();
                //sd.sname = dr["sname"].ToString(); // old school
                //sd.sname2 = dr["id"].ToString(); //status
                sd.rowName = dr["rowName"].ToString();
                sd.mob = dr["mob"].ToString();
                st.Add(sd);
            }

            return View(st);

        }

        public ActionResult AcceptanceWating(int id)
        {
            List<studentsWait> st = new List<studentsWait>();
            var dd = (from ts in db.studentsWait
                      join sc in db.TSchools on ts.oldSchid equals sc.SchoolID into gj
                      from sc in gj.DefaultIfEmpty()

                      where ts.schid == id
                      orderby (ts.id)
                      select new
                      {
                          id = ts.id,
                          sid = ts.sid,
                          stname = ts.sname1 + " " + ts.sname2 + " " + ts.sname3 + " " + ts.snname4,
                          fid = ts.fid,
                          ftname = ts.fname1 + " " + ts.fname2 + " " + ts.fname3 + " " + ts.fname4,
                          oldsch = sc.School,
                          stuts1 = ts.Sstate == 1 ? "قبول نهائى" : ts.Sstate == 2 ? "يلزم مراجعة اللجنة المركزية " : ts.Sstate == 3 ? "لا يوجد مقعد شاغر" : ts.Sstate == 1002 ? "يوجد ملاحظات على نظامية التسجيل" : " غير محدد",
                          srow = ts.rowName,
                          mob = ts.mob


                      });
            foreach (var lp in dd)
            {
                studentsWait sd = new studentsWait();
                sd.id = lp.id;
                sd.sname1 = lp.stname;
                sd.fname1 = lp.ftname;
                sd.sid = lp.sid;
                sd.fid = lp.fid;
                sd.sname = lp.oldsch; // old school
                sd.sname2 = lp.stuts1; //status
                sd.rowName = lp.srow;
                sd.mob = lp.mob;
                st.Add(sd);
            }
            ViewBag.sid = id;
            return View(st);

        }

        public ActionResult AcceptanceWatingM()
        {
            string id5 = "";// sc1.DecParametar(Request.Cookies["main"]["schid"]);

            int id = Convert.ToInt32(id5);
            List<studentsWait> st = new List<studentsWait>();
            var dd = (from ts in db.studentsWait
                      join sc in db.TSchools on ts.oldSchid equals sc.SchoolID into gj
                      from sc in gj.DefaultIfEmpty()

                      where ts.schid == id
                      orderby (ts.id)
                      select new
                      {
                          id = ts.id,
                          sid = ts.sid,
                          stname = ts.sname1 + " " + ts.sname2 + " " + ts.sname3 + " " + ts.snname4,
                          fid = ts.fid,
                          ftname = ts.fname1 + " " + ts.fname2 + " " + ts.fname3 + " " + ts.fname4,
                          oldsch = sc.School,
                          stuts1 = ts.Sstate == 1 ? "قبول نهائى" : ts.Sstate == 2 ? "يلزم مراجعة اللجنة المركزية " : ts.Sstate == 3 ? "لا يوجد مقعد شاغر" : ts.Sstate == 1002 ? "يوجد ملاحظات على نظامية التسجيل" : " غير محدد",
                          srow = ts.rowName,
                          mob = ts.mob


                      });
            foreach (var lp in dd)
            {
                studentsWait sd = new studentsWait();
                sd.id = lp.id;
                sd.sname1 = lp.stname;
                sd.fname1 = lp.ftname;
                sd.sid = lp.sid;
                sd.fid = lp.fid;
                sd.sname = lp.oldsch; // old school
                sd.sname2 = lp.stuts1; //status
                sd.rowName = lp.srow;
                sd.mob = lp.mob;
                st.Add(sd);
            }

            return View(st);

        }

        // send sms
        private bool SendSms(string Mob, string messages)
        {

            string username = "966532295510";
            string password = "Mhsg1966";
            string sendar = "Enjazq";

            string lnk = "https:" + "//www.hisms.ws/api.php?send_sms&username=" + username + "&password=" + password + "&numbers=" + Mob + "&sender=" + sendar + "&message=" + messages + "";

            HttpClient clint = new HttpClient();
            var response = clint.GetStringAsync(lnk);
            string rs = response.Result;
            string ss = rs.Substring(0, 1);
            if (ss == "3")
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
