﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using log4net.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using qortuba.Data;
using qortuba.Models;
using qortuba.Services;
using System.Web;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using qortuba.Models.Class;

namespace qortuba.Controllers
{
    public class authController : Controller
    {
       static MyFunctions fun = new MyFunctions();
        private const string V = "name";
        private static  ApplicationDbContext db;

        //MyFunctions fun = new MyFunctions();
        // GET: auth
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly Microsoft.Extensions.Logging.ILogger _logger;

        public authController(
            ApplicationDbContext DB,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            db = DB;
        }



        [AllowAnonymous]
        public ActionResult Index()
        {

            if (User.Identity.Name == "")
            {

                if (User.Identity.Name == null)
                {
                    return View();

                  //  return StatusCode(500);
                }
              //  string iid = sc.DecParametar(User.Identity.Name);
               // var tImployees = db.TImployees.Where(p => p.Identity == iid).FirstOrDefault();
             //   if (tImployees == null)
             //   {

            //        return NotFound();
           //     }

               // string nm = "sc.DecParametar(Request.Cookies[\"main\"][\"name\"])";
             //   vballModel(tImployees);
            //    tImployees.SSMA_TimeStamp = new byte[0];
                //ViewBag.userId = sc.DecParametar(User.Identity.Name);
           //     ViewBag.name = nm;

                return View();


            }
            else
            {
                return View();
            }

        }

        [AllowAnonymous]
        public ActionResult login()
        {
            //   Session.Clear();
            //   Session.Abandon();
            return View();
        }
        // login page test
        [AllowAnonymous]
        public ActionResult main()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> login(userAccount ua)
        {
            //var user = db.TImployees.SingleOrDefault(u => u.Identity == "ff");
            //if (user != null)
            //{
            //    if (user.EmpRule.ToString() != null)
            //    {
            //        string id = sc.EncParametar(user.Identity);
            //        // FormsAuthentication.SetAuthCookie(id, false);
            //                        var userClaims = new List<Claim>
            //        {
            //            new Claim(ClaimTypes.Name, user.Identity)
            //        };
            //        var principal = new ClaimsPrincipal(new ClaimsIdentity(userClaims, "local"));
            //        await HttpContext.Authentication.SignInAsync(id, principal);
            //        //   HttpCookie coock = new HttpCookie("main");
            //        string fname = sc.EncParametar(user.FullName);
            //        string wid = sc.EncParametar(user.WID.ToString());
            //        string shid = sc.EncParametar(user.SchID.ToString());
            //        //  coock.Values.Add("name", fname);
            //        //  coock.Values.Add("WId", wid);
            //        // coock.Values.Add("schid", shid);
            //        //HttpContext.Response.SetCookie(coock);



            var result = await _signInManager.PasswordSignInAsync(ua.id, ua.password,false, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.error = "خطأ فى الرقم الوطنى او كلمة المرور .";

            }

            //    }
            //    else
            //    {

            //        ViewBag.error = "الرجاء مراجعة مدير النظام للحصول على الصلاحيات";

            //    }

            //}
            //else
            //{
            //    //   Session.Clear();
            //    //    Session.Abandon();

            //}

            return View("Login");

        }

        [Authorize]
        public static async Task<bool> IsInRoleAsync(string role, string applicationUserId, ApplicationDbContext dbContext)
        {
            db = dbContext;
            var employee = await db.TImployees.SingleOrDefaultAsync(m => m.ApplicationUserId == applicationUserId);

            if (employee != null)
            {
                DataTable dt = fun.fireDataTable("select " + role + " from User_Roles where role_id =" + employee.EmpRule);
                if (dt.Rows.Count > 0)
                {
                    return  Convert.ToBoolean(dt.Rows[0][0]);
                }
                else
                {
                return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void vballModel(TImployees tImployees)
        {
            var day1 = new List<SelectListItem>();
            var month1 = new List<SelectListItem>();
            var yearl = new List<SelectListItem>();
            for (int i = 1; i <= 31; i++)
            { day1.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
            for (int i = 1; i <= 12; i++)
            { month1.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
            for (int i = 1350; i <= 1500; i++)
            { yearl.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
            ViewBag.QID = new SelectList(db.QType, "QTID", "QType1", tImployees.QID);
            ViewBag.NatID = new SelectList(db.TNationality, "NatID", "Nationality", tImployees.NatID);
            ViewBag.quien = new SelectList(db.TQualified, "QID", "Qualified", tImployees.quien);
            ViewBag.SchID = new SelectList(db.TSchools, "SchoolID", "School", tImployees.SchID);
            ViewBag.SchIDPrev = new SelectList(db.TSchools, "SchoolID", "School", tImployees.SchIDPrev);
            ViewBag.SpecialID = new SelectList(db.TSpecialSub, "SpeID", "Special", tImployees.SpecialID);
            ViewBag.SpecialIDOrig = new SelectList(db.TSpecialSub, "SpeID", "Special", tImployees.SpecialIDOrig);
            ViewBag.SubID = new SelectList(db.TSubject, "SubID", "Subject", tImployees.SubID);
            ViewBag.WID = new SelectList(db.TWork, "WID", "Work", tImployees.WID);
            ViewBag.WIDJ = new SelectList(db.TWorkJob, "WIDJ", "WorkJ", tImployees.WIDJ);

            ViewBag.EmpStatus = new SelectList(db.TempStatus, "EmpStatus", "EndType", tImployees.EmpStatus);
            ViewBag.Reduced = new SelectList(db.ReducedTypes, "ReduedCode", "ReducedName", tImployees.Reduced);
            ViewBag.Married = new SelectList(db.T_Married, "NU", "nem", tImployees.Married);
            ViewBag.BirthDated = new SelectList(day1, "Value", "Text", tImployees.BirthDated);
            ViewBag.BirthDatem = new SelectList(month1, "Value", "Text", tImployees.BirthDatem);
            ViewBag.BirthDatey = new SelectList(yearl, "Value", "Text", tImployees.BirthDatey);

            ViewBag.WorkDated = new SelectList(day1, "Value", "Text", tImployees.WorkDated);
            ViewBag.WorkDatem = new SelectList(month1, "Value", "Text", tImployees.WorkDatem);
            ViewBag.WorkDatey = new SelectList(yearl, "Value", "Text", tImployees.WorkDatey);
        }

        public ActionResult ErrorPage()
        {
            return View();
        } 

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            //Session.Clear();
            //Session.Abandon();
            //FormsAut.hentication.SignOut();
            //HttpContext.Response.Cookies.Set(new HttpCookie("main") { Value = string.Empty });
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> UpdateUser()
        {
            var qr = (from emp in db.TImployees
                      join Tschool in db.TSchools on emp.SchID equals Tschool.SchoolID
                      where Tschool.Center == 14
                      select new
                      {
                          username = emp.Identity,
                          password = "Abc@123!"
                      }).ToList();
            List<userList> lst = new List<userList>();
            foreach (var itm in qr)
            {
                userList ulst = new userList();
                ulst.username = itm.username;
                ulst.password = itm.password;
                lst.Add(ulst);
                var adminRole = new IdentityRole("admin");
                string eml = itm.username + "@email.com";
                var user = new ApplicationUser { UserName = itm.username, Email = eml };
                var result = await _userManager.CreateAsync(user, "Abc@123!");
                if (result.Succeeded)
                {

                    Console.WriteLine("success");
                }

            }

            return View(lst);
        }


    }
}