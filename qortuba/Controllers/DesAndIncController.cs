﻿using qortuba.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using qortuba.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Rotativa.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace qortuba.Controllers
{
    //[Authorize]
    
    public class DesAndIncController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly IHostingEnvironment _HostEnvironment;
        Models.Class.MyFunctions fun = new Models.Class.MyFunctions();
        public DesAndIncController(ApplicationDbContext Context, IHostingEnvironment HostEnvironment)
        {
            db = Context;
            _HostEnvironment = HostEnvironment;
        }
      //  [Authorize(Roles = "Reduce")]
        public ActionResult Index()
        {
            List<School> list = new List<School>();
            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }
            return View(list);

        }

        public ActionResult ShowDesAndInc(int id)
        {
                
                List<DesAndInc> list = new List<DesAndInc>();
            DataRow dr = fun.fireDataRow("select School from TSchools where SchoolID = " + id + "");
            ViewBag.Sname = dr["School"].ToString();
            DataTable dtable = fun.fireStoreProcedure1("DesAndInc",id.ToString());
                foreach (DataRow sdr in dtable.Rows)
                {
                    DesAndInc imp = new DesAndInc();

                    imp.Id = sdr["ID"].ToString();
                if (imp.Id != "25" && imp.Id != "99") {
                    imp.Subject = sdr["SUB"].ToString();
                    imp.Req = sdr["req"].ToString();
                    imp.Exs = sdr["exs"].ToString();
                    imp.Dess = sdr["dess"].ToString();
                    imp.Inc = sdr["inc"].ToString();
                    imp.IncTeach = sdr["incTeac"].ToString();

                    list.Add(imp);
                }
                }
                return View(list);
        }


        public IActionResult ShowDesAndIncPDF(int id)

        {
            if (id > 0)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\DesAndInc.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);

                DataTable dt = fun.fireStoreProcedure1("DesAndInc", id.ToString());
                var pdf = new ViewAsPdf("DesInc", dt)
                {

                    CustomSwitches = customSwitches,
                    PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                    PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                    PageSize = Rotativa.AspNetCore.Options.Size.A4
                };
                return pdf;
            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");

        }

        public ActionResult ShowDesAndIncR()
        {
            ViewBag.SchID = new SelectList(db.TSchools.Where(t => t.Center == 14), "SchoolID", "School");
            return View();
        }
        public ActionResult DesInc(int id)
        {
            DataTable dt = fun.fireStoreProcedure1("DesAndInc", id.ToString());
            return View(dt);
        }
        public IActionResult ShowDesAndIncX(int id)

        {
            if (id > 0)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\DesAndInc.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);




                DataTable dt = fun.fireStoreProcedure1("DesAndInc", id.ToString());


                    var pdf = new ViewAsPdf("DesInc", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");



        }

        public ActionResult ShowInc(int id)
        {

            List<DesAndInc> list = new List<DesAndInc>();
            DataRow dr = fun.fireDataRow("select School from TSchools where SchoolID = " + id + "");
            ViewBag.Sname = dr["School"].ToString();
            DataTable dtable = fun.fireStoreProcedure1("IncTeach", id.ToString());
            foreach (DataRow sdr in dtable.Rows)
            {
                DesAndInc imp = new DesAndInc();
                imp.Subject = sdr["SUB"].ToString();  
                imp.Inc = sdr["inc"].ToString();
                imp.Req = sdr["req"].ToString();
                list.Add(imp);
            }
            return View(list);
        }
        public ActionResult Showdes(int id)
        {

            List<SchoolDes> list = new List<SchoolDes>();
            DataRow dr = fun.fireDataRow("select School from TSchools where SchoolID = " + id + "");
            ViewBag.Sid = id;
            ViewBag.Sname = dr["School"].ToString();
            DataTable dtable = fun.fireDataTable("select id,schID,schName,subID,subName,[des],req,stateReq,CONVERT(VARCHAR(10),dessDate,111) as dessDate from SchoolDess where schID = " + id+"");
            foreach (DataRow sdr in dtable.Rows)
            {
                SchoolDes imp = new SchoolDes();
                imp.subName = sdr["subName"].ToString();
                imp.dess = sdr["des"].ToString();
                imp.req = sdr["req"].ToString();
                imp.dessDate = sdr["dessDate"].ToString();
                list.Add(imp);
            }
            return View(list);
        }
        public ActionResult ExDess(int id) {

            DataTable dt = fun.fireStoreProcedure1("desTeach", id.ToString());
            foreach (DataRow dr in dt.Rows)
            {
                DataTable dd = fun.fireDataTable("select * from SchoolDess where schID = "+dr["Schid"].ToString()+ " and subID = " + dr["ID"].ToString() + "");
                if (dd.Rows.Count > 0) {
                    
                    fun.fireSQL("update SchoolDess set [des] = "+dr["dess"].ToString()+" ,req = "+dr["req"].ToString()+",dessDate = GETDATE() where schID = "+ dr["Schid"].ToString() + " and subID = "+ dr["ID"].ToString() + "");
                }
                else
                {
                    fun.fireSQL("insert into SchoolDess values(" + dr["Schid"].ToString() + ",'" + dr["schName"].ToString() + "'," + dr["ID"].ToString() + ",'" + dr["SUB"].ToString() + "'," + dr["dess"].ToString() + "," + dr["req"].ToString() + ",'0',GETDATE())");


                }
            }

            return RedirectToAction("Showdes", new { id = id });
       }
        public IActionResult ShowIncPDF(int id)

        {
            if (id > 0)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\DesAndIncTech.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);




                DataTable dt = fun.fireStoreProcedure1("IncTeach", id.ToString());


                var pdf = new ViewAsPdf("ShowIncRTech", dt)
                {

                    CustomSwitches = customSwitches,

                    PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                    PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                    PageSize = Rotativa.AspNetCore.Options.Size.A4
                };
                return pdf;


            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");



        }

        public ActionResult ShowIncR()
        {
            ViewBag.SchID = new SelectList(db.TSchools.Where(t => t.Center == 14), "SchoolID", "School");
            return View();
        }
        public ActionResult ShowIncRTech(int id)
        {
            DataTable dt = fun.fireStoreProcedure1("IncTeach", id.ToString());
            return View(dt);
        }

        public void ShowIncX(int id)

        {

            DataTable dt = fun.fireStoreProcedure1("IncTeach", id.ToString());


            //ReportDocument rd = new ReportDocument();
            //rd.Load(Path.Combine(Path.GetFileName("~/Reports"), "IncTeach.rpt"));
            ////  DataRow dr = fun.fireDataRow("select convert(char(10),cast(CURRENT_TIMESTAMP as datetime),131) as HijryDate");

            //DateTimeFormatInfo DTFormat = new CultureInfo("ar-sa", false).DateTimeFormat;
            //DTFormat.Calendar = new HijriCalendar();
            //DTFormat.ShortDatePattern = "dd/mm/yyyy";

            //string myDate = DateTime.Today.Date.ToString("yyyy/MM/dd", DTFormat);


            //rd.SetDataSource(dt);
            //rd.SetParameterValue("hjdate", myDate);
            //Response.Buffer = false;
            //Response.ClearContent();
            //Response.ClearHeaders();
            //rd.SummaryInfo.ReportTitle = "الزيادة بالمعلمين";
            //rd.ExportToHttpResponse(ExportFormatType.Excel, System.Web.HttpContext.Current.Response, false, "الزيادة بالمعلين");

        }




    }
}