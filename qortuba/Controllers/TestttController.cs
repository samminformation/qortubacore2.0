﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using qortuba.Data;
using qortuba.Models.Class;

namespace qortuba.Controllers
{
    [Produces("application/json")]
    [Route("api/Testtt/[Action]")]
    public class TestttController : Controller
    {

        private readonly ApplicationDbContext db;
        MyFunctions fun = new MyFunctions();
        public TestttController(ApplicationDbContext Context)   
        {
            db = Context;
        }
  
        public async Task<IActionResult> AllEmp()
        {
            var remp =await db.TImployeesR.ToListAsync();
            return Ok(remp);
        }

    }
}