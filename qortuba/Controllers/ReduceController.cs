﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using qortuba.Models;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using qortuba.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using qortuba.Models.Class;

namespace qortuba.Controllers
{
    //[Authorize]
    public class ReduceController : Controller
    {
        private readonly ApplicationDbContext db;
        MyFunctions fun = new MyFunctions();
        public ReduceController(ApplicationDbContext Context)
        {
            db = Context;
        }
        // GET: Reduce
   //     [Authorize(Roles = "Count")]
        public ActionResult Index()
        {

            var red = db.ReducedTypes;
            return View(red.ToList());

        }
        [HttpPost]
        public JsonResult addRow(ReducedTypes reducedTypes)
        {

            db.ReducedTypes.Add(reducedTypes);
            db.SaveChanges();

            return Json(null);
        }
        [HttpPost]
        public JsonResult updateRow(ReducedTypes reducedTypes)
        {
            //    db.Entry(reducedTypes).State =EntityState.Modified;
            db.Update(reducedTypes);
            db.SaveChanges();


            return Json(null);
        }
        [HttpPost]
        public JsonResult deleteRow(int id)
        {
            ReducedTypes reducedTypes = db.ReducedTypes.Find(id);
            db.ReducedTypes.Remove(reducedTypes);
            db.SaveChanges();

            return Json(null);
        }
        //    [Authorize(Roles = "globalCount")]
        public ActionResult GeneralReduce()
        {

            List<GReduce> list = new List<GReduce>();

            DataTable dtable = fun.fireDataTable("SELECT ts.[id], ts.[Rows],ts.[Subject],sub.[Subject] as 'subN',ts.Class_No,ts.LevelNo,tst.SchoolsType,tl.NameLevel,tlr.NameRow from TLevel_Subject ts" +
                " inner join TSubject sub on ts.[Subject] = sub.SubID" +
                " inner join TLevel_Row tlr on ts.[Rows] = tlr.[Rows]" +
                " inner join TSchoolsType tst on ts.PlanType = tst.SchoolTyp" +
                " inner join TLevel tl on ts.LevelNo = tl.LevelNo");
            foreach (DataRow sdr in dtable.Rows)
            {
                GReduce GR = new GReduce();

                GR.id = sdr["id"].ToString();
                GR.RSub = sdr["subN"].ToString();
                GR.NameLevel = sdr["NameLevel"].ToString();
                GR.NameRow = sdr["NameRow"].ToString();
                GR.SchoolsType = sdr["SchoolsType"].ToString();
                GR.Class_No = sdr["Class_No"].ToString();

                list.Add(GR);
            }


            ViewBag.adela = db.adela.Where(p => p.adServ == 5).ToList();

            return View(list);



        }


        public JsonResult GeneralReduceUpdate(int id, int Rc)
        {
            if (id.ToString() != null && Rc.ToString() != null)
            {
                fun.fireSQL("UPDATE TLevel_Subject set Class_No = " + Rc + " where id=" + id + "");
            }


            return Json(null);
        }


        //custom Reduce
        // [Authorize(Roles = "customCount")]
        public ActionResult CustomReduce()
        {

            List<School> list = new List<School>();

            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }


            return View(list);

        }
        public ActionResult CustomReduceSchool(int id)
        {
            List<GReduce> list = new List<GReduce>();

            DataTable dtable = fun.fireDataTable("select id ,Tsc.School,tst.SchoolsType,tl.NameLevel,tlr.NameRow,sub.[Subject],CR.W_Class from Custom_Reduce CR " +
                " inner join TSchools Tsc on CR.SchID = Tsc.SchoolID" +
                " inner join TSubject sub on CR.Sub_id = sub.SubID" +
                " inner join TLevel tl on CR.S_Level = tl.LevelNo" +
                "  inner join TSchoolsType tst on CR.planeType = tst.SchoolTyp" +
                " inner join TLevel_Row tlr on CR.S_Row = tlr.[Rows]");
            foreach (DataRow sdr in dtable.Rows)
            {
                GReduce GR = new GReduce();

                GR.id = sdr["id"].ToString();
                GR.SchoolsType = sdr["School"].ToString();
                GR.PlaneType = sdr["SchoolsType"].ToString();
                GR.NameLevel = sdr["NameLevel"].ToString();
                GR.NameRow = sdr["NameRow"].ToString();
                GR.RSub = sdr["Subject"].ToString();
                GR.Class_No = sdr["W_Class"].ToString();

                list.Add(GR);
            }
            ViewBag.SubLevel = new SelectList(db.TLevel, "LevelNo", "NameLevel");
            ViewBag.SubRow = new SelectList(db.TLevel_Row, "Rows", "NameRow");
            ViewBag.SubId = new SelectList(db.TSubject, "SubID", "Subject");
            ViewBag.Subtype = new SelectList(db.TSchoolsType, "SchoolTyp", "SchoolsType");

            return View(list);
        }

        // create Reduce
        public JsonResult CustomReduceCreate(GReduce gReduce)
        {
            if (gReduce.NameLevel != null && gReduce.NameRow != null && gReduce.Class_No != null)
            {
                fun.fireSQL("insert into Custom_Reduce values (" + gReduce.SchoolsType + "," + gReduce.NameLevel + "," + gReduce.NameRow + "," + gReduce.RSub + "," + gReduce.Class_No + " , " + gReduce.PlaneType + " )");
            }


            return Json(null);
        }
        public JsonResult CustomReduceUpdate(GReduce gReduce)
        {
            if (gReduce.id != null && gReduce.NameLevel != null && gReduce.NameRow != null && gReduce.Class_No != null)
            {
                fun.fireSQL("update Custom_Reduce set planeType = " + gReduce.PlaneType + " , S_Level=" + gReduce.NameLevel + " , S_Row = " + gReduce.NameRow + ",Sub_id = " + gReduce.RSub + ",W_Class = " + gReduce.Class_No + " where id = " + gReduce.id + " ");
            }


            return Json(null);
        }

        public JsonResult CustomReduceDel(int id)
        {
            if (id.ToString() != null)
            {
                fun.fireSQL("delete from Custom_Reduce where id = " + id + "");
            }


            return Json(null);
        }

        [HttpGet]
        public JsonResult CustomReduceget(int id)
        {
            List<GReduce> list = new List<GReduce>();
            DataTable dtable = fun.fireDataTable("select * from Custom_Reduce where id = " + id + "");
            foreach (DataRow sdr in dtable.Rows)
            {
                GReduce GR = new GReduce();

                GR.id = sdr["id"].ToString();
                GR.SchoolsType = sdr["SchID"].ToString();
                GR.PlaneType = sdr["planeType"].ToString();
                GR.NameLevel = sdr["S_Level"].ToString();
                GR.NameRow = sdr["S_Row"].ToString();
                GR.RSub = sdr["Sub_id"].ToString();
                GR.Class_No = sdr["W_Class"].ToString();

                list.Add(GR);
            }


            return Json(list/*, System.Web.Mvc.JsonRequestBehavior.AllowGet*/);
        }
    }
}