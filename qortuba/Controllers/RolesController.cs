﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using qortuba.Data;
using qortuba.Models;
using qortuba.Models.Class;
using qortuba.Services;

namespace qortuba.Controllers
{
    //[Authorize]
    public class RolesController : Controller
    {

        private readonly ApplicationDbContext db;
        MyFunctions fun = new MyFunctions();

        // GET: auth
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly Microsoft.Extensions.Logging.ILogger _logger;

        public RolesController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }
        // GET: Roles
        [Authorize(Roles = "UserRole")]
        public ActionResult Index()
        {

            return View(db.User_Roles.ToList());

        }
        public ActionResult Create()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Create(User_Roles user_Roles)
        {
            if (ModelState.IsValid)
            {
                db.User_Roles.Add(user_Roles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user_Roles);
        }


        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User_Roles user_Roles = db.User_Roles.Find(id);
            if (user_Roles == null)
            {
                return NotFound();
            }
            return View(user_Roles);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User_Roles user_Roles)
        {
            if (ModelState.IsValid)
            {
                db.Update(user_Roles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user_Roles);
        }
        [HttpPost]
        public JsonResult deleteTable(int id)
        {
            try
            {
                if (id.ToString() != null)
                {

                    MyFunctions fun = new MyFunctions();
                    fun.fireSQL("DELETE FROM User_Roles WHERE role_id = " + id + " ");
                }


            }
            catch (Exception e)
            {

                ViewBag.error = e.Message.ToString();

            }


            return Json(null/*JsonRequestBehavior.AllowGet*/);
        }


        public ActionResult recordCat(int? id)
        {
            if (id == null)
            {
               // return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            recordCat recordCat = new recordCat();
            DataTable dt = fun.fireDataTable("SELECT * FROM recordCat WHERE role_id = " + id + "");
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                ViewBag.id = Convert.ToInt32(dr["id"].ToString());
            }
            else
            {
                ViewBag.id = 0;
            }
            ViewBag.wid = new SelectList(db.TWork, "WID", "Work");
            ViewBag.role_id = new SelectList(db.User_Roles, "role_id", "Role_name", id);


            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult recordCat(recordCat recordCat)
        {
            if (ModelState.IsValid)
            {

                DataTable dt = fun.fireDataTable("SELECT * FROM recordCat WHERE role_id = " + recordCat.role_id + "");
                if (dt.Rows.Count > 0)
                {

                    try
                    {
                        DataRow dr = dt.Rows[0];
                        fun.fireSQL("UPDATE TImployees SET EmpRule = " + dr["role_id"].ToString() + " WHERE TImployees.WID = " + dr["wid"].ToString() + "");
                        db.Update(recordCat);
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        ViewBag.error = e.Message.ToString();
                    }
                    return RedirectToAction("Index");

                }
                else
                {
                    try
                    {
                        DataRow dr = dt.Rows[0];
                        fun.fireSQL("UPDATE TImployees SET EmpRule = " + dr["role_id"].ToString() + " WHERE TImployees.WID = " + dr["wid"].ToString() + "");
                        db.recordCat.Add(recordCat);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    catch (Exception e)
                    {
                        ViewBag.error = e.Message.ToString();
                    }



                }




            }
            return RedirectToAction("Index");
        }

        public ActionResult empRole(int? id)
        {
            if (id == null)
            {
              //  return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<emp> list = new List<emp>();

            DataTable dtable = fun.fireStoreProcedure("empRole");
            foreach (DataRow sdr in dtable.Rows)
            {
                emp imp = new emp();

                imp.id = sdr["Identity"].ToString();
                imp.fullName = sdr["fullName"].ToString();
                imp.school = sdr["School"].ToString();
                imp.subject = sdr["subject"].ToString();
                imp.user_role = sdr["user_role"].ToString();
                list.Add(imp);
            }

            var rl = db.User_Roles.Where(t => t.role_id == id).SingleOrDefault();
            ViewBag.role_id = rl.role_id.ToString();
            ViewBag.RoleName = rl.Role_name.ToString();
            return View(list);
        }





        public ActionResult empRole1(int? id, int role_id)
        {
            if (ModelState.IsValid)
            {


                if (id.ToString() != null && role_id.ToString() != null)
                {
                    try
                    {
                        fun.fireSQL("UPDATE TImployees SET EmpRule = " + role_id + " WHERE TImployees.[Identity] = '" + id + "'");
                    }
                    catch (Exception e)
                    {
                        ViewBag.error = e.Message.ToString();
                    }

                    return RedirectToAction("Index");

                }


            }
            return RedirectToAction("Index");
        }
    }
}