﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using qortuba.Data;
using qortuba.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace qortuba.Controllers
{
    public class AcceptanceController : Controller
    {
        private readonly ApplicationDbContext db;
       // securityParametar sc = new securityParametar();
        public AcceptanceController(ApplicationDbContext Context)
        {
            db = Context;
        }
        public ActionResult Index()
        {
            ViewBag.adela = db.adela.Where(p => p.adServ == 6).ToList();
            return View();
        }

        public ActionResult watingList(int sid, int srow, string sname, string rowName)
        {
            if (sid > 0 && srow > 0)
            {
                List<studentsWait> st = new List<studentsWait>();
                var dd = (from ts in db.studentsWait
                          join sc in db.TSchools on ts.oldSchid equals sc.SchoolID into gj
                          from sc in gj.DefaultIfEmpty()

                          where ts.schid == sid && ts.srow == srow && ts.Sstate != 1
                          orderby (ts.id)
                          select new
                          {
                              id = ts.id,
                              sid = ts.sid,
                              stname = ts.sname1 + " " + ts.sname2 + " " + ts.sname3 + " " + ts.snname4,
                              fid = ts.fid,
                              ftname = ts.fname1 + " " + ts.fname2 + " " + ts.fname3 + " " + ts.fname4,
                              oldsch = sc.School,
                              stuts1 = ts.Sstate == 1 ? "قبول نهائى" : ts.Sstate == 2 ? "يلزم مراجعة اللجنة المركزية " : ts.Sstate == 3 ? "لا يوجد مقعد شاغر" : ts.Sstate == 1002 ? "يوجد ملاحظات على نظامية التسجيل" : "إنتظار",
                              srow = ts.rowName,
                              mob = ts.mob


                          });
                foreach (var lp in dd)
                {
                    studentsWait sd = new studentsWait();
                    sd.id = lp.id;
                    sd.sname1 = lp.stname;
                    sd.fname1 = lp.ftname;
                    sd.sid = lp.sid;
                    sd.fid = lp.fid;
                    sd.sname = lp.oldsch; // old school
                    sd.sname2 = lp.stuts1; //status
                    sd.rowName = lp.srow;
                    sd.mob = lp.mob;
                    st.Add(sd);
                }

                ViewBag.sname = sname;
                ViewBag.srow = rowName;
                return View(st);

            }
            else
            {
                return View("Index");
            }

        }
        public ActionResult RecordStudent(students students)
        {


            return View("RecordStudent", students);
        }

        public ActionResult RecordStudentWait(studentsWait students)
        {


            return View("RecordStudentWait", students);
        }

        public ActionResult AddStudent(IFormFile homeCert, IFormFile familyCert, IFormFile oldCert, IFormFile care, IFormFile transfare, students students)
        {


            //if (!this.IsCaptchaValid(""))

            //{

            //    @ViewBag.error = "خطأ.. الرجاء مراجعة البيانات";




            //    return View("RecordStudent", students);
            //}
            //else
            //{

            int ss = db.students.Where(p => p.sid == students.sid).Count();
            string[] ex = { ".png", ".jpg", ".jpeg", ".pdf" };

            if (ss >= 1)
            {
                @ViewBag.error = "خطأ.. الطالب مسجل من قبل";
                return View("RecordStudent", students);

            }
            else
            {
                string homec = "";
                string familyC = "";
                string oldC = "";
                string carC = "";
                string tras = "";







                if (homeCert != null && homeCert.Length > 0)
                {

                    string extension = Path.GetExtension(homeCert.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = homeCert.Length;
                    if (a == true && sz <= 16777216)
                    {
                        if (!Directory.Exists(Path.GetFileName("~/Upload/homeCert/" + students.schid + "")))
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/homeCert/" + students.schid + "/" + students.sid + extension));
                            homec = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                homeCert.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/homeCert/" + students.schid + "/" + students.sid + extension));
                            homec = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                homeCert.CopyToAsync(fileSteam);
                            }


                        }
                    }
                }








                if (familyCert != null && familyCert.Length > 0)
                {

                    string extension = Path.GetExtension(familyCert.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = familyCert.Length;
                    if (a == true && sz <= 16777216)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/familyCert/" + students.schid + "")))
                        {

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/familyCert/" + students.schid + ""));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/familyCert/" + students.schid + "/" + students.sid + extension));
                            familyC = students.sid + extension;


                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                familyCert.CopyToAsync(fileSteam);
                            }

                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/familyCert/" + students.schid + "/" + students.sid + extension));
                            familyC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                familyCert.CopyToAsync(fileSteam);
                            }
                        }
                    }
                }

                if (oldCert != null && oldCert.Length > 0)
                {

                    string extension = Path.GetExtension(oldCert.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = oldCert.Length;
                    if (a == true && sz <= 16777216)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/oldCert/" + students.schid + "")))
                        {

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/oldCert/" + students.schid + ""));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/oldCert/" + students.schid + "/" + students.sid + extension));
                            oldC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                oldCert.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/oldCert/" + students.schid + "/" + students.sid + extension));
                            oldC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                oldCert.CopyToAsync(fileSteam);
                            }
                        }
                    }
                }

                if (care != null && care.Length > 0)
                {

                    string extension = Path.GetExtension(care.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = care.Length;
                    if (a == true && sz <= 16777216)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/care/" + students.schid + "")))
                        {

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/care/" + students.schid + ""));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/care/" + students.schid + "/" + students.sid + extension));
                            carC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                care.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            string FullPath = Path.Combine(Path.GetFileName("~/Upload/care/" + students.schid + "/" + students.sid + extension));
                            carC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                care.CopyToAsync(fileSteam);
                            }
                        }
                    }
                }

                if (transfare != null && transfare.Length > 0)
                {

                    string extension = Path.GetExtension(transfare.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = transfare.Length;
                    if (a == true && sz <= 16777216)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/transfare/" + students.schid + "")))
                        {

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/transfare/" + students.schid + ""));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/transfare/" + students.schid + "/" + students.sid + extension));
                            tras = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                transfare.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/care/" + students.schid + "/" + students.sid + extension));
                            tras = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                transfare.CopyToAsync(fileSteam);
                            }
                        }
                    }
                }

                students st = new students();
                st.familyCert = familyC;
                st.fid = students.fid;
                st.fname1 = students.fname1;
                st.fname2 = students.fname2;
                st.fname3 = students.fname3;
                st.fname4 = students.fname4;
                st.homeCert = homec;
                st.mob = students.mob;
                st.oldCert = oldC;
                st.oldSchid = students.oldSchid;
                st.schid = students.schid;
                st.sid = students.sid;
                st.sname1 = students.sname1;
                st.sname2 = students.sname2;
                st.sname3 = students.sname3;
                st.snname4 = students.snname4;
                st.transfare = tras;
                st.srow = students.srow;
                st.sname = students.sname;
                st.rowName = students.rowName;
                st.nat = students.nat;
                st.care = carC;
                st.dateA = DateTime.Now;
                st.acceptS = students.acceptS;
                db.students.Add(st);
                db.SaveChanges();
                var locs = db.TSchools.Where(p => p.SchoolID == st.schid).FirstOrDefault();
                string messages = " عزيزي ولي الأمر " +
         " يلزمك مراجعة إدارة المدرسة خلال ٧٢ ساعة مصطحباً الإثبابات اللازمة أو ستفقد حقك في القبول ";

                var res = SendSms(st.mob, messages);

                return View("SuccessSutudent", st);
            }
            //}


        }

        public async Task<ActionResult> AddStudentWait(IFormFile homeCert, IFormFile familyCert, IFormFile oldCert, IFormFile care, IFormFile transfare, studentsWait students)
        {
            //if (!this.IsCaptchaValid(""))
            //{

            //    @ViewBag.error = "خطأ.. الرجاء مراجعة البيانات";
            //    return View("RecordStudent", students);
            //}
            //else
            //{

            int ss = db.studentsWait.Where(p => p.sid == students.sid).Count();
            string[] ex = { ".png", ".jpg", ".jpeg", ".pdf" };

            if (ss >= 1)
            {
                @ViewBag.error = "خطأ.. الطالب مسجل من قبل";
                return View("RecordStudentWait", students);

            }
            else
            {
                string homec = "";
                string familyC = "";
                string oldC = "";
                string carC = "";
                string tras = "";


                if (homeCert != null && homeCert.Length > 0)
                {

                    string extension = Path.GetExtension(homeCert.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = homeCert.Length;
                    if (a == true && sz <= 16777216)
                    {
                        if (!Directory.Exists(Path.GetFileName("~/Upload/homeCert/" + students.schid + "")))
                        {

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/homeCert/" + students.schid + ""));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/homeCert/" + students.schid + "/" + students.sid + extension));
                            homec = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                homeCert.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/homeCert/" + students.schid + "/" + students.sid + extension));
                            homec = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                homeCert.CopyToAsync(fileSteam);
                            }
                        }
                    }
                }

                if (familyCert != null && familyCert.Length > 0)
                {

                    string extension = Path.GetExtension(familyCert.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = familyCert.Length;
                    if (a == true && sz <= 16777216)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/familyCert/" + students.schid + "")))
                        {

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/familyCert/" + students.schid + ""));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/familyCert/" + students.schid + "/" + students.sid + extension));
                            familyC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                await familyCert.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/familyCert/" + students.schid + "/" + students.sid + extension));
                            familyC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                familyCert.CopyToAsync(fileSteam);
                            }
                        }
                    }
                }

                if (oldCert != null && oldCert.Length > 0)
                {

                    string extension = Path.GetExtension(oldCert.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = oldCert.Length;
                    if (a == true && sz <= 16777216)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/oldCert/" + students.schid + "")))
                        {

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/oldCert/" + students.schid + ""));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/oldCert/" + students.schid + "/" + students.sid + extension));
                            oldC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                oldCert.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/oldCert/" + students.schid + "/" + students.sid + extension));
                            oldC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                oldCert.CopyToAsync(fileSteam);
                            }
                        }
                    }
                }

                if (care != null && care.Length > 0)
                {

                    string extension = Path.GetExtension(care.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = care.Length;
                    if (a == true && sz <= 16777216)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/care/" + students.schid + "")))
                        {
                            // var filePath = Path.Combine(Directory.GetCurrentDirectory(), "Upload", fileName + ".pdf");

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/care/" + students.schid + ""));
                            string FullPath = Path.Combine(Path.GetFileName("~/Upload/care/" + students.schid + "/" + students.sid + extension));
                            carC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                await care.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/care/" + students.schid + "/" + students.sid + extension));
                            carC = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                care.CopyToAsync(fileSteam);
                            }
                        }
                    }
                }

                if (transfare != null && transfare.Length > 0)
                {

                    string extension = Path.GetExtension(transfare.FileName);
                    bool a = Array.Exists(ex, element => element == extension);
                    long sz = transfare.Length;
                    if (a == true && sz <= 16777216)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/transfare/" + students.schid + "")))
                        {

                            Directory.CreateDirectory(Path.GetFileName("~/Upload/transfare/" + students.schid + ""));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/transfare/" + students.schid + "/" + students.sid + extension));
                            tras = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                transfare.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {

                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/care/" + students.schid + "/" + students.sid + extension));
                            tras = students.sid + extension;
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                transfare.CopyToAsync(fileSteam);
                            }
                        }
                    }
                }

                studentsWait st = new studentsWait();
                st.familyCert = familyC;
                st.fid = students.fid;
                st.fname1 = students.fname1;
                st.fname2 = students.fname2;
                st.fname3 = students.fname3;
                st.fname4 = students.fname4;
                st.homeCert = homec;
                st.mob = students.mob;
                st.oldCert = oldC;
                st.oldSchid = students.oldSchid;
                st.schid = students.schid;
                st.sid = students.sid;
                st.sname1 = students.sname1;
                st.sname2 = students.sname2;
                st.sname3 = students.sname3;
                st.snname4 = students.snname4;
                st.transfare = tras;
                st.srow = students.srow;
                st.sname = students.sname;
                st.rowName = students.rowName;
                st.nat = students.nat;
                st.care = carC;
                st.dateA = DateTime.Now;
                st.acceptS = students.acceptS;

                db.studentsWait.Add(st);
                db.SaveChanges();



                return View("SuccessSutudentWait", st);
            }
            //}


        }

        public ActionResult SuccessSutudent(students students)
        {


            return View();

        }

        public ActionResult SuccessSutudentWait(studentsWait students)
        {

            return View();

        }
        [HttpGet]
        public JsonResult locationMap()
        {



            var dt = db.TSchools.Where(p => p.Center == 14);
            List<locationMap> lst = new List<locationMap>();
            foreach (var dd in dt)
            {
                locationMap lc = new locationMap();
                if (dd.location != null)
                {
                    if (dd.location.Length > 10)
                    {
                        string loc = dd.location;
                        int l1 = loc.IndexOf("@");
                        int l2 = loc.IndexOf(",");
                        int g1 = loc.IndexOf(",", l2 + 1);
                        string lat = dd.location.Substring(l1 + 1, (l2 - 1) - l1);
                        string lng = dd.location.Substring(l2 + 1, (g1 - 1) - l2);
                        lc.SchID = dd.SchoolID;
                        lc.Schname = dd.School;
                        lc.levelNo = dd.LevelNo.ToString();
                        lc.lat = lat;
                        lc.lag = lng;
                        lst.Add(lc);

                    }
                }
            }

            return Json(lst/*,JsonRequestBehavior.AllowGet*/);
        }

        [HttpGet]
        public JsonResult locationMapH(string hay)
        {

            var dt = db.TSchools.Where(p => p.Liver == hay);
            List<locationMap> lst = new List<locationMap>();
            foreach (var dd in dt)
            {
                locationMap lc = new locationMap();
                if (dd.location != null)
                {
                    if (dd.location.Length > 10)
                    {
                        string loc = dd.location;
                        int l1 = loc.IndexOf("@");
                        int l2 = loc.IndexOf(",");
                        int g1 = loc.IndexOf(",", l2 + 1);
                        string lat = dd.location.Substring(l1 + 1, (l2 - 1) - l1);
                        string lng = dd.location.Substring(l2 + 1, (g1 - 1) - l2);
                        lc.SchID = dd.SchoolID;
                        lc.Schname = dd.School;
                        lc.lat = lat;
                        lc.lag = lng;
                        lc.levelNo = dd.LevelNo.ToString();
                        lst.Add(lc);

                    }
                }
            }

            return Json(lst/*, JsonRequestBehavior.AllowGet*/);
        }


        [HttpGet]
        public JsonResult locationMapS(int id)
        {


            var dt = db.TSchools.Where(p => p.SchoolID == id);
            List<locationMap> lst = new List<locationMap>();
            foreach (var dd in dt)
            {
                locationMap lc = new locationMap();
                if (dd.location != null)
                {
                    if (dd.location.Length > 10)
                    {
                        string loc = dd.location;
                        int l1 = loc.IndexOf("@");
                        int l2 = loc.IndexOf(",");
                        int g1 = loc.IndexOf(",", l2 + 1);
                        string lat = dd.location.Substring(l1 + 1, (l2 - 1) - l1);
                        string lng = dd.location.Substring(l2 + 1, (g1 - 1) - l2);
                        lc.SchID = dd.SchoolID;
                        lc.Schname = dd.School;
                        lc.levelNo = dd.LevelNo.ToString();
                        lc.lat = lat;
                        lc.lag = lng;
                        lst.Add(lc);

                    }
                }
            }

            return Json(lst/*, JsonRequestBehavior.AllowGet*/);
        }

        [HttpGet]
        public JsonResult locationMapL(int id)
        {


            var dt = db.TSchools.Where(p => p.LevelNo == id);
            List<locationMap> lst = new List<locationMap>();
            foreach (var dd in dt)
            {
                locationMap lc = new locationMap();
                if (dd.location != null)
                {
                    if (dd.location.Length > 10)
                    {
                        string loc = dd.location;
                        int l1 = loc.IndexOf("@");
                        int l2 = loc.IndexOf(",");
                        int g1 = loc.IndexOf(",", l2 + 1);
                        string lat = dd.location.Substring(l1 + 1, (l2 - 1) - l1);
                        string lng = dd.location.Substring(l2 + 1, (g1 - 1) - l2);
                        lc.SchID = dd.SchoolID;
                        lc.Schname = dd.School;
                        lc.levelNo = dd.LevelNo.ToString();
                        lc.lat = lat;
                        lc.lag = lng;
                        lst.Add(lc);

                    }
                }
            }

            return Json(lst/*, JsonRequestBehavior.AllowGet*/);
        }

        [HttpGet]
        public JsonResult popupData(locationMap locationM)
        {


            if (locationM.SchID > 0)
            {
                var dd = (from tc in db.TCLassRow
                          join lv in db.TLevel_Row on tc.Rows equals lv.Rows
                          join sc in db.TSchools on tc.SchoolID equals sc.SchoolID
                          where tc.SchoolID == locationM.SchID
                          orderby (tc.Rows)
                          select new
                          {
                              RW = tc.Rows,
                              Rname = lv.NameRow,
                              emty = tc.CLassR > tc.pupil ? tc.CLassR - tc.pupil : 0,
                              Schid = tc.SchoolID,
                              SchName = sc.School

                          });


                string st1 = "<div  class=\"panel panel-primary\" style=\"width: 390px;margin-top: 20px \"> " +
                        " <div class=\"panel-heading\"><spam>اسم المدرسة :</spam><spam style=\"color: yellow;font-size: 15px;\">" + locationM.Schname + "</spam></div> " +
                        " <div class=\"panel-body\"> " +
                        "     <table class=\"table\"> " +
                        "         <thead> " +
                        "             <tr> " +
                        "                 <th>الصف</th> " +
                        "                 <th>الشواغر</th> " +
                        "             </tr> " +
                        "         </thead> " +
                        "         <tbody> ";
                string tr = "";
                foreach (var et in dd)
                {
                    if (et.emty > 0)
                    {
                        tr += " <tr>  <td>" + et.Rname + "</td> <td>" + et.emty + "</td> <td><a class=\"btn btn-success clck\" placeholder=\"" + et.Schid + " \" alt=\"" + et.RW + "\" href=\"/Acceptance/RecordStudent?schid=" + et.Schid + "&srow=" + et.RW + "&sname=" + et.SchName + "&rowName=" + et.Rname + "\" >حجز مقعد</a> </td>  </tr> ";

                    }
                    else
                    {
                        tr += " <tr>  <td>" + et.Rname + "</td> <td>" + et.emty + "</td> <td><a class=\"btn btn-warning clck\" placeholder=\"" + et.Schid + " \" alt=\"" + et.RW + "\" href=\"/Acceptance/RecordStudentWait?schid=" + et.Schid + "&srow=" + et.RW + "&sname=" + et.SchName + "&rowName=" + et.Rname + "\" >إنتظار</a> </td><td><a class=\"btn btn-info clck\" placeholder=\"" + et.Schid + " \" alt=\"" + et.RW + "\" href=\"/Acceptance/watingList?sid=" + et.Schid + "&srow=" + et.RW + "&sname=" + et.SchName + "&rowName=" + et.Rname + "\" >قائمة الانتظار</a> </td>  </tr> ";

                    }
                }
                string ed = " </tbody> </table> </div > </div > ";

                string col = st1 + tr + ed;

                return Json(col/*,JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                return Json(null);
            }
        }

        // send sms
        private bool SendSms(string Mob, string messages)
        {

            string username = "966532295510";
            string password = "Mhsg1966";
            string sendar = "Enjazq";

            string lnk = "https:" + "//www.hisms.ws/api.php?send_sms&username=" + username + "&password=" + password + "&numbers=" + Mob + "&sender=" + sendar + "&message=" + messages + "";

            HttpClient clint = new HttpClient();
            var response = clint.GetStringAsync(lnk);
            string rs = response.Result;
            string ss = rs.Substring(0, 1);
            if (ss == "3")
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public ActionResult AcceptanceStd(students students)
        {
            var old = db.students.Where(p => p.id == students.id).FirstOrDefault();
            string fname = old.sname1 + " " + old.sname2 + " " + old.sname3 + " " + old.snname4;

            if (students.Sstate == 1)
            {
                old.Sstate = students.Sstate;

                db.SaveChanges();
                var cr = db.TCLassRow.Where(p => p.SchoolID == students.schid && p.Rows == students.srow).FirstOrDefault();
                if (cr != null)
                {
                    int? i = cr.pupil + 1;
                    cr.pupil = Convert.ToInt16(i);

                    db.SaveChanges();
                }

                var locs = db.TSchools.Where(p => p.SchoolID == old.schid).FirstOrDefault();

                string messages = " عزيزي ولي الأمر " +
         " تم قبول الطالب / " + fname + " فى مدرسة / " + old.sname + " فى الصف / " + old.rowName + " ";
                var res = SendSms(old.mob, messages);
                ViewBag.Sstate = new SelectList(db.acceptstates, "id", "acceptState", students.Sstate);
                ViewBag.oldSchid = new SelectList(db.TSchools, "SchoolID", "School", students.oldSchid);
                ViewBag.nat = new SelectList(db.TNationality, "NatID", "Nationality", students.nat);
                ViewBag.state = 2;

                return RedirectToAction("AcceptanceApprove", "TSchools", new { id = students.id });

            }
            else if (students.Sstate == 2)
            {
                string messages = " عزيزي ولي الأمر " +
         " عذراً ..لقد تم وضع الطالب / " + fname + " فى قائمة الانتظار لحين مراجعة اللجنة المركزية باالادارة العامة " +
         "  موقع الادارة   " + "https://maps.app.goo.gl/8DS5i2ugW7wWMHFt8";
                var res = SendSms(old.mob, messages);
                var od = db.studentsWait.Where(p => p.sid == students.sid).FirstOrDefault();
                if (od != null)
                {
                    od.Sstate = students.Sstate;


                    db.SaveChanges();
                }
                else
                {
                    studentsWait sw = new studentsWait();
                    sw.acceptS = students.acceptS;
                    sw.care = students.care;
                    sw.familyCert = students.familyCert;
                    sw.fid = students.fid;
                    sw.fname1 = students.fname1;
                    sw.fname2 = students.fname2;
                    sw.fname3 = students.fname3;
                    sw.fname4 = students.fname4;
                    sw.homeCert = students.homeCert;
                    sw.mob = students.mob;
                    sw.nat = students.nat;
                    sw.notess = students.notess;
                    sw.oldCert = students.oldCert;
                    sw.oldSchid = students.oldSchid;
                    sw.rowName = students.rowName;
                    sw.schid = students.schid;
                    sw.sid = students.sid;
                    sw.sname = students.sname;
                    sw.sname1 = students.sname1;
                    sw.sname2 = students.sname2;
                    sw.sname3 = students.sname3;
                    sw.snname4 = students.snname4;
                    sw.srow = students.srow;
                    sw.Sstate = students.Sstate;
                    db.studentsWait.Add(sw);
                    db.SaveChanges();
                    var dd = db.students.Where(p => p.id == students.id).FirstOrDefault();
                    db.students.Remove(dd);
                    db.SaveChanges();


                }
                ViewBag.state = 2;
                return RedirectToAction("AcceptanceState", "TSchools", new { id = students.schid });
            }
            else if (students.Sstate == 3)
            {
                string messages = " عزيزي ولي الأمر " +
         " عذراً ..لا يوجد مقعد شاغر للطالب / " + fname + " يمكنك التسجيل فى اقرب مدرسة اخرى " +
         "  موقع لجنة القبول بالمكتب   " + "https://maps.app.goo.gl/8DS5i2ugW7wWMHFt8";
                var res = SendSms(old.mob, messages);
                var od = db.studentsWait.Where(p => p.sid == students.sid).FirstOrDefault();
                if (od != null)
                {

                    db.studentsWait.Remove(od);
                    db.SaveChanges();
                }
                else
                {
                    studentsR stdr = new studentsR();
                    stdr.acceptS = students.acceptS;
                    stdr.care = students.care;
                    stdr.familyCert = students.familyCert;
                    stdr.fid = students.fid;
                    stdr.fname1 = students.fname1;
                    stdr.fname2 = students.fname2;
                    stdr.fname3 = students.fname3;
                    stdr.fname4 = students.fname4;
                    stdr.homeCert = students.homeCert;
                    stdr.mob = students.mob;
                    stdr.nat = students.nat;
                    stdr.notess = students.notess;
                    stdr.oldSchid = students.oldSchid;
                    stdr.rowName = students.rowName;
                    stdr.schid = students.schid;
                    stdr.sid = students.sid;
                    stdr.sname = students.sname;
                    stdr.sname1 = students.sname1;
                    stdr.sname2 = students.sname2;
                    stdr.sname3 = students.sname3;
                    stdr.snname4 = students.snname4;
                    stdr.srow = students.srow;
                    stdr.Sstate = students.Sstate;
                    stdr.transfare = students.transfare;
                    stdr.dateA = students.dateA;
                    stdr.dateR = DateTime.Now;
                    db.studentsR.Add(stdr);
                    db.SaveChanges();

                    var ss = db.students.Where(p => p.id == students.id).FirstOrDefault();

                    db.students.Remove(ss);
                    db.SaveChanges();

                }
                ViewBag.state = 3;
                return RedirectToAction("AcceptanceState", "TSchools", new { id = students.schid });
            }
            else if (students.Sstate == 1002)
            {
                string messages = " عزيزي ولي الأمر " +
         " عذراً تم الغاء طلبكم..يوجد ملاحظات على نظامية التسجيل  للطالب / " + fname + " يمكنك مراجعة إدارة المدرسة او لجنة القبول بالمكتب " +
         "  موقع لجنة القبول بالمكتب   " + "https://maps.app.goo.gl/8DS5i2ugW7wWMHFt8";
                var res = SendSms(old.mob, messages);
                var od = db.studentsWait.Where(p => p.sid == students.sid).FirstOrDefault();
                if (od != null)
                {
                    db.studentsWait.Remove(od);
                    db.SaveChanges();
                }
                else
                {
                    studentsR stdr = new studentsR();
                    stdr.acceptS = students.acceptS;
                    stdr.care = students.care;
                    stdr.familyCert = students.familyCert;
                    stdr.fid = students.fid;
                    stdr.fname1 = students.fname1;
                    stdr.fname2 = students.fname2;
                    stdr.fname3 = students.fname3;
                    stdr.fname4 = students.fname4;
                    stdr.homeCert = students.homeCert;
                    stdr.mob = students.mob;
                    stdr.nat = students.nat;
                    stdr.notess = students.notess;
                    stdr.oldSchid = students.oldSchid;
                    stdr.rowName = students.rowName;
                    stdr.schid = students.schid;
                    stdr.sid = students.sid;
                    stdr.sname = students.sname;
                    stdr.sname1 = students.sname1;
                    stdr.sname2 = students.sname2;
                    stdr.sname3 = students.sname3;
                    stdr.snname4 = students.snname4;
                    stdr.srow = students.srow;
                    stdr.Sstate = students.Sstate;
                    stdr.transfare = students.transfare;
                    stdr.dateA = students.dateA;
                    stdr.dateR = DateTime.Now;
                    db.studentsR.Add(stdr);
                    db.SaveChanges();
                    var ss = db.students.Where(p => p.id == students.id).FirstOrDefault();
                    db.students.Remove(ss);
                    db.SaveChanges();

                }
                ViewBag.state = 4;
                return RedirectToAction("AcceptanceState", "TSchools", new { id = students.schid });
            }

            return RedirectToAction("AcceptanceApprove", "TSchools", new { id = students.schid });

        }

        [HttpGet]
        public JsonResult getDistance(double lat, double lng)
        {


            double rdis;
            var dt = (from sc in db.TSchools
                      join lv in db.TLevel on sc.LevelNo equals lv.LevelNo

                      where sc.location != null
                      orderby (sc.SchoolID)
                      select new
                      {
                          scid = sc.SchoolID,
                          scName = sc.School,
                          scLevel = lv.NameLevel,
                          sclevcode = lv.LevelNo,
                          slocation = sc.location
                      });
            List<locationMap> lst = new List<locationMap>();
            foreach (var dd in dt)
            {
                locationMap lc = new locationMap();
                if (dd.slocation != null)
                {
                    if (dd.slocation.Length > 10)
                    {
                        string loc = dd.slocation;
                        int l1 = loc.IndexOf("@");
                        int l2 = loc.IndexOf(",");
                        int g1 = loc.IndexOf(",", l2 + 1);
                        string lat1 = dd.slocation.Substring(l1 + 1, (l2 - 1) - l1);
                        string lng1 = dd.slocation.Substring(l2 + 1, (g1 - 1) - l2);
                        lc.SchID = dd.scid;
                        lc.Schname = dd.scName;
                        lc.levelNo = dd.scLevel;
                        lc.lat = lat1;
                        lc.lag = lng1;
                        lc.latD = Convert.ToDouble(lat1);
                        lc.lagD = Convert.ToDouble(lng1);
                        lc.levcode = dd.sclevcode;
                        //var sCoord = new GeoCoordinate(lat, lng);
                        //var eCoord = new GeoCoordinate(lc.latD, lc.lagD);
                        //rdis = (sCoord.GetDistanceTo(eCoord)) / 1000;
                        // lc.dis = Math.Round(rdis,3);

                        lst.Add(lc);
                    }
                }
            }
            string t1 = "<table id=\"tds\" class=\"table\"> <thead style=\"background-color: #e8e8e8;\"><tr><th>م</th><th>كود المدرسة</th><th>اسم المدرسة</th><th>المستوى</th><th>بُعد المسافة</th></tr></thead><tbody>";
            string t2 = "";
            var lt = lst.OrderBy(p => p.dis);
            int i = 1;
            foreach (var vr in lt)
            {
                if (vr.levcode == 1)
                {
                    t2 += "<tr style=\"background-color:#ffeded;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 2)
                {
                    t2 += "<tr style=\"background-color: #d0daff;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 3)
                {
                    t2 += "<tr style=\"background-color:#d0ffe9;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }


                i++;
            }

            string t3 = "</tbody> </table>";

            string al = t1 + t2 + t3;
            return Json(al/*, JsonRequestBehavior.AllowGet*/);
        }

        // primary get 
        [HttpGet]
        public JsonResult getDistanceP(double lat, double lng)
        {


            double rdis;
            var dt = (from sc in db.TSchools
                      join lv in db.TLevel on sc.LevelNo equals lv.LevelNo

                      where sc.location != null && sc.LevelNo == 1
                      orderby (sc.SchoolID)
                      select new
                      {
                          scid = sc.SchoolID,
                          scName = sc.School,
                          scLevel = lv.NameLevel,
                          sclevcode = lv.LevelNo,
                          slocation = sc.location
                      });
            List<locationMap> lst = new List<locationMap>();
            foreach (var dd in dt)
            {
                locationMap lc = new locationMap();
                if (dd.slocation != null)
                {
                    if (dd.slocation.Length > 10)
                    {
                        string loc = dd.slocation;
                        int l1 = loc.IndexOf("@");
                        int l2 = loc.IndexOf(",");
                        int g1 = loc.IndexOf(",", l2 + 1);
                        string lat1 = dd.slocation.Substring(l1 + 1, (l2 - 1) - l1);
                        string lng1 = dd.slocation.Substring(l2 + 1, (g1 - 1) - l2);
                        lc.SchID = dd.scid;
                        lc.Schname = dd.scName;
                        lc.levelNo = dd.scLevel;
                        lc.lat = lat1;
                        lc.lag = lng1;
                        lc.latD = Convert.ToDouble(lat1);
                        lc.lagD = Convert.ToDouble(lng1);
                        lc.levcode = dd.sclevcode;
                        //var sCoord = new GeoCoordinate(lat, lng);
                        //var eCoord = new GeoCoordinate(lc.latD, lc.lagD);
                        //rdis = (sCoord.GetDistanceTo(eCoord)) / 1000;
                        //lc.dis = Math.Round(rdis, 3);

                        lst.Add(lc);
                    }
                }
            }
            string t1 = "<table id=\"tds\" class=\"table\"> <thead style=\"background-color: #e8e8e8;\"><tr><th>م</th><th>كود المدرسة</th><th>اسم المدرسة</th><th>المستوى</th><th>بُعد المسافة</th></tr></thead><tbody>";
            string t2 = "";
            var lt = lst.OrderBy(p => p.dis);
            int i = 1;
            foreach (var vr in lt)
            {
                if (vr.levcode == 1)
                {
                    t2 += "<tr style=\"background-color:#ffeded;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 2)
                {
                    t2 += "<tr style=\"background-color: #d0daff;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 3)
                {
                    t2 += "<tr style=\"background-color:#d0ffe9;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }


                i++;
            }

            string t3 = "</tbody> </table>";

            string al = t1 + t2 + t3;
            return Json(al/*, JsonRequestBehavior.AllowGet*/);
        }

        // prepratory 
        [HttpGet]
        public JsonResult getDistancPr(double lat, double lng)
        {


            double rdis;
            var dt = (from sc in db.TSchools
                      join lv in db.TLevel on sc.LevelNo equals lv.LevelNo

                      where sc.location != null && sc.LevelNo == 2
                      orderby (sc.SchoolID)
                      select new
                      {
                          scid = sc.SchoolID,
                          scName = sc.School,
                          scLevel = lv.NameLevel,
                          sclevcode = lv.LevelNo,
                          slocation = sc.location
                      });
            List<locationMap> lst = new List<locationMap>();
            foreach (var dd in dt)
            {
                locationMap lc = new locationMap();
                if (dd.slocation != null)
                {
                    if (dd.slocation.Length > 10)
                    {
                        string loc = dd.slocation;
                        int l1 = loc.IndexOf("@");
                        int l2 = loc.IndexOf(",");
                        int g1 = loc.IndexOf(",", l2 + 1);
                        string lat1 = dd.slocation.Substring(l1 + 1, (l2 - 1) - l1);
                        string lng1 = dd.slocation.Substring(l2 + 1, (g1 - 1) - l2);
                        lc.SchID = dd.scid;
                        lc.Schname = dd.scName;
                        lc.levelNo = dd.scLevel;
                        lc.lat = lat1;
                        lc.lag = lng1;
                        lc.latD = Convert.ToDouble(lat1);
                        lc.lagD = Convert.ToDouble(lng1);
                        lc.levcode = dd.sclevcode;
                        //var sCoord = new GeoCoordinate(lat, lng);
                        //var eCoord = new GeoCoordinate(lc.latD, lc.lagD);
                        //rdis = (sCoord.GetDistanceTo(eCoord)) / 1000;
                        //lc.dis = Math.Round(rdis, 3);

                        lst.Add(lc);
                    }
                }
            }
            string t1 = "<table id=\"tds\" class=\"table\"> <thead style=\"background-color: #e8e8e8;\"><tr><th>م</th><th>كود المدرسة</th><th>اسم المدرسة</th><th>المستوى</th><th>بُعد المسافة</th></tr></thead><tbody>";
            string t2 = "";
            var lt = lst.OrderBy(p => p.dis);
            int i = 1;
            foreach (var vr in lt)
            {
                if (vr.levcode == 1)
                {
                    t2 += "<tr style=\"background-color:#ffeded;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 2)
                {
                    t2 += "<tr style=\"background-color: #d0daff;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 3)
                {
                    t2 += "<tr style=\"background-color:#d0ffe9;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }


                i++;
            }

            string t3 = "</tbody> </table>";

            string al = t1 + t2 + t3;
            return Json(al/*, JsonRequestBehavior.AllowGet*/);
        }

        // secondry

        [HttpGet]
        public JsonResult getDistanceS(double lat, double lng)
        {


            double rdis;
            var dt = (from sc in db.TSchools
                      join lv in db.TLevel on sc.LevelNo equals lv.LevelNo

                      where sc.location != null && sc.LevelNo == 3
                      orderby (sc.SchoolID)
                      select new
                      {
                          scid = sc.SchoolID,
                          scName = sc.School,
                          scLevel = lv.NameLevel,
                          sclevcode = lv.LevelNo,
                          slocation = sc.location
                      });
            List<locationMap> lst = new List<locationMap>();
            foreach (var dd in dt)
            {
                locationMap lc = new locationMap();
                if (dd.slocation != null)
                {
                    if (dd.slocation.Length > 10)
                    {
                        string loc = dd.slocation;
                        int l1 = loc.IndexOf("@");
                        int l2 = loc.IndexOf(",");
                        int g1 = loc.IndexOf(",", l2 + 1);
                        string lat1 = dd.slocation.Substring(l1 + 1, (l2 - 1) - l1);
                        string lng1 = dd.slocation.Substring(l2 + 1, (g1 - 1) - l2);
                        lc.SchID = dd.scid;
                        lc.Schname = dd.scName;
                        lc.levelNo = dd.scLevel;
                        lc.lat = lat1;
                        lc.lag = lng1;
                        lc.latD = Convert.ToDouble(lat1);
                        lc.lagD = Convert.ToDouble(lng1);
                        lc.levcode = dd.sclevcode;
                        //var sCoord = new GeoCoordinate(lat, lng);
                        //var eCoord = new GeoCoordinate(lc.latD, lc.lagD);
                        //rdis = (sCoord.GetDistanceTo(eCoord)) / 1000;
                        //lc.dis = Math.Round(rdis, 3);

                        lst.Add(lc);
                    }
                }
            }
            string t1 = "<table id=\"tds\" class=\"table\"> <thead style=\"background-color: #e8e8e8;\"><tr><th>م</th><th>كود المدرسة</th><th>اسم المدرسة</th><th>المستوى</th><th>بُعد المسافة</th></tr></thead><tbody>";
            string t2 = "";
            var lt = lst.OrderBy(p => p.dis);
            int i = 1;
            foreach (var vr in lt)
            {
                if (vr.levcode == 1)
                {
                    t2 += "<tr style=\"background-color:#ffeded;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 2)
                {
                    t2 += "<tr style=\"background-color: #d0daff;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 3)
                {
                    t2 += "<tr style=\"background-color:#d0ffe9;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }


                i++;
            }

            string t3 = "</tbody> </table>";

            string al = t1 + t2 + t3;
            return Json(al/*, JsonRequestBehavior.AllowGet*/);
        }

        // other
        [HttpGet]
        public JsonResult getDistanceO(double lat, double lng)
        {


            double rdis;
            var dt = (from sc in db.TSchools
                      join lv in db.TLevel on sc.LevelNo equals lv.LevelNo

                      where sc.location != null && sc.LevelNo > 3
                      orderby (sc.SchoolID)
                      select new
                      {
                          scid = sc.SchoolID,
                          scName = sc.School,
                          scLevel = lv.NameLevel,
                          sclevcode = lv.LevelNo,
                          slocation = sc.location
                      });
            List<locationMap> lst = new List<locationMap>();
            foreach (var dd in dt)
            {
                locationMap lc = new locationMap();
                if (dd.slocation != null)
                {
                    if (dd.slocation.Length > 10)
                    {
                        string loc = dd.slocation;
                        int l1 = loc.IndexOf("@");
                        int l2 = loc.IndexOf(",");
                        int g1 = loc.IndexOf(",", l2 + 1);
                        string lat1 = dd.slocation.Substring(l1 + 1, (l2 - 1) - l1);
                        string lng1 = dd.slocation.Substring(l2 + 1, (g1 - 1) - l2);
                        lc.SchID = dd.scid;
                        lc.Schname = dd.scName;
                        lc.levelNo = dd.scLevel;
                        lc.lat = lat1;
                        lc.lag = lng1;
                        lc.latD = Convert.ToDouble(lat1);
                        lc.lagD = Convert.ToDouble(lng1);
                        lc.levcode = dd.sclevcode;
                        //var sCoord = new GeoCoordinate(lat, lng);
                        //var eCoord = new GeoCoordinate(lc.latD, lc.lagD);
                        //rdis = (sCoord.GetDistanceTo(eCoord)) / 1000;
                        //lc.dis = Math.Round(rdis, 3);

                        lst.Add(lc);
                    }
                }
            }
            string t1 = "<table id=\"tds\" class=\"table\"> <thead style=\"background-color: #e8e8e8;\"><tr><th>م</th><th>كود المدرسة</th><th>اسم المدرسة</th><th>المستوى</th><th>بُعد المسافة</th></tr></thead><tbody>";
            string t2 = "";
            var lt = lst.OrderBy(p => p.dis);
            int i = 1;
            foreach (var vr in lt)
            {
                if (vr.levcode == 1)
                {
                    t2 += "<tr style=\"background-color:#ffeded;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 2)
                {
                    t2 += "<tr style=\"background-color: #d0daff;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }
                else if (vr.levcode == 3)
                {
                    t2 += "<tr style=\"background-color:#d0ffe9;\"><td>" + i + "</td><td>" + vr.SchID + "</td><td><p class=\"clk\" id=\"" + vr.SchID + "\" onclick=\"lod(this); \">" + vr.Schname + "</p></td><td>" + vr.levelNo + "</td><td><span style=\"color:blue; \">" + vr.dis + " </span><span> كم </span> </td></tr>";

                }


                i++;
            }

            string t3 = "</tbody> </table>";

            string al = t1 + t2 + t3;
            return Json(al/*, JsonRequestBehavior.AllowGet*/);
        }









        public ActionResult AcceptanceMain(int id)
        {
            ViewBag.id = id;
            return View();
        }

        public ActionResult AcceptanceStdWait(studentsWait students)
        {
            var old = db.studentsWait.Where(p => p.id == students.id).FirstOrDefault();
            string fname = old.sname1 + " " + old.sname2 + " " + old.sname3 + " " + old.snname4;

            if (students.Sstate == 1)
            {
                old.Sstate = students.Sstate;

                db.SaveChanges();
                var cr = db.TCLassRow.Where(p => p.SchoolID == students.schid && p.Rows == students.srow).FirstOrDefault();
                if (cr != null)
                {
                    int? i = cr.pupil + 1;
                    cr.pupil = Convert.ToInt16(i);

                    db.SaveChanges();
                }

                var locs = db.TSchools.Where(p => p.SchoolID == old.schid).FirstOrDefault();

                string messages = " عزيزي ولي الأمر " +
         " تم قبول الطالب / " + fname + " فى مدرسة / " + old.sname + " فى الصف / " + old.rowName + " " +
         "  موقع المدرسة   " + locs.locshot;
                var res = SendSms(old.mob, messages);

                ViewBag.Sstate = new SelectList(db.acceptstates, "id", "acceptState", students.Sstate);
                ViewBag.oldSchid = new SelectList(db.TSchools, "SchoolID", "School", students.oldSchid);
                ViewBag.nat = new SelectList(db.TNationality, "NatID", "Nationality", students.nat);
                ViewBag.state = 2;

                return RedirectToAction("AcceptanceApproveWait", "TSchools", new { id = students.id });

            }
            else if (students.Sstate == 2)
            {
                string messages = " عزيزي ولي الأمر " +
         " عذراً ..لقد تم وضع الطالب / " + fname + " فى قائمة الانتظار لحين مراجعة اللجنة المركزية باالادارة العامة " +
         "  موقع الادارة   " + "https://maps.app.goo.gl/8DS5i2ugW7wWMHFt8";
                var res = SendSms(old.mob, messages);
                var od = db.studentsWait.Where(p => p.sid == students.sid).FirstOrDefault();
                if (od != null)
                {
                    od.Sstate = students.Sstate;


                    db.SaveChanges();
                }

                ViewBag.state = 2;
                return RedirectToAction("AcceptanceWating", "TSchools", new { id = students.schid });
            }
            else if (students.Sstate == 3)
            {
                string messages = " عزيزي ولي الأمر " +
         " عذراً ..لا يوجد مقعد شاغر للطالب / " + fname + " يمكنك التسجيل فى اقرب مدرسة اخرى " +
         "  موقع لجنة القبول بالمكتب   " + "https://maps.app.goo.gl/8DS5i2ugW7wWMHFt8";
                var res = SendSms(old.mob, messages);
                var od = db.studentsWait.Where(p => p.sid == students.sid).FirstOrDefault();
                if (od != null)
                {
                    studentsR stdr = new studentsR();
                    stdr.acceptS = students.acceptS;
                    stdr.care = students.care;
                    stdr.familyCert = students.familyCert;
                    stdr.fid = students.fid;
                    stdr.fname1 = students.fname1;
                    stdr.fname2 = students.fname2;
                    stdr.fname3 = students.fname3;
                    stdr.fname4 = students.fname4;
                    stdr.homeCert = students.homeCert;
                    stdr.mob = students.mob;
                    stdr.nat = students.nat;
                    stdr.notess = students.notess;
                    stdr.oldSchid = students.oldSchid;
                    stdr.rowName = students.rowName;
                    stdr.schid = students.schid;
                    stdr.sid = students.sid;
                    stdr.sname = students.sname;
                    stdr.sname1 = students.sname1;
                    stdr.sname2 = students.sname2;
                    stdr.sname3 = students.sname3;
                    stdr.snname4 = students.snname4;
                    stdr.srow = students.srow;
                    stdr.Sstate = students.Sstate;
                    stdr.transfare = students.transfare;
                    stdr.dateA = students.dateA;
                    stdr.dateR = DateTime.Now;
                    db.studentsR.Add(stdr);
                    db.SaveChanges();


                    db.studentsWait.Remove(od);
                    db.SaveChanges();
                }

                ViewBag.state = 3;
                return RedirectToAction("AcceptanceWating", "TSchools", new { id = students.schid });
            }
            else if (students.Sstate == 1002)
            {
                string messages = " عزيزي ولي الأمر " +
         " عذراً تم الغاء طلبكم..يوجد ملاحظات على نظامية التسجيل  للطالب / " + fname + " يمكنك مراجعة إدارة المدرسة او لجنة القبول بالمكتب " +
         "  موقع لجنة القبول بالمكتب   " + "https://maps.app.goo.gl/8DS5i2ugW7wWMHFt8";
                var res = SendSms(old.mob, messages);
                var od = db.studentsWait.Where(p => p.sid == students.sid).FirstOrDefault();
                if (od != null)
                {
                    studentsR stdr = new studentsR();
                    stdr.acceptS = students.acceptS;
                    stdr.care = students.care;
                    stdr.familyCert = students.familyCert;
                    stdr.fid = students.fid;
                    stdr.fname1 = students.fname1;
                    stdr.fname2 = students.fname2;
                    stdr.fname3 = students.fname3;
                    stdr.fname4 = students.fname4;
                    stdr.homeCert = students.homeCert;
                    stdr.mob = students.mob;
                    stdr.nat = students.nat;
                    stdr.notess = students.notess;
                    stdr.oldSchid = students.oldSchid;
                    stdr.rowName = students.rowName;
                    stdr.schid = students.schid;
                    stdr.sid = students.sid;
                    stdr.sname = students.sname;
                    stdr.sname1 = students.sname1;
                    stdr.sname2 = students.sname2;
                    stdr.sname3 = students.sname3;
                    stdr.snname4 = students.snname4;
                    stdr.srow = students.srow;
                    stdr.Sstate = students.Sstate;
                    stdr.transfare = students.transfare;
                    stdr.dateA = students.dateA;
                    stdr.dateR = DateTime.Now;
                    db.studentsR.Add(stdr);
                    db.SaveChanges();
                    db.studentsWait.Remove(od);
                    db.SaveChanges();
                }

                ViewBag.state = 4;
                return RedirectToAction("AcceptanceWating", "TSchools", new { id = students.schid });
            }


            return RedirectToAction("AcceptanceApproveWait", "TSchools", new { id = students.schid });
        }

         
        // MANAGER
        public ActionResult AcceptanceMainM()
        {
            string id1 = "";// sc.DecParametar(Request.Cookies["main"]["schid"]);

            int id = Convert.ToInt32(id1);
            ViewBag.id = id;
            return View();
        }

    }
}