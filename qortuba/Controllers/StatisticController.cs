﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using qortuba.Data;
using qortuba.Models;
using qortuba.Models.Class;

namespace qortuba.Controllers
{
    public class StatisticController : Controller
    {
        private readonly ApplicationDbContext db;
        MyFunctions fun = new MyFunctions();
        public StatisticController(ApplicationDbContext Context)
        {
            db = Context;
        }
        // [Authorize]

        // GET: Statistic
        public ActionResult DesAndIncStatistic()
        {
            List<DesAndInc> list = new List<DesAndInc>();
            var AllSchool = db.TSchools.ToList().Take(2);
            foreach (var schoolse in AllSchool)
            {
                DataRow dr = fun.fireDataRow("select School from TSchools where SchoolID = " +schoolse.SchoolID.ToString() + "");
                ViewBag.Sname = dr["School"].ToString();
                DataTable dtable = fun.fireStoreProcedure1("DesAndInc", schoolse.SchoolID.ToString());
                foreach (DataRow sdr in dtable.Rows)
                {
                    DesAndInc imp = new DesAndInc();

                    imp.Id = sdr["ID"].ToString();
                    if (imp.Id != "25" && imp.Id != "99")
                    {
                        imp.Subject = sdr["SUB"].ToString();
                        imp.Req = sdr["req"].ToString();
                        imp.Exs = sdr["exs"].ToString();
                        imp.Dess = sdr["dess"].ToString();
                        imp.SchoolName = dr["School"].ToString();
                        imp.Inc = sdr["inc"].ToString();
                        imp.IncTeach = sdr["incTeac"].ToString();
                        list.Add(imp);
                    }
                }
            }

            return View(list);
        }
        public ActionResult DesAndIncSubject()
        {
            List<DesAndInc> list = new List<DesAndInc>();
            var AllSchool = db.TSchools.ToList().Take(2);
            foreach (var schoolse in AllSchool)
            {
               
                DataTable dtable = fun.fireStoreProcedure1("DesAndInc", schoolse.SchoolID.ToString());
                foreach (DataRow sdr in dtable.Rows)
                {
                    DesAndInc imp = new DesAndInc();

                    imp.Id = sdr["ID"].ToString();
                    if (imp.Id != "25" && imp.Id != "99")
                    {
                        imp.Subject = sdr["SUB"].ToString();
                        imp.Req = sdr["req"].ToString();
                        imp.Exs = sdr["exs"].ToString();
                        imp.Dess = sdr["dess"].ToString();
                        imp.SchoolName = sdr["School"].ToString();
                        imp.Inc = sdr["inc"].ToString();
                        imp.IncTeach = sdr["incTeac"].ToString();
                        list.Add(imp);
                    }
                }
            }

            return View(list);
        }
    }
}