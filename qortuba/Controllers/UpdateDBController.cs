﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using qortuba.Data;
using qortuba.Models;
using qortuba.Models.Class;

namespace qortuba.Controllers
{
 //   [Authorize]
    public class UpdateDBController : Controller
    {
        //MyFunctionsAccess facc = new MyFunctionsAccess();
        private readonly ApplicationDbContext db;
        MyFunctions fun = new MyFunctions();
        public UpdateDBController(ApplicationDbContext Context)     
        {
            db = Context;
        }
        // GET: UpdateDB
      //  [Authorize(Roles = "DataBaseUpdate")]
        public ActionResult Index()
        {
           
                return View();
         
        }
        [HttpPost]
        public ActionResult Index(IFormFile file)
        {

            if (file.Length > 0)
            {
                string ext = Path.GetExtension(file.FileName);
                if (ext == ".accdb")
                {
                    var path = Path.Combine(Path.GetFileName("~/App_Data/"), "qortoba.accdb");
                    using (var fileSteam = new FileStream(path, FileMode.Create))
                    {
                        file.CopyToAsync(fileSteam);
                    }
                    TempData["success"] = "تم الرفع بنجاح";
                    return RedirectToAction("Index");
                }
                else {
                    TempData["error"] = "تم الرفع بنجاح";
                    return RedirectToAction("Index");

                }
                
            }

            return RedirectToAction("Index");
        }

        public ActionResult UpdateDB() {

            DataTable dt=new DataTable();//= facc.SchemaDataTable("access");

            foreach (DataRow dr in dt.Rows) {
               string tname = dr[2].ToString();

                if (tname == "TImployees" || tname == "TSchools") {
                    DataTable dts= new DataTable();//= facc.fireDataTable("select * from " + tname);
                    string cs = "";//ConfigurationManager.ConnectionStrings["ConnMada"].ConnectionString;
                    SqlConnection cn = new SqlConnection(cs);
                    cn.Open();
                    string txt = tname + "Temp";
                    fun.fireSQL("DELETE FROM " + txt + ";");
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(cn);
                    bulkCopy.DestinationTableName = "dbo." + txt;
                    DataTableReader reader =  dts.CreateDataReader();
                    bulkCopy.WriteToServer(reader);
                    //fun.fireSQL("update TImployees set [password] = '@A#m1Po$' where [Identity] = '1012557854'");
                    cn.Close();
                } else if (tname == "TCLassRow") {

                    DataTable dts=new DataTable();//= facc.fireDataTable("select * from " + tname);
                    string cs = "";// ConfigurationManager.ConnectionStrings["ConnMada"].ConnectionString;
                    SqlConnection cn = new SqlConnection(cs);
                    cn.Open();
                    fun.fireSQL("DELETE FROM " + tname + ";");
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(cn);
                    bulkCopy.DestinationTableName = "dbo." + tname;
                    DataTableReader reader = dts.CreateDataReader();
                    bulkCopy.WriteToServer(reader);
            
                    cn.Close();

                }
              
            }

            fun.fireStoreProcedure("updateDB");
            TempData["success"] = "تم التحديث بنجاح";
            return RedirectToAction("Index");
        }
        public Boolean check(string tname) {
          
            string[] ls = {
                "ANeedEdare","ClassCompare","Classes","CodeRep","CommonSch","Departs","FormsTypes","GroupN","GroupS",
                "HayNames","Manages","MoveType","NeedCondition","NeedCondition1","NeedGFields","NeedItemType","NeedOptions",
                "OutMove","PlaneEdar","PlanNeed","PSubClas","PWorkClas","QTerm","QType","QYtBEMARK","ReducedTypes","ReviewCodes",
                "SchoollFields","SchStatistics","SchTypes","SubErr","SubNotOk","SuperLaw","SuperVis","T_area","T_Center","T_CenterPa",
                "T_CenterPaTypes","T_ClassType","T_contract","T_country","T_Door","T_Married","T_paralyse","T_PlanTemp","T_Teach",
                "T_Varway1","T_yer","T_yes_no","TBuilding","TCadE_door","TCLassRow","TeachFields","TempStatus","TempTSubj","TimplAutoUpdate",
                "TImployees","TImployeesCompl","TimployeesDel","TLevel","TLevel_Row","TLevel_Row_Code","TLevel_Subject","TNationality",
                "TQualified","TransTypes","TSchools","TSchoolsType","TSector","TSpecial","TSpecial_LevErr","TSpecial_Sub","TSpecial_Sub1",
                "TSpecialSub","Tstatus","TSubject","TSubjectSuperv","TSubTotal","Tuition","TUniversity","TWork","TWorkEdar","TWorkJob",
                "Years","ZCurrent","ZIDLosses","ZNeedAll"

            };

            string[] ls1 = {
                "HayNames","TCLassRow","TImployees","TSchools"};
            int pos = Array.IndexOf(ls1, tname);
            if (pos > -1)
            {
                return true;
            }
            else {
                return false;
            }
        }

        public ActionResult UpdatePassword() {
            
                fun.fireStoreProcedure("EmppassMode");
                TempData["success"] = "تم التحديث بنجاح";
                return RedirectToAction("Index");
            
        }

        // password Generator
        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
    }
}