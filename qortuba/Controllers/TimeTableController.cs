﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using qortuba.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using qortuba.Data;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using qortuba.Models.ModeView;
using Microsoft.EntityFrameworkCore;
using Rotativa.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace qortuba.Controllers
{

    public class TimeTableController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly IHostingEnvironment _HostEnvironment;
        Models.Class.MyFunctions fun = new Models.Class.MyFunctions();
        public TimeTableController(ApplicationDbContext Context, IHostingEnvironment HostEnvironment)
        {
            db = Context;
            _HostEnvironment = HostEnvironment;
        }

        // GET: TimeTable
      //  [Authorize(Roles = "TimeTable")]
        public ActionResult Index()
        {
            List<School> list = new List<School>();
            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }
            ViewBag.adela = db.adela.Where(p => p.adServ == 3).ToList();
            return View(list);

        }

        public ActionResult ShowTables(int id)
        {
            DataTable dt = fun.fireStoreProcedure3("TimeTableAuto", id.ToString(), "5", "8");
            List<DesForTime> ListDf = new List<DesForTime>();
            foreach (DataRow dr in dt.Rows)
            {
                DesForTime dsf = new DesForTime();
                dsf.empId = dr["empId"].ToString();
                dsf.Teacher = dr["TEACHAR"].ToString();
                dsf.suID = dr["suID"].ToString();
                dsf.subj = dr["subj"].ToString();
                dsf.stat = dr["stat"].ToString();
                dsf.Reduce = dr["Reduce"].ToString();
                dsf.def = dr["def"].ToString();
                dsf.addclass = dr["addClass"].ToString();
                dsf.addSub = dr["addSubClass"].ToString();
                ListDf.Add(dsf);
            }
            DataTable dt1 = fun.fireStoreProcedure1("DesIntable", id.ToString());

            List<DesForTime> ListDf1 = new List<DesForTime>();
            foreach (DataRow dr1 in dt1.Rows)
            {
                DesForTime dsf1 = new DesForTime();
                dsf1.empId = dr1["ID"].ToString();
                dsf1.subj = dr1["SUB"].ToString();
                dsf1.stat = dr1["dess"].ToString();
                ListDf1.Add(dsf1);
            }


            CultureInfo cultures = CultureInfo.CreateSpecificCulture("ar-SA");
            Models.ModeView.Schools ischools = new Models.ModeView.Schools();

            ischools.tSchools = db.TSchools.Include(t => t.T_area).Include(t => t.T_Center).Include(t => t.TBuilding).Include(t => t.TLevel).Include(t => t.TSchoolsType).Include(t => t.TSector).Include(t => t.Tuition1).Where(t => t.SchoolID == id).FirstOrDefault();
            ischools.tCLassRow = db.TCLassRow.Include(t => t.TLevel_Row).Where(t => t.SchoolID == id).ToList().OrderBy(p => p.Rows);
            ischools.ttimetable = db.TimeTable.Where(t => t.sch_id == id).ToList();
            ischools.desForTime = ListDf;
            ischools.desTimeTable = ListDf1;
            ViewBag.cClass = db.TCLassRow.Where(t => t.SchoolID == id).Sum(t => t.CLass);
            ViewBag.cStud = db.TCLassRow.Where(t => t.SchoolID == id).Sum(t => t.pupil);
            ischools.Tsubjects = db.TSubject.ToList();
            ischools.reducedTypes = db.ReducedTypes.ToList();
            string text = DateTime.Now.ToString("yyyy -MMMM -dd", cultures);


            return View(ischools);
        }

        public JsonResult EditT(string id, string id1, int sp, int rd)
        {
            if (id != null && sp.ToString() != null && rd.ToString() != null)
            {
                fun.fireSQL("UPDATE TImployees SET addClasses = " + sp + " , addSubClasses = " + rd + " WHERE [Identity] = '" + id + "'");
            }

            return Json(null);
        }

        public IActionResult printTeach(string id)
        {
            if (id != null)
            {

                var header = Path.GetFileName("~/static/headerSchoolEmpwork.html");
                var footer = Path.GetFileName("~/static/footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                //var pdf = new ViewAsPdf("Reportempwork1", dt)
                //{

                //    CustomSwitches = customSwitches,

                //    PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                //    PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                //    PageSize = Rotativa.AspNetCore.Options.Size.A4
                //};


            //    return new ActionAsPdf(
            //        "ShowTable",
            //        new { id = id })
            //    {
            //        PageMargins = new Rotativa.AspNetCore.Options.Margins(10, 2, 2, 10),
            //        PageSize = Rotativa.AspNetCore.Options.Size.A3,
            //        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,

            //        FileName = "الجدول العام.pdf"
            //    };

            }

            return View();
        }


        public ActionResult TimeTable(int id)
        {
            if (id == null)
            {
                //     return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TSchools tSchools = db.TSchools.Find(id);
            if (tSchools == null)
            {
                return NotFound();
            }


            return View(tSchools);
        }
        public ActionResult Create(int id)
        {

            TimeTableView timeTable = new TimeTableView();
            timeTable.vMethods = Populatevm();

            ViewBag.id = id;
            return View(timeTable);
        }

        private static List<SelectListItem> Populatevm()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            string constr = "";// ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                string query = " SELECT id, viewName FROM viewMethod";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    //using (SqlDataReader sdr = cmd.ExecuteReader())
                    //{
                    //    while (sdr.Read())
                    //    {
                    //        items.Add(new SelectListItem
                    //        {
                    //            Text = sdr["viewName"].ToString(),
                    //            Value = sdr["id"].ToString()
                    //        });
                    //    }
                    //}
                    con.Close();
                }
            }

            return items;
        }



        public void BulkInsert(DataTable dt)
        {

            string cs = "";// ConfigurationManager.ConnectionStrings["ConnMada"].ConnectionString;
            SqlConnection cn = new SqlConnection(cs);
            cn.Open();

            SqlBulkCopy bulkCopy = new SqlBulkCopy(cn);
            bulkCopy.DestinationTableName = "dbo.periods";
            DataTableReader reader = dt.CreateDataReader();
            bulkCopy.WriteToServer(reader);
            cn.Close();

        }


        public int sumTrue(day_s day1)
        {
            int st = Convert.ToInt32(day1.Saturday);
            int su = Convert.ToInt32(day1.Sunday);
            int mo = Convert.ToInt32(day1.Monday);
            int tu = Convert.ToInt32(day1.Tuesday);
            int we = Convert.ToInt32(day1.Wednesday);
            int th = Convert.ToInt32(day1.Thursday);
            int fr = Convert.ToInt32(day1.Friday);

            int total = st + su + mo + tu + we + th + fr;
            return total;
        }

        public ArrayList rand(int nm)
        {

            ArrayList num = new ArrayList();
            for (int i = 1; i <= nm; i++)
            {
                num.Add(i);

            }
            return num;

        }




        public ActionResult CreateM(Models.ModeView.TimeTableView timeTableView)
        {
            DataTable dt = new DataTable();
            //creating columns for DataTable  
            dt.Columns.Add(new DataColumn("id", typeof(int)));
            dt.Columns.Add(new DataColumn("T_id", typeof(int)));
            dt.Columns.Add(new DataColumn("day_id", typeof(int)));
            dt.Columns.Add(new DataColumn("period_id", typeof(int)));
            dt.Columns.Add(new DataColumn("class_id", typeof(string)));
            dt.Columns.Add(new DataColumn("subject_id", typeof(int)));
            dt.Columns.Add(new DataColumn("emp_id", typeof(int)));
            dt.Columns.Add(new DataColumn("Tnum", typeof(int)));
            dt.Columns.Add(new DataColumn("Trow", typeof(int)));

            DataRow rw;

            var dd = db.TCLassRow.Where(t => t.SchoolID == timeTableView.timeTable.sch_id).ToList().OrderBy(t => t.Rows);

            ArrayList classRow = new ArrayList();
            foreach (var we in dd)
            {
                for (int i = 1; i <= we.CLass; i++)
                {
                    classRow.Add(i.ToString() + "/" + we.Rows.ToString());

                }

            }
            ViewBag.classes = classRow;
            ViewBag.countR = dd.Count();
            int daynum = 1;// sumTrue(timeTableView.day_S);
            ViewBag.days = daynum;
            ViewBag.subject = new SelectList(db.TSubject, "SubID", "Subject");
            ViewBag.teacher = new SelectList(db.TImployees.Where(t => t.SchID == timeTableView.timeTable.sch_id), "Identity", "name1");
            timeTableView.timeTable.cr_id = "";// sc.DecParametar(User.Identity.Name);
            DateTime dateTime = DateTime.Now;
            timeTableView.timeTable.cr_date = dateTime;
            timeTableView.timeTable.day_num = daynum;


            db.TimeTable.Add(timeTableView.timeTable);
            timeTableView.day_S.T_id = timeTableView.timeTable.id;
            db.day_s.Add(timeTableView.day_S);
            db.SaveChanges();
            int? dy = timeTableView.timeTable.per_num;
            decimal sss = Convert.ToDecimal(dy);
            foreach (var clR in classRow)
            {
                for (int i = 1; i <= ViewBag.days * dy; i++)
                {

                    decimal rv = (i / sss);
                    decimal dyNum = Math.Ceiling(rv);

                    rw = dt.NewRow();
                    rw["T_id"] = timeTableView.timeTable.id;
                    rw["day_id"] = dyNum;
                    rw["period_id"] = i;
                    rw["class_id"] = clR.ToString();
                    dt.Rows.Add(rw);
                }
            }
            BulkInsert(dt);
            timeTableView.periods = db.periods.Where(t => t.T_id == timeTableView.timeTable.id).ToList();
            return View("CreateManual", timeTableView);
        }

        public ActionResult CreateD(Models.ModeView.TimeTableView timeTableView)
        {
            DataTable dt = new DataTable();
            //creating columns for DataTable  
            dt.Columns.Add(new DataColumn("id", typeof(int)));
            dt.Columns.Add(new DataColumn("T_id", typeof(int)));
            dt.Columns.Add(new DataColumn("day_id", typeof(int)));
            dt.Columns.Add(new DataColumn("period_id", typeof(int)));
            dt.Columns.Add(new DataColumn("class_id", typeof(string)));
            dt.Columns.Add(new DataColumn("subject_id", typeof(int)));
            dt.Columns.Add(new DataColumn("emp_id", typeof(int)));
            dt.Columns.Add(new DataColumn("Tnum", typeof(int)));
            dt.Columns.Add(new DataColumn("Trow", typeof(int)));
            DataRow rw;
            var dd = db.TCLassRow.Where(t => t.SchoolID == timeTableView.timeTable.sch_id).ToList().OrderBy(t => t.Rows); //جدول عدد الفصول وعدد الطلبة فى كل صف
            ArrayList classRow = new ArrayList(); // قائمة بعدد الفصول فى كل صف
            foreach (var we in dd)
            {
                for (int i = 1; i <= we.CLass; i++)
                {
                    classRow.Add(i.ToString() + "/" + we.Rows.ToString());   // تعبئة القائمة الفصول 1/1
                }
            }
            ViewBag.classes = classRow;
            ViewBag.countR = dd.Count();
            int daynum = sumTrue(timeTableView.day_S); // جلب عدد الايام الدراسية فى كل اسبوع
            ViewBag.days = daynum;
            ViewBag.subject = new SelectList(db.TSubject, "SubID", "Subject"); // جلب المواد
            ViewBag.teacher = new SelectList(db.TImployees.Where(t => t.SchID == timeTableView.timeTable.sch_id), "Identity", "name1"); // جلب المدرسين
            timeTableView.timeTable.cr_id = "";// sc.DecParametar(User.Identity.Name); // كود منشئ الجدول
            DateTime dateTime = DateTime.Now;
            timeTableView.timeTable.cr_date = dateTime;
            timeTableView.timeTable.day_num = daynum;

            db.TimeTable.Add(timeTableView.timeTable); // اضافة جدول جديد
            timeTableView.day_S.T_id = timeTableView.timeTable.id;
            db.day_s.Add(timeTableView.day_S); // اضافة الايام
            db.SaveChanges(); // حفظ الجدول والايام
            int? dy = timeTableView.timeTable.per_num; // عدد الحصص فى اليوم
            decimal sss = Convert.ToDecimal(dy);
            foreach (var clR in classRow)  // تكرار على كل فصل لاضافة الحصص
            {
                for (int i = 1; i <= ViewBag.days * dy; i++)
                {

                    decimal rv = (i / sss);
                    decimal dyNum = Math.Ceiling(rv);

                    rw = dt.NewRow();
                    rw["T_id"] = timeTableView.timeTable.id; // كود الجدول
                    rw["day_id"] = dyNum; // اليوم
                    rw["period_id"] = i;  // الحصة 
                    rw["class_id"] = clR.ToString(); // الفصل
                    dt.Rows.Add(rw);
                }
            }
            BulkInsert(dt);// ادخال كلى للحصص
            string id = timeTableView.timeTable.sch_id.Value.ToString(); // كود المدرسة
            string dayss = sumTrue(timeTableView.day_S).ToString(); // عدد الايام
            string classes = timeTableView.timeTable.per_num.Value.ToString(); // الحصص
            string tid = timeTableView.timeTable.id.ToString();
            fun.fireStoreProcedure4("TimeTable2", id, dayss, classes, tid); // عرض الجدول



            return RedirectToAction("UpdateManual", new { id = tid });  // التحويل لعرض الجدول
        }


        public ActionResult CreateC(Models.ModeView.TimeTableView timeTableView)
        {

            int daynum = sumTrue(timeTableView.day_S);
            ViewBag.days = daynum;

            timeTableView.timeTable.cr_id = "";// sc.DecParametar(User.Identity.Name);
            DateTime dateTime = DateTime.Now;
            timeTableView.timeTable.cr_date = dateTime;
            timeTableView.timeTable.day_num = daynum;

            db.TimeTable.Add(timeTableView.timeTable);
            timeTableView.day_S.T_id = timeTableView.timeTable.id;
            db.day_s.Add(timeTableView.day_S);
            db.SaveChanges();





            ViewBag.tid = timeTableView.timeTable.id;



            return View();
        }

        public ActionResult CreateCEdit(int id)
        {

            ViewBag.tid = id;

            return View();
        }

        [HttpPost]
        public JsonResult InsertP(periods periods)
        {

            if (periods.id != 0)
            {
                db.Update(periods);
                db.SaveChanges();
            }

            return Json(periods/*,JsonRequestBehavior.AllowGet*/);
        }


        public ActionResult UpdateManual(int id)
        {
            Models.ModeView.TimeTableView timeTableView = new Models.ModeView.TimeTableView();
            timeTableView.timeTable = db.TimeTable.Where(t => t.id == id).FirstOrDefault();
            timeTableView.day_S = db.day_s.Where(t => t.T_id == id).FirstOrDefault();
            timeTableView.periods = db.periods.Where(t => t.T_id == id).ToList();
            timeTableView.tImployees = db.TImployees.Where(p => p.SchID == timeTableView.timeTable.sch_id).ToList();
            timeTableView.tSubject = db.TSubject.ToList();

            var dd = db.TCLassRow.Where(t => t.SchoolID == timeTableView.timeTable.sch_id).ToList().OrderBy(p => p.Rows);
            //  DataTable dt = fun.fireDataTable("select * from TCLassRow where SchoolID = "+ timeTableView.timeTable.sch_id + " order by [Rows]  ");

            List<string> te = new List<string>();
            foreach (var we in dd)
            {
                for (int i = 1; i <= we.CLass; i++)
                {
                    te.Add(i.ToString() + "/" + we.Rows.ToString());

                }

            }


            ViewBag.classes = te;
            ViewBag.days = sumTrue(timeTableView.day_S);
            ViewBag.subject = new SelectList(db.TSubject, "SubID", "Subject");
            ViewBag.teacher = new SelectList(db.TImployees.Where(t => t.SchID == timeTableView.timeTable.sch_id), "Identity", "name1");
            return View(timeTableView);
        }

        public ActionResult showTimeTableReport(int id)

        {
            var header = Path.GetFileName("~/static/headerSchoolEmpwork.html");
            var footer = Path.GetFileName("~/static/footer.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);


            //var pdf = new ViewAsPdf("Reportempwork1", dt)
            //{

            //    CustomSwitches = customSwitches,

            //    PageMargins = new Rotativa.Options.Margins(40, 10, 15, 10),
            //    PageOrientation = Rotativa.Options.Orientation.Landscape,
            //    PageSize = Rotativa.Options.Size.A4
            //};


            //return new ActionAsPdf(
            //    "ShowTable",
            //    new { id = id })
            //{
            //    PageMargins = new Rotativa.Options.Margins(10, 2, 2, 10),
            //    PageSize = Rotativa.Options.Size.A2,
            //    PageOrientation = Rotativa.Options.Orientation.Landscape,

            //    FileName = "الجدول العام.pdf" };
            return View();
            //Can Be Resolve In Any Time
        }

        public ActionResult ShowTable(int id)
        {
            Models.ModeView.TimeTableView timeTableView = new Models.ModeView.TimeTableView();
            timeTableView.timeTable = db.TimeTable.Where(t => t.id == id).FirstOrDefault();
            timeTableView.day_S = db.day_s.Where(t => t.T_id == id).FirstOrDefault();
            timeTableView.periods = db.periods.Where(t => t.T_id == id).ToList();
            timeTableView.tImployees = db.TImployees.ToList();
            timeTableView.tSubject = db.TSubject.ToList();

            var dd = db.TCLassRow.Where(t => t.SchoolID == timeTableView.timeTable.sch_id).ToList().OrderBy(p => p.Rows);

            //  DataTable dt = fun.fireDataTable("select * from TCLassRow where SchoolID = " + timeTableView.timeTable.sch_id + " order by [Rows]  ");

            List<string> te = new List<string>();
            foreach (var we in dd)
            {
                for (int i = 1; i <= we.CLass; i++)
                {
                    te.Add(i.ToString() + "/" + we.Rows.ToString());

                }

            }

            DataRow dr = fun.fireDataRow("select School from TSchools where SchoolID = " + timeTableView.timeTable.sch_id + "");
            ViewBag.Sname = dr["School"].ToString();
            ViewBag.classes = te;
            ViewBag.days = sumTrue(timeTableView.day_S);
            ViewBag.subject = new SelectList(db.TSubject, "SubID", "Subject");
            ViewBag.teacher = new SelectList(db.TImployees.Where(t => t.SchID == timeTableView.timeTable.sch_id), "Identity", "name1");

            CultureInfo arSA = new CultureInfo("ar-SA");
            arSA.DateTimeFormat.Calendar = new HijriCalendar();
            string dttt = timeTableView.timeTable.cr_date.Value.ToString("dd/MM/yyyy");

            ViewBag.Cdate = dttt;
            return View(timeTableView);
        }




        [HttpPost]
        public JsonResult DeleteM(periods periods)
        {
            if (periods.id != 0)
            {

                fun.fireSQL("UPDATE periods SET day_id = NULL , subject_id = NULL ,emp_id = NULL ,Tnum = NULL ,Trow = NULL WHERE id = " + periods.id + "");
            }

            return Json(periods/*, JsonRequestBehavior.AllowGet*/);
        }

        public JsonResult deleteTable(int id)
        {
            fun.fireSQL("DELETE FROM periods where T_id = " + id + "");
            fun.fireSQL("DELETE FROM day_s where T_id = " + id + "");
            fun.fireSQL("DELETE FROM TimeTable where id = " + id + "");

            return Json(null);
        }

        // change active table
        public JsonResult ChangeActive(int id)
        {
            if (id.ToString() != null)
            {

                fun.fireSQL("UPDATE TimeTable set caseA = 0 ");
                fun.fireSQL("UPDATE TimeTable set caseA = 1 where id = " + id + " ");

            }
            return Json(null);
        }

        public ActionResult PrintPage()
        {


            return View();

        }
        public ActionResult PrintPageTable(int id)
        {
            Models.ModeView.Schools scl = new Models.ModeView.Schools();
            scl.ttimetable = db.TimeTable.Where(t => t.sch_id == id);

            return View(scl);

        }

        //teachar TimeTable
        public ActionResult printTeachA(string id)
        {
            if (id != null)
            {
                string tid = "1";
                TimeTableView timeTableView = new TimeTableView();

                List<TeachReport> list = new List<TeachReport>();

                DataTable dtable = fun.fireStoreProcedure1("TeachReport", id);
                foreach (DataRow sdr in dtable.Rows)
                {
                    TeachReport teach = new TeachReport();

                    teach.id = sdr["id"].ToString();
                    teach.t_id = sdr["t_id"].ToString();
                    teach.class_id = sdr["class_id"].ToString();
                    teach.dayname = sdr["dayname"].ToString();
                    teach.day_id = Convert.ToInt32(sdr["day_id"].ToString());
                    teach.period_id = sdr["period_id"].ToString();
                    teach.Subject = sdr["Subject"].ToString();
                    teach.daynum = Convert.ToInt32(sdr["daynum"].ToString());
                    teach.pernum = Convert.ToInt32(sdr["pernum"].ToString());
                    tid = sdr["t_id"].ToString();
                    list.Add(teach);
                }

                //timeTableView.teachReports = list;
                //

                DataRow dataRow = fun.fireDataRow("select ts.School, concat(ti.name1,' ',ti.name2,' ',ti.name3,' ',ti.FName) as fullName  from TImployees ti " +
" inner join TSchools ts on ti.SchId = ts.SchoolID " +
"where ti.[Identity] = '" + id + "' ");
                ViewBag.name = dataRow["fullName"].ToString();
                ViewBag.School = dataRow["School"].ToString();
                return View(timeTableView);
            }

            else
            {
                return View();
            }
        }


        public ActionResult showTeachReport(int id)

        {
            var header = Path.GetFileName("~/static/headerSchoolEmpwork.html");
            var footer = Path.GetFileName("~/static/footer.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);


            //var pdf = new ViewAsPdf("Reportempwork1", dt)
            //{

            //    CustomSwitches = customSwitches,

            //    PageMargins = new Rotativa.Options.Margins(40, 10, 15, 10),
            //    PageOrientation = Rotativa.Options.Orientation.Landscape,
            //    PageSize = Rotativa.Options.Size.A4
            //};


            //return new ActionAsPdf(
            //    "printTeachA",
            //    new { id = id })
            //{
            //    PageMargins = new Rotativa.Options.Margins(10, 2, 2, 10),
            //    PageSize = Rotativa.Options.Size.A4,
            //    PageOrientation = Rotativa.Options.Orientation.Landscape,

            //    FileName = "الجدول معلم.pdf"
            //};
            return View();
            //Can Be Resolve In Any Time
        }

        public ActionResult TeachReport()
        {


            return View();
        }


        public ActionResult IndexTeach()
        {
            List<School> list = new List<School>();

            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }


            return View(list);
        }

        public ActionResult SchoolTeacher(int id)
        {
            DataTable dt = fun.fireStoreProcedure3("TimeTableAuto", id.ToString(), "5", "8");
            List<DesForTime> ListDf = new List<DesForTime>();
            foreach (DataRow dr in dt.Rows)
            {
                DesForTime dsf = new DesForTime();
                dsf.empId = dr["empId"].ToString();
                dsf.Teacher = dr["TEACHAR"].ToString();
                dsf.suID = dr["suID"].ToString();
                dsf.subj = dr["subj"].ToString();
                dsf.stat = dr["stat"].ToString();
                dsf.Reduce = dr["Reduce"].ToString();
                dsf.def = dr["def"].ToString();
                dsf.addclass = dr["addClass"].ToString();
                dsf.addSub = dr["addSubClass"].ToString();
                ListDf.Add(dsf);
            }

            CultureInfo cultures = CultureInfo.CreateSpecificCulture("ar-SA");

            Models.ModeView.Schools ischools = new Models.ModeView.Schools();
            ischools.tSchools = db.TSchools.Include(t => t.T_area).Include(t => t.T_Center).Include(t => t.TBuilding).Include(t => t.TLevel).Include(t => t.TSchoolsType).Include(t => t.TSector).Include(t => t.Tuition1).Where(t => t.Center == 14).FirstOrDefault();
            ischools.tCLassRow = db.TCLassRow.Include(t => t.TLevel_Row).Where(t => t.SchoolID == id).ToList();
            //  ischools.ttimetable = db.TimeTable.Include(t => t.TImployees).Where(t => t.sch_id == id).ToList();
            ischools.desForTime = ListDf;
            ViewBag.cClass = db.TCLassRow.Where(t => t.SchoolID == id).Sum(t => t.CLass);
            ViewBag.cStud = db.TCLassRow.Where(t => t.SchoolID == id).Sum(t => t.pupil);
            ischools.Tsubjects = db.TSubject.ToList();
            ischools.reducedTypes = db.ReducedTypes.ToList();
            string text = DateTime.Now.ToString("yyyy -MMMM -dd", cultures);


            return View(ischools);


        }

        // Subject Changed
        [HttpGet]
        public JsonResult SubjectChanged(subjectChangeT sch)
        {
            //Can Be Resolve In Any Time

            var dst = new Stack();// db.SubjectChangeTimeTable(sch.subid, sch.schid, sch.ttid, sch.perid, sch.dyid);             //Can Be Resolve In Any Time stack adding new

            if (dst != null)
            {

                return Json(dst/*, JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                return Json(null);

            }

        }
       // [Authorize(Roles = "TimeTableManager")]
        public ActionResult ShowTablesMannager()
        {
            string id = "10141";// sc.DecParametar(Request.Cookies["main"]["schid"]);
            DataTable dt = fun.fireStoreProcedure3("TimeTableAuto", id, "5", "8");
            List<DesForTime> ListDf = new List<DesForTime>();
            foreach (DataRow dr in dt.Rows)
            {
                DesForTime dsf = new DesForTime();
                dsf.empId = dr["empId"].ToString();
                dsf.Teacher = dr["TEACHAR"].ToString();
                dsf.suID = dr["suID"].ToString();
                dsf.subj = dr["subj"].ToString();
                dsf.stat = dr["stat"].ToString();
                dsf.Reduce = dr["Reduce"].ToString();
                dsf.def = dr["def"].ToString();
                dsf.addclass = dr["addClass"].ToString();
                dsf.addSub = dr["addSubClass"].ToString();
                ListDf.Add(dsf);
            }
            DataTable dt1 = fun.fireStoreProcedure1("DesIntable", id.ToString());

            List<DesForTime> ListDf1 = new List<DesForTime>();
            foreach (DataRow dr1 in dt1.Rows)
            {
                DesForTime dsf1 = new DesForTime();
                dsf1.empId = dr1["ID"].ToString();
                dsf1.subj = dr1["SUB"].ToString();
                dsf1.stat = dr1["dess"].ToString();
                ListDf1.Add(dsf1);
            }


            CultureInfo cultures = CultureInfo.CreateSpecificCulture("ar-SA");
            Models.ModeView.Schools ischools = new Models.ModeView.Schools();
            int? id1 = Convert.ToInt32(id);

            ischools.tSchools = db.TSchools.Include(t => t.T_area).Include(t => t.T_Center).Include(t => t.TBuilding).Include(t => t.TLevel).Include(t => t.TSchoolsType).Include(t => t.TSector).Include(t => t.Tuition1).Where(t => t.SchoolID == id1).FirstOrDefault();
            ischools.tCLassRow = db.TCLassRow.Include(t => t.TLevel_Row).Where(t => t.SchoolID == id1).ToList();
            ischools.ttimetable = db.TimeTable.Where(t => t.sch_id == id1).ToList();
            ischools.desForTime = ListDf;
            ischools.desTimeTable = ListDf1;
            ViewBag.cClass = db.TCLassRow.Where(t => t.SchoolID == id1).Sum(t => t.CLass);
            ViewBag.cStud = db.TCLassRow.Where(t => t.SchoolID == id1).Sum(t => t.pupil);
            ischools.Tsubjects = db.TSubject.ToList();
            ischools.reducedTypes = db.ReducedTypes.ToList();
            string text = DateTime.Now.ToString("yyyy -MMMM -dd", cultures);

            ViewBag.adela = db.adela.Where(p => p.adServ == 3).ToList();
            return View(ischools);
        }
        public ActionResult showTeachReportE()
        {
            string id = "1000116937"; //sc.DecParametar(User.Identity.Name);
            var header = Path.GetFileName("~/static/headerSchoolEmpwork.html");
            var footer = Path.GetFileName("~/static/footer.html");
            string customSwitches = string.Format("--header-html  \"{0}\" " +
                        "--header-spacing \"8\" " +
                        "--header-font-name \"Open Sans\" " +
                        "--footer-font-size \"8\" " +
                        "--footer-font-name \"Open Sans\" " +
                        "--header-font-size \"10\" " +
                        //"--footer-right \"Pag: [page] de [toPage]\""+
                        "--footer-html  \"{1}\" ", header, footer);


            //var pdf = new ViewAsPdf("Reportempwork1", dt)
            //{

            //    CustomSwitches = customSwitches,

            //    PageMargins = new Rotativa.Options.Margins(40, 10, 15, 10),
            //    PageOrientation = Rotativa.Options.Orientation.Landscape,
            //    PageSize = Rotativa.Options.Size.A4
            //};


            //return new ActionAsPdf(
            //    "printTeachA",
            //    new { id = id })
            //{
            //    PageMargins = new Rotativa.Options.Margins(10, 2, 2, 10),
            //    PageSize = Rotativa.Options.Size.A4,
            //    PageOrientation = Rotativa.Options.Orientation.Landscape,

            //    FileName = "جدول معلم.pdf"
            //};

            return View();            //Can Be Resolve In Any Time

        }

        // times ***************************************************************************
        [HttpPost]
        // add Times in custom table
        public JsonResult addtimes(TTDur tTDur)
        {

            if (tTDur.periodt.Length > 0 && tTDur.startH > 0 && tTDur.endH > 0)
            {
                var td = db.TTDur.Where(p => p.tid == tTDur.tid).Max(p => p.idC) + 1;
                if (td == null) { td = 1; }
                TTDur tT = new TTDur();
                tT.idC = td;
                tT.periodt = tTDur.periodt;
                tT.startH = tTDur.startH;
                tT.startM = tTDur.startM;
                tT.endH = tTDur.endH;
                tT.endM = tTDur.endM;
                tT.teach = tTDur.teach;
                tT.tid = tTDur.tid;
                db.TTDur.Add(tT);
                db.SaveChanges();
            }
            var ttdr = db.TTDur.Where(p => p.tid == tTDur.tid);

            ViewBag.tid = tTDur.tid;
            return Json(ttdr/*,JsonRequestBehavior.AllowGet*/);

        }

        [HttpPost]
        // update Times in custom table
        public JsonResult UpdateTimes(TTDur tTDur)
        {

            var td = db.TTDur.Where(p => p.tid == tTDur.tid);

            var tT = db.TTDur.Where(p => p.id == tTDur.id).FirstOrDefault();
            tT.idC = tTDur.idC;
            tT.periodt = tTDur.periodt;
            tT.startH = tTDur.startH;
            tT.startM = tTDur.startM;
            tT.endH = tTDur.endH;
            tT.endM = tTDur.endM;
            tT.teach = tTDur.teach;
            tT.tid = tTDur.tid;
            db.Update(tT);
            db.SaveChanges();
            var ttdr = db.TTDur.Where(p => p.tid == tTDur.tid);
            ViewBag.tid = tTDur.tid;
            return Json(ttdr/*,JsonRequestBehavior.AllowGet*/);
        }


        [HttpPost]
        // update Times in custom table
        public JsonResult DeleteTimes(TTDur tTDur)
        {


            var tT = db.TTDur.Where(p => p.id == tTDur.id).FirstOrDefault();
            db.TTDur.Remove(tT);
            db.SaveChanges();
            var ttdr = db.TTDur.Where(p => p.tid == tTDur.tid);
            ViewBag.tid = tTDur.tid;
            return Json(ttdr/*, JsonRequestBehavior.AllowGet*/);
        }

        // show
        [HttpPost]
        public JsonResult getItems(int id)
        {
            var ttdr = db.TTDur.Where(p => p.tid == id);

            if (ttdr != null)
            {
                ViewBag.tid = id;
                return Json(ttdr/*, JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                ViewBag.tid = id;
                return Json(null);
            }
        }



        // class Shapes *************************************************************************
        // add Times in custom table
        public JsonResult addshapes(TTRowsshape tTRowsshape)
        {

            TTRowsshape tT = new TTRowsshape();
            tT.tid = tTRowsshape.tid;
            tT.shapesss = tTRowsshape.shapesss;
            tT.rowss = tTRowsshape.rowss;
            tT.@class = tTRowsshape.@class;
            db.TTRowsshape.Add(tT);
            db.SaveChanges();



            ViewBag.tid = tTRowsshape.tid;
            return Json(null);

        }

        [HttpPost]
        // update Times in custom table
        public JsonResult Updateshapes(TTRowsshape tTRowsshape)
        {

            var tT = db.TTRowsshape.Where(p => p.id == tTRowsshape.id).FirstOrDefault();

            tT.shapesss = tTRowsshape.shapesss;
            tT.rowss = tTRowsshape.rowss;
            tT.@class = tTRowsshape.@class;
            db.Update(tT);
            db.SaveChanges();

            ViewBag.tid = tTRowsshape.tid;
            return Json(null);
        }


        [HttpPost]
        // update Times in custom table
        public JsonResult Deleteshapes(TTRowsshape tTRowsshape)
        {

            var tT = db.TTRowsshape.Where(p => p.id == tTRowsshape.id).FirstOrDefault();
            db.TTRowsshape.Remove(tT);
            db.SaveChanges();

            ViewBag.tid = tTRowsshape.tid;
            return Json(null);
        }

        // show
        [HttpPost]
        public JsonResult getshapes(int id)
        {
            var ttdr = db.TTRowsshape.Where(p => p.tid == id);

            if (ttdr != null)
            {
                ViewBag.tid = id;
                return Json(ttdr/*, JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                ViewBag.tid = id;
                return Json(null);
            }

        }

        // color  *************************************************************************
        // show
        // add Times in custom table
        public JsonResult addColor(TTColor tTColor)
        {

            TTColor tT = new TTColor();
            tT.tid = tTColor.tid;
            tT.subID = tTColor.subID;
            tT.Scolor = tTColor.Scolor;

            db.TTColor.Add(tT);
            db.SaveChanges();

            ViewBag.tid = tTColor.tid;
            return Json(null);

        }

        [HttpPost]
        // update Times in custom table
        public JsonResult UpdateColor(TTColor tTColor)
        {
            var tT = db.TTColor.Where(p => p.id == tTColor.id).FirstOrDefault();
            tT.tid = tTColor.tid;
            tT.subID = tTColor.subID;
            tT.Scolor = tTColor.Scolor;
            db.Update(tT);
            db.SaveChanges();


            ViewBag.tid = tTColor.tid;
            return Json(null);
        }


        [HttpPost]
        // update Times in custom table
        public JsonResult DeleteColor(TTColor tTColor)
        {

            var tT = db.TTColor.Where(p => p.id == tTColor.id).FirstOrDefault();
            db.TTColor.Remove(tT);
            db.SaveChanges();

            ViewBag.tid = tTColor.tid;
            return Json(null);
        }

        // show
        [HttpPost]
        public JsonResult getColor(int id)
        {
            var ttdr = db.TTColor.Where(p => p.tid == id);
            if (ttdr != null)
            {
                ViewBag.tid = id;
                return Json(ttdr/*, JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                ViewBag.tid = id;
                return Json(null);
            }

        }






        //    looooooooooooood dorpdown ////////////////////////////////////////////////////////////////////////////




        [HttpGet]
        public JsonResult getsubject()
        {
            var ss = db.TSubject.ToList();
            var cls = new List<SelectListItem>();
            foreach (var sd in ss)
            { cls.Add(new SelectListItem { Text = sd.SubID.ToString(), Value = sd.Subject }); }

            return Json(cls/*, JsonRequestBehavior.AllowGet*/);
        }

        [HttpGet]
        public JsonResult getteachar(int id)
        {
            var tteach = db.TTTeacharCustom.Where(p => p.tid == id).ToList();
            if (tteach.Count == 0)
            {
                var sc = db.TimeTable.Where(p => p.id == id).FirstOrDefault();
                fun.fireStoreProcedure2("ALLTEACHARS", sc.sch_id.ToString(), id.ToString());

                tteach = db.TTTeacharCustom.Where(p => p.tid == id).ToList();
            }


            return Json(tteach/*, JsonRequestBehavior.AllowGet*/);
        }

        [HttpGet]
        public JsonResult getRestriction(restrictionData restrictionData)
        {

            if (restrictionData.tid != null && restrictionData.eid != null)
            {
                int td = Convert.ToInt32(restrictionData.tid);
                int ed = Convert.ToInt32(restrictionData.eid);

                var chk = db.restriction.Where(p => p.tid == td && p.empid == ed).FirstOrDefault();
                if (chk == null)
                {
                    fun.fireStoreProcedure2("resInser", restrictionData.tid, restrictionData.eid);
                }

                var cnum = db.TimeTable.Where(p => p.id == td).FirstOrDefault();
                string th = "";
                string td1 = "";
                string stloop = "";
                string rsloop = "";
                string rwloop = "";
                string col = "";
                for (int i = 1; i <= cnum.per_num; i++)
                {
                    stloop = stloop + " <th class=\"reshead\" alt=\" " + i + " \">" + i + "</th> ";

                }
                th = "<tr style=\"background-color: antiquewhite; \">" + "<th></th>" + stloop + "</tr>";
                for (int y = 1; y <= cnum.day_num; y++)
                {
                    var dname = db.daysName.Where(p => p.id == y).FirstOrDefault();
                    td1 = "<td class=\"resDays\" alt=\"" + y + "\" style=\"background-color: antiquewhite; \">" + dname.dayname + "</td>";
                    var rs = db.restriction.Where(p => p.tid == td && p.dayid == y && p.empid == ed);
                    rsloop = "";
                    foreach (var e in rs)
                    {
                        string ss = "<img class=\"chk\" src=\"../../images/res/" + e.stutas1 + ".png\" style=\"width: 30px; height: 30px; \" placeholder=\"" + e.stutas1 + "\" alt=\"" + e.id + "\">";
                        rsloop = rsloop + "<td>" + ss + "</td>";

                    }
                    rwloop = rwloop + "<tr>" + td1 + rsloop + "</tr>";

                }

                col = th + rwloop;

                return Json(col/*, JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                return Json(null);
            }



        }

        [HttpGet]
        public JsonResult updateRestriction(restrictionData restrictionData)
        {

            if (restrictionData.tid != null && restrictionData.eid != null)
            {
                int td = Convert.ToInt32(restrictionData.eid); // id restriction
                var rss = db.restriction.Where(p => p.id == td).FirstOrDefault();
                switch (rss.stutas1)
                {
                    case 1:
                        rss.stutas1 = 2;
                        break;
                    case 2:
                        rss.stutas1 = 3;
                        break;
                    case 3:
                        rss.stutas1 = 1;
                        break;

                }

                db.Update(rss);
                db.SaveChanges();
                var cnum = db.TimeTable.Where(p => p.id == rss.tid).FirstOrDefault();
                string th = "";
                string td1 = "";
                string stloop = "";
                string rsloop = "";
                string rwloop = "";
                string col = "";
                for (int i = 1; i <= cnum.per_num; i++)
                {
                    stloop = stloop + " <th class=\"reshead\" alt=\" " + i + " \">" + i + "</th> ";

                }
                th = "<tr style=\"background-color: antiquewhite; \">" + "<th></th>" + stloop + "</tr>";
                for (int y = 1; y <= cnum.day_num; y++)
                {
                    var dname = db.daysName.Where(p => p.id == y).FirstOrDefault();
                    td1 = "<td class=\"resDays\" alt=\"" + y + "\" style=\"background-color: antiquewhite; \">" + dname.dayname + "</td>";
                    var rs = db.restriction.Where(p => p.tid == rss.tid && p.dayid == y && p.empid == rss.empid);
                    rsloop = "";
                    foreach (var e in rs)
                    {
                        string ss = "<img class=\"chk\" src=\"../../images/res/" + e.stutas1 + ".png\" style=\"width: 30px; height: 30px; \" placeholder=\"" + e.stutas1 + "\" alt=\"" + e.id + "\">";
                        rsloop = rsloop + "<td>" + ss + "</td>";

                    }
                    rwloop = rwloop + "<tr>" + td1 + rsloop + "</tr>";

                }

                col = th + rwloop;

                return Json(col/*, System.Web.Mvc.JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                return Json(null);
            }



        }

        [HttpGet]
        public JsonResult updateHeadRestriction(restrictionData restrictionData)
        {

            if (restrictionData.tid != null && restrictionData.eid != null)
            {
                int td = Convert.ToInt32(restrictionData.tid); // table id
                int per = Convert.ToInt32(restrictionData.eid); // per id
                var rss = db.restriction.Where(p => p.tid == td && p.periodid == per).FirstOrDefault();
                switch (rss.stutas1)
                {
                    case 1:
                        fun.fireSQL("update restriction set stutas1 = 2 where tid = " + td + " and periodid = " + per + "");
                        break;
                    case 2:
                        fun.fireSQL("update restriction set stutas1 = 3 where tid = " + td + " and periodid = " + per + "");
                        break;
                    case 3:
                        fun.fireSQL("update restriction set stutas1 = 1 where tid = " + td + " and periodid = " + per + "");
                        break;

                }



                var cnum = db.TimeTable.Where(p => p.id == td).FirstOrDefault();
                string th = "";
                string td1 = "";
                string stloop = "";
                string rsloop = "";
                string rwloop = "";
                string col = "";
                for (int i = 1; i <= cnum.per_num; i++)
                {
                    stloop = stloop + " <th class=\"reshead\" alt=\" " + i + " \">" + i + "</th> ";

                }
                th = "<tr style=\"background-color: antiquewhite; \">" + "<th></th>" + stloop + "</tr>";
                for (int y = 1; y <= cnum.day_num; y++)
                {
                    var dname = db.daysName.Where(p => p.id == y).FirstOrDefault();
                    td1 = "<td class=\"resDays\" alt=\"" + y + "\" style=\"background-color: antiquewhite; \">" + dname.dayname + "</td>";
                    var rs = db.restriction.Where(p => p.tid == td && p.dayid == y && p.empid == rss.empid);
                    rsloop = "";
                    foreach (var e in rs)
                    {
                        string ss = "<img class=\"chk\" src=\"../../images/res/" + e.stutas1 + ".png\" style=\"width: 30px; height: 30px; \" placeholder=\"" + e.stutas1 + "\" alt=\"" + e.id + "\">";
                        rsloop = rsloop + "<td>" + ss + "</td>";

                    }
                    rwloop = rwloop + "<tr>" + td1 + rsloop + "</tr>";

                }

                col = th + rwloop;

                return Json(col/*, JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                return Json(null);
            }



        }


        [HttpGet]
        public JsonResult updateDaysRestriction(restrictionData restrictionData)
        {

            if (restrictionData.tid != null && restrictionData.eid != null)
            {
                int td = Convert.ToInt32(restrictionData.tid); // table id
                int dy = Convert.ToInt32(restrictionData.eid); // day id
                var rss = db.restriction.Where(p => p.tid == td && p.dayid == dy).FirstOrDefault();
                switch (rss.stutas1)
                {
                    case 1:
                        fun.fireSQL("update restriction set stutas1 = 2 where tid = " + td + " and dayid = " + dy + "");
                        break;
                    case 2:
                        fun.fireSQL("update restriction set stutas1 = 3 where tid = " + td + " and dayid = " + dy + "");
                        break;
                    case 3:
                        fun.fireSQL("update restriction set stutas1 = 1 where tid = " + td + " and dayid = " + dy + "");
                        break;

                }



                var cnum = db.TimeTable.Where(p => p.id == td).FirstOrDefault();
                string th = "";
                string td1 = "";
                string stloop = "";
                string rsloop = "";
                string rwloop = "";
                string col = "";
                for (int i = 1; i <= cnum.per_num; i++)
                {
                    stloop = stloop + " <th class=\"reshead\" alt=\" " + i + " \">" + i + "</th> ";

                }
                th = "<tr style=\"background-color: antiquewhite; \">" + "<th></th>" + stloop + "</tr>";
                for (int y = 1; y <= cnum.day_num; y++)
                {
                    var dname = db.daysName.Where(p => p.id == y).FirstOrDefault();
                    td1 = "<td class=\"resDays\" alt=\"" + y + "\" style=\"background-color: antiquewhite; \">" + dname.dayname + "</td>";
                    var rs = db.restriction.Where(p => p.tid == td && p.dayid == y && p.empid == rss.empid);
                    rsloop = "";
                    foreach (var e in rs)
                    {
                        string ss = "<img class=\"chk\" src=\"../../images/res/" + e.stutas1 + ".png\" style=\"width: 30px; height: 30px; \" placeholder=\"" + e.stutas1 + "\" alt=\"" + e.id + "\">";
                        rsloop = rsloop + "<td>" + ss + "</td>";

                    }
                    rwloop = rwloop + "<tr>" + td1 + rsloop + "</tr>";

                }

                col = th + rwloop;

                return Json(col/*, JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                return Json(null);
            }



        }



        //show custom teachar
        [HttpGet]
        public JsonResult getcustom(restrictionData restrictionData)
        {

            if (restrictionData.tid != null && restrictionData.eid != null)
            {
                int td = Convert.ToInt32(restrictionData.tid);
                int ed = Convert.ToInt32(restrictionData.eid);
                var chk = db.TTteachClassess.Where(p => p.tid == td && p.empid == ed);

                return Json(chk/*, JsonRequestBehavior.AllowGet*/);
            }
            else
            {
                return Json(null);
            }



        }

        // get cweek
        [HttpGet]
        public JsonResult getcweek()
        {
            var cls = new List<SelectListItem>();
            for (int i = 1; i <= 40; i++)
            { cls.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }

            return Json(cls/*, JsonRequestBehavior.AllowGet*/);
        }

        // get sub
        [HttpGet]
        public JsonResult getsub(int tid)
        {

            DataTable dt = fun.fireDataTable("select TTColor.subID,ts.[Subject] as sub from TTColor " +
            "inner join TSubject ts on TTColor.subID = ts.SubID " +
            "where TTColor.tid = " + tid + " ");
            var cls = new List<SelectListItem>();
            foreach (DataRow sd in dt.Rows)
            { cls.Add(new SelectListItem { Text = sd["subID"].ToString(), Value = sd["sub"].ToString() }); }

            return Json(cls/*, JsonRequestBehavior.AllowGet*/);
        }

        // get classess
        [HttpGet]
        public JsonResult getClassess(int tid)
        {

            DataTable dt = fun.fireDataTable("select * from TTRowsshape " +
            "where TTRowsshape.tid = " + tid + "" +
            "order by rowss,class");
            var cls = new List<SelectListItem>();
            foreach (DataRow sd in dt.Rows)
            { cls.Add(new SelectListItem { Text = sd["id"].ToString(), Value = sd["shapesss"].ToString() }); }

            return Json(cls/*, JsonRequestBehavior.AllowGet*/);
        }

        // get class type مفرد - مزدوج
        [HttpGet]
        public JsonResult getClssTpe()
        {

            DataTable dt = fun.fireDataTable("select * from classTpe");
            var cls = new List<SelectListItem>();
            foreach (DataRow sd in dt.Rows)
            { cls.Add(new SelectListItem { Text = sd["id"].ToString(), Value = sd["cltyype"].ToString() }); }

            return Json(cls/*, JsonRequestBehavior.AllowGet*/);
        }


        // add Times in custom table
        public JsonResult addcustom(TTteachClassess tteach)
        {

            TTteachClassess tT = new TTteachClassess();
            tT.tid = tteach.tid;
            tT.subId = tteach.subId;
            tT.empid = tteach.empid;
            tT.clsType = tteach.clsType;
            tT.clsinweek = tteach.clsinweek;
            tT.classid = tteach.classid;
            db.TTteachClassess.Add(tT);
            db.SaveChanges();
            ViewBag.tid = tteach.tid;
            return Json(null);

        }

        [HttpPost]
        // update Times in custom table
        public JsonResult Updatecustom(TTteachClassess tteach)
        {
            var tT = db.TTteachClassess.Where(p => p.id == tteach.id).FirstOrDefault();

            tT.subId = tteach.subId;
            tT.clsType = tteach.clsType;
            tT.clsinweek = tteach.clsinweek;
            tT.classid = tteach.classid;
            db.Update(tT);
            db.SaveChanges();

            ViewBag.tid = tteach.tid;
            return Json(null);
        }


        [HttpPost]
        // update Times in custom table
        public JsonResult Deletecustom(TTteachClassess tteach)
        {

            var tT = db.TTteachClassess.Where(p => p.id == tteach.id).FirstOrDefault();
            db.TTteachClassess.Remove(tT);
            db.SaveChanges();

            ViewBag.tid = tteach.tid;
            return Json(null);
        }

        [HttpGet]
        public JsonResult getSubTeach(int id)
        {
            var tteach = db.TTTeacharCustom.Where(p => p.tid == id).ToList();
            if (tteach.Count == 0)
            {
                var sc = db.TimeTable.Where(p => p.id == id).FirstOrDefault();
                fun.fireStoreProcedure2("ALLTEACHARS", sc.sch_id.ToString(), id.ToString());

                tteach = db.TTTeacharCustom.Where(p => p.tid == id).ToList();
            }
            var cls = new List<SelectListItem>();
            foreach (var sd in tteach)
            { cls.Add(new SelectListItem { Text = sd.id.ToString(), Value = sd.empName }); }

            return Json(cls/*, JsonRequestBehavior.AllowGet*/);
        }

        [HttpGet]
        public JsonResult getSupervisorsjob()
        {
            var tteach = db.superJob.ToList();
            var cls = new List<SelectListItem>();
            foreach (var sd in tteach)
            { cls.Add(new SelectListItem { Text = sd.id.ToString(), Value = sd.sjob }); }

            return Json(cls/*, JsonRequestBehavior.AllowGet*/);
        }

        [HttpGet]
        public JsonResult getSupervisorsdays()
        {
            var tteach = db.daysName.ToList();
            var cls = new List<SelectListItem>();
            foreach (var sd in tteach)
            { cls.Add(new SelectListItem { Text = sd.id.ToString(), Value = sd.dayname }); }

            return Json(cls/*, JsonRequestBehavior.AllowGet*/);
        }


        [HttpGet]
        public JsonResult getsupervisors(int id)
        {
            var sp = db.supervisors.Where(p => p.tid == id);

            return Json(sp/*, JsonRequestBehavior.AllowGet*/);
        }

        [HttpPost]
        // add Times in custom table
        public JsonResult addsuper(supervisors supervisors)
        {

            supervisors tT = new supervisors();
            tT.tid = supervisors.tid;
            tT.supervisor = supervisors.supervisor;
            tT.empid = supervisors.empid;
            tT.dayid = supervisors.dayid;
            db.supervisors.Add(tT);
            db.SaveChanges();



            ViewBag.tid = supervisors.tid;
            return Json(null);

        }

        [HttpPost]
        // update Times in custom table
        public JsonResult Updatesuper1(supervisors tteach)
        {

            var tT = db.supervisors.Where(p => p.id == tteach.id).FirstOrDefault();

            tT.supervisor = tteach.supervisor;
            tT.empid = tteach.empid;
            tT.dayid = tteach.dayid;

            db.Update(tT);
            db.SaveChanges();

            ViewBag.tid = tteach.tid;
            return Json(null);
        }


        [HttpPost]
        // update Times in custom table
        public JsonResult Deletesuper(supervisors tteach)
        {

            var tT = db.supervisors.Where(p => p.id == tteach.id).FirstOrDefault();
            db.supervisors.Remove(tT);
            db.SaveChanges();

            ViewBag.tid = tteach.tid;
            return Json(null);
        }

    }
}