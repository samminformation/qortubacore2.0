﻿using qortuba.Data;
using qortuba.Models;
using System.Collections.Generic;
using System.Data;
using System;
using System.Linq;
using System.Globalization;
using System.IO;
using qortuba.Models.ModeView;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace qortuba.Controllers
{
    //[Authorize] 
    public class DelegationController : Controller
    {
        private readonly ApplicationDbContext db;
       Models.Class.MyFunctions fun = new Models.Class.MyFunctions();

        public DelegationController(ApplicationDbContext context)
        {
            db = context;
        }
        // GET: Delegation
        //[Authorize(Roles = "firstDel")]
        public ActionResult Index()
        {

            List<School> list = new List<School>();

            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }


            return View(list);


        }
        public ActionResult IndexAccept(int id)
        {

            List<Delegation> list = new List<Delegation>();

            DataTable dtable = fun.fireDataTable("select * from Delegation where TSchoolId = " + id + " and DelState = 2 ");
            foreach (DataRow sdr in dtable.Rows)
            {
                Delegation sch = new Delegation();

                sch.EmpId = Convert.ToInt32(sdr["EmpId"].ToString());
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.FSchoolId = Convert.ToInt32(sdr["FSchoolId"].ToString());
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.FEdCircle = sdr["FEdCircle"].ToString();

                list.Add(sch);
            }


            return View(list);

        }
        public ActionResult TerminalAccept(int id)
        {

            List<DelegateShow> list = new List<DelegateShow>();

            DataTable dtable = fun.fireDataTable("select EmpId,EmpName,SubName,FSchoolName,TSchoolName,ISNULL(StateAfter,0) as StateAfter,convert(char(10),cast(FirstDeleDate as datetime),131) as FirstDeleDate, "
+ " CASE "
+ " WHEN ReqDelDate = '1900-01-01' or ReqDelDate = '' THEN '' "
+ " ELSE convert(char(10), cast(ReqDelDate as datetime), 131) "
+ " END as ReqDelDate "
+ " from Delegation where FSchoolId = " + id + " and DelState = 2 ");
            foreach (DataRow sdr in dtable.Rows)
            {
                DelegateShow sch = new DelegateShow();
                sch.EmpId = sdr["EmpId"].ToString();
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.TSchoolName = sdr["TSchoolName"].ToString();
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.FirstDeleDate = sdr["FirstDeleDate"].ToString();
                sch.ReqDelDate = sdr["ReqDelDate"].ToString();
                sch.StateAfter = Convert.ToInt32(sdr["StateAfter"].ToString());
                list.Add(sch);
            }


            return View(list);

        }
        [HttpPost]
        public JsonResult excuteTerminal(int id)
        {
            if (id.ToString() != null)
            {
                var dd = db.Delegation.Where(p => p.EmpId == id && p.DelState == 2).FirstOrDefault();


                if (dd.ReqDelDate.Value.Year == 1900)
                {
                    dd.ReqDelDate = DateTime.Now;
                    dd.StateAfter = 2;
                    db.Entry(dd).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();
                }

            }

            return Json(null);
        }
        [HttpPost]
        public JsonResult delTerminal(int id)
        {
            if (id.ToString() != null)
            {
                var dd = db.Delegation.Where(p => p.EmpId == id && p.DelState == 2 && p.StateAfter == 2).FirstOrDefault();

                if (dd.ReqDelDate.Value.Year != 1900)
                {
                    DateTime dt = new DateTime(1900, 01, 01);


                    dd.ReqDelDate = dt;
                    dd.StateAfter = 1;
                    db.Entry(dd).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();
                }

            }

            return Json(null);
        }


        // accept Delegation
        public ActionResult AcceptDelegation(int id)
        {

            List<DelegateShow> list = new List<DelegateShow>();

            DataTable dtable = fun.fireDataTable("select EmpId,EmpName,SubName,FSchoolName,TSchoolName,ISNULL(StateAfter,0) as StateAfter,convert(char(10),cast(FirstDeleDate as datetime),131) as FirstDeleDate,convert(char(10),cast(ReqDelDate as datetime),131) as ReqDelDate , "
+ " CASE "
+ " WHEN AcceptedDelDate = '1900-01-01' or AcceptedDelDate = '' THEN '' "
+ " ELSE convert(char(10), cast(AcceptedDelDate as datetime), 131) "
+ " END as AcceptedDelDate "
+ " from Delegation where TSchoolId = " + id + " and DelState = 2 and StateAfter = 2 or StateAfter = 3");
            foreach (DataRow sdr in dtable.Rows)
            {
                DelegateShow sch = new DelegateShow();
                sch.EmpId = sdr["EmpId"].ToString();
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.TSchoolName = sdr["TSchoolName"].ToString();
                sch.FirstDeleDate = sdr["FirstDeleDate"].ToString();
                sch.ReqDelDate = sdr["ReqDelDate"].ToString();
                sch.AcceptedDelDate = sdr["AcceptedDelDate"].ToString();
                sch.StateAfter = Convert.ToInt32(sdr["StateAfter"].ToString());
                list.Add(sch);
            }


            return View(list);

        }
        [HttpPost]
        public JsonResult excuteAccept(int id)
        {
            if (id.ToString() != null)
            {
                var dd = db.Delegation.Where(p => p.EmpId == id && p.DelState == 2 && p.StateAfter == 2).FirstOrDefault();


                if (dd.AcceptedDelDate.Value.Year == 1900)
                {
                    var eid = db.TImployees.Where(p => p.Identity == id.ToString()).FirstOrDefault();
                    var sid = db.TImployees.Where(p => p.SchID == eid.SchID && p.WID == 1).FirstOrDefault();
                    dd.AcceptedDelDate = DateTime.Now;
                    dd.ManagerTo = sid.name1 + " " + sid.name2 + " " + sid.name3 + " " + sid.FName;
                    dd.StateAfter = 3;
                    db.Entry(dd).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();
                }

            }

            return Json(null);
        }
        [HttpPost]
        public JsonResult delAccept(int id)
        {
            if (id.ToString() != null)
            {
                var dd = db.Delegation.Where(p => p.EmpId == id && p.DelState == 2 && p.StateAfter == 3).FirstOrDefault();

                if (dd.ReqDelDate.Value.Year != 1900)
                {
                    DateTime dt = new DateTime(1900, 01, 01);
                    dd.AcceptedDelDate = dt;
                    dd.StateAfter = 2;
                    db.Entry(dd).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();
                }

            }
            return Json(null);
        }
        [HttpPost]
        public JsonResult DelDelegation(int id)
        {
            if (id.ToString() != null)
            {
                var dd = db.Delegation.Where(p => p.id == id).FirstOrDefault();

                if (dd.id > 0)
                {
                    db.Delegation.Remove(dd);

                    db.SaveChanges();
                }

            }
            return Json(null);
        }
        //[Authorize(Roles = "DisDel")]
        public ActionResult TrueAccept()
        {

            List<DelegateShow> list = new List<DelegateShow>();

            DataTable dtable = fun.fireDataTable("select id,EmpId,EmpName,SubName,DelState,ISNULL(StateAfter,0) as StateAfter,FSchoolId,FSchoolName,FEdCircle,TSchoolId,TSchoolName,TEdCircle,convert(char(10),cast(FirstDeleDate as datetime),131) as FirstDeleDate,convert(char(10),cast(ReqDelDate as datetime),131) as ReqDelDate ,convert(char(10),cast(AcceptedDelDate as datetime),131) as AcceptedDelDate , CONCAT(EndDay,'/',EndMonth,'/',EndYear) as Deldate , "
+ " CASE "
+ " WHEN DelegationDate = '1900-01-01' or DelegationDate = '' THEN '' "
+ " ELSE convert(char(10), cast(DelegationDate as datetime), 131) "
+ " END as DelegationDate, "
+ " CASE "
+ " WHEN goodOrNot = 1  THEN 'المعلم مبادر في الندب ' "
+ " WHEN goodOrNot = 2  THEN 'المعلم لم يبادر في الندب ' "
+ " ELSE 'لم يتم التحديد' "
+ " END as goodOrNot, "
+ " CASE "
+ " WHEN EndDel = '1900-01-01' or EndDel = '' THEN '' "
+ " ELSE convert(char(10), cast(EndDel as datetime), 131) "
+ " END as EndDel "
+ " from Delegation where StateAfter = 3 or StateAfter = 4");
            foreach (DataRow sdr in dtable.Rows)
            {
                DelegateShow sch = new DelegateShow();

                sch.id = sdr["id"].ToString();
                sch.EmpId = sdr["EmpId"].ToString();
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.FSchoolId = sdr["FSchoolId"].ToString();
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.FEdCircle = sdr["FEdCircle"].ToString();
                sch.TSchoolId = sdr["TSchoolId"].ToString();
                sch.TSchoolName = sdr["TSchoolName"].ToString();
                sch.TEdCircle = sdr["TEdCircle"].ToString();
                sch.FirstDeleDate = sdr["FirstDeleDate"].ToString();
                sch.DelegationDate = sdr["DelegationDate"].ToString();
                sch.AcceptedDelDate = sdr["AcceptedDelDate"].ToString();
                sch.ReqDelDate = sdr["ReqDelDate"].ToString();
                sch.goodOrNot = sdr["goodOrNot"].ToString();
                sch.DelState = sdr["DelState"].ToString();

                list.Add(sch);
            }


            return View(list);



        }
        public ActionResult TrueAcceptEx(int id)
        {

            List<DelegateShow> list = new List<DelegateShow>();

            DataTable dtable = fun.fireDataTable("select dg.id,EmpId,EmpName,SubName,DelState,ISNULL(StateAfter,0) as StateAfter,FSchoolId,FSchoolName,FEdCircle,TSchoolId,TSchoolName,TEdCircle,convert(char(10),cast(FirstDeleDate as datetime),131) as FirstDeleDate,convert(char(10),cast(ReqDelDate as datetime),131) as ReqDelDate ,convert(char(10),cast(AcceptedDelDate as datetime),131) as AcceptedDelDate,ISNULL(EndDay,0) as EndDay,ISNULL(EndMonth,0) as EndMonth,ISNULL(EndYear,0) as EndYear,ISNULL(goodOrNot,0) as goodOrNot1,CONCAT(EndDay,'/',EndMonth,'/',EndYear) as Deldate ,DelegationType,DelDays,DelClass, "
+ " CASE "
+ " WHEN DelegationDate = '1900-01-01' or DelegationDate = '' THEN '' "
+ " ELSE convert(char(10), cast(DelegationDate as datetime), 131) "
+ " END as DelegationDate, "
+ " CASE "
+ " WHEN goodOrNot = 1  THEN 'المعلم مبادر في الندب ' "
+ " WHEN goodOrNot = 2  THEN 'المعلم لم يبادر في الندب ' "
+ " ELSE 'لم يتم التحديد' "
+ " END as goodOrNot, "
+ " CASE "
+ " WHEN EndDel = '1900-01-01' or EndDel = '' THEN '' "
+ " ELSE convert(char(10), cast(EndDel as datetime), 131) "
+ " END as EndDel "
+ " from Delegation dg " +
" left join DelegationType dt on dg.DelType = dt.id " +
"where StateAfter = 3 or StateAfter = 4  and dg.id = " + id + "");
            foreach (DataRow sdr in dtable.Rows)
            {
                DelegateShow sch = new DelegateShow();
                sch.id = sdr["id"].ToString();
                sch.EmpId = sdr["EmpId"].ToString();
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.FSchoolId = sdr["FSchoolId"].ToString();
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.FEdCircle = sdr["FEdCircle"].ToString();
                sch.TSchoolId = sdr["TSchoolId"].ToString();
                sch.TSchoolName = sdr["TSchoolName"].ToString();
                sch.TEdCircle = sdr["TEdCircle"].ToString();
                sch.FirstDeleDate = sdr["FirstDeleDate"].ToString();
                sch.DelegationDate = sdr["DelegationDate"].ToString();
                sch.AcceptedDelDate = sdr["AcceptedDelDate"].ToString();
                sch.ReqDelDate = sdr["ReqDelDate"].ToString();
                sch.goodOrNot = sdr["goodOrNot"].ToString();
                sch.DelState = sdr["DelState"].ToString();
                sch.Deldate = sdr["Deldate"].ToString();
                sch.DelType = sdr["DelegationType"].ToString();
                sch.DelDays = sdr["DelDays"].ToString();
                sch.DelClass = sdr["DelClass"].ToString();

                var day1 = new List<SelectListItem>();
                var month1 = new List<SelectListItem>();
                var yearl = new List<SelectListItem>();
                var goodOrnot = new List<SelectListItem>();
                goodOrnot.Add(new SelectListItem { Text = "لم يتم التحديد", Value = "0" });
                goodOrnot.Add(new SelectListItem { Text = "بادر بالندب", Value = "1" });
                goodOrnot.Add(new SelectListItem { Text = "لم يبادر بالندب", Value = "2" });

                for (int i = 1; i <= 31; i++)
                { day1.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
                for (int i = 1; i <= 12; i++)
                { month1.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
                for (int i = 1350; i <= 1500; i++)
                { yearl.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
                int dd = Convert.ToInt32(sdr["EndDay"].ToString());
                int mm = Convert.ToInt32(sdr["EndMonth"].ToString());
                int yy = Convert.ToInt32(sdr["EndYear"].ToString());

                ViewBag.EndDay = new SelectList(day1, "Value", "Text", dd);
                ViewBag.EndMonth = new SelectList(month1, "Value", "Text", mm);
                ViewBag.EndYear = new SelectList(yearl, "Value", "Text", yy);
                int gd = Convert.ToInt32(sdr["goodOrNot1"].ToString());
                ViewBag.goodOrNot = new SelectList(goodOrnot, "Value", "Text", gd);

                list.Add(sch);
            }


            return View(list);

        }

        public ActionResult TrueAcceptExBButton(DelegateShow dg)
        {

            int idd = Convert.ToInt32(dg.id);

            var dd = db.Delegation.Where(p => p.id == idd).FirstOrDefault();

            if (dd.DelegationDate.Value.Year > 1900)
            {
                fun.fireSQL("update Delegation set StateAfter = 4, EndDay = " + dg.EndDay + ",EndMonth = '" + dg.EndMonth + "',EndYear = '" + dg.EndYear + "',goodOrNot = '" + dg.goodOrNot + "' where id = " + dg.id + "");
                return RedirectToAction("TrueAcceptEx", new { id = dg.id });
            }
            else
            {
                fun.fireSQL("update Delegation set StateAfter = 4, EndDay = " + dg.EndDay + ",EndMonth = '" + dg.EndMonth + "',EndYear = '" + dg.EndYear + "',goodOrNot = '" + dg.goodOrNot + "',DelegationDate = getDate() where id = " + dg.id + "");
                return RedirectToAction("TrueAcceptEx", new { id = dg.id });
            }




        }
        //[Authorize(Roles = "delegation")]
        public ActionResult IndexDele()
        {


            List<DelegateShow> list = new List<DelegateShow>();

            DataTable dtable = fun.fireDataTable("SELECT [id],[FSchoolId],[FSchoolName],[FEdCircle],[EmpId],[EmpName],[SubCode],[SubName],[TSchoolId],[TSchoolName],[TEdCircle],convert(char(10),cast([DelegationDate] as datetime),131) as DelegationDate,[DayName],[ManagerName],convert(char(10),cast([FirstDeleDate] as datetime),131) as FirstDeleDate "
+ ", CASE "
+ "WHEN AcceptedDelDate = '1900-01-01' THEN '' "
+ "WHEN AcceptedDelDate > '1900-01-01' THEN convert(char(10), cast([AcceptedDelDate] as datetime), 131) "
+ "END as AcceptedDelDate "
+ " , CASE "
+ "     WHEN DelState = 1 THEN 'ندب مبدئى' "
+ "      WHEN DelState = 2 and AcceptedDelDate = '1900-01-01' THEN 'تم تنفيذ الندب' "
+ "     WHEN DelState = 2 and AcceptedDelDate > '1900-01-01' THEN 'تم قبول الندب' "
+ " END AS DelState "
+ "   FROM[dbo].[Delegation] ");
            foreach (DataRow sdr in dtable.Rows)
            {
                DelegateShow sch = new DelegateShow();
                sch.id = sdr["id"].ToString();
                sch.EmpId = sdr["EmpId"].ToString();
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.FSchoolId = sdr["FSchoolId"].ToString();
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.FEdCircle = sdr["FEdCircle"].ToString();
                sch.TSchoolId = sdr["TSchoolId"].ToString();
                sch.TSchoolName = sdr["TSchoolName"].ToString();
                sch.TEdCircle = sdr["TEdCircle"].ToString();
                sch.FirstDeleDate = sdr["FirstDeleDate"].ToString();
                sch.DelegationDate = sdr["DelegationDate"].ToString();
                sch.AcceptedDelDate = sdr["AcceptedDelDate"].ToString();
                sch.DelState = sdr["DelState"].ToString();

                list.Add(sch);
            }

            ViewBag.adela = db.adela.Where(p => p.adServ == 4).ToList();
            return View(list);


        }


        public ActionResult ImportDecision(int id)
        {
            fun.fireSQL("update Delegation set AcceptedDelDate = GETDATE(),StateAfter = 3 where EmpId = " + id + "");
            DataRow dr2 = fun.fireDataRow("select count(id) as cu from Delegation where EmpId = " + id + "");
            int chk = Convert.ToInt32(dr2["cu"].ToString());
            if (chk == 0)
            {
                DataTable dt1 = fun.fireStoreProcedure1("DelegationDecision", id.ToString());
                List<DelegationDecision> ListDD = new List<DelegationDecision>();

                foreach (DataRow dr1 in dt1.Rows)
                {
                    DelegationDecision DD = new DelegationDecision();
                    DD.TID = dr1["TID"].ToString();
                    DD.SchId = dr1["Schid"].ToString();
                    DD.dayname1 = dr1["dayname1"].ToString();
                    DD.dateHajry = dr1["dateHajry"].ToString();
                    DD.TeachName = dr1["TeachName"].ToString();
                    DD.ManagName = dr1["ManagName"].ToString();
                    ListDD.Add(DD);

                }
                if (TempData["SuccessMsg"] != null)
                {
                    ViewBag.SuccessM = TempData["SuccessMsg"].ToString();
                    ViewBag.PrintShow = TempData["PrintShow"].ToString();
                }
                if (TempData["ErrorMsg"] != null)
                {
                    ViewBag.ErrorM = TempData["ErrorMsg"].ToString();
                }
                return View(ListDD);

            }
            else
            {

                DataRow dr1 = fun.fireDataRow("select Top 1 * from Delegation where EmpId = " + id + "");
                List<DelegationDecision> ListDD = new List<DelegationDecision>();

                DelegationDecision DD = new DelegationDecision();
                DD.DelID = dr1["id"].ToString();
                DD.TID = dr1["EmpId"].ToString();
                DD.SchId = dr1["FSchoolId"].ToString();
                DD.dayname1 = dr1["DayName"].ToString();
                DD.dateHajry = dr1["DelegationDate"].ToString();
                DD.TeachName = dr1["EmpName"].ToString();
                DD.ManagName = dr1["ManagerName"].ToString();
                ListDD.Add(DD);

                ViewBag.PrintShow = "exist";

                return View(ListDD);

            }

        }

        public ActionResult TerminalDecision(int id)
        {
            fun.fireSQL("update Delegation set AcceptedDelDate = GETDATE(),StateAfter = 2 where EmpId = " + id + "");
            DataRow dr2 = fun.fireDataRow("select count(id) as cu from Delegation where EmpId = " + id + "");
            int chk = Convert.ToInt32(dr2["cu"].ToString());
            if (chk == 0)
            {
                DataTable dt1 = fun.fireStoreProcedure1("DelegationDecision", id.ToString());
                List<DelegationDecision> ListDD = new List<DelegationDecision>();

                foreach (DataRow dr1 in dt1.Rows)
                {
                    DelegationDecision DD = new DelegationDecision();
                    DD.TID = dr1["TID"].ToString();
                    DD.SchId = dr1["Schid"].ToString();
                    DD.dayname1 = dr1["dayname1"].ToString();
                    DD.dateHajry = dr1["dateHajry"].ToString();
                    DD.TeachName = dr1["TeachName"].ToString();
                    DD.ManagName = dr1["ManagName"].ToString();
                    ListDD.Add(DD);

                }
                if (TempData["SuccessMsg"] != null)
                {
                    ViewBag.SuccessM = TempData["SuccessMsg"].ToString();
                    ViewBag.PrintShow = TempData["PrintShow"].ToString();
                }
                if (TempData["ErrorMsg"] != null)
                {
                    ViewBag.ErrorM = TempData["ErrorMsg"].ToString();
                }
                return View(ListDD);

            }
            else
            {

                DataRow dr1 = fun.fireDataRow("select Top 1 * from Delegation where EmpId = " + id + "");
                List<DelegationDecision> ListDD = new List<DelegationDecision>();

                DelegationDecision DD = new DelegationDecision();
                DD.DelID = dr1["id"].ToString();
                DD.TID = dr1["EmpId"].ToString();
                DD.SchId = dr1["FSchoolId"].ToString();
                DD.dayname1 = dr1["DayName"].ToString();
                DD.dateHajry = dr1["DelegationDate"].ToString();
                DD.TeachName = dr1["EmpName"].ToString();
                DD.ManagName = dr1["ManagerName"].ToString();
                ListDD.Add(DD);

                ViewBag.PrintShow = "exist";

                return View(ListDD);

            }

        }
        //[Authorize(Roles = "TractingDel")]
        public ActionResult TracktDel()
        {

            return View();

        }

        public ActionResult AcceptDel()
        {
            List<School> list = new List<School>();

            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }


            return View(list);
        }
        //[Authorize(Roles = "TerminalDel")]
        public ActionResult TermIndexDel()
        {

            List<School> list = new List<School>();

            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }


            return View(list);


        }
        //[Authorize(Roles = "AcceptDel")]
        public ActionResult AcceptIndexDel()
        {

            List<School> list = new List<School>();

            DataTable dtable = fun.fireStoreProcedure("SchoolTable");
            foreach (DataRow sdr in dtable.Rows)
            {
                School sch = new School();

                sch.SchoolID = sdr["SchoolID"].ToString();
                sch.SchoolName = sdr["SchoolName"].ToString();
                sch.circleAll = sdr["circleAll"].ToString();
                sch.name6 = sdr["name6"].ToString();
                sch.NameLevel = sdr["NameLevel"].ToString();
                sch.SchoolsType = sdr["SchoolsType"].ToString();
                sch.City = sdr["City"].ToString();
                sch.Tuition1 = sdr["Tuition1"].ToString();
                sch.Liver = sdr["Liver"].ToString();
                list.Add(sch);
            }


            return View(list);

        }
        //Export 
        public ActionResult ExportDelegation(int id)
        {

            DataTable dt = fun.fireStoreProcedure3("TimeTableAuto", id.ToString(), "5", "8");
            List<DesForTime> ListDf = new List<DesForTime>();
            foreach (DataRow dr in dt.Rows)
            {
                DesForTime dsf = new DesForTime();
                dsf.empId = dr["empId"].ToString();
                dsf.Teacher = dr["TEACHAR"].ToString();
                dsf.subj = dr["subj"].ToString();
                dsf.Reduce = dr["Reduce"].ToString();
                dsf.Estat = dr["Estat"].ToString();
                ListDf.Add(dsf);
            }
            DataTable dt1 = fun.fireStoreProcedure1("IncTeach", id.ToString());

            List<DesForTime> ListDf1 = new List<DesForTime>();
            foreach (DataRow dr1 in dt1.Rows)
            {
                DesForTime dsf1 = new DesForTime();
                dsf1.subj = dr1["SUB"].ToString();
                dsf1.addclass = dr1["inc"].ToString();
                dsf1.addSub = dr1["req"].ToString();
                ListDf1.Add(dsf1);
            }


            CultureInfo cultures = CultureInfo.CreateSpecificCulture("ar-SA");

            Schools ischools = new Schools();

            ischools.tSchools = db.TSchools.Include(t => t.T_area).Include(t => t.T_Center).Include(t => t.TBuilding).Include(t => t.TLevel).Include(t => t.TSchoolsType).Include(t => t.TSector).Include(t => t.Tuition1).Where(t => t.Center == 14).FirstOrDefault();
            ischools.tCLassRow = db.TCLassRow.Include(t => t.TLevel_Row).Where(t => t.SchoolID == id).ToList();
            // ischools.ttimetable = db.TimeTable.Include(t => t.TImployees).Where(t => t.sch_id == id).ToList();
            ischools.desForTime = ListDf;
            ischools.desTimeTable = ListDf1;
            ViewBag.cClass = db.TCLassRow.Where(t => t.SchoolID == id).Sum(t => t.CLass);
            ViewBag.cStud = db.TCLassRow.Where(t => t.SchoolID == id).Sum(t => t.pupil);
            ischools.Tsubjects = db.TSubject.ToList();
            ischools.reducedTypes = db.ReducedTypes.ToList();
            string text = DateTime.Now.ToString("yyyy -MMMM -dd", cultures);
            DataTable dtd = fun.fireDataTable("select EmpId,EmpName,SubName,convert(char(10),cast(FirstDeleDate as datetime),131) as FirstDeleDate from Delegation where FSchoolId = " + id + "");
            List<DelegateShow> dg = new List<DelegateShow>();
            foreach (DataRow dr in dtd.Rows)
            {
                DelegateShow dh = new DelegateShow();
                dh.EmpId = dr["EmpId"].ToString();
                dh.EmpName = dr["EmpName"].ToString();
                dh.SubName = dr["SubName"].ToString();
                dh.FirstDeleDate = dr["FirstDeleDate"].ToString();
                dg.Add(dh);
            }

            ischools.dele = dg;

            return View(ischools);



        }

        public ActionResult ExpoertDecision(int id)
        {
            vmboard vmb = new vmboard();
            DataTable dr2 = fun.fireDataTable("select ISNULL(resid,0) as resid,id,other from Delegation where EmpId = " + id + "");

            if (dr2.Rows.Count > 0)
            {
                ViewBag.resid = new SelectList(db.reson, "id", "chos", dr2.Rows[0]["resid"].ToString());
                DataTable dt1 = fun.fireStoreProcedure1("DelegationDecision", id.ToString());
                List<DelegationDecision> ListDD = new List<DelegationDecision>();
                List<dboard> ListDD1 = new List<dboard>();
                foreach (DataRow dr1 in dt1.Rows)
                {
                    DelegationDecision dc = new DelegationDecision();

                    dc.TID = dr1["TID"].ToString();
                    dc.SchId = dr1["Schid"].ToString();
                    dc.dayname1 = dr1["dayname1"].ToString();
                    dc.dateHajry = dr1["dateHajry"].ToString();
                    dc.TeachName = dr1["TeachName"].ToString();
                    dc.ManagName = dr1["ManagName"].ToString();
                    dc.DelID = dr2.Rows[0]["id"].ToString();
                    dc.other = dr2.Rows[0]["other"].ToString();
                    ListDD.Add(dc);

                }
                DataTable dt2 = fun.fireDataTable("select * from Mboard where [sid] = " + dt1.Rows[0]["Schid"].ToString() + "");
                foreach (DataRow drr in dt2.Rows)
                {
                    dboard dbor = new dboard();
                    dbor.name = drr["empName"].ToString();
                    dbor.job = drr["job"].ToString();

                    ListDD1.Add(dbor);

                }

                if (TempData["SuccessMsg"] != null)
                {
                    ViewBag.SuccessM = TempData["SuccessMsg"].ToString();
                    ViewBag.PrintShow = TempData["PrintShow"].ToString();
                }
                if (TempData["ErrorMsg"] != null)
                {
                    ViewBag.ErrorM = TempData["ErrorMsg"].ToString();
                }
                vmb.DelDec = ListDD;
                vmb.dbod = ListDD1;
                ViewBag.chk = dr2.Rows[0]["resid"].ToString();
                return View(vmb);

            }
            else
            {
                ViewBag.resid = new SelectList(db.reson, "id", "chos");

                DataRow dr1 = fun.fireDataRow("select Top 1 * from Delegation where EmpId = " + id + "");
                DelegationDecision dc = new DelegationDecision();
                List<DelegationDecision> ListDD = new List<DelegationDecision>();
                dc.TID = dr1["TID"].ToString();
                dc.SchId = dr1["Schid"].ToString();
                dc.dayname1 = dr1["dayname1"].ToString();
                dc.dateHajry = dr1["dateHajry"].ToString();
                dc.TeachName = dr1["TeachName"].ToString();
                dc.ManagName = dr1["ManagName"].ToString();
                ListDD.Add(dc);
                vmb.DelDec = ListDD;
                ViewBag.PrintShow = "exist";
                ViewBag.chk = "0";
                return View(vmb);

            }

        }
        public ActionResult ExpoertDecisionEXc(int id)
        {

            vmboard vmb = new vmboard();
            DataTable dr2 = fun.fireDataTable("select ISNULL(resid,0) as resid,id,other,DelType,DelDays,DelClass from Delegation where EmpId = " + id + "");

            if (dr2.Rows.Count > 0)
            {
                ViewBag.resid = new SelectList(db.reson, "id", "chos", dr2.Rows[0]["resid"].ToString());
                ViewBag.DelType = new SelectList(db.DelegationType, "id", "DelegationType1", dr2.Rows[0]["DelType"].ToString());
                DataTable dt1 = fun.fireStoreProcedure1("DelegationDecision", id.ToString());
                List<DelegationDecision> ListDD = new List<DelegationDecision>();
                List<dboard> ListDD1 = new List<dboard>();
                foreach (DataRow dr1 in dt1.Rows)
                {
                    DelegationDecision dc = new DelegationDecision();

                    dc.TID = dr1["TID"].ToString();
                    dc.SchId = dr1["Schid"].ToString();
                    dc.dayname1 = dr1["dayname1"].ToString();
                    dc.dateHajry = dr1["dateHajry"].ToString();
                    dc.TeachName = dr1["TeachName"].ToString();
                    dc.ManagName = dr1["ManagName"].ToString();
                    dc.DelID = dr2.Rows[0]["id"].ToString();
                    dc.other = dr2.Rows[0]["other"].ToString();
                    dc.DelType = dr2.Rows[0]["DelType"].ToString();
                    dc.DelDays = dr2.Rows[0]["DelDays"].ToString();
                    dc.DelClass = dr2.Rows[0]["DelClass"].ToString();
                    ListDD.Add(dc);

                }
                DataTable dt2 = fun.fireDataTable("select * from Mboard where [sid] = " + dt1.Rows[0]["Schid"].ToString() + "");
                foreach (DataRow drr in dt2.Rows)
                {
                    dboard dbor = new dboard();
                    dbor.name = drr["empName"].ToString();
                    dbor.job = drr["job"].ToString();

                    ListDD1.Add(dbor);

                }

                if (TempData["SuccessMsg"] != null)
                {
                    ViewBag.SuccessM = TempData["SuccessMsg"].ToString();
                    ViewBag.PrintShow = TempData["PrintShow"].ToString();
                }
                if (TempData["ErrorMsg"] != null)
                {
                    ViewBag.ErrorM = TempData["ErrorMsg"].ToString();
                }
                vmb.DelDec = ListDD;
                vmb.dbod = ListDD1;
                ViewBag.chk = dr2.Rows[0]["resid"].ToString();
                return View(vmb);

            }
            else
            {
                ViewBag.resid = new SelectList(db.reson, "id", "chos");
                ViewBag.DelType = new SelectList(db.DelegationType, "id", "DelegationType1");
                DataTable dt1 = fun.fireStoreProcedure1("DelegationDecision", id.ToString());
                DelegationDecision dc = new DelegationDecision();
                List<DelegationDecision> ListDD = new List<DelegationDecision>();
                foreach (DataRow dr1 in dt1.Rows)
                {
                    dc.TID = dr1["TID"].ToString();
                    dc.SchId = dr1["Schid"].ToString();
                    dc.dayname1 = dr1["dayname1"].ToString();
                    dc.dateHajry = dr1["dateHajry"].ToString();
                    dc.TeachName = dr1["TeachName"].ToString();
                    dc.ManagName = dr1["ManagName"].ToString();

                    ListDD.Add(dc);
                }
                vmb.DelDec = ListDD;
                ViewBag.PrintShow = "exist";
                ViewBag.chk = "0";
                return View(vmb);

            }




        }
        [HttpPost]
        public JsonResult DeleteDelEXc(int id)
        {
            if (id.ToString() != null)
            {
                var dd = db.Delegation.Where(p => p.EmpId == id && p.DelState == 1).FirstOrDefault();
                db.Delegation.Remove(dd);
                db.SaveChanges();
            }

            return Json(null);
        }
        public ActionResult ExpoertDecisionEx(Delegation dg)
        {


            if (dg.EmpId.ToString() != null)
            {
                DataTable dt1 = fun.fireStoreProcedure1("DelegationDecision", dg.EmpId.ToString());
                DataTable dr2 = fun.fireDataTable("select id from Delegation where EmpId = " + dg.EmpId.ToString() + "");

                if (dr2.Rows.Count > 0)
                {
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        DelegationDecision DD = new DelegationDecision();
                        DD.TID = dr1["TID"].ToString();
                        DD.SchId = dr1["Schid"].ToString();
                        DD.Tsub = dr1["Tsub"].ToString();
                        DD.TSubId = dr1["TsubID"].ToString();
                        DD.Sname = dr1["SchName"].ToString();
                        DD.SCircle = dr1["SCircle"].ToString();
                        DD.dayname1 = dr1["dayname1"].ToString();
                        DD.dateHajry = dr1["dateHajry"].ToString();
                        DD.TeachName = dr1["TeachName"].ToString();
                        DD.ManagName = dr1["ManagName"].ToString();

                        DataRow Dr = fun.fireDataRow("select ISNULL(MAX(id),0)+1 as mx from Delegation");
                        string id1 = Dr["mx"].ToString();
                        if (id1 != null)
                        {
                            fun.fireSQL("update Delegation set resid = " + dg.resid + " , other = '" + dg.other + "',DelType =" + dg.DelType + ",DelDays ='" + dg.DelDays + "',DelClass ='" + dg.DelClass + "' where EmpId = " + dg.EmpId + "");
                            TempData["SuccessMsg"] = "تم تنفيذ الندب المبدئى";
                            TempData["PrintShow"] = "'طباعة";
                            return RedirectToAction("ExpoertDecision", new { id = dg.EmpId });
                        }
                        else
                        {
                            TempData["SuccessMsg"] = "";
                        }

                    }
                }
                else
                {
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        DelegationDecision DD = new DelegationDecision();
                        DD.TID = dr1["TID"].ToString();
                        DD.SchId = dr1["Schid"].ToString();
                        DD.Tsub = dr1["Tsub"].ToString();
                        DD.TSubId = dr1["TsubID"].ToString();
                        DD.Sname = dr1["SchName"].ToString();
                        DD.SCircle = dr1["SCircle"].ToString();
                        DD.dayname1 = dr1["dayname1"].ToString();
                        DD.dateHajry = dr1["dateHajry"].ToString();
                        DD.TeachName = dr1["TeachName"].ToString();
                        DD.ManagName = dr1["ManagName"].ToString();
                        DataRow Dr = fun.fireDataRow("select ISNULL(MAX(id),0)+1 as mx from Delegation");
                        string id1 = Dr["mx"].ToString();
                        if (id1 != null)
                        {

                            fun.fireSQL("INSERT INTO Delegation VALUES (" + id1 + "," + DD.SchId + ",'" + DD.Sname + "','" + DD.SCircle + "'," + dg.EmpId + ",'" + DD.TeachName + "'," + DD.TSubId + ",'" + DD.Tsub + "',0,'','','' ,1 ,'" + DD.dayname1 + "','" + DD.ManagName + "',(SELECT CONVERT(VARCHAR(10), getdate(), 111)),'',''," + dg.resid + ",'" + dg.other + "',1,'',0,'',0,0,0,'" + dg.DelType + "','" + dg.DelDays + "','" + dg.DelClass + "')");
                            TempData["SuccessMsg"] = "تم تنفيذ الندب المبدئى";
                            TempData["PrintShow"] = "'طباعة";
                            return RedirectToAction("ExpoertDecision", new { id = dg.EmpId });
                        }
                        else
                        {
                            TempData["SuccessMsg"] = "";
                        }

                    }
                }
            }


            TempData["ErrorMsg"] = "يوجد مشكلة فى تنفيد الندب المبدئى";
            return RedirectToAction("ExpoertDecision", dg.EmpId);
        }

        // action to print first delegation
        public ActionResult PrintFirstDelegation(int id)
        {
            vmboard vmb = new vmboard();
            DataTable dr2 = fun.fireDataTable("select ISNULL(resid,0) as resid,id,other from Delegation where EmpId = " + id + "");

            if (dr2.Rows.Count > 0)
            {
                ViewBag.resid = new SelectList(db.reson, "id", "chos", dr2.Rows[0]["resid"].ToString());
                DataTable dt1 = fun.fireStoreProcedure1("DelegationDecision", id.ToString());
                List<DelegationDecision> ListDD = new List<DelegationDecision>();
                List<dboard> ListDD1 = new List<dboard>();
                foreach (DataRow dr1 in dt1.Rows)
                {
                    DelegationDecision dc = new DelegationDecision();

                    dc.TID = dr1["TID"].ToString();
                    dc.SchId = dr1["Schid"].ToString();
                    dc.dayname1 = dr1["dayname1"].ToString();
                    dc.dateHajry = dr1["dateHajry"].ToString();
                    dc.TeachName = dr1["TeachName"].ToString();
                    dc.ManagName = dr1["ManagName"].ToString();
                    dc.DelID = dr2.Rows[0]["id"].ToString();
                    dc.other = dr2.Rows[0]["other"].ToString();
                    ListDD.Add(dc);

                }
                DataTable dt2 = fun.fireDataTable("select * from Mboard where [sid] = " + dt1.Rows[0]["Schid"].ToString() + "");
                foreach (DataRow drr in dt2.Rows)
                {
                    dboard dbor = new dboard();
                    dbor.name = drr["empName"].ToString();
                    dbor.job = drr["job"].ToString();

                    ListDD1.Add(dbor);

                }

                if (TempData["SuccessMsg"] != null)
                {
                    ViewBag.SuccessM = TempData["SuccessMsg"].ToString();
                    ViewBag.PrintShow = TempData["PrintShow"].ToString();
                }
                if (TempData["ErrorMsg"] != null)
                {
                    ViewBag.ErrorM = TempData["ErrorMsg"].ToString();
                }
                vmb.DelDec = ListDD;
                vmb.dbod = ListDD1;
                ViewBag.chk = dr2.Rows[0]["resid"].ToString();
            }
            return View(vmb);
        }

        public ActionResult PrintEndDelegation(int id)
        {

            DataTable dt = fun.fireDataTable("select CONCAT(Delegation.id,'-',1441) as id,ManagerName,ManagerTo,convert(char(10),cast(GETDATE() as datetime),131) as hajry,convert(char(10),cast(GETDATE() as datetime),121) as Melad,EmpId,EmpName,SubName,DelState,ISNULL(StateAfter,0) as StateAfter,FSchoolId,FSchoolName,FEdCircle,TSchoolId,TSchoolName,TEdCircle,convert(char(10),cast(FirstDeleDate as datetime),131) as FirstDeleDate,convert(char(10),cast(ReqDelDate as datetime),131) as ReqDelDate ,convert(char(10),cast(AcceptedDelDate as datetime),131) as AcceptedDelDate,ISNULL(EndDay,0) as EndDay,ISNULL(EndMonth,0) as EndMonth,ISNULL(EndYear,0) as EndYear,ISNULL(goodOrNot,0) as goodOrNot1,CONCAT(EndDay,'/',EndMonth,'/',EndYear) as EndDel1 ,other,resid,DelegationType,DelDays,DelClass,Delegation.DelType as DelTypeID, "
+ " CASE "
+ " WHEN DelegationDate = '1900-01-01' or DelegationDate = '' THEN '' "
+ " ELSE convert(char(10), cast(DelegationDate as datetime), 131) "
+ " END as DelegationDate, "
+ " CASE "
+ " WHEN goodOrNot = 1  THEN 'المعلم مبادر في الندب ويستحق حافز الاجازة الاضافي' "
+ " WHEN goodOrNot = 2  THEN 'المعلم لم يبادر في الندب ولايستحق حافز الاجازة الاضافي ' "
+ " ELSE 'لم يتم التحديد' "
+ " END as goodOrNot "
+ " from Delegation"
+ " left join DelegationType dt on Delegation.DelType = dt.id"
+ " where StateAfter = 4 and Delegation.id = " + id + "");
            EndReport Er = new EndReport();
            List<dboard> List2 = new List<dboard>();
            List<DelegateShow> List = new List<DelegateShow>();
            if (dt.Rows.Count > 0)
            {


                foreach (DataRow dr1 in dt.Rows)
                {

                    DelegateShow vm = new DelegateShow();
                    vm.id = dr1["id"].ToString();
                    vm.ManagerName = dr1["ManagerName"].ToString();
                    vm.ManagerTo = dr1["ManagerTo"].ToString();
                    vm.hajry = dr1["hajry"].ToString();
                    vm.Melad = dr1["Melad"].ToString();
                    vm.EmpId = dr1["EmpId"].ToString();
                    vm.EmpName = dr1["EmpName"].ToString();
                    vm.SubName = dr1["SubName"].ToString();
                    vm.DelState = dr1["DelState"].ToString();
                    vm.FSchoolId = dr1["FSchoolId"].ToString();
                    vm.FSchoolName = dr1["FSchoolName"].ToString();
                    vm.TSchoolId = dr1["TSchoolId"].ToString();
                    vm.TSchoolName = dr1["TSchoolName"].ToString();
                    vm.FirstDeleDate = dr1["FirstDeleDate"].ToString();
                    vm.ReqDelDate = dr1["ReqDelDate"].ToString();
                    vm.AcceptedDelDate = dr1["AcceptedDelDate"].ToString();
                    vm.EndDel = dr1["EndDel1"].ToString();
                    vm.DelType = dr1["DelegationType"].ToString();
                    vm.DelDays = dr1["DelDays"].ToString();
                    vm.DelClass = dr1["DelClass"].ToString();
                    vm.DelTypeID = dr1["DelTypeID"].ToString();

                    vm.DelegationDate = dr1["DelegationDate"].ToString();
                    vm.goodOrNot = dr1["goodOrNot"].ToString();

                    vm.other = dr1["other"].ToString();
                    vm.resid = dr1["resid"].ToString();
                    var man = db.TImployees.Where(p => p.WID == 75).FirstOrDefault();
                    var Asses = db.TImployees.Where(p => p.WID == 74).FirstOrDefault();
                    vm.ManName = man.name1 + " " + man.name2 + " " + man.name3 + " " + man.FName;
                    vm.ManCode = man.Identity;
                    vm.ManSchid = man.SchID.ToString();
                    vm.AssesName = Asses.name1 + " " + Asses.name2 + " " + Asses.name3 + " " + Asses.FName;
                    vm.AssesSchid = Asses.SchID.ToString();


                    List.Add(vm);

                }
                DataTable dt2 = fun.fireDataTable("select * from Mboard where [sid] = " + dt.Rows[0]["FSchoolId"].ToString() + "");
                foreach (DataRow drr in dt2.Rows)
                {
                    dboard dbor = new dboard();
                    dbor.name = drr["empName"].ToString();
                    dbor.job = drr["job"].ToString();

                    List2.Add(dbor);

                }

                if (TempData["SuccessMsg"] != null)
                {
                    ViewBag.SuccessM = TempData["SuccessMsg"].ToString();
                    ViewBag.PrintShow = TempData["PrintShow"].ToString();
                }
                if (TempData["ErrorMsg"] != null)
                {
                    ViewBag.ErrorM = TempData["ErrorMsg"].ToString();
                }

                ViewBag.chk = dt.Rows[0]["resid"].ToString();
            }
            //Er.dele = List;
            //Er.dbod = List2;
            return View(Er);
        }
        public ActionResult PrintEndDelegationReport(int id)
        {

            if (id.ToString() != null)
            {

                var header = Path.GetFileName("~/static/headerDel.html");
                var footer = Path.GetFileName("~/static/footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);
                //return new ActionAsPdf(
                //    "PrintEndDelegation",
                //    new { id = id })
                //{
                //    PageMargins = new Rotativa.Options.Margins(10, 2, 2, 10),
                //    PageSize = Rotativa.Options.Size.A4,
                //    PageOrientation = Rotativa.Options.Orientation.Portrait,

                //    FileName = "قرار النهائى للندب.pdf"
                //};

            }

            return View();
        }
        // print first delegation
        //public ActionResult PrintFirstDelegationReport(int id)

        //{

        //        var header = Server.MapPath("~/static/headerDel.html");
        //        var footer = Server.MapPath("~/static/footer.html");
        //        string customSwitches = string.Format("--header-html  \"{0}\" " +
        //                    "--header-spacing \"8\" " +
        //                    "--header-font-name \"Open Sans\" " +
        //                    "--footer-font-size \"8\" " +
        //                    "--footer-font-name \"Open Sans\" " +
        //                    "--header-font-size \"10\" " +
        //                    //"--footer-right \"Pag: [page] de [toPage]\""+
        //                    "--footer-html  \"{1}\" ", header, footer);


        //    DataRow dr1 = fun.fireDataRow("select Top 1 * from Delegation where EmpId = " + id + "");
        //    List<DelegationDecision> ListDD = new List<DelegationDecision>();

        //    DelegationDecision DD = new DelegationDecision();
        //    DD.DelID = dr1["id"].ToString();
        //    DD.TID = dr1["EmpId"].ToString();
        //    DD.SchId = dr1["FSchoolId"].ToString();
        //    DD.dayname1 = dr1["DayName"].ToString();
        //    DD.dateHajry = dr1["DelegationDate"].ToString();
        //    DD.TeachName = dr1["EmpName"].ToString();
        //    DD.ManagName = dr1["ManagerName"].ToString();
        //    ListDD.Add(DD);

        //    var pdf = new ViewAsPdf("PrintFirstDelegation", ListDD)
        //            {

        //                CustomSwitches = customSwitches,

        //                PageMargins = new Rotativa.Options.Margins(40, 10, 15, 10),
        //                PageOrientation = Rotativa.Options.Orientation.Portrait,
        //                PageSize = Rotativa.Options.Size.A4
        //            };
        //            return pdf;

        //}
        public ActionResult PrintFirstDelegationReport(int id)
        {
            if (id.ToString() != null)
            {

                var header = Path.GetFileName("~/static/headerDel.html");
                var footer = Path.GetFileName("~/static/footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);
                //return new ActionAsPdf(
                //    "PrintFirstDelegation",
                //    new { id = id })
                //{
                //    PageMargins = new Rotativa.Options.Margins(10, 2, 2, 10),
                //    PageSize = Rotativa.Options.Size.A3,
                //    PageOrientation = Rotativa.Options.Orientation.Portrait,

                //    FileName = "قرار ندب مبدئى.pdf"
                //};

            }

            return View();
        }
        //[Authorize(Roles = "SManager")]
        public ActionResult IndexManager()
        {


            List<ManagerPage> list = new List<ManagerPage>();

            DataTable dtable = fun.fireDataTable("select ti.[Identity] as id,CONCAT(name1,' ',name2 ,' ', name3 , ' ', FName) as FullName ,ts.SchoolID ,ts.School , CONCAT (ts.circleEd , ' - ',ts.circleEd2 , ' - ', ts.circleEd3 , ' - ' , ts.circleEd4 ) as Circle from TImployees ti" +
            " inner join TSchools ts on ti.SchID = ts.SchoolID " +
            " where WID = 1 and ts.Center = 14");
            foreach (DataRow sdr in dtable.Rows)
            {
                ManagerPage Mgp = new ManagerPage();
                Mgp.id = sdr["id"].ToString();
                Mgp.FullName = sdr["FullName"].ToString();
                Mgp.SchoolId = Convert.ToInt32(sdr["SchoolID"].ToString());
                Mgp.School = sdr["School"].ToString();
                Mgp.Circle = sdr["Circle"].ToString();

                list.Add(Mgp);
            }


            return View(list);


        }

        // Manager
        public ActionResult ManagerPage(int id)
        {

            Schools ischools = new Schools();

            ischools.tSchools = db.TSchools.Include(t => t.T_area).Include(t => t.T_Center).Include(t => t.TBuilding).Include(t => t.TLevel).Include(t => t.TSchoolsType).Include(t => t.TSector).Include(t => t.Tuition1).Where(t => t.Center == 14).Where(t => t.SchoolID == id).SingleOrDefault();
            ischools.tCLassRow = db.TCLassRow.Include(t => t.TLevel_Row).Where(t => t.SchoolID == id).ToList();

            return View(ischools);
        }

        [HttpPost]
        public ActionResult ManagerPage(IFormFile file1, IFormFile file2, int id)
        {



            if (file1 != null && file1.Length > 0)
            {

                if (!Directory.Exists(Path.GetFileName("~/Upload/" + id + "/Secret")))
                {
                    Directory.CreateDirectory(Path.GetFileName("~/Upload/" + id + "/Secret"));
                    var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + id + "/Secret/1.png"));
                    using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                    {
                        file1.CopyToAsync(fileSteam);
                    }
                }
                else
                {
                    var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + id + "/Secret/1.png"));
                    using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                    {
                        file1.CopyToAsync(fileSteam);
                    }
                }

            }
            if (file2 != null && file2.Length > 0)
            {

                if (!Directory.Exists(Path.GetFileName("~/Upload/" + id + "/Secret")))
                {
                    Directory.CreateDirectory(Path.GetFileName("~/Upload/" + id + "/Secret"));
                    var FullPath1 = Path.Combine(Path.GetFileName("~/Upload/" + id + "/Secret/2.png"));
                    using (var fileSteam = new FileStream(FullPath1, FileMode.Create))
                    {
                        file2.CopyToAsync(fileSteam);
                    }
                }
                else
                {
                    var FullPath1 = Path.Combine(Path.GetFileName("~/Upload/" + id + "/Secret/2.png"));
                    using (var fileSteam = new FileStream(FullPath1, FileMode.Create))
                    {
                        file2.CopyToAsync(fileSteam);
                    }
                }
            }
            Schools ischools = new Schools();

            ischools.tSchools = db.TSchools.Include(t => t.T_area).Include(t => t.T_Center).Include(t => t.TBuilding).Include(t => t.TLevel).Include(t => t.TSchoolsType).Include(t => t.TSector).Include(t => t.Tuition1).Where(t => t.Center == 14).Where(t => t.SchoolID == id).SingleOrDefault();
            ischools.tCLassRow = db.TCLassRow.Include(t => t.TLevel_Row).Where(t => t.SchoolID == id).ToList();
            return View(ischools);
        }
        //[Authorize(Roles = "ManagerPage")]
        public ActionResult ManagerPageSp()
        {

            //string id = sc.DecParametar(User.Identity.Name);

            var src = db.TImployees.Where(p => p.Identity == "1000116937").FirstOrDefault();


            Schools ischools = new Schools();

            ischools.tSchools = db.TSchools.Include(t => t.T_area).Include(t => t.T_Center).Include(t => t.TBuilding).Include(t => t.TLevel).Include(t => t.TSchoolsType).Include(t => t.TSector).Include(t => t.Tuition1).Where(t => t.Center == 14).Where(t => t.SchoolID == src.SchID).SingleOrDefault();
            ischools.tCLassRow = db.TCLassRow.Include(t => t.TLevel_Row).Where(t => t.SchoolID == src.SchID).ToList();
            return View(ischools);



        }

        [HttpPost]
        public ActionResult ManagerPageSp(IFormFile file1, IFormFile file2, int id)
        {

            if (file1 != null && file1.Length > 0)
            {

                if (!Directory.Exists(Path.GetFileName("~/Upload/" + id + "/Secret")))
                {
                    Directory.CreateDirectory(Path.GetFileName("~/Upload/" + id + "/Secret"));
                    var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + id + "/Secret/1.png"));
                    using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                    {
                        file1.CopyToAsync(fileSteam);
                    }
                }
                else
                {
                    var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + id + "/Secret/1.png"));
                    using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                    {
                        file1.CopyToAsync(fileSteam);
                    }
                }

            }
            if (file2 != null && file2.Length > 0)
            {

                if (!Directory.Exists(Path.GetFileName("~/Upload/" + id + "/Secret")))
                {
                    Directory.CreateDirectory(Path.GetFileName("~/Upload/" + id + "/Secret"));
                    var FullPath1 = Path.Combine(Path.GetFileName("~/Upload/" + id + "/Secret/2.png"));
                    using (var fileSteam = new FileStream(FullPath1, FileMode.Create))
                    {
                        file2.CopyToAsync(fileSteam);
                    }
                }
                else
                {
                    var FullPath1 = Path.Combine(Path.GetFileName("~/Upload/" + id + "/Secret/2.png"));
                    using (var fileSteam = new FileStream(FullPath1, FileMode.Create))
                    {
                        file2.CopyToAsync(fileSteam);
                    }
                }
            }
            Schools ischools = new Schools();

            ischools.tSchools = db.TSchools.Include(t => t.T_area).Include(t => t.T_Center).Include(t => t.TBuilding).Include(t => t.TLevel).Include(t => t.TSchoolsType).Include(t => t.TSector).Include(t => t.Tuition1).Where(t => t.Center == 14).Where(t => t.SchoolID == id).SingleOrDefault();
            ischools.tCLassRow = db.TCLassRow.Include(t => t.TLevel_Row).Where(t => t.SchoolID == id).ToList();
            return RedirectToAction("ManagerPageSp");
        }

        // Delegation
        //[Authorize(Roles = "TransferDel")]
        public ActionResult DelegationPage()
        {

            return View();

        }


        public JsonResult getEmp()
        {

            List<empDelegate> list = new List<empDelegate>();

            DataTable dtable = fun.fireDataTable("select te.[Identity] as empId ,CONCAT(te.name1 , ' ',te.name2 ,' ',te.name3,' ',te.FName) as empName , "
+ " te.SchID as schoolIID, ts.School as schoolName, te.SpecialID as specId, tp.Special as specName, CONCAT(ts.circleEd, ' ', ts.circleEd2, ' ', ts.circleEd3, ' ', ts.circleEd4) as CirId "
+ "  from TImployees te "
+ " inner join Delegation dg on te.[Identity] = cast(dg.EmpId as nvarchar(20)) "
+ " inner join TSchools ts on te.SchID = ts.SchoolID "
+ " inner join TSpecial tp on te.SpecialID = tp.SpeID "
+ " where dg.DelState = 1");
            foreach (DataRow sdr in dtable.Rows)
            {
                empDelegate ed = new empDelegate();

                ed.empId = sdr["empId"].ToString();
                ed.schollID = sdr["schoolIID"].ToString();
                ed.specId = sdr["specId"].ToString();
                ed.CirId = sdr["CirId"].ToString();
                ed.empName = sdr["empName"].ToString();
                ed.schoolName = sdr["schoolName"].ToString();
                ed.specName = sdr["specName"].ToString();

                list.Add(ed);
            }


            return Json(list/*, JsonRequestBehavior.AllowGet*/);

        }
        public JsonResult getHTeachers()
        {

            List<hSchool> list = new List<hSchool>();

            DataTable dtable = fun.fireDataTable("select SubCode,SubName,count(id) as cu from Delegation where DelState = 1 group by SubCode,SubName order by SubCode");
            foreach (DataRow sdr in dtable.Rows)
            {
                hSchool hs = new hSchool();

                hs.schName = " <li class=\"static \" > <button class=\" uHead \" >  <span class=\"badge badge - primary cblue \"  >" + sdr["cu"].ToString() + "</span> <p class=\"Phead \" id=' " + sdr["SubCode"].ToString() + "'>" + sdr["SubName"].ToString() + "</p> </button> </li> ";
                list.Add(hs);
                DataTable dtable1 = fun.fireDataTable("select dg.id,te.[Identity] as empId ,CONCAT(te.name1 , ' ',te.name2 ,' ',te.name3,' ',te.FName) as empName ,dt.DelegationType,dg.DelDays,dg.DelClass, "
+ " te.SchID as schoolIID, ts.School as schoolName, te.SpecialID as specId, tp.Special as specName, CONCAT(ts.circleEd, ' ', ts.circleEd2, ' ', ts.circleEd3, ' ', ts.circleEd4) as CirId "
+ "  from TImployees te "
+ " inner join Delegation dg on te.[Identity] = cast(dg.EmpId as nvarchar(20)) "
+ " inner join TSchools ts on te.SchID = ts.SchoolID "
+ " inner join TSpecial tp on te.SpecialID = tp.SpeID "
+ " left join DelegationType dt on dg.DelType= dt.id  "
+ " where dg.DelState = 1 and SubCode = " + sdr["SubCode"].ToString() + "");
                foreach (DataRow sdr1 in dtable1.Rows)
                {
                    hSchool hc = new hSchool();
                    hc.schName = " <li> <div class=\"content\"> <ul class=\"inLi\"> <li class=\"list-group-item dr\"> <p style=\"display: inline-block\" name='" + sdr1["id"].ToString() + "' id='" + sdr1["empId"].ToString() + "'>الاسم :</p><p style=\"display: inline-block\" class=\"name1\">" + sdr1["empName"].ToString() + "</p> "
                + "<a href = \"#bar\" class=\"protip\" data-pt-position=\"top\" data-pt-scheme=\"blue\" data-pt-title=\"كود المعلم : " + sdr1["empId"].ToString() + " "
                + "    &lt;br&gt; "
                 + "   المدرسة الاساسية  : " + sdr1["schoolName"].ToString() + "  "
                  + "    &lt;br&gt; "
                       + "   المادة  : " + sdr1["specName"].ToString() + " "
                       + "    &lt;br&gt; "
                       + "   نوع الندب  : " + sdr1["DelegationType"].ToString() + " "
                       + "    &lt;br&gt; "
                       + "   عدد الايام  : " + sdr1["DelDays"].ToString() + " "
                       + "    &lt;br&gt; "
                           + "   عدد الحصص  : " + sdr1["DelClass"].ToString() + "\" > "
                  + "<span class=\"badge badge-primary cOrange \" >i</span> </a> </li> </ul> </div></li>";
                    list.Add(hc);

                }
            }


            return Json(list/*, System.Web.Mvc.JsonRequestBehavior.AllowGet*/);

        }

        public JsonResult getHSchool()
        {

            List<hSchool> list = new List<hSchool>();

            DataTable dtable = fun.fireDataTable("select schID,schName,sum(req) as req from SchoolDess group by schID,schName");
            foreach (DataRow sdr in dtable.Rows)
            {
                hSchool hs = new hSchool();

                hs.schName = " <li class=\"static \" > <button class=\" uHead1 \" >  <span class=\"badge badge - primary cblue \"  >" + sdr["req"].ToString() + "</span> <p class=\"Phead \" id=' " + sdr["schID"].ToString() + "'>" + sdr["schName"].ToString() + "</p> </button> </li> ";
                list.Add(hs);
                DataTable dtable1 = fun.fireDataTable("select subID,subName,req from SchoolDess where schID = " + sdr["schID"].ToString() + " group by subID,subName,req");
                foreach (DataRow sdr1 in dtable1.Rows)
                {
                    hSchool hc = new hSchool();
                    hc.schName = " <li class=\"headSub\" > <button class=\" uHsubb\"> <span class=\"badge badge - primary cblue\" style=\"\">" + sdr1["req"].ToString() + "</span> <p class=\"Phsub\" id=' " + sdr1["subID"].ToString() + "'>" + sdr1["subName"].ToString() + "</p> </button> </li>  ";
                    list.Add(hc);
                    DataTable dtable2 = fun.fireDataTable("select dg.id, te.[Identity] as empId ,CONCAT(te.name1 , ' ',te.name2 ,' ',te.name3,' ',te.FName) as empName ,dt.DelegationType,dg.DelDays,dg.DelClass,  "
+ " te.SchID as schoolIID, dg.FSchoolName as schoolName, te.SpecialID as specId, tp.Special as specName, CONCAT(ts.circleEd, ' ', ts.circleEd2, ' ', ts.circleEd3, ' ', ts.circleEd4) as CirId "
+ "  from TImployees te "
+ " inner join Delegation dg on te.[Identity] = cast(dg.EmpId as nvarchar(20)) "
+ " inner join TSchools ts on te.SchID = ts.SchoolID "
+ " inner join TSpecial tp on te.SpecialID = tp.SpeID "
+ " left join DelegationType dt on dg.DelType= dt.id  "
+ " where dg.DelState = 2 and SubCode = " + sdr1["subID"].ToString() + " and ts.SchoolID = " + sdr["schID"].ToString() + "");
                    foreach (DataRow sdr2 in dtable2.Rows)
                    {

                        hSchool hc1 = new hSchool();
                        hc1.schName = " <li> <div class=\"content\"> <ul class=\"inLi\"> <li style=\"background-image: linear-gradient(to bottom, #ececec, #acda63);\" class=\"list-group-item dr\"> <p style=\"display: inline-block\">الاسم :</p><p style=\"display: inline-block\" class=\"name1\">" + sdr2["empName"].ToString() + "</p> "
                    + "<a href = \"#bar\" class=\"protip\" data-pt-position=\"top\" data-pt-scheme=\"blue\" data-pt-title=\"كود المعلم : " + sdr2["empId"].ToString() + " "
                    + "    &lt;br&gt; "
                     + "   المدرسة الاساسية  : " + sdr2["schoolName"].ToString() + "  "
                      + "    &lt;br&gt; "
                       + "   المادة  : " + sdr2["specName"].ToString() + " "
                       + "    &lt;br&gt; "
                       + "   نوع الندب  : " + sdr2["DelegationType"].ToString() + " "
                       + "    &lt;br&gt; "
                       + "   عدد الايام  : " + sdr2["DelDays"].ToString() + " "
                       + "    &lt;br&gt; "
                           + "   عدد الحصص  : " + sdr2["DelClass"].ToString() + "\" > "
                      + "<span class=\"badge badge-primary cOrange \" >i</span>   </a>  <span name='" + sdr2["id"].ToString() + "'  class=\"badge badge-primary cRed closeT \" alt='" + sdr2["empId"].ToString() + "' >X</span> </li> </ul> </div></li>";
                        list.Add(hc1);

                    }

                }
            }


            return Json(list/*, JsonRequestBehavior.AllowGet*/);

        }
        [HttpPost]
        public JsonResult addDele(int idDel, int empid, int schid)
        {
            try
            {
                if (empid > 0)
                {
                    DataTable dd = fun.fireDataTable("select School,CONCAT(circleEd,' ',circleEd2,' ',circleEd3,' ',circleEd4) as circle from TSchools where SchoolID = " + schid + "");
                    foreach (DataRow dr in dd.Rows)
                    {
                        fun.fireSQL("update Delegation set DelState = 2,StateAfter = 1,TSchoolId = " + schid + ",TSchoolName = '" + dr["School"].ToString() + "',TEdCircle = '" + dr["circle"].ToString() + "' where id = " + idDel + "");
                        DataTable dt = fun.fireDataTable("select SchID , SchIDPrev from TImployees where [Identity] = '" + empid + "'");
                        if (dt.Rows.Count > 0)
                        {
                            var Dele = db.Delegation.Where(p => p.id == idDel).FirstOrDefault();
                            if (Dele.DelType == 1)
                            {
                                foreach (DataRow dr1 in dt.Rows)
                                {
                                    fun.fireSQL("update TImployees set SchID = " + schid + " , SchIDPrev = " + dr1["SchID"].ToString() + " where [Identity] = '" + empid + "'");

                                }

                            }


                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Json(null);

        }
        [HttpPost]
        public JsonResult DelDele(int idDele, int empid)
        {
            try
            {
                if (empid > 0)
                {
                    fun.fireSQL("update Delegation set DelState = 1,StateAfter = 1,TSchoolId = '',TSchoolName = '',TEdCircle = '' where id = " + idDele + "");
                    DataTable dt = fun.fireDataTable("select SchID , SchIDPrev from TImployees where [Identity] = '" + empid + "'");
                    if (dt.Rows.Count > 0)
                    {
                        var Dele = db.Delegation.Where(p => p.id == idDele).FirstOrDefault();
                        if (Dele.DelType == 1)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                fun.fireSQL("update TImployees set SchID = " + dr["SchIDPrev"].ToString() + " , SchIDPrev = " + dr["SchID"].ToString() + " where [Identity] = '" + empid + "'");

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }

            return Json(null);

        }

        // import
        public ActionResult ImportDelegation()
        {
            return View();
        }
        public JsonResult getTrack(int tid)
        {



            DataTable dtable = fun.fireDataTable("SELECT [id],[FSchoolId],[FSchoolName],[FEdCircle],[EmpId],[EmpName],[SubCode],[SubName],[TSchoolId],[TSchoolName],[TEdCircle],[DayName],[ManagerName],convert(char(10),cast([FirstDeleDate] as datetime),131) as FirstDeleDate,IsNull(StateAfter,0) as StateAfter "
+ ", CASE "
+ "WHEN ReqDelDate = '1900-01-01' THEN '' "
+ "WHEN ReqDelDate > '1900-01-01' THEN convert(char(10), cast([ReqDelDate] as datetime), 131) "
+ "END as ReqDelDate "
+ ", CASE "
+ "WHEN DelegationDate = '1900-01-01' THEN '' "
+ "WHEN DelegationDate > '1900-01-01' THEN convert(char(10), cast([DelegationDate] as datetime), 131) "
+ "END as DelegationDate "
+ ", CASE "
+ "WHEN AcceptedDelDate = '1900-01-01' THEN '' "
+ "WHEN AcceptedDelDate > '1900-01-01' THEN convert(char(10), cast([AcceptedDelDate] as datetime), 131) "
+ "END as AcceptedDelDate "
+ " , CASE "
+ "     WHEN DelState = 1 THEN '1' "
+ "      WHEN DelState = 2 and AcceptedDelDate = '1900-01-01' THEN '2' "
+ "     WHEN DelState = 2 and AcceptedDelDate > '1900-01-01' THEN '3' "
+ " END AS DelState "
+ "   FROM[dbo].[Delegation] where [id] = " + tid + "");
            foreach (DataRow sdr in dtable.Rows)
            {
                DelegateShow sch = new DelegateShow();
                sch.id = sdr["id"].ToString();
                sch.EmpId = sdr["EmpId"].ToString();
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.FSchoolId = sdr["FSchoolId"].ToString();
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.FEdCircle = sdr["FEdCircle"].ToString();
                sch.TSchoolId = sdr["TSchoolId"].ToString();
                sch.TSchoolName = sdr["TSchoolName"].ToString();
                sch.TEdCircle = sdr["TEdCircle"].ToString();
                sch.FirstDeleDate = sdr["FirstDeleDate"].ToString();
                sch.DelegationDate = sdr["DelegationDate"].ToString();
                sch.AcceptedDelDate = sdr["AcceptedDelDate"].ToString();
                sch.ReqDelDate = sdr["ReqDelDate"].ToString();
                sch.FEdCircle = sdr["FEdCircle"].ToString();
                sch.DelState = sdr["DelState"].ToString();
                int st = Convert.ToInt32(sdr["StateAfter"].ToString());
                sch.StateAfter = st;

                return Json(sch/*, JsonRequestBehavior.AllowGet*/);
            }


            return Json(null);

        }
        public JsonResult getEmpT(int tid)
        {



            DataTable dtable = fun.fireDataTable("SELECT [id],[FSchoolId],[FSchoolName],[FEdCircle],[EmpId],[EmpName],[SubCode],[SubName],[TSchoolId],[TSchoolName],[TEdCircle],[DayName],[ManagerName],convert(char(10),cast([FirstDeleDate] as datetime),131) as FirstDeleDate,IsNull(StateAfter,0) as StateAfter "
+ ", CASE "
+ "WHEN ReqDelDate = '1900-01-01' THEN '' "
+ "WHEN ReqDelDate > '1900-01-01' THEN convert(char(10), cast([ReqDelDate] as datetime), 131) "
+ "END as ReqDelDate "
+ ", CASE "
+ "WHEN DelegationDate = '1900-01-01' THEN '' "
+ "WHEN DelegationDate > '1900-01-01' THEN convert(char(10), cast([DelegationDate] as datetime), 131) "
+ "END as DelegationDate "
+ ", CASE "
+ "WHEN AcceptedDelDate = '1900-01-01' THEN '' "
+ "WHEN AcceptedDelDate > '1900-01-01' THEN convert(char(10), cast([AcceptedDelDate] as datetime), 131) "
+ "END as AcceptedDelDate "
+ " , CASE "
+ "     WHEN DelState = 1 THEN '1' "
+ "      WHEN DelState = 2 and AcceptedDelDate = '1900-01-01' THEN '2' "
+ "     WHEN DelState = 2 and AcceptedDelDate > '1900-01-01' THEN '3' "
+ " END AS DelState "
+ "   FROM[dbo].[Delegation] where EmpId = " + tid + "");
            foreach (DataRow sdr in dtable.Rows)
            {
                DelegateShow sch = new DelegateShow();
                sch.id = sdr["id"].ToString();
                sch.EmpId = sdr["EmpId"].ToString();
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.FSchoolId = sdr["FSchoolId"].ToString();
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.FEdCircle = sdr["FEdCircle"].ToString();
                sch.TSchoolId = sdr["TSchoolId"].ToString();
                sch.TSchoolName = sdr["TSchoolName"].ToString();
                sch.TEdCircle = sdr["TEdCircle"].ToString();
                sch.FirstDeleDate = sdr["FirstDeleDate"].ToString();
                sch.DelegationDate = sdr["DelegationDate"].ToString();
                sch.AcceptedDelDate = sdr["AcceptedDelDate"].ToString();
                sch.ReqDelDate = sdr["ReqDelDate"].ToString();
                sch.FEdCircle = sdr["FEdCircle"].ToString();
                sch.DelState = sdr["DelState"].ToString();
                int st = Convert.ToInt32(sdr["StateAfter"].ToString());
                sch.StateAfter = st;

                return Json(sch/*, JsonRequestBehavior.AllowGet*/);
            }


            return Json(null);

        }

        public ActionResult ExportDelegationManager()
        {
            string id = ""; //sc.DecParametar(Request.Cookies["main"]["schid"]);

            DataTable dt = fun.fireStoreProcedure3("TimeTableAuto", id, "5", "8");
            List<DesForTime> ListDf = new List<DesForTime>();
            foreach (DataRow dr in dt.Rows)
            {
                DesForTime dsf = new DesForTime();
                dsf.empId = dr["empId"].ToString();
                dsf.Teacher = dr["TEACHAR"].ToString();
                dsf.subj = dr["subj"].ToString();
                dsf.Reduce = dr["Reduce"].ToString();
                dsf.Estat = dr["Estat"].ToString();
                ListDf.Add(dsf);
            }
            DataTable dt1 = fun.fireStoreProcedure1("IncTeach", id.ToString());

            List<DesForTime> ListDf1 = new List<DesForTime>();
            foreach (DataRow dr1 in dt1.Rows)
            {
                DesForTime dsf1 = new DesForTime();
                dsf1.subj = dr1["SUB"].ToString();
                dsf1.addclass = dr1["inc"].ToString();
                dsf1.addSub = dr1["req"].ToString();
                ListDf1.Add(dsf1);
            }


            CultureInfo cultures = CultureInfo.CreateSpecificCulture("ar-SA");

            Schools ischools = new Schools();
            int id1 = Convert.ToInt32(id);
            ischools.tSchools = db.TSchools.Include(t => t.T_area).Include(t => t.T_Center).Include(t => t.TBuilding).Include(t => t.TLevel).Include(t => t.TSchoolsType).Include(t => t.TSector).Include(t => t.Tuition1).Where(t => t.Center == 14).FirstOrDefault();
            ischools.tCLassRow = db.TCLassRow.Include(t => t.TLevel_Row).Where(t => t.SchoolID == id1).ToList();
            // ischools.ttimetable = db.TimeTable.Include(t => t.TImployees).Where(t => t.sch_id == id).ToList();
            ischools.desForTime = ListDf;
            ischools.desTimeTable = ListDf1;
            ViewBag.cClass = db.TCLassRow.Where(t => t.SchoolID == id1).Sum(t => t.CLass);
            ViewBag.cStud = db.TCLassRow.Where(t => t.SchoolID == id1).Sum(t => t.pupil);
            ischools.Tsubjects = db.TSubject.ToList();
            ischools.reducedTypes = db.ReducedTypes.ToList();
            string text = DateTime.Now.ToString("yyyy -MMMM -dd", cultures);
            DataTable dtd = fun.fireDataTable("select EmpId,EmpName,SubName,convert(char(10),cast(FirstDeleDate as datetime),131) as FirstDeleDate from Delegation where FSchoolId = " + id + "");
            List<DelegateShow> dg = new List<DelegateShow>();
            foreach (DataRow dr in dtd.Rows)
            {
                DelegateShow dh = new DelegateShow();
                dh.EmpId = dr["EmpId"].ToString();
                dh.EmpName = dr["EmpName"].ToString();
                dh.SubName = dr["SubName"].ToString();
                dh.FirstDeleDate = dr["FirstDeleDate"].ToString();
                dg.Add(dh);
            }

            ischools.dele = dg;
            ViewBag.adela = db.adela.Where(p => p.adServ == 4).ToList();
            return View(ischools);



        }

        public ActionResult AcceptDelegationManager()
        {
            string id = "";// sc.DecParametar(Request.Cookies["main"]["schid"]);
            List<DelegateShow> list = new List<DelegateShow>();

            DataTable dtable = fun.fireDataTable("select EmpId,EmpName,SubName,FSchoolName,TSchoolName,ISNULL(StateAfter,0) as StateAfter,convert(char(10),cast(FirstDeleDate as datetime),131) as FirstDeleDate,convert(char(10),cast(ReqDelDate as datetime),131) as ReqDelDate , "
+ " CASE "
+ " WHEN AcceptedDelDate = '1900-01-01' or AcceptedDelDate = '' THEN '' "
+ " ELSE convert(char(10), cast(AcceptedDelDate as datetime), 131) "
+ " END as AcceptedDelDate "
+ " from Delegation where TSchoolId = " + id + " and DelState = 2 and StateAfter = 2 or StateAfter = 3");
            foreach (DataRow sdr in dtable.Rows)
            {
                DelegateShow sch = new DelegateShow();
                sch.EmpId = sdr["EmpId"].ToString();
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.TSchoolName = sdr["TSchoolName"].ToString();
                sch.FirstDeleDate = sdr["FirstDeleDate"].ToString();
                sch.ReqDelDate = sdr["ReqDelDate"].ToString();
                sch.AcceptedDelDate = sdr["AcceptedDelDate"].ToString();
                sch.StateAfter = Convert.ToInt32(sdr["StateAfter"].ToString());
                list.Add(sch);
            }


            return View(list);

        }

        public ActionResult ExpoertDecisionEXcManager(int id)
        {

            DataTable dt = new DataTable();// fun.fireDataTable("select * from TImployees where SchID = " + id1 + " and [identity] = '" + id + "'");
            if (dt.Rows.Count > 0)
            {
                vmboard vmb = new vmboard();
                DataTable dr2 = fun.fireDataTable("select ISNULL(resid,0) as resid,id,other,DelType,DelDays,DelClass from Delegation where EmpId = " + id + "");

                if (dr2.Rows.Count > 0)
                {
                    ViewBag.resid = new SelectList(db.reson, "id", "chos", dr2.Rows[0]["resid"].ToString());
                    ViewBag.DelType = new SelectList(db.DelegationType, "id", "DelegationType1", dr2.Rows[0]["DelType"].ToString());
                    DataTable dt1 = fun.fireStoreProcedure1("DelegationDecision", id.ToString());
                    List<DelegationDecision> ListDD = new List<DelegationDecision>();
                    List<dboard> ListDD1 = new List<dboard>();
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        DelegationDecision dc = new DelegationDecision();

                        dc.TID = dr1["TID"].ToString();
                        dc.SchId = dr1["Schid"].ToString();
                        dc.dayname1 = dr1["dayname1"].ToString();
                        dc.dateHajry = dr1["dateHajry"].ToString();
                        dc.TeachName = dr1["TeachName"].ToString();
                        dc.ManagName = dr1["ManagName"].ToString();
                        dc.DelID = dr2.Rows[0]["id"].ToString();
                        dc.other = dr2.Rows[0]["other"].ToString();
                        dc.DelType = dr2.Rows[0]["DelType"].ToString();
                        dc.DelDays = dr2.Rows[0]["DelDays"].ToString();
                        dc.DelClass = dr2.Rows[0]["DelClass"].ToString();
                        ListDD.Add(dc);

                    }
                    DataTable dt2 = fun.fireDataTable("select * from Mboard where [sid] = " + dt1.Rows[0]["Schid"].ToString() + "");
                    foreach (DataRow drr in dt2.Rows)
                    {
                        dboard dbor = new dboard();
                        dbor.name = drr["empName"].ToString();
                        dbor.job = drr["job"].ToString();

                        ListDD1.Add(dbor);

                    }

                    if (TempData["SuccessMsg"] != null)
                    {
                        ViewBag.SuccessM = TempData["SuccessMsg"].ToString();
                        ViewBag.PrintShow = TempData["PrintShow"].ToString();
                    }
                    if (TempData["ErrorMsg"] != null)
                    {
                        ViewBag.ErrorM = TempData["ErrorMsg"].ToString();
                    }
                    vmb.DelDec = ListDD;
                    vmb.dbod = ListDD1;
                    ViewBag.chk = dr2.Rows[0]["resid"].ToString();
                    return View(vmb);

                }
                else
                {
                    ViewBag.resid = new SelectList(db.reson, "id", "chos");
                    ViewBag.DelType = new SelectList(db.DelegationType, "id", "DelegationType1");
                    DataTable dt1 = fun.fireStoreProcedure1("DelegationDecision", id.ToString());
                    DelegationDecision dc = new DelegationDecision();
                    List<DelegationDecision> ListDD = new List<DelegationDecision>();
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        dc.TID = dr1["TID"].ToString();
                        dc.SchId = dr1["Schid"].ToString();
                        dc.dayname1 = dr1["dayname1"].ToString();
                        dc.dateHajry = dr1["dateHajry"].ToString();
                        dc.TeachName = dr1["TeachName"].ToString();
                        dc.ManagName = dr1["ManagName"].ToString();

                        ListDD.Add(dc);
                    }
                    vmb.DelDec = ListDD;
                    ViewBag.PrintShow = "exist";
                    ViewBag.chk = "0";
                    return View(vmb);

                }
            }
            else
            {
                return RedirectToAction("ExportDelegationManager");

            }

        }
        public ActionResult TerminalAcceptManager()
        {

            string id = "";// sc.DecParametar(Request.Cookies["main"]["schid"]);
            List<DelegateShow> list = new List<DelegateShow>();

            DataTable dtable = fun.fireDataTable("select EmpId,EmpName,SubName,FSchoolName,TSchoolName,ISNULL(StateAfter,0) as StateAfter,convert(char(10),cast(FirstDeleDate as datetime),131) as FirstDeleDate, "
+ " CASE "
+ " WHEN ReqDelDate = '1900-01-01' or ReqDelDate = '' THEN '' "
+ " ELSE convert(char(10), cast(ReqDelDate as datetime), 131) "
+ " END as ReqDelDate "
+ " from Delegation where FSchoolId = " + id + " and DelState = 2 ");
            foreach (DataRow sdr in dtable.Rows)
            {
                DelegateShow sch = new DelegateShow();
                sch.EmpId = sdr["EmpId"].ToString();
                sch.EmpName = sdr["EmpName"].ToString();
                sch.SubName = sdr["SubName"].ToString();
                sch.TSchoolName = sdr["TSchoolName"].ToString();
                sch.FSchoolName = sdr["FSchoolName"].ToString();
                sch.FirstDeleDate = sdr["FirstDeleDate"].ToString();
                sch.ReqDelDate = sdr["ReqDelDate"].ToString();
                sch.StateAfter = Convert.ToInt32(sdr["StateAfter"].ToString());
                list.Add(sch);
            }


            return View(list);

        }

    }
}