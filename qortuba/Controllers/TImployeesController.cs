﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using qortuba.Models;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using qortuba.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using qortuba.Services;
using log4net.Core;
using Microsoft.Extensions.Logging;

namespace qortuba.Controllers
{

    public class TImployeesController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
       Models.Class.MyFunctions fun = new Models.Class.MyFunctions();
        public TImployeesController(ApplicationDbContext Context, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender)
        {
            db = Context;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }



    



        // GET: TImployees
        //    [Authorize(Roles = "empShow")]
        public ActionResult Index()
        {

            List<emp> list = new List<emp>();
            DataTable dtable = fun.fireStoreProcedure("emp");
            foreach (DataRow sdr in dtable.Rows)
            {
                emp imp = new emp();

                imp.id = sdr["Identity"].ToString();
                imp.fullName = sdr["fullName"].ToString();
                imp.school = sdr["School"].ToString();
                imp.subject = sdr["subject"].ToString();
                imp.special = sdr["special"].ToString();
                imp.work = sdr["work"].ToString();
                imp.nat = sdr["nat"].ToString();
                imp.place = sdr["place"].ToString();
                imp.TStatus = sdr["TStatus"].ToString(); 
                list.Add(imp);

            }

        //   ViewBag.adela = db.adela.Where(p => p.adServ == 1).ToList();


            return View(list);


        }
       // [Authorize(Roles = "TeacherPage")]
        public ActionResult EmpData()
        {

            string wid = "" + 6;// sc.DecParametar(Request.Cookies["main"]["WId"]);
            if (wid == String.Empty)
            {
                //   return  System.Web.Mvc.HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

           // string st = sc.DecParametar(User.Identity.Name);
            var tImployees = db.TImployees.SingleOrDefault(p => p.Identity == "1000116937");
            if (tImployees == null)
            {
                return NotFound();
            }
            vballModel(tImployees);
             ViewBag.wd = wid;
            tImployees.SSMA_TimeStamp = new byte[0];
            ViewBag.adela = db.adela.Where(p => p.adServ == 1).ToList();
            return View(tImployees);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
      //  [Authorize(Roles = "TeacherPage")]
        public ActionResult EmpData(IFormFile file1, IFormFile file2, IFormFile file3, TImployees tImployees)
        {
            try
            {

                var ent = db.TImployees.Where(p => p.Identity == tImployees.Identity).FirstOrDefault();
                ent.Mopaul = tImployees.Mopaul;
                ent.phone = tImployees.phone;
                ent.Amail = tImployees.Amail;
                if (ModelState.IsValid)
                {
                    if (file1 != null && file1.Length > 0)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps")))
                        {
                            Directory.CreateDirectory(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps"));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file1.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file1.CopyToAsync(fileSteam);
                            }
                        }

                    }
                    if (file2 != null && file2.Length > 0)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/" + tImployees.WID + "/officeGroup")))
                        {
                            Directory.CreateDirectory(Path.GetFileName("~/Upload/" + tImployees.WID + "/officeGroup"));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.WID + "/officeGroup/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file2.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.WID + "/officeGroup/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file2.CopyToAsync(fileSteam);
                            }
                        }

                    }
                    if (file3 != null && file3.Length > 0)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/officeAc")))
                        {
                            Directory.CreateDirectory(Path.GetFileName("~/Upload/officeAc"));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/officeAc/offSig.png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file3.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/officeAc/offSig.png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file3.CopyToAsync(fileSteam);
                            }
                        }

                    }
                    vballModel(ent);
                    db.Entry(ent).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("EmpData");
                }

                vballModel(ent);
                tImployees.SSMA_TimeStamp = new byte[0];
                return View(ent);



            }
            catch (Exception e)
            {
                vballModel(tImployees);
                ViewBag.error = "يوجد تداخل بين الرقم الوطنى المدخل مع رقم اخر .. تأكد من الرقم القومى";
                return View(tImployees);
            }
        }

        // GET: TImployees/Details/5
    //    [Authorize(Roles = "empDetails")]
        public ActionResult Details(string id)
        {

            if (id == null)
            {
                //   return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tImployees = db.TImployees.Where(p => p.Identity == id).FirstOrDefault();
            if (tImployees == null)
            {
                return NotFound();
            }
            vballModel(tImployees);
            tImployees.SSMA_TimeStamp = new byte[0];
            return View(tImployees);
        }

        // GET: TImployees/Create
      //  [Authorize(Roles = "empInsert")]
        public ActionResult Create()
        {

            vball();
            return View();
        }

        // POST: TImployees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
   //     [Authorize(Roles = "empInsert")]
        public async System.Threading.Tasks.Task<ActionResult> CreateAsync(TImployees tImployees)
        {
            try
            {

                if (ModelState.IsValid)
                {


                    var user = new ApplicationUser { UserName = tImployees.Identity, Email = tImployees.Amail };
                    var result = await _userManager.CreateAsync(user, tImployees.Identity);
                    if (result.Succeeded)
                    {


                        tImployees.SSMA_TimeStamp = new byte[0];
                        var chk = db.TImployees.Where(t => t.Identity == tImployees.Identity).FirstOrDefault();
                        if (chk != null)
                        {
                            ViewBag.error = "هذا المعلم مسجل من قبل";
                            return View(tImployees);
                        }
                    //    tImployees.ApplicationUserId = user.Id;
                        db.TImployees.Add(tImployees);
                        db.SaveChanges();
                        ViewBag.success = "تم التسجيل بنجاح";
                        return RedirectToAction("Index");

                    }


                }
                vballModel(tImployees);

                return View(tImployees);

            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View(tImployees);

            }

        }

        // GET: TImployees/Edit/5
        [Authorize(Roles = "empUpdate")]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                //      return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tImployees = db.TImployees.Where(p => p.Identity == id).FirstOrDefault();
            if (tImployees == null)
            {
                return NotFound();
            }
            vballModel(tImployees);
            tImployees.SSMA_TimeStamp = new byte[0];
            return View(tImployees);
        }

        // POST: TImployees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
      //  [Authorize(Roles = "empUpdate")]
        public ActionResult Edit(IFormFile file1, IFormFile file2, IFormFile file3, TImployees tImployees)
        {
            try
            {

                var ent = db.TImployees.Where(p => p.Identity == tImployees.Identity).FirstOrDefault();
                if (ModelState.IsValid)
                {
                    if (file1 != null && file1.Length > 0)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps")))
                        {
                            Directory.CreateDirectory(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps"));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file1.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file1.CopyToAsync(fileSteam);
                            }
                        }

                    }
                    if (file2 != null && file2.Length > 0)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/" + tImployees.WID + "/officeGroup")))
                        {
                            Directory.CreateDirectory(Path.GetFileName("~/Upload/" + tImployees.WID + "/officeGroup"));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.WID + "/officeGroup/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file2.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.WID + "/officeGroup/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file2.CopyToAsync(fileSteam);
                            }
                        }

                    }
                    if (file3 != null && file3.Length > 0)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/officeAc")))
                        {
                            Directory.CreateDirectory(Path.GetFileName("~/Upload/officeAc"));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/officeAc/offSig.png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file3.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/officeAc/offSig.png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file3.CopyToAsync(fileSteam);
                            }
                        }

                    }

                    ent.name1 = tImployees.name1;
                    ent.name2 = tImployees.name2;
                    ent.name3 = tImployees.name3;
                    ent.FName = tImployees.FName;

                    ent.SchID = tImployees.SchID;
                    ent.Identity2 = tImployees.Identity2;
                    ent.SchIDPrev = tImployees.SchIDPrev;
                    ent.SubID = tImployees.SubID;

                    ent.SpecialIDOrig = tImployees.SpecialIDOrig;
                    ent.SpecialID = tImployees.SpecialID;
                    ent.EmpStatus = tImployees.EmpStatus;

                    ent.Reduced = tImployees.Reduced;
                    ent.WID = tImployees.WID;
                    ent.WorkDated = tImployees.WorkDated;

                    ent.WorkDatem = tImployees.WorkDatem;
                    ent.WorkDatey = tImployees.WorkDatey;
                    ent.Married = tImployees.Married;

                    ent.SunNo = tImployees.SunNo;
                    ent.BirthDated = tImployees.BirthDated;
                    ent.BirthDatem = tImployees.BirthDatem;
                    ent.BirthDatey = tImployees.BirthDatey;

                    ent.BirthPlace = tImployees.BirthPlace;
                    ent.NatID = tImployees.NatID;


                    ent.Mopaul = tImployees.Mopaul;
                    ent.phone = tImployees.phone;
                    ent.Amail = tImployees.Amail;
                    vballModel(tImployees);
                    if (tImployees.WID == 75)
                    {
                        fun.fireSQL("update TImployees set WID = 0 where  WID = 75");
                        db.Entry(ent).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (tImployees.WID == 76)
                    {
                        fun.fireSQL("update TImployees set WID = 0 where  WID = 76");
                        db.Entry(ent).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (tImployees.WID == 73)
                    {
                        fun.fireSQL("update TImployees set WID = 0 where  WID = 73");
                        db.Entry(ent).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (tImployees.WID == 74)
                    {
                        fun.fireSQL("update TImployees set WID = 0 where  WID = 74");
                        db.Entry(ent).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (tImployees.WID == 84)
                    {
                        fun.fireSQL("update TImployees set WID = 0 where  WID = 84");
                        db.Entry(ent).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        db.Entry(ent).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    return RedirectToAction("Edit", new { id = tImployees.Identity });
                }

                vballModel(tImployees);
                tImployees.SSMA_TimeStamp = new byte[0];
                return View(tImployees);


            }
            catch (Exception e)
            {
                vballModel(tImployees);
                ViewBag.error = "يوجد تداخل بين الرقم الوطنى المدخل مع رقم اخر .. تأكد من الرقم القومى";
                return View(tImployees);
            }
        }
        [Authorize(Roles = "empUpdate")]
        public ActionResult EditEmp(string id)
        {
            if (id == null)
            {
                //  return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tImployees = db.TImployees.Where(p => p.Identity == id).FirstOrDefault();
            if (tImployees == null)
            {
                return NotFound();
            }
            vballModel(tImployees);
            tImployees.SSMA_TimeStamp = new byte[0];
            return View(tImployees);
        }
        public ActionResult EditEmp(IFormFile file1, TImployees tImployees)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (file1 != null && file1.Length > 0)
                    {

                        if (!Directory.Exists(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps")))
                        {
                            Directory.CreateDirectory(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps"));
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file1.CopyToAsync(fileSteam);
                            }
                        }
                        else
                        {
                            var FullPath = Path.Combine(Path.GetFileName("~/Upload/" + tImployees.SchID + "/emps/" + tImployees.Identity + ".png"));
                            using (var fileSteam = new FileStream(FullPath, FileMode.Create))
                            {
                                file1.CopyToAsync(fileSteam);
                            }
                        }

                    }
                    vballModel(tImployees);
                    db.Entry(tImployees).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                vballModel(tImployees);
                tImployees.SSMA_TimeStamp = new byte[0];
                return View(tImployees);
            }
            catch (Exception e)
            {
                vballModel(tImployees);
                ViewBag.error = "يوجد تداخل بين الرقم الوطنى المدخل مع رقم اخر .. تأكد من الرقم القومى";
                return View(tImployees);
            }
        }

        // GET: TImployees/Delete/5
        [Authorize(Roles = "empDelete")]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                //     return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TImployees tImployees = db.TImployees.Find(id);
            if (tImployees == null)
            {
                return NotFound();
            }

            vballModel(tImployees);


            return View(tImployees);
        }

        // POST: TImployees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "empDelete")]
        public ActionResult DeleteConfirmed(string id)
        {

            TImployees tImployees = db.TImployees.Find(id);
            db.TImployees.Remove(tImployees);
            db.SaveChanges();
            return RedirectToAction("Index");


        }

        public ActionResult report1()
        {
            //  DataTable dt = db.TSchools.Where(t=>t.Center == 14).Select(t => new { t.SchoolID, t.School }).ToDataTable();
            DataTable dt = fun.fireDataTable("select  SchoolID as [كود المدرسة]  , School as [المدرسة] from TSchools");
            return View(dt);
        }

        public ActionResult runRe(string id)
        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = " + id + "");
            if (statment != null)
            {
                var header = Path.GetFileName("~/static/header.html");
                var footer = Path.GetFileName("~/static/footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    //var pdf = new ViewAsPdf("report1", dt)
                    //{

                    //    CustomSwitches = customSwitches,
                    //    PageMargins = new Rotativa.Options.Margins(40, 10, 15, 10),
                    //    PageOrientation = Rotativa.Options.Orientation.Portrait,
                    //    PageSize = Rotativa.Options.Size.A4
                    //};
                    return View();
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    //var pdf = new ViewAsPdf("report1", dt)
                    //{

                    //    CustomSwitches = customSwitches,
                    //    PageMargins = new Rotativa.Options.Margins(30, 10, 15, 10),
                    //    PageOrientation = Rotativa.Options.Orientation.Landscape,
                    //    PageSize = Rotativa.Options.Size.A4
                    //};
                    return View();
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }
        public void vball()
        {
            var day1 = new List<SelectListItem>();
            var month1 = new List<SelectListItem>();
            var yearl = new List<SelectListItem>();
            for (int i = 1; i <= 31; i++)
            { day1.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
            for (int i = 1; i <= 12; i++)
            { month1.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
            for (int i = 1350; i <= 1500; i++)
            { yearl.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }


            ViewBag.QID = new SelectList(db.QType, "QTID", "QType1");
            ViewBag.NatID = new SelectList(db.TNationality, "NatID", "Nationality");
            ViewBag.quien = new SelectList(db.TQualified, "QID", "Qualified");
            ViewBag.SchID = new SelectList(db.TSchools.Where(t => t.Center == 14), "SchoolID", "School");
            ViewBag.SchIDPrev = new SelectList(db.TSchools, "SchoolID", "School");
            ViewBag.SpecialID = new SelectList(db.TSpecialSub, "SpeID", "Special");
            ViewBag.SpecialIDOrig = new SelectList(db.TSpecialSub, "SpeID", "Special");
            ViewBag.SubID = new SelectList(db.TSubject, "SubID", "Subject");
            ViewBag.WID = new SelectList(db.TWork, "WID", "Work");
            ViewBag.WIDJ = new SelectList(db.TWorkJob, "WIDJ", "WorkJ");
            ViewBag.EmpStatus = new SelectList(db.TempStatus, "EmpStatus", "EndType");
            ViewBag.Reduced = new SelectList(db.ReducedTypes, "ReduedCode", "ReducedName");
            ViewBag.Married = new SelectList(db.T_Married, "NU", "nem");
            ViewBag.BirthDated = new SelectList(day1, "Value", "Text");
            ViewBag.BirthDatem = new SelectList(month1, "Value", "Text");
            ViewBag.BirthDatey = new SelectList(yearl, "Value", "Text");

            ViewBag.WorkDated = new SelectList(day1, "Value", "Text");
            ViewBag.WorkDatem = new SelectList(month1, "Value", "Text");
            ViewBag.WorkDatey = new SelectList(yearl, "Value", "Text");

        }
        public void vballModel(TImployees tImployees)
        {
            var day1 = new List<SelectListItem>();
            var month1 = new List<SelectListItem>();
            var yearl = new List<SelectListItem>();
            for (int i = 1; i <= 31; i++)
            { day1.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
            for (int i = 1; i <= 12; i++)
            { month1.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
            for (int i = 1350; i <= 1500; i++)
            { yearl.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() }); }
            ViewBag.QID = new SelectList(db.QType, "QTID", "QType1", tImployees.QID);
            ViewBag.NatID = new SelectList(db.TNationality, "NatID", "Nationality", tImployees.NatID);
            ViewBag.quien = new SelectList(db.TQualified, "QID", "Qualified", tImployees.quien);
            ViewBag.SchID = new SelectList(db.TSchools, "SchoolID", "School", tImployees.SchID);
            ViewBag.SchIDPrev = new SelectList(db.TSchools, "SchoolID", "School", tImployees.SchIDPrev);
            ViewBag.SpecialID = new SelectList(db.TSpecialSub, "SpeID", "Special", tImployees.SpecialID);
            ViewBag.SpecialIDOrig = new SelectList(db.TSpecialSub, "SpeID", "Special", tImployees.SpecialIDOrig);
            ViewBag.SubID = new SelectList(db.TSubject, "SubID", "Subject", tImployees.SubID);
            ViewBag.WID = new SelectList(db.TWork, "WID", "Work", tImployees.WID);
            ViewBag.WIDJ = new SelectList(db.TWorkJob, "WIDJ", "WorkJ", tImployees.WIDJ);

            ViewBag.EmpStatus = new SelectList(db.TempStatus, "EmpStatus", "EndType", tImployees.EmpStatus);
            ViewBag.Reduced = new SelectList(db.ReducedTypes, "ReduedCode", "ReducedName", tImployees.Reduced);
            ViewBag.Married = new SelectList(db.T_Married, "NU", "nem", tImployees.Married);
            ViewBag.BirthDated = new SelectList(day1, "Value", "Text", tImployees.BirthDated);
            ViewBag.BirthDatem = new SelectList(month1, "Value", "Text", tImployees.BirthDatem);
            ViewBag.BirthDatey = new SelectList(yearl, "Value", "Text", tImployees.BirthDatey);

            ViewBag.WorkDated = new SelectList(day1, "Value", "Text", tImployees.WorkDated);
            ViewBag.WorkDatem = new SelectList(month1, "Value", "Text", tImployees.WorkDatem);
            ViewBag.WorkDatey = new SelectList(yearl, "Value", "Text", tImployees.WorkDatey);
        }


        public ActionResult report2()
        {
            //DataTable dt = db.TImployees.Where(t => t.SchID == 1).Select(t => new{ t.SchID,t.SchIDPrev }).ToDataTable();



            return View(/*dt*/);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
