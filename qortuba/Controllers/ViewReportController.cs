﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using qortuba.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Rotativa.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace qortuba.Controllers
{
  //  [Authorize]
    public class ViewReportController : Controller
    {
       Models.Class.MyFunctions fun = new Models.Class.MyFunctions();
        private readonly ApplicationDbContext db;
        private readonly IHostingEnvironment _HostEnvironment;
        public ViewReportController(ApplicationDbContext context, IHostingEnvironment HostEnvironment)
        {
            db = context;
            _HostEnvironment = HostEnvironment;

        }
        // GET: ViewReport
        public ActionResult Index()
        {
            return View();
        }

        public IActionResult EmpsReport()

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 1 ");
            if (statment != null)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\header.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);




                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("Emps", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {

                    var pdf = new ViewAsPdf("Emps", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }

        // EXCEL  EXPORT
        public FileResult EmpsReportX()
        {          
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 1 ");

            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());
     
            dt.TableName = "جميع العاملين";
            XLWorkbook wb = new XLWorkbook();
            
            wb.Worksheets.Add(dt);
            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع العاملين.xlsx");
        }


        public ActionResult ReportSchhool()
        {
            ViewBag.SchID = new SelectList(db.TSchools.Where(t=>t.Center == 14), "SchoolID", "School");
            return View();
        }
        
        public ActionResult showRschool(int id)
        {
            DataTable dt = fun.fireStoreProcedureWithP("EmpSchool",id.ToString());
            return View(dt);
        }
        public IActionResult showRschool1(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 2 ");
            if (statment != null)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\headerSchoolEmp.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and SchID =" + id.ToString() + " ");

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("showRschool", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    var pdf = new ViewAsPdf("showRschool", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                } 

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }

        // excel
        public FileResult showRschool1X(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 2 ");
            
               DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and SchID =" + id.ToString() + " ");

                dt.TableName = "جميع العاملين بمدرسة محددة";
                XLWorkbook wb = new XLWorkbook();

                wb.Worksheets.Add(dt);
                MemoryStream stream = new MemoryStream();
                wb.SaveAs(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع العاملين بمدرسة محددة.xlsx");
     
        }
        // try to use Crystal report to do dynamic report
        public void Remp()
        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 3 ");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());


            //ReportDocument rd = new ReportDocument();
            //rd.Load(Path.Combine(Path.GetFileName("~/Reports"), "test.rpt"));
            //List<TextObject> textObjects = rd.ReportDefinition.Sections["Section2"].ReportObjects.OfType<TextObject>().ToList();
            //List<FieldObject> fobject = rd.ReportDefinition.Sections["Section3"].ReportObjects.OfType<FieldObject>().ToList();




            //fobject[1].Height = 300;
            //rd.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
            //rd.PrintOptions.PaperSize = PaperSize.PaperA4;
            //rd.SetDataSource(dt);

            //Response.Buffer = false;
            //Response.ClearContent();
            //Response.ClearHeaders();
            //rd.SummaryInfo.ReportTitle = "الموظفون";
            //rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, false, "الموظفون");

        }

        // emp work
        public ActionResult Reportempwork()
        {
            ViewBag.WID = new SelectList(db.TWork, "WID", "Work");
            return View();
        }
        public ActionResult Reportempwork1(int id)
        {
            DataTable dt = fun.fireStoreProcedureWithP("EmpWork", id.ToString());
            return View(dt);
            
        }

        public IActionResult showRempwork(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 3 ");
            if (statment != null)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\headerSchoolEmpwork.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and TImployees.WID =" + id.ToString() + " ");

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("Reportempwork1", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    var pdf = new ViewAsPdf("Reportempwork1", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }

        //excel
        public FileResult showRempworkX(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 3 ");

            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and TImployees.WID =" + id.ToString() + " ");

            dt.TableName = "جميع العاملين حسب الوظيفة";
            XLWorkbook wb = new XLWorkbook();

            wb.Worksheets.Add(dt);
            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع العاملين حسب الوظيفة.xlsx");

        }

        // emp subject
        public ActionResult ReportempSubject()
        {
            ViewBag.SubID = new SelectList(db.TSubject, "SubID", "Subject");
            return View();
        }
        public IActionResult ReportempSubject1(int id)
        {
            //   DataTable dt = fun.fireStoreProcedureWithP("EmpSub", id.ToString());
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 4");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and TImployees.SubID =" + id.ToString());
            return View(dt);

        }
     

        public IActionResult showRempSubject(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 4");
            if (statment != null)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\headerSchoolEmpSub.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");

                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and TImployees.SubID =" + id.ToString() );

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("ReportempSubject1", dt)
                    {

                        CustomSwitches = customSwitches,
                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    var pdf = new ViewAsPdf("ReportempSubject1", dt)
                    {

                        CustomSwitches = customSwitches,
                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }

        //excel
        public FileResult showRempSubjectX(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 4 ");

            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and TImployees.SubID =" + id.ToString()+"");

            dt.TableName = "جميع العاملين حسب المادة";
            XLWorkbook wb = new XLWorkbook();

            wb.Worksheets.Add(dt);
            MemoryStream stream = new MemoryStream();
            wb.SaveAs(stream);
            return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "جميع العاملين حسب المادة.xlsx");

        }




        // emp Info
        public ActionResult ReportempTnfo()
        {
            
            return View();
        }
        public ActionResult ReportempInfo(int id)
        {
          

            return View();

        }

        public IActionResult ReporInfoEmp(int id)
        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 5 ");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and [Identity] = '" + id + "'");
            return View(dt);

        }

        public IActionResult showRempInfo(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 5 ");
            if (statment != null)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\headerEmpInfo.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and [Identity] = '" + id + "'");

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("ReporInfoEmp", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    var pdf = new ViewAsPdf("Reportempwork1", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");
        }
      
        
        public ActionResult ReportempPass()
        {

            return View();
        }
        public void showReportempPass(int id)

        {
            if (id.ToString() != null)
            {
                DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 12 ");
                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and [Identity] = '" + id + "'");


                //ReportDocument rd = new ReportDocument();
             //   rd.Load(Path.Combine(Path.GetFileName("~/Reports"), "EmpInfopass.rpt"));
                //  DataRow dr = fun.fireDataRow("select convert(char(10),cast(CURRENT_TIMESTAMP as datetime),131) as HijryDate");

                DateTimeFormatInfo DTFormat = new CultureInfo("ar-sa", false).DateTimeFormat;
                DTFormat.Calendar = new HijriCalendar();
                DTFormat.ShortDatePattern = "dd/mm/yyyy";

                string myDate = DateTime.Today.Date.ToString("yyyy/MM/dd", DTFormat);


             //   rd.SetDataSource(dt);
           //     rd.SetParameterValue("hjdate", myDate);
                //Response.Buffer = false;
                //Response.ClearContent();
                //Response.ClearHeaders();
                //rd.SummaryInfo.ReportTitle = "بيانات معلم";
                //rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, false, "الموظفون");
            }
        }

        // report all emp static
        public void staticRempInfo()

        {
           
                DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 11 ");
                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() );


            //    ReportDocument rd = new ReportDocument();
             //   rd.Load(Path.Combine(Path.GetFileName("../Reports"), "Allemp.rpt"));
                //  DataRow dr = fun.fireDataRow("select convert(char(10),cast(CURRENT_TIMESTAMP as datetime),131) as HijryDate");

                DateTimeFormatInfo DTFormat = new CultureInfo("ar-sa", false).DateTimeFormat;
                DTFormat.Calendar = new HijriCalendar();
                DTFormat.ShortDatePattern = "dd/mm/yyyy";

                string myDate = DateTime.Today.Date.ToString("yyyy/MM/dd", DTFormat);


               // rd.SetDataSource(dt);
             //   rd.SetParameterValue("hjdate", myDate);
                //Response.Buffer = false;
                //Response.ClearContent();
                //Response.ClearHeaders();
                //rd.SummaryInfo.ReportTitle = "بيانات المعلمين";
                //rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, false, "المعلمين");
            
        }

        public void staticRempInfoX()

        {

            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 11 ");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString());


        //    ReportDocument rd = new ReportDocument();
          //  rd.Load(Path.Combine(Path.GetFileName("../Reports"), "Allemp.rpt"));
            //  DataRow dr = fun.fireDataRow("select convert(char(10),cast(CURRENT_TIMESTAMP as datetime),131) as HijryDate");

            DateTimeFormatInfo DTFormat = new CultureInfo("ar-sa", false).DateTimeFormat;
            DTFormat.Calendar = new HijriCalendar();
            DTFormat.ShortDatePattern = "dd/mm/yyyy";

            string myDate = DateTime.Today.Date.ToString("yyyy/MM/dd", DTFormat);


          //  rd.SetDataSource(dt);
        //    rd.SetParameterValue("hjdate", myDate);
            //Response.Buffer = false;
            //Response.ClearContent();
            //Response.ClearHeaders();
            //rd.SummaryInfo.ReportTitle = "بيانات المعلمين";
            //rd.ExportToHttpResponse(ExportFormatType.Excel, System.Web.HttpContext.Current.Response, false, "المعلمين");

        }
        // school Info
        public ActionResult ReportSchoolInfo()
        {


            return View();

        }

        public ActionResult schoolData(int id)
        {

            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 6 ");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and SchoolID  = " + id + "");
            return View(dt);

        }

        public IActionResult showRSchoolInfo(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 6 ");
           

            if (statment != null)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\headerschool.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and SchoolID  = " + id + "");

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("schoolData", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    var pdf = new ViewAsPdf("schoolData", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");

        }

        public void showRSchoolInfoX(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 6 ");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " and SchoolID  = " + id + "");


          //  ReportDocument rd = new ReportDocument();
         //   rd.Load(Path.Combine(Path.GetFileName("~/Reports"), "SchoolInfo.rpt"));

            DateTimeFormatInfo DTFormat = new CultureInfo("ar-sa", false).DateTimeFormat;
            DTFormat.Calendar = new HijriCalendar();
            DTFormat.ShortDatePattern = "dd/mm/yyyy";

            string myDate = DateTime.Today.Date.ToString("yyyy/MM/dd", DTFormat);


          //  rd.SetDataSource(dt);
          //  rd.SetParameterValue("hjdate", myDate);
            //Response.Buffer = false;
            //Response.ClearContent();
            //Response.ClearHeaders();
            //rd.SummaryInfo.ReportTitle = "بيانات مدرسة";
            //rd.ExportToHttpResponse(ExportFormatType.Excel, System.Web.HttpContext.Current.Response, false, "بيانات مدرسة");

        }

        // school Rows
        public ActionResult ReportSchoolRows()
        {


            return View();

        }
        public ActionResult RschoolRow(int id)
        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 7 ");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " WHERE TCLassRow.SchoolID  = " + id + "");
            return View(dt);

        }
        public IActionResult showRSchoolRows(int id)

        {
               DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 7 ");
            
            if (statment != null)
            {
                string webRootPath = _HostEnvironment.WebRootPath;
                var header = Path.Combine(webRootPath, "static\\headerschoolRow.html");
                var footer = Path.Combine(webRootPath, "static\\footer.html");
                string customSwitches = string.Format("--header-html  \"{0}\" " +
                            "--header-spacing \"8\" " +
                            "--header-font-name \"Open Sans\" " +
                            "--footer-font-size \"8\" " +
                            "--footer-font-name \"Open Sans\" " +
                            "--header-font-size \"10\" " +
                            //"--footer-right \"Pag: [page] de [toPage]\""+
                            "--footer-html  \"{1}\" ", header, footer);


                DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " WHERE TCLassRow.SchoolID  = " + id + "");

                if (statment["viewMethod"].ToString() == "رأسى")
                {
                    var pdf = new ViewAsPdf("RschoolRow", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Portrait,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }
                else if (statment["viewMethod"].ToString() == "أفقى")
                {
                    var pdf = new ViewAsPdf("RschoolRow", dt)
                    {

                        CustomSwitches = customSwitches,

                        PageMargins = new Rotativa.AspNetCore.Options.Margins(40, 10, 15, 10),
                        PageOrientation = Rotativa.AspNetCore.Options.Orientation.Landscape,
                        PageSize = Rotativa.AspNetCore.Options.Size.A4
                    };
                    return pdf;
                }

            }

            return Content(@"<div  class='alert-danger '>خطأ فى التحميل</div>");

        }

        public void showRSchoolRowsX(int id)

        {
            DataRow statment = fun.fireDataRow("SELECT sqlstring,viewMethod from ReportD WHERE id = 7 ");
            DataTable dt = fun.fireDataTable(statment["sqlstring"].ToString() + " WHERE TCLassRow.SchoolID  = " + id + "");


           // ReportDocument rd = new ReportDocument();
        //    rd.Load(Path.Combine(Path.GetFileName("~/Reports"), "SchoolRows.rpt"));

            DateTimeFormatInfo DTFormat = new CultureInfo("ar-sa", false).DateTimeFormat;
            DTFormat.Calendar = new HijriCalendar();
            DTFormat.ShortDatePattern = "dd/mm/yyyy";

            string myDate = DateTime.Today.Date.ToString("yyyy/MM/dd", DTFormat);


            //rd.SetDataSource(dt);
          //  rd.SetParameterValue("hjdate", myDate);
            //Response.Buffer = false;
            //Response.ClearContent();
            //Response.ClearHeaders();
            //rd.SummaryInfo.ReportTitle = "بيانات صفوف مدرسة";
            //rd.ExportToHttpResponse(ExportFormatType.Excel, System.Web.HttpContext.Current.Response, false, "صفوف مدرسة");

        }
       // [Authorize(Roles = "UserAccount")]
        public void staticEmpPass()

        {

            try
            {
                DataTable dt = fun.fireDataTable("select [Identity] as id,CONCAT(name1,' ',name2,' ',name3,' ',FName) as Ename,sc.School as school,[password] as password1 from TImployees "
    + " inner join TSchools sc on TImployees.SchID = sc.SchoolID "
    + " where sc.Center = 14 "
    + " order by TImployees.SchID");

//
              //  ReportDocument rd = new ReportDocument();
             //   rd.Load(Path.Combine(Path.GetFileName("../Reports"), "Epassword.rpt"));
                //  DataRow dr = fun.fireDataRow("select convert(char(10),cast(CURRENT_TIMESTAMP as datetime),131) as HijryDate");

                DateTimeFormatInfo DTFormat = new CultureInfo("ar-sa", false).DateTimeFormat;
                DTFormat.Calendar = new HijriCalendar();
                DTFormat.ShortDatePattern = "dd/mm/yyyy";

                string myDate = DateTime.Today.Date.ToString("yyyy/MM/dd", DTFormat);


              //  rd.SetDataSource(dt);
            //    rd.SetParameterValue("hjdate", myDate);
                //Response.Buffer = false;
                //Response.ClearContent();
                //Response.ClearHeaders();
                //rd.SummaryInfo.ReportTitle = "كلمات مرور العاملين";
                //rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, false, "كلمات مرور العاملين");
            }
            catch (Exception hex)
            {
              
                    //react on remote host closed the connection exception.
                    var msg = hex.Message;
                
            }
          
        }
    }
}