using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using qortuba.Models;

namespace qortuba.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        public virtual DbSet<Custom_Reduce> Custom_Reduce { get; set; }
        public virtual DbSet<day_s> day_s { get; set; }
        public virtual DbSet<daysName> daysName { get; set; }
        public virtual DbSet<Delegation> Delegation { get; set; }
        public virtual DbSet<Departs> Departs { get; set; }
        public virtual DbSet<GroupN> GroupN { get; set; }
        public virtual DbSet<HayNames> HayNames { get; set; }
        public virtual DbSet<jobsMember> jobsMember { get; set; }
        public virtual DbSet<Mboard> Mboard { get; set; }
        public virtual DbSet<NeedCondition> NeedCondition { get; set; }
        public virtual DbSet<NeedCondition1> NeedCondition1 { get; set; }
        public virtual DbSet<NeedOptions> NeedOptions { get; set; }
        public virtual DbSet<periods> periods { get; set; }
        public virtual DbSet<PSubClas> PSubClas { get; set; }
        public virtual DbSet<User_Roles> User_Roles { get; set; }

        public virtual DbSet<QType> QType { get; set; }
        public virtual DbSet<QYtBEMARK> QYtBEMARK { get; set; }
        public virtual DbSet<recordCat> recordCat { get; set; }
        public virtual DbSet<ReducedTypes> ReducedTypes { get; set; }
        public virtual DbSet<ReportCat> ReportCat { get; set; }
        public virtual DbSet<ReportD> ReportD { get; set; }
        public virtual DbSet<ReviewCodes> ReviewCodes { get; set; }
        public virtual DbSet<RowName> RowName { get; set; }
        public virtual DbSet<SchoolDess> SchoolDess { get; set; }
        public virtual DbSet<SchTypes> SchTypes { get; set; }
        public virtual DbSet<SuperLaw> SuperLaw { get; set; }
        public virtual DbSet<SuperVis> SuperVis { get; set; }
        public virtual DbSet<supervision> supervision { get; set; }
        public virtual DbSet<supervision_tasks> supervision_tasks { get; set; }
        public virtual DbSet<T_area> T_area { get; set; }
        public virtual DbSet<T_Center> T_Center { get; set; }
        public virtual DbSet<T_CenterPaTypes> T_CenterPaTypes { get; set; }
        public virtual DbSet<T_ClassType> T_ClassType { get; set; }
        public virtual DbSet<T_contract> T_contract { get; set; }
        public virtual DbSet<T_country> T_country { get; set; }
        public virtual DbSet<T_Door> T_Door { get; set; }
        public virtual DbSet<T_Married> T_Married { get; set; }
        public virtual DbSet<T_PlanTemp> T_PlanTemp { get; set; }
        public virtual DbSet<T_Varway1> T_Varway1 { get; set; }
        public virtual DbSet<T_yes_no> T_yes_no { get; set; }
        public virtual DbSet<TBuilding> TBuilding { get; set; }
        public virtual DbSet<TCadE_door> TCadE_door { get; set; }
        public virtual DbSet<TimeTable> TimeTable { get; set; }
        public virtual DbSet<TImployees> TImployees { get; set; }
        public virtual DbSet<TImployeesCompl> TImployeesCompl { get; set; }
        public virtual DbSet<TImployeesR> TImployeesR { get; set; }
        public virtual DbSet<TLevel> TLevel { get; set; }
        public virtual DbSet<TLevel_Row> TLevel_Row { get; set; }
        public virtual DbSet<TLevel_Row_Code> TLevel_Row_Code { get; set; }
        public virtual DbSet<TLevel_Subject> TLevel_Subject { get; set; }
        public virtual DbSet<TNationality> TNationality { get; set; }
        public virtual DbSet<TQualified> TQualified { get; set; }
        public virtual DbSet<TransTypes> TransTypes { get; set; }
        public virtual DbSet<TSchools> TSchools { get; set; }
        public virtual DbSet<TSchoolsR> TSchoolsR { get; set; }
        public virtual DbSet<TSchoolsType> TSchoolsType { get; set; }
        public virtual DbSet<TSector> TSector { get; set; }
        public virtual DbSet<TSpecial> TSpecial { get; set; }
        public virtual DbSet<TSpecial_LevErr> TSpecial_LevErr { get; set; }
        public virtual DbSet<TSpecial_Sub> TSpecial_Sub { get; set; }
        public virtual DbSet<TSpecial_Sub1> TSpecial_Sub1 { get; set; }
        public virtual DbSet<TSpecialSub> TSpecialSub { get; set; }
        public virtual DbSet<Tstatus> Tstatus { get; set; }
        public virtual DbSet<TSubject> TSubject { get; set; }
        public virtual DbSet<TSubjectSuperv> TSubjectSuperv { get; set; }
        public virtual DbSet<TSubTotal> TSubTotal { get; set; }
        public virtual DbSet<Tuition> Tuition { get; set; }
        public virtual DbSet<TUniversity> TUniversity { get; set; }
        public virtual DbSet<TWork> TWork { get; set; }
        public virtual DbSet<TWorkEdar> TWorkEdar { get; set; }
        public virtual DbSet<TWorkJob> TWorkJob { get; set; }
        public virtual DbSet<viewMethod> viewMethod { get; set; }
        public virtual DbSet<Years> Years { get; set; }
        public virtual DbSet<ZCurrent> ZCurrent { get; set; }
        public virtual DbSet<ZNeedAll> ZNeedAll { get; set; }
        public virtual DbSet<ClassCompare> ClassCompare { get; set; }
        public virtual DbSet<Classes> Classes { get; set; }
        public virtual DbSet<CommonSch> CommonSch { get; set; }
        public virtual DbSet<ExportDelegation> ExportDelegation { get; set; }
        public virtual DbSet<FormsTypes> FormsTypes { get; set; }
        public virtual DbSet<ImportDelegation> ImportDelegation { get; set; }
        public virtual DbSet<OutMove> OutMove { get; set; }
        public virtual DbSet<PlanNeed> PlanNeed { get; set; }
        public virtual DbSet<PWorkClas> PWorkClas { get; set; }
        public virtual DbSet<T_CenterPa> T_CenterPa { get; set; }
        public virtual DbSet<TCLassRow> TCLassRow { get; set; }
        public virtual DbSet<TempStatus> TempStatus { get; set; }
        public virtual DbSet<TempTSubj> TempTSubj { get; set; }
        public virtual DbSet<TimplAutoUpdate> TimplAutoUpdate { get; set; }
        public virtual DbSet<TimployeesDel> TimployeesDel { get; set; }
        public virtual DbSet<TImployeesTemp> TImployeesTemp { get; set; }
        public virtual DbSet<ZIDLosses> ZIDLosses { get; set; }
        public virtual DbSet<reson> reson { get; set; }
        public virtual DbSet<DelegationType> DelegationType { get; set; }
        public virtual DbSet<TTColor> TTColor { get; set; }
        public virtual DbSet<TTConfigerDur> TTConfigerDur { get; set; }
        public virtual DbSet<TTDur> TTDur { get; set; }
        public virtual DbSet<TTRowsshape> TTRowsshape { get; set; }
        public virtual DbSet<TTTeacharCustom> TTTeacharCustom { get; set; }
        public virtual DbSet<restriction> restriction { get; set; }
        public virtual DbSet<TTteachClassess> TTteachClassess { get; set; }
        public virtual DbSet<superJob> superJob { get; set; }
        public virtual DbSet<supervisors> supervisors { get; set; }
        public virtual DbSet<students> students { get; set; }
        public virtual DbSet<Studentstate> Studentstate { get; set; }
        public virtual DbSet<studentsWait> studentsWait { get; set; }
        public virtual DbSet<acceptstates> acceptstates { get; set; }
        public virtual DbSet<adela> adela { get; set; }
        public virtual DbSet<guardian> guardian { get; set; }
        public virtual DbSet<locationnss> locationnss { get; set; }
        public virtual DbSet<studentsR> studentsR { get; set; }
    }


}
